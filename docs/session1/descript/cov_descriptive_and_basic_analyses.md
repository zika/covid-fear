<h1>Table of Contents<span class="tocSkip"></span></h1>
<div class="toc"><ul class="toc-item"><li><span><a href="#Descriptives" data-toc-modified-id="Descriptives-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Descriptives</a></span><ul class="toc-item"><li><span><a href="#Means-and-distributions" data-toc-modified-id="Means-and-distributions-1.1"><span class="toc-item-num">1.1&nbsp;&nbsp;</span>Means and distributions</a></span></li><li><span><a href="#Histograms" data-toc-modified-id="Histograms-1.2"><span class="toc-item-num">1.2&nbsp;&nbsp;</span>Histograms</a></span><ul class="toc-item"><li><span><a href="#Demographics" data-toc-modified-id="Demographics-1.2.1"><span class="toc-item-num">1.2.1&nbsp;&nbsp;</span>Demographics</a></span></li><li><span><a href="#Objective-measures" data-toc-modified-id="Objective-measures-1.2.2"><span class="toc-item-num">1.2.2&nbsp;&nbsp;</span>Objective measures</a></span></li><li><span><a href="#COVID-related-worries" data-toc-modified-id="COVID-related-worries-1.2.3"><span class="toc-item-num">1.2.3&nbsp;&nbsp;</span>COVID-related worries</a></span></li></ul></li></ul></li><li><span><a href="#Correlations" data-toc-modified-id="Correlations-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Correlations</a></span><ul class="toc-item"><li><span><a href="#Questionnaire-correlations" data-toc-modified-id="Questionnaire-correlations-2.1"><span class="toc-item-num">2.1&nbsp;&nbsp;</span>Questionnaire correlations</a></span></li><li><span><a href="#Correlations-of-key-variables-[collapsed-measures]" data-toc-modified-id="Correlations-of-key-variables-[collapsed-measures]-2.2"><span class="toc-item-num">2.2&nbsp;&nbsp;</span>Correlations of key variables [collapsed measures]</a></span></li><li><span><a href="#Correlations-of-ALL-variables-[]" data-toc-modified-id="Correlations-of-ALL-variables-[]-2.3"><span class="toc-item-num">2.3&nbsp;&nbsp;</span>Correlations of ALL variables []</a></span></li></ul></li></ul></div>


```python
import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
from IPython.display import display, Markdown, Latex
df = pd.read_csv('../../data/session1/clean_dataset.csv')
```

    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/statsmodels/tools/_testing.py:19: FutureWarning: pandas.util.testing is deprecated. Use the functions in the public API at pandas.testing instead.
      import pandas.util.testing as tm


This script should generate a report for a given session

## Descriptives

### Means and distributions



```python
df.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>lastpage</th>
      <th>SESSIONID</th>
      <th>sr_age</th>
      <th>q6_me_inf</th>
      <th>q6_close_person_inf</th>
      <th>q6_close_person_died</th>
      <th>q6_econ_impact_me</th>
      <th>q6_econ_impact_closep</th>
      <th>q6_work_home</th>
      <th>q6_apply_soc_dist</th>
      <th>...</th>
      <th>stai_ta</th>
      <th>stai_sa</th>
      <th>sticsa_ta</th>
      <th>sticsa_cog_ta</th>
      <th>sticsa_som_ta</th>
      <th>sticsa_sa</th>
      <th>sticsa_cog_sa</th>
      <th>sticsa_som_sa</th>
      <th>bdi</th>
      <th>cat</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>402.000000</td>
      <td>402.0</td>
      <td>400.00000</td>
      <td>397.000000</td>
      <td>397.000000</td>
      <td>397.000000</td>
      <td>402.000000</td>
      <td>402.000000</td>
      <td>402.000000</td>
      <td>402.000000</td>
      <td>...</td>
      <td>401.000000</td>
      <td>402.000000</td>
      <td>402.000000</td>
      <td>402.000000</td>
      <td>402.000000</td>
      <td>401.000000</td>
      <td>401.000000</td>
      <td>401.000000</td>
      <td>402.000000</td>
      <td>401.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>11.987562</td>
      <td>1.0</td>
      <td>27.60000</td>
      <td>0.055416</td>
      <td>0.050378</td>
      <td>0.105793</td>
      <td>2.925373</td>
      <td>3.880597</td>
      <td>4.149254</td>
      <td>6.500000</td>
      <td>...</td>
      <td>45.800499</td>
      <td>40.654229</td>
      <td>35.601990</td>
      <td>19.781095</td>
      <td>15.820896</td>
      <td>32.665835</td>
      <td>18.142145</td>
      <td>14.523691</td>
      <td>12.093698</td>
      <td>31.840399</td>
    </tr>
    <tr>
      <th>std</th>
      <td>0.205521</td>
      <td>0.0</td>
      <td>5.86139</td>
      <td>0.229078</td>
      <td>0.218999</td>
      <td>0.307961</td>
      <td>2.181172</td>
      <td>2.249006</td>
      <td>2.770995</td>
      <td>1.019143</td>
      <td>...</td>
      <td>12.164913</td>
      <td>12.438872</td>
      <td>10.803657</td>
      <td>7.031943</td>
      <td>4.772402</td>
      <td>10.510854</td>
      <td>7.045016</td>
      <td>4.469347</td>
      <td>9.383892</td>
      <td>16.839966</td>
    </tr>
    <tr>
      <th>min</th>
      <td>8.000000</td>
      <td>1.0</td>
      <td>18.00000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>...</td>
      <td>21.000000</td>
      <td>19.000000</td>
      <td>21.000000</td>
      <td>10.000000</td>
      <td>11.000000</td>
      <td>21.000000</td>
      <td>10.000000</td>
      <td>11.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>12.000000</td>
      <td>1.0</td>
      <td>23.00000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>6.000000</td>
      <td>...</td>
      <td>36.000000</td>
      <td>31.000000</td>
      <td>27.000000</td>
      <td>14.000000</td>
      <td>12.000000</td>
      <td>24.000000</td>
      <td>12.000000</td>
      <td>11.000000</td>
      <td>5.000000</td>
      <td>18.000000</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>12.000000</td>
      <td>1.0</td>
      <td>27.00000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>4.000000</td>
      <td>5.000000</td>
      <td>7.000000</td>
      <td>...</td>
      <td>45.000000</td>
      <td>40.000000</td>
      <td>34.000000</td>
      <td>19.000000</td>
      <td>14.500000</td>
      <td>30.000000</td>
      <td>17.000000</td>
      <td>13.000000</td>
      <td>10.500000</td>
      <td>32.000000</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>12.000000</td>
      <td>1.0</td>
      <td>32.00000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.000000</td>
      <td>6.000000</td>
      <td>7.000000</td>
      <td>7.000000</td>
      <td>...</td>
      <td>55.000000</td>
      <td>49.000000</td>
      <td>42.000000</td>
      <td>24.000000</td>
      <td>18.000000</td>
      <td>38.000000</td>
      <td>23.000000</td>
      <td>16.000000</td>
      <td>17.000000</td>
      <td>44.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>12.000000</td>
      <td>1.0</td>
      <td>40.00000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>7.000000</td>
      <td>7.000000</td>
      <td>7.000000</td>
      <td>7.000000</td>
      <td>...</td>
      <td>76.000000</td>
      <td>76.000000</td>
      <td>84.000000</td>
      <td>40.000000</td>
      <td>44.000000</td>
      <td>84.000000</td>
      <td>40.000000</td>
      <td>44.000000</td>
      <td>50.000000</td>
      <td>80.000000</td>
    </tr>
  </tbody>
</table>
<p>8 rows × 67 columns</p>
</div>



### Histograms
#### Demographics


```python
g = sns.distplot(df.loc[:,["sr_age"]], bins=15, color="blue", hist=True, kde=False).set_title('Age')

```


![png](output_5_0.png)



```python
a = np.squeeze((df.loc[:,["sr_gender"]]))
g = sns.countplot(a).set_title('Gender')
```


![png](output_6_0.png)


#### Objective measures


```python
a = np.squeeze((df.loc[:,["q6_me_inf"]]))
g = sns.countplot(a).set_title('Were you infected?')
ax = plt.gca()
ax.set_xticklabels(["No", "Yes"])
for p in ax.patches:
    ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x()+0.35, p.get_height()+3))
```


![png](output_8_0.png)



```python
a = np.squeeze((df.loc[:,["q6_close_person_inf"]]))
g = sns.countplot(a).set_title('Was a close person to you infected?')
ax = plt.gca()
ax.set_xticklabels(["No", "Yes"])
for p in ax.patches:
    ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x()+0.35, p.get_height()+3))
```


![png](output_9_0.png)



```python
a = np.squeeze((df.loc[:,["q6_close_person_died"]]))
g = sns.countplot(a).set_title('Did a close person die/get very serious?')
ax = plt.gca()
ax.set_xticklabels(["No", "Yes"])
for p in ax.patches:
    ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x()+0.35, p.get_height()+3))
```


![png](output_10_0.png)



```python
a = (df.loc[:,"q6_econ_impact_me":"q6_risk_group_closep"])
s_str = ["Economic impact me", "Econ impact close person", "Currently work from home", "Apply social distancing", "I belong to risk group", "Close person belongs to risk group"]
a
fig, ax = plt.subplots(2, 3, figsize=(20, 10))
for idx, (col, sf) in enumerate(zip(a, ax.flatten())):
    g = sns.distplot(a[col], ax=sf, bins=7, color="darkblue", hist=True, kde=False,
                    hist_kws={'range':(1,7), 'edgecolor':'black', 'alpha':0.8},)
    g.set_title("Objective measures")
    sf.set_xticks([1,7])
    sf.set_xticklabels(["Strongly disagree", "Strongly Agree"]) 
    sf.set_xlabel("")
    sf.set_title(s_str[idx])
    #sf.set_rotation(30)
     
```


![png](output_11_0.png)



```python
a = (df.loc[:,"q6_houshold_membs"])
s_str = ["Household members"]
g = sns.distplot(a, bins=5, color="darkblue", hist=True, kde=False,
                hist_kws={'range':(1,5), 'edgecolor':'black', 'alpha':0.8});
fig = plt.gca()
fig.set_xticks([1,5])
fig.set_xticklabels(["1","5+"])
fig.set_xlabel("Household members")


```




    Text(0.5, 0, 'Household members')




![png](output_12_1.png)



```python
a = (df.loc[:,"q6_media_freq"])
#s_str = ["How often do "]
g = sns.countplot(a, color="darkblue").set_title("How often do you follow the COVID-related news?");
fig = plt.gca();
fig.set_xticks(np.arange(5)-0.5);
fig.set_xticklabels(fig.get_xticklabels(), rotation=45);
```


![png](output_13_0.png)



```python
a = (df.loc[:,"q6_media_valence"])
g = sns.distplot(a, bins=7, color="darkblue", hist=True, kde=False,
                hist_kws={'range':(-3,4), 'edgecolor':'black', 'alpha':0.8});
fig = plt.gca()
fig.set_xticks(np.arange(-3,4))
fig.set_xlabel("Consumed media valence")
```




    Text(0.5, 0, 'Consumed media valence')




![png](output_14_1.png)


#### COVID-related worries


```python
a = (df.loc[:,"q7_worry_infected":"q7_vir_made_lab"])
s_str = ["Worry I will get infected", "Worry I will die", "Worry about economic impact on me", "Worry something bad will happen to me", "Worry there won't be sufficient help", "Worry close person will get infected", "Worry close person will die/get serious", "Worry about shortages", "WE are in a period of danger", "We are in a period of safety", "I was surprised when pandemic broke out", "I was very scared initially", "People overreact to it", "Virus is not as dangerous", "Virus is made in lab"]
a
fig, ax = plt.subplots(5, 3, figsize=(20, 15))
fig.subplots_adjust( wspace=0.3, hspace=0.3)
for idx, (col, sf) in enumerate(zip(a, ax.flatten())):
    g = sns.distplot(a[col], ax=sf, bins=7, color="darkblue", hist=True, kde=False,
                    hist_kws={'range':(1,7), 'edgecolor':'black', 'alpha':0.4},)
    g.set_title("Objective measures")
    sf.set_xticks([1,7])
    sf.set_xticklabels(["Strongly disagree", "Strongly Agree"]) 
    sf.set_xlabel("")
    sf.set_title(s_str[idx])

```


![png](output_16_0.png)



```python
a = (df.loc[:,"q7_inf_worry_frequency"]).replace({"Nearly every day (more than half the days)": "(d) Nearly all days",
                                                 "On one or several days": "(b) One or several",
                                                 "Never": "(a) Never", "On about half the days": "(c) On about hald the days"})
a = a.sort_values()
g = sns.countplot(a, color="darkblue").set_title("Worried about getting infected (out of past 2 weeks)");
fig = plt.gca();
fig.set_xticklabels(fig.get_xticklabels(), rotation=45);
```


![png](output_17_0.png)



```python
a = (df.loc[:,"q7_diff_beh_freq"]).replace({"Nearly every day (more than half the days)": "(d) Nearly all days",
                                                 "On one or several days": "(b) One or several",
                                                 "Never": "(a) Never", "On about half the days": "(c) On about hald the days"})
a = a.sort_values()
g = sns.countplot(a, color="darkblue").set_title("Behaved differently (out of past 2 weeks)");
fig = plt.gca();
fig.set_xticklabels(fig.get_xticklabels(), rotation=45);
```


![png](output_18_0.png)



```python
a = (df.loc[:,"q7_beh_wash_hands":"q7_anx_another_beh"])
s_str = ["Meticulously wash hands", "Avoid people", "Avoid public places",
         "Avoid touching surfaces outside of my house", "Avoid standing close to ppl",
         "Avoid eating food prepared by others", "Avoid using public transport",
         "Avoid visiting doctor", "Avoid other behaviours"]
a
fig, ax = plt.subplots(3, 3, figsize=(20, 15))
fig.subplots_adjust( wspace=0.4, hspace=0.3)
for idx, (col, sf) in enumerate(zip(a, ax.flatten())):
    g = sns.distplot(a[col], ax=sf, bins=7, color="darkblue", hist=True, kde=False,
                    hist_kws={'range':(1,7), 'edgecolor':'black', 'alpha':0.4},)
    g.set_title("Objective measures")
    sf.set_xticks([1,7])
    sf.set_xticklabels(["Strongly disagree", "Strongly Agree"]) 
    sf.set_xlabel("")
    sf.set_title(s_str[idx])

```


![png](output_19_0.png)



```python
#### COVID-related predictions / estimates
```


```python
a = (df.loc[:,"q8_prob_inf_me":"q8_prob_inf_avgp"])
s_str = ["Prob: I get infected", "Prob: I die", "Prob: Severe economic impact on me",
        "Prob: Close person infected", "Prob: Close person die", "Prob: Average person infected"]

fig, ax = plt.subplots(2,3, figsize=(20, 15))
fig.subplots_adjust( wspace=0.4, hspace=0.3)
for idx, (col, sf) in enumerate(zip(a, ax.flatten())):
    g = sns.distplot(a[col].astype(int), ax=sf, bins=10, color="pink", hist=True, kde=False,
                    hist_kws={'range':(1,100), 'edgecolor':'black', 'alpha':0.7},)
    g.set_title("Probability estimates")
    #sf.set_xticks([1,7])
    #sf.set_xticklabels(["Strongly disagree", "Strongly Agree"]) 
    sf.set_xlabel("")
    sf.set_title(s_str[idx])

#sns.distplot(a)
```


![png](output_21_0.png)



```python
from datetime import datetime, timedelta
from collections import OrderedDict
dates = ["2020-05-01", "2023-06-01"]
start, end = [datetime.strptime(_, "%Y-%m-%d") for _ in dates]
mostr = OrderedDict(((start + timedelta(_)).strftime(r"%Y-%m"), None) for _ in range((end - start).days)).keys()

un = df.loc[:,"q8_t_pand_end"].value_counts(mostr,  sort=True)
un = un.sort_index()

fig = plt.figure(figsize=(16, 6))
fig = plt.bar(x = un.index, height = un, color="darkblue")
plt.xticks(rotation=70);
ax = plt.gca()
ax.set_title("Expected end of pandemic");


```


![png](output_22_0.png)



```python
dates = ["2020-05-01", "2023-06-01"]
start, end = [datetime.strptime(_, "%Y-%m-%d") for _ in dates]
mostr = OrderedDict(((start + timedelta(_)).strftime(r"%Y-%m"), None) for _ in range((end - start).days)).keys()

un = df.loc[:,"q8_t_life_back_norm"].value_counts(mostr,  sort=True)
un = un.sort_index()

fig = plt.figure(figsize=(16, 6))
fig = plt.bar(x = un.index, height = un, color="darkblue")
plt.xticks(rotation=70);
ax = plt.gca()
ax.set_title("When will life come back to normal?");
```


![png](output_23_0.png)



```python
a = np.squeeze((df.loc[:,["q8_secondw"]]))
g = sns.countplot(a).set_title('Will there be a second wave?')
ax = plt.gca()
ax.set_xticklabels(["No", "Yes"])
for p in ax.patches:
    ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x()+0.35, p.get_height()+3))
```


![png](output_24_0.png)



```python
dates = ["2020-05-01", "2023-06-01"]
start, end = [datetime.strptime(_, "%Y-%m-%d") for _ in dates]
mostr = OrderedDict(((start + timedelta(_)).strftime(r"%Y-%m"), None) for _ in range((end - start).days)).keys()

un = df.loc[:,"q8_t_secondw_when"].value_counts(mostr,  sort=True)
un = un.sort_index()

fig = plt.figure(figsize=(16, 6))
fig = plt.bar(x = un.index, height = un, color="darkblue")
plt.xticks(rotation=70);
ax = plt.gca()
ax.set_title("If yes, when will the second wave come?");
```


![png](output_25_0.png)



```python

```


```python

```


```python
dates = ["2020-05-01", "2023-06-01"]
start, end = [datetime.strptime(_, "%Y-%m-%d") for _ in dates]
mostr = OrderedDict(((start + timedelta(_)).strftime(r"%Y-%m"), None) for _ in range((end - start).days)).keys()

un = df.loc[:,"q8_t_econ_back_norm"].value_counts(mostr,  sort=True)
un = un.sort_index()

fig = plt.figure(figsize=(16, 6))
fig = plt.bar(x = un.index, height = un, color="darkblue")
plt.xticks(rotation=70);
ax = plt.gca()
ax.set_title("When will the economy come back to normal?");
```


![png](output_28_0.png)


## Correlations


```python
import seaborn as sns
import matplotlib.pyplot as plt
ax = sns.distplot(df.q7_vir_made_lab)
print(df.q7_vir_not_as_dangerous.value_counts())

q_c = ["q7_vir_made_lab", "q7_vir_not_as_dangerous"]
g = sns.jointplot(x=q_c[0], y=q_c[1], data=df, kind="kde");
g.ax_joint.set_xticks(np.arange(1,8));
g.ax_joint.set_yticks(np.arange(1,8));
```

    2.0    128
    1.0    117
    3.0     59
    4.0     47
    5.0     27
    6.0     15
    7.0      9
    Name: q7_vir_not_as_dangerous, dtype: int64



![png](output_30_1.png)



![png](output_30_2.png)



```python

```

### Questionnaire correlations



```python
keep_vars = ["stai_ta",  "stai_sa", "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta",  "sticsa_sa", "sticsa_cog_sa", "sticsa_som_sa", "bdi","cat"]
voi = df.loc[:, df.columns.intersection(keep_vars) ]
g = sns.pairplot(voi, corner=True, diag_kind="kde", kind="reg")
g.map_lower(corrfunc)
```




    <seaborn.axisgrid.PairGrid at 0x7f5f0277e6a0>




![png](output_33_1.png)


### Correlations of key variables [collapsed measures]


```python
key_vars = ["covid_worry", "covid_avoidance_beh", "covid_spec_anxiety", "covid_prob_estimates", "covid_end_est"]
qnames = ["STAI-TRAIT", "STAI-STATE", "STICSA-TRAIT", "STICSA-TRAIT-COGNITIVE", "STICSA-TRAIT-SOMATIC", "STICSA-STATE", "STICSA-STATE-COGNITIVE", "STICSA-STATE-SOMATIC", "BDI", "Catastrophizing"]
questionnaires = ["stai_ta",  "stai_sa", "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta",  "sticsa_sa", "sticsa_cog_sa", "sticsa_som_sa", "bdi","cat"]
for qidx, qs in enumerate(questionnaires):
    display(Markdown("#### "+qnames[qidx]))
    keep_vars = np.append(key_vars, qs)
    voi = df.loc[:, df.columns.intersection(keep_vars) ]   
    g = sns.pairplot(voi, corner=True, diag_kind="kde", kind="reg")
    g.map_lower(corrfunc)
    plt.subplots_adjust(top=0.9)
    g.fig.suptitle(qnames[qidx])
```


#### STAI-TRAIT



#### STAI-STATE



#### STICSA-TRAIT



#### STICSA-TRAIT-COGNITIVE



#### STICSA-TRAIT-SOMATIC



#### STICSA-STATE



#### STICSA-STATE-COGNITIVE



#### STICSA-STATE-SOMATIC



#### BDI



#### Catastrophizing



![png](output_35_10.png)



![png](output_35_11.png)



![png](output_35_12.png)



![png](output_35_13.png)



![png](output_35_14.png)



![png](output_35_15.png)



![png](output_35_16.png)



![png](output_35_17.png)



![png](output_35_18.png)



![png](output_35_19.png)


### Correlations of ALL variables []


```python
var_groups = [["q6_econ_impact_me","q6_econ_impact_closep","q6_work_home","q6_apply_soc_dist", "q6_risk_group", "q6_risk_group_closep","q6_houshold_membs", "q6_media_valence"], 
              ["q7_worry_infected","q7_worry_die","q7_worry_econ_impact","q7_worry_sthg_bad","q7_worry_insuf_help", "q7_worry_closep_inf", "q7_closep_die","q7_worry_shortage"],
              ["q7_period_rel_danger","q7_period_rel_safety", "q7_initial_surprise","q7_initial_scared","q7_people_overreact","q7_vir_not_as_dangerous","q7_vir_made_lab"],
              ["q7_beh_wash_hands", "q7_beh_avoid_ppl", "q7_beh_avoid_public_places"],
              ["q7_anx_touching_surf","q7_anx_stand_close_to_ppl","q7_anx_eating_food_out","q7_anx_public_transp", "q7_anx_visit_doc", "q7_anx_another_beh"],
              ["q8_prob_inf_me", "q8_prob_die_me", "q8_prob_econ_imp_me", "q8_prob_inf_closep", "q8_prob_die_closep","q8_prob_inf_avgp"],
              ["q8_t_pand_end_days","q8_t_life_back_norm_days","q8_t_secondw_when_days","q8_t_econ_back_norm_days"]   
]
print(len(var_groups))
group_names = ["Objective measures", "Worries", "Worries&Attitudes", "Behaviours", "Anxieties", "Objective Probabilities", "Time Estimates"]
qnames = ["STAI-TRAIT", "STAI-STATE", "STICSA-TRAIT", "STICSA-TRAIT-COGNITIVE", "STICSA-TRAIT-SOMATIC", "STICSA-STATE", "STICSA-STATE-COGNITIVE", "STICSA-STATE-SOMATIC", "BDI", "Catastrophizing"]
questionnaires = ["stai_ta",  "stai_sa", "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta",  "sticsa_sa", "sticsa_cog_sa", "sticsa_som_sa", "bdi","cat"]

for qidx, qs in enumerate(questionnaires):
    display(Markdown("#### "+qnames[qidx]))
    for gr in range(len(var_groups)):
        display(Markdown("##### "+group_names[gr]))
        keep_vars = np.append(var_groups[gr], qs)
        voi = df.loc[:, df.columns.intersection(keep_vars) ]   
        g = sns.pairplot(voi, corner=True, diag_kind="kde", kind="reg")
        g.map_lower(corrfunc)
        plt.subplots_adjust(top=0.9)
        g.fig.suptitle(group_names[gr]+": "+qnames[qidx])
```

    7



#### STAI-TRAIT



##### Objective measures



##### Worries



##### Worries&Attitudes



##### Behaviours



##### Anxieties



##### Objective Probabilities



##### Time Estimates



#### STAI-STATE



##### Objective measures



##### Worries



##### Worries&Attitudes



##### Behaviours



##### Anxieties



##### Objective Probabilities



##### Time Estimates



#### STICSA-TRAIT



##### Objective measures



##### Worries



##### Worries&Attitudes



##### Behaviours



##### Anxieties



##### Objective Probabilities



##### Time Estimates


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



#### STICSA-TRAIT-COGNITIVE



##### Objective measures


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries&Attitudes


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Behaviours


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Anxieties


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Objective Probabilities


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Time Estimates


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



#### STICSA-TRAIT-SOMATIC



##### Objective measures


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries&Attitudes


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Behaviours


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Anxieties


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Objective Probabilities


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Time Estimates


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



#### STICSA-STATE



##### Objective measures


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries&Attitudes


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Behaviours


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Anxieties


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Objective Probabilities


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Time Estimates


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



#### STICSA-STATE-COGNITIVE



##### Objective measures


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries&Attitudes


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Behaviours


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Anxieties


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Objective Probabilities


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Time Estimates


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



#### STICSA-STATE-SOMATIC



##### Objective measures


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries&Attitudes


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Behaviours


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Anxieties


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Objective Probabilities


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Time Estimates


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



#### BDI



##### Objective measures


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries&Attitudes


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Behaviours


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Anxieties


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Objective Probabilities


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Time Estimates


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



#### Catastrophizing



##### Objective measures


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Worries&Attitudes


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Behaviours


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Anxieties


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Objective Probabilities


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



##### Time Estimates


    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/seaborn/axisgrid.py:1295: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      squeeze=False)



![png](output_37_131.png)



![png](output_37_132.png)



![png](output_37_133.png)



![png](output_37_134.png)



![png](output_37_135.png)



![png](output_37_136.png)



![png](output_37_137.png)



![png](output_37_138.png)



![png](output_37_139.png)



![png](output_37_140.png)



![png](output_37_141.png)



![png](output_37_142.png)



![png](output_37_143.png)



![png](output_37_144.png)



![png](output_37_145.png)



![png](output_37_146.png)



![png](output_37_147.png)



![png](output_37_148.png)



![png](output_37_149.png)



![png](output_37_150.png)



![png](output_37_151.png)



![png](output_37_152.png)



![png](output_37_153.png)



![png](output_37_154.png)



![png](output_37_155.png)



![png](output_37_156.png)



![png](output_37_157.png)



![png](output_37_158.png)



![png](output_37_159.png)



![png](output_37_160.png)



![png](output_37_161.png)



![png](output_37_162.png)



![png](output_37_163.png)



![png](output_37_164.png)



![png](output_37_165.png)



![png](output_37_166.png)



![png](output_37_167.png)



![png](output_37_168.png)



![png](output_37_169.png)



![png](output_37_170.png)



![png](output_37_171.png)



![png](output_37_172.png)



![png](output_37_173.png)



![png](output_37_174.png)



![png](output_37_175.png)



![png](output_37_176.png)



![png](output_37_177.png)



![png](output_37_178.png)



![png](output_37_179.png)



![png](output_37_180.png)



![png](output_37_181.png)



![png](output_37_182.png)



![png](output_37_183.png)



![png](output_37_184.png)



![png](output_37_185.png)



![png](output_37_186.png)



![png](output_37_187.png)



![png](output_37_188.png)



![png](output_37_189.png)



![png](output_37_190.png)



![png](output_37_191.png)



![png](output_37_192.png)



![png](output_37_193.png)



![png](output_37_194.png)



![png](output_37_195.png)



![png](output_37_196.png)



![png](output_37_197.png)



![png](output_37_198.png)



![png](output_37_199.png)



![png](output_37_200.png)

