## Berlin study on COVID-19 and mental health

Dear participants,

as we are approaching the fifth survey we would like to take the opportunity to let you know a bit more about the study you are taking part in as well as to answer some of the common questions that you have raised.

First of all, we are really impressed with by engagement and commitment on your part! These are tough times. Many of us are struggling as we are facing difficult personal situations, uncertainty and segregation.
Many of us have serious worries and concerns about our lives and futures, and those of our loved ones. Despite all of this, you take the time to complete surveys and let us know in great detail about your worries and thoughts.
We very much appreciate the time you all take in completing the survey, including the voluntary open questions at the end. We read those carefully and try to incorporate your feedback into the surveys.

We are also very positively surprised about the low dropout rates. Only about 3% of participants don't complete the surveys on time, and this is often due to outstanding circumstances or due to our message ending up in a spam box.
As you know, we designed the payment scheme to encourage long-term participation, so this might not be that surprising. However, it is quite clear from your comments and the time you have taken that most of you engage with the
study beyond purely financial reasons, because you genuinely want to help. As mental health researchers, we are very grateful for that. The richness of your comments allows us to form a more detailed picture. This helps us to see the data via a more personalized lens that goes beyond quantitative analysis.

Many of you have enquired about the details of the study and a few other points related to it. To let you know that we listen and care, we answered the five most commonly asked questions below.

Lastly, we'd like to ask you to keep filling out the surveys and to provide us with as much feedback as possible. No matter if you are very worried about COVID-19 or fed up by any mention of it, your data will be extremely important
to paint the overall picture.

On behalf of the team, sincerely, thank you.

Ondrej



### What is the project about?
The study looks at how people's worries and thoughts in relation to COVID-19 develop over time. In each survey we ask you to consider the period since the last survey (unless specified otherwise) and to let us know about how you feel and what you worry about.
In other words, the surveys take a snapshot of your current state and track how this changes over time in relation to your individual circumstances as well as global events. The data are extremely unique and invaluable to our understanding of real-world
dynamics related to mental health. As scientists, we often collect data in laboratories using simplified tasks and questionnaires. This study is so unique because it allows us to study *real* people in the context of *real* lives. This will be invaluable in informing mental health response as well as providing empirical evidence to support or refute theories of cognition.

### Where can I see the summary of the data? Are they available online?
As much as we would love to share the data as we go, the results might bias answers to some questions. It is therefore not possible to share even some snippets with you at this point. Once the study is complete, the data will be analyzed and published in an open-access journal, so that everyone can see it.


### What happens if I miss a session?
Initially, we set to exclude participants who missed a session. However, a number of you have missed a session due to reasonable circumstances. We have therefore decided to allow participant re-entry if **one** session is missed. Should that happen to
you, please send us a message on Prolific or via email (covidstudyberlin@mpib-berlin.mpg.de) as soon as possible.


### Why are all the questions the same all the time?
We try to measure the *same* things across time, so the questions can not be altered. For each survey we ask you to think about the time period since the last session. In the end, this will allow us to zoom out and see how worries about and perceptions of COVID-19 change over time. If you find the questions too repetitive we have to ask for you patience. On a related note, do not try to remember what you may have answered during the last survey, each survey should reflect only the specific time window since the last survey.


### What's the future timing of the sessions?
The exact timing of the sessions is not yet set. For now, the sessions occur every two weeks but soon the frequency will drop to one survey per month. We may decide to increase the frequency again in the future. If that happens we will make sure to let you know.
