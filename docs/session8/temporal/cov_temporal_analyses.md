<h1>Table of Contents<span class="tocSkip"></span></h1>
<div class="toc"><ul class="toc-item"></ul></div>


```python
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import warnings; warnings.simplefilter('ignore')
```

    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/statsmodels/tools/_testing.py:19: FutureWarning: pandas.util.testing is deprecated. Use the functions in the public API at pandas.testing instead.
      import pandas.util.testing as tm


Analysis over time - state and covid measures


```python
conf = {"sessions": [1,2,3, 4,5, 6, 7,8],
        "questions": ["q6_me_inf", "q6_close_person_inf", "q6_close_person_died", "q6_media_valence",
"covid_worry", "covid_avoidance_beh", "covid_spec_anxiety", "covid_prob_estimates",  "covid_end_est", "stai_sa","sticsa_sa"]}

conf["questions"] = ["q6_econ_impact_me","q6_econ_impact_closep","q6_work_home","q6_apply_soc_dist", "q6_risk_group", "q6_risk_group_closep","q6_houshold_membs", "q6_media_valence", 
              "q7_worry_infected","q7_worry_die","q7_worry_econ_impact","q7_worry_sthg_bad","q7_worry_insuf_help", "q7_worry_closep_inf", "q7_closep_die","q7_worry_shortage",
              "q7_period_rel_danger","q7_period_rel_safety", "q7_initial_surprise","q7_initial_scared","q7_people_overreact","q7_vir_not_as_dangerous","q7_vir_made_lab",
              "q7_beh_wash_hands", "q7_beh_avoid_ppl", "q7_beh_avoid_public_places",
              "q7_anx_touching_surf","q7_anx_stand_close_to_ppl","q7_anx_eating_food_out","q7_anx_public_transp", "q7_anx_visit_doc", "q7_anx_another_beh",
              "q8_prob_inf_me", "q8_prob_die_me", "q8_prob_econ_imp_me", "q8_prob_inf_closep", "q8_prob_die_closep","q8_prob_inf_avgp",
              "q8_t_pand_end_days","q8_t_life_back_norm_days","q8_t_secondw_when_days","q8_t_econ_back_norm_days"]   


p = pd.read_csv("../../data/session2/clean_dataset_reduced.csv")
sns.palplot(sns.color_palette("Set2"))
for q in conf["questions"]:
    df = pd.DataFrame()
    for sess in conf["sessions"]:
        temp = pd.read_csv("../../data/session"+str(sess)+"/clean_dataset.csv")
        temp["session"] = sess
        df = pd.concat([df, temp.loc[:,temp.columns.intersection(["session", q])]])
    fig =  plt.figure(figsize=(7,6))
    ax = plt.gca()
    #ax = sns.violinplot(x="session", y=q, data=df)
    
    bx = sns.violinplot(x="session", y=q, data=df, palette="Set2")
    df["sess2"] = df["session"]-1
    ax = sns.lineplot(x="sess2", y=q, data=df)


```


![png](output_3_0.png)



![png](output_3_1.png)



![png](output_3_2.png)



![png](output_3_3.png)



![png](output_3_4.png)



![png](output_3_5.png)



![png](output_3_6.png)



![png](output_3_7.png)



![png](output_3_8.png)



![png](output_3_9.png)



![png](output_3_10.png)



![png](output_3_11.png)



![png](output_3_12.png)



![png](output_3_13.png)



![png](output_3_14.png)



![png](output_3_15.png)



![png](output_3_16.png)



![png](output_3_17.png)



![png](output_3_18.png)



![png](output_3_19.png)



![png](output_3_20.png)



![png](output_3_21.png)



![png](output_3_22.png)



![png](output_3_23.png)



![png](output_3_24.png)



![png](output_3_25.png)



![png](output_3_26.png)



![png](output_3_27.png)



![png](output_3_28.png)



![png](output_3_29.png)



![png](output_3_30.png)



![png](output_3_31.png)



![png](output_3_32.png)



![png](output_3_33.png)



![png](output_3_34.png)



![png](output_3_35.png)



![png](output_3_36.png)



![png](output_3_37.png)



![png](output_3_38.png)



![png](output_3_39.png)



![png](output_3_40.png)



![png](output_3_41.png)



![png](output_3_42.png)



```python
### Splits by anxiety
indif = pd.read_csv("../../data/session1/clean_dataset.csv")
indif = indif.loc[:,indif.columns.intersection(["PID",  "stai_ta", "stai_sa", "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "sticsa_sa", "sticsa_cog_sa", "sticsa_som_sa", "bdi", "cat"
])].set_index(["PID"])

ms_vars = ["stai_ta",  "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "bdi", "cat"]
## Do median split
for msv in ms_vars:
    me = indif[msv].median()
    indif[msv+"_ms"]= 1
    indif[msv+"_ms"][indif[msv]>me] = 2

indif.drop(columns=ms_vars)
    
for q in conf["questions"]:    
    qdf = pd.DataFrame()
    for sess in conf["sessions"]:
  
        temp = pd.read_csv("../../data/session"+str(sess)+"/clean_dataset.csv").set_index(["PROLIFICID"])
        df = pd.DataFrame()
        df = indif
        a = temp.loc[:,temp.columns.intersection([q])]

        df = df.join(a, rsuffix="2") 
        sqdf = pd.DataFrame()
        sqdf = pd.concat([sqdf, df.loc[:,df.columns.intersection([q, "stai_ta_ms",  "sticsa_ta_ms", "sticsa_cog_ta_ms", "sticsa_som_ta_ms", "bdi_ms", "cat_ms"])]])
        sqdf["session"] = sess
        qdf = pd.concat([qdf, sqdf])
    
    toplot =["sticsa_ta_ms", "sticsa_cog_ta_ms", "sticsa_som_ta_ms"];
    fig =  plt.figure(figsize=(15,5))
    #qdf=qdf.loc[qdf.GROUP=="UK",:]
    for idx, i in enumerate(toplot):
        plt.subplot(1,3,idx+1)
        ax = plt.gca()
        #ax = sns.violinplot(x="session", y=q, data=df)
        
        ax = sns.violinplot(x="session", y=q, data=qdf, hue=i, split=True, palette="autumn")
        qdf["sess2"] = qdf["session"]-1
        ax = sns.lineplot(x="sess2", y=q, data=qdf, hue=i, palette="autumn", legend=False)
        new_labels = ['Low', 'High']
        leg=ax.axes.get_legend()
        leg.set_title(i)
        for t, l in zip(leg.texts, new_labels): t.set_text(l)
        #sns.plt.show()
        #for t, l in zip(ax.axes, new_labels): t.set_text(l)
    
        #sns.plt.show()

    
    
        

```


![png](output_4_0.png)



![png](output_4_1.png)



![png](output_4_2.png)



![png](output_4_3.png)



![png](output_4_4.png)



![png](output_4_5.png)



![png](output_4_6.png)



![png](output_4_7.png)



![png](output_4_8.png)



![png](output_4_9.png)



![png](output_4_10.png)



![png](output_4_11.png)



![png](output_4_12.png)



![png](output_4_13.png)



![png](output_4_14.png)



![png](output_4_15.png)



![png](output_4_16.png)



![png](output_4_17.png)



![png](output_4_18.png)



![png](output_4_19.png)



![png](output_4_20.png)



![png](output_4_21.png)



![png](output_4_22.png)



![png](output_4_23.png)



![png](output_4_24.png)



![png](output_4_25.png)



![png](output_4_26.png)



![png](output_4_27.png)



![png](output_4_28.png)



![png](output_4_29.png)



![png](output_4_30.png)



![png](output_4_31.png)



![png](output_4_32.png)



![png](output_4_33.png)



![png](output_4_34.png)



![png](output_4_35.png)



![png](output_4_36.png)



![png](output_4_37.png)



![png](output_4_38.png)



![png](output_4_39.png)



![png](output_4_40.png)



![png](output_4_41.png)



```python
### Splits by anxiety & Country 
indif = pd.read_csv("../../data/session1/clean_dataset.csv")
indif = indif.loc[:,indif.columns.intersection(["PID", "GROUP",  "stai_ta", "stai_sa", "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "sticsa_sa", "sticsa_cog_sa", "sticsa_som_sa", "bdi", "cat"
])].set_index(["PID"])

ms_vars = ["stai_ta",  "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "bdi", "cat"]
## Do median split
for msv in ms_vars:
    me = indif[msv].median()
    indif[msv+"_ms"]= 1
    indif[msv+"_ms"][indif[msv]>me] = 2

indif.drop(columns=ms_vars)
print(indif.columns)
for q in conf["questions"]:    
    qdf = pd.DataFrame()
    for sess in conf["sessions"]:
  
        temp = pd.read_csv("../../data/session"+str(sess)+"/clean_dataset.csv").set_index(["PROLIFICID"])
        df = pd.DataFrame()
        df = indif
        a = temp.loc[:,temp.columns.intersection([q, "GROUP"])]

        df = df.join(a, rsuffix="2") 
        sqdf = pd.DataFrame()
        sqdf = pd.concat([sqdf, df.loc[:,df.columns.intersection([q, "GROUP",  "stai_ta_ms",  "sticsa_ta_ms", "sticsa_cog_ta_ms", "sticsa_som_ta_ms", "bdi_ms", "cat_ms"])]])
        sqdf["session"] = sess
        qdf = pd.concat([qdf, sqdf])
    
    toplot =["sticsa_ta_ms", "sticsa_cog_ta_ms", "sticsa_som_ta_ms"];
    fig =  plt.figure(figsize=(15,10))
    #print(qdf)
    countries = ["BE", "UK"]
    for idx, i in enumerate(toplot):
        for idx_c, ctry in enumerate(countries):
            plt.subplot(2,3,(idx+1)+idx_c*3  )
            ax = plt.gca()
            #ax = sns.violinplot(x="session", y=q, data=df)
            qdf_loc=qdf.loc[qdf.GROUP==ctry,:]
           #print(qdf_loc)
            ax = sns.violinplot(x="session", y=q, data=qdf_loc, hue=i, split=True, palette="autumn")
            qdf_loc["sess2"] = qdf_loc["session"]-1
            ax = sns.lineplot(x="sess2", y=q, data=qdf_loc, hue=i, palette="autumn", legend=False)
            new_labels = ['Low', 'High']
            leg=ax.axes.get_legend()
            leg.set_title(ctry+": "+i )
            for t, l in zip(leg.texts, new_labels): t.set_text(l)
```

    Index(['GROUP', 'stai_ta', 'stai_sa', 'sticsa_ta', 'sticsa_cog_ta',
           'sticsa_som_ta', 'sticsa_sa', 'sticsa_cog_sa', 'sticsa_som_sa', 'bdi',
           'cat', 'stai_ta_ms', 'sticsa_ta_ms', 'sticsa_cog_ta_ms',
           'sticsa_som_ta_ms', 'bdi_ms', 'cat_ms'],
          dtype='object')



![png](output_5_1.png)



![png](output_5_2.png)



![png](output_5_3.png)



![png](output_5_4.png)



![png](output_5_5.png)



![png](output_5_6.png)



![png](output_5_7.png)



![png](output_5_8.png)



![png](output_5_9.png)



![png](output_5_10.png)



![png](output_5_11.png)



![png](output_5_12.png)



![png](output_5_13.png)



![png](output_5_14.png)



![png](output_5_15.png)



![png](output_5_16.png)



![png](output_5_17.png)



![png](output_5_18.png)



![png](output_5_19.png)



![png](output_5_20.png)



![png](output_5_21.png)



![png](output_5_22.png)



![png](output_5_23.png)



![png](output_5_24.png)



![png](output_5_25.png)



![png](output_5_26.png)



![png](output_5_27.png)



![png](output_5_28.png)



![png](output_5_29.png)



![png](output_5_30.png)



![png](output_5_31.png)



![png](output_5_32.png)



![png](output_5_33.png)



![png](output_5_34.png)



![png](output_5_35.png)



![png](output_5_36.png)



![png](output_5_37.png)



![png](output_5_38.png)



![png](output_5_39.png)



![png](output_5_40.png)



![png](output_5_41.png)



![png](output_5_42.png)

