<!-- docs/_sidebar.md -->
* [Home](/)
---
* [Proposal](proposal.md)
---
**Sessions**
* [Session1](session1.md)
* [Session 5 - temporal](https://zika.mpib.berlin/covid-fear/shared/#/session5/temporal/cov_temporal_analyses)
* [Session 8 - temporal](https://zika.mpib.berlin/covid-fear/shared/#/session8/temporal/cov_temporal_analyses)
* [Session 12 - temporal](https://zika.mpib.berlin/covid-fear/shared/#/session12/temporal/cov_temporal_analyses)
