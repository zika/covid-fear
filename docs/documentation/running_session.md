
### Intro
- each survey will start on Monday noon and end on Wednesday noon
- a reminder will be sent to both cohorts on Friday (3 days in advance)

### Preparation session (3 days before)

#### Folder organization
- the parent folder can be called anything on a given machine, here it is `Project4_covid`  
![dir_structure](img/folder_structure.png)
- it contains three folders:
  1. `covid-fear` is the git repo with scripts (anything outside of this should NOT be on git)
  2. `covid_study_materials` is a folder with sensitive information
  3. `data` contains all the files that aren't scripts

- to prepare a session, one needs to set up the `covid-fear` and `data` folder, and to pull the repo `https://git.mpib-berlin.mpg.de/zika/covid-fear` to `covid-fear`

#### Get data
1. Get data from last session's lime survey - this isn't essential but it can help to decide whether a certain participant actually answered correctly and therefore whether they should be invited.  
    a. Login page: https://survey3.gwdg.de/index.php?r=admin/authentication/sa/login  
    b. Go to the most recent survey, navigate to `responses` and click `Export->Export Responses` on the top  
    c. Before exporting, 2 things need to be changed:  
      i. `Export question as:` change `Full question text` to `Question Code`  
      ii. Toggle `Convert spaces in question text to underscores` to `On`  
    d. The click `Export` and save the data as `main_data.csv` to `data/session{X-1}` (previous session if X is the current session)  
2. Get data for last session from prolific separately for UK and Berlin (`More`>`Download export`)' and save to `data/session{X}/prolific_data_uk.csv` and `data/session{X}/prolific_data_be.csv`  
3. If there are any participants that should be re-included manually (e.g. they missed 1 session), a file called `be_manually_added.csv` (`uk` for UK sample) with csv list of prolific IDs needs to be added to `data/session{X}/working_data`.
3. Open the python script `covid-fear/scripts/cov_generate_subject_lists.py`, change session to current session (the one that's about to run), make sure that `generate_bonus_files = 0` and run. Note that this already assumes the folders are created and that there is a `participants_germany.csv` file in `data/session2/working_data/`  (get it from Ondrej but delete it after as it contains sensitive information)
4. Run the script, this will generate the lists of participants in `data/session{X}/working_data`

#### Reminder
- get list of subjects to recontact (above), for UK this is a list of Prolific IDs and for Berlin it's a list of email addresses
- the reminder text can be adjusted given specific message, the template is on Google drive [here](https://docs.google.com/document/d/1NL3rJkXszHNzo8lw0qqLZfGNq_EK95219HNk9oyb8Fg/edit)
- we also attach the payment schedule that needs to be generated: go to the repo and open the jupyter notebook in docs/figures.ipynb, change session number and generate the new figure

##### UK part
 - On Prolific, go to the study from previous session in "completed"
 - In "more" select "Bulk message", paste the list of ids and the message - make sure it's nicely formatted

##### Berlin part
 -  log in to covidstudyberlin@
 - prepare a message and set covidstudyberlin@ as receiver, then copy the comma-separated email list to BCC, so that they cannot see each other.

##### Drop outs
- people who dropped out / didn't complete need to be contacted and paid at rate $9/hr, the script above also generates that list


### Main session: Prepare materials and open surveys

#### Part 1 – Lime Survey

1. In **Lime Survey** we create a new study and make sure that all questions and descriptions are adequate for the purpose. This means one has to export the most recent version from the previous survey (which contains any most recent changes). Any changes must be logged here on the documentation in the `Versions and Changes` section. In order to do that we go to survey template – click *Display/Export* – *Survey Structure* – *Export*. It saves the file with *.lss* extension.  
2. Back to the main page of lime survey – Click *Surveys* – *Create a New Survey* – *Import* – Choose saved file – *Import*.  This creates a new survey identical to the previous one.
3. Now you can update information suitable for the new survey:
    - survey title and description
    - image with payment schedule and arrow for current session
4. After we finished everything in the Overview Section of the Survey, we need to copy and **save Survey URL** to give it later to Prolific.  
5. After that we can activate the survey in Lime Survey (it will ask some questions, but we keep everything as it is). In the end Lime Survey asks if we allow *Open-Access Mode* – Choose *No*.  

#### Part 2 – Prolific

1. We go to already completed study and click *Action* – *Duplicate* – It creates a new study in the *Unpublished* section. Do this for both Berlin (BE) and UK.
2. **Important** Here we edit the name and the form to set up the study. We need to click *Show Advanced* there and write the actual name of the study (internal name, e.g. covid-UK-session3).
3. The Construction of the Link:
  - First part of the link before `&PROLIFIC=` is the link of the Lime Survey.
  - Prolific Automatically changes {{%PROLIFIC-PID%}}  - if we want to send personalized link, we have to change exactly this part to their ID.
  - We need to make sure that the SESSIONID and Berlin/UK parameters are adjusted.
  - In this section there is also return link (participants need it to get back). We can check it in the *Texts Elements Section* of **Lime Survey**, in *End Message* click Source – the link there should match the second link in Prolific.

4. Audience Section:
  - Which devices should participants use? We choose Tablet and Desktop.
  - *Custom Prescreening* – *Custom Whitelist* - Here we copy the IDs from the data file (for example from `data/session{X}/uk_participants_who_didnt_fill_yet.csv`).

5. Participants Section:
  - How long do you estimate it will take each participant to complete the study? 8 min
  - How much to pay? 0.80 pounds.
  - in `long` session, the bonus needs to be added to the study description on Prolific AND in LS.

6. After that we publish the study and it appears in the Active Section. (Or save as draft and share with Ondrej/Katya to quickly go through the survey to check).

7. Then we send the Bulk messages notifying about the beginning of the study to UK and emails to Berlin.

### How to get the data from the survey (Lime Survey)?

Once the study is completed we go to Responses in Lime Survey, where we will be able to see all submissions. Here we also have information about their Prolific ID. With the help of this ID we are able to understand to which extent they completed the study and control them if they run out of time etc.

Besides, here we will also be able to control for the number of submissions per participant.

In order to access the data follow: *Export – Export Responses* – In the new window choose csv format in the *Format Section*. In *Headings Section*, the questions should be exported as Question Code. Besides “Strip HTML code” and “Convert spaces in question text to underscores” should be on. After that simply click Export and save it to the folder as the `main_data.csv`.

### How to get the data from the survey (Prolific)?

In Completed Section will be able to see all studies we ran on Prolific before with the internal names Covid-UK-session# and Covid-Berlin-session#. Here we can collect information about the time it took each participant to complete the study. You can message them, approve their participation (you won’t be able to cancel that – once you click they get paid) or reject them.

If we see that it wasn't completed (`TIMED-OUT` or `RETURNED`) we message them: British via Prolific and Germans via Email and ask what happened. If it figures out that they could not complete it because something went wrong, we can manually send them the link and after we checked in Lime Survey that they completed it, we can manually approve their participation in Prolific and then we write them back.

To export data from Prolific we click *More – Download Export* – It downloads csv file which you should save as prolific_data_be.csv and prolific_data_uk.csv. After that the special script will take both files and will generate uk/berlin_list_session#.txt with all the IDs and emails of responsible participants whom we should contact next time.
