# **Participant comments from surveys 4 - 8**

**Fears and worries related to COVID:**

1. Participants are afraid that the second wave may happen. They assume that economy has been prioritized over health and the governments have removed certain restriction way too soon.
2. At the same time participants are afraid to lose their financial security and/or careers.
3. Media misinformation.
4. Many participants feel bored and are tired of the current lifestyle.
5. Fears about government taking over basic human freedoms. Participants are concerned about how these freedoms will be returned back. Too much power of government over citizens at the moment.
6. Other participants are concerned that not all people have been following the governmental guidance.
7. Impact on society.
8. Restrictions negatively influence interpersonal relationships.
9. Many students are worried about the quality of their education and their performance is dramatically decreased because of remote education according to them.
10. American and other protests (some participants are concerned with the fact that these protests are happening/started to happen during pandemic).
11. Hospitals are neglecting people with COVID unrelated health issues.

**Comments regarding survey:**

1. Grammatical errors.
2. Occasional typos.
3. Some participants are worried that there is too much personal information being asked (example: post code).
4. People do not receive links to Prolific sometimes.
5. Emails from Prolific reach spam folder.
6. Complains about PayPal.
7. Payment issues.