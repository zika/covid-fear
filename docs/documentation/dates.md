## Overview of survey dates



|   | Open date  | Closed date  | Invited UK | Completed/Good UK  | Invited BE | Completed/Good BE | Comment  |  
|---|---|---|---|---|---|---|---|
| Session 1 | 18/4/2020  | 1/5/2020  |  N/A  | 330  | 100  | 96  |  |   
| Session 2 |  4/5/2020 | 11/5/2020  |  313 | 306  | 90  |  89  |  |   
| Session 3 |  18/5/2020 | 20/5/2020  |  306  | 293  | 89  | 85  |  Berlin 2 re-invited for sess4  |   
| Session 4 |  1/6/2020 | 3/6/2020  | 293  |  286 | 87  |  84 |   |   
| Session 5 | 15/6/2020  | 17/6/2020  |  286 |   | 85  |   |  Berlin 1 re-invited  |   
| Session 6 |   |   |   |   |   |   |   |   
| Session 7 |   |   |   |   |   |   |   |   
| Session 8 |   |   |   |   |   |   |   |   
| Session 9 |   |   |   |   |   |   |   |   
| Session 10 |   |   |   |   |   |   |   |   
| Session 11 |   |   |   |   |   |   |   |   
| Session 12 |   |   |   |   |   |   |   |   
| Session 13 |   |   |   |   |   |   |   |   
| Session 14 |   |   |   |   |   |   |   |   
| Session 15 |   |   |   |   |   |   |   |   
