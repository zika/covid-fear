<h1>Table of Contents<span class="tocSkip"></span></h1>
<div class="toc"><ul class="toc-item"></ul></div>


```python
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
```

    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/statsmodels/tools/_testing.py:19: FutureWarning: pandas.util.testing is deprecated. Use the functions in the public API at pandas.testing instead.
      import pandas.util.testing as tm


Analysis over time - state and covid measures


```python
conf = {"sessions": [1,2,3, 4,5],
        "questions": ["q6_me_inf", "q6_close_person_inf", "q6_close_person_died", "q6_media_valence",
"covid_worry", "covid_avoidance_beh", "covid_spec_anxiety", "covid_prob_estimates",  "covid_end_est", "stai_sa","sticsa_sa"]}

conf["questions"] = ["q6_econ_impact_me","q6_econ_impact_closep","q6_work_home","q6_apply_soc_dist", "q6_risk_group", "q6_risk_group_closep","q6_houshold_membs", "q6_media_valence", 
              "q7_worry_infected","q7_worry_die","q7_worry_econ_impact","q7_worry_sthg_bad","q7_worry_insuf_help", "q7_worry_closep_inf", "q7_closep_die","q7_worry_shortage",
              "q7_period_rel_danger","q7_period_rel_safety", "q7_initial_surprise","q7_initial_scared","q7_people_overreact","q7_vir_not_as_dangerous","q7_vir_made_lab",
              "q7_beh_wash_hands", "q7_beh_avoid_ppl", "q7_beh_avoid_public_places",
              "q7_anx_touching_surf","q7_anx_stand_close_to_ppl","q7_anx_eating_food_out","q7_anx_public_transp", "q7_anx_visit_doc", "q7_anx_another_beh",
              "q8_prob_inf_me", "q8_prob_die_me", "q8_prob_econ_imp_me", "q8_prob_inf_closep", "q8_prob_die_closep","q8_prob_inf_avgp",
              "q8_t_pand_end_days","q8_t_life_back_norm_days","q8_t_secondw_when_days","q8_t_econ_back_norm_days"]   


p = pd.read_csv("../../data/session2/clean_dataset_reduced.csv")
sns.palplot(sns.color_palette("Set2"))
for q in conf["questions"]:
    df = pd.DataFrame()
    for sess in conf["sessions"]:
        temp = pd.read_csv("../../data/session"+str(sess)+"/clean_dataset.csv")
        temp["session"] = sess
        df = pd.concat([df, temp.loc[:,temp.columns.intersection(["session", q])]])
    fig =  plt.figure(figsize=(4,7))
    ax = plt.gca()
    #ax = sns.violinplot(x="session", y=q, data=df)
    
    bx = sns.violinplot(x="session", y=q, data=df, palette="Set2")
    df["sess2"] = df["session"]-1
    ax = sns.lineplot(x="sess2", y=q, data=df)


```

    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:22: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).



![png](output_3_1.png)



![png](output_3_2.png)



![png](output_3_3.png)



![png](output_3_4.png)



![png](output_3_5.png)



![png](output_3_6.png)



![png](output_3_7.png)



![png](output_3_8.png)



![png](output_3_9.png)



![png](output_3_10.png)



![png](output_3_11.png)



![png](output_3_12.png)



![png](output_3_13.png)



![png](output_3_14.png)



![png](output_3_15.png)



![png](output_3_16.png)



![png](output_3_17.png)



![png](output_3_18.png)



![png](output_3_19.png)



![png](output_3_20.png)



![png](output_3_21.png)



![png](output_3_22.png)



![png](output_3_23.png)



![png](output_3_24.png)



![png](output_3_25.png)



![png](output_3_26.png)



![png](output_3_27.png)



![png](output_3_28.png)



![png](output_3_29.png)



![png](output_3_30.png)



![png](output_3_31.png)



![png](output_3_32.png)



![png](output_3_33.png)



![png](output_3_34.png)



![png](output_3_35.png)



![png](output_3_36.png)



![png](output_3_37.png)



![png](output_3_38.png)



![png](output_3_39.png)



![png](output_3_40.png)



![png](output_3_41.png)



![png](output_3_42.png)



![png](output_3_43.png)



```python
### Splits by anxiety
indif = pd.read_csv("../../data/session1/clean_dataset.csv")
indif = indif.loc[:,indif.columns.intersection(["PID", "stai_ta", "stai_sa", "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "sticsa_sa", "sticsa_cog_sa", "sticsa_som_sa", "bdi", "cat"
])].set_index(["PID"])

ms_vars = ["stai_ta",  "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "bdi", "cat"]
## Do median split
for msv in ms_vars:
    me = indif[msv].median()
    indif[msv+"_ms"]= 1
    indif[msv+"_ms"][indif[msv]>me] = 2

indif.drop(columns=ms_vars)
    
for q in conf["questions"]:    
    qdf = pd.DataFrame()
    for sess in conf["sessions"]:
  
        temp = pd.read_csv("../../data/session"+str(sess)+"/clean_dataset.csv").set_index(["PROLIFICID"])
        df = pd.DataFrame()
        df = indif
        a = temp.loc[:,temp.columns.intersection([q])]

        df = df.join(a, rsuffix="2") 
        sqdf = pd.DataFrame()
        sqdf = pd.concat([sqdf, df.loc[:,df.columns.intersection([q, "stai_ta_ms",  "sticsa_ta_ms", "sticsa_cog_ta_ms", "sticsa_som_ta_ms", "bdi_ms", "cat_ms"])]])
        sqdf["session"] = sess
        qdf = pd.concat([qdf, sqdf])
    
    toplot = ["sticsa_ta_ms", "sticsa_cog_ta_ms", "sticsa_som_ta_ms"];
    fig =  plt.figure(figsize=(15,5))
    for idx, i in enumerate(toplot):
        plt.subplot(1,3,idx+1)
        ax = plt.gca()
        #ax = sns.violinplot(x="session", y=q, data=df)

        ax = sns.violinplot(x="session", y=q, data=qdf, hue=i, split=True, palette="Set2")
        qdf["sess2"] = qdf["session"]-1
        ax = sns.lineplot(x="sess2", y=q, data=qdf, hue=i, palette="Set2", legend=False)
        new_labels = ['Low', 'High']
        leg=ax.axes.get_legend()
        leg.set_title(i)
        for t, l in zip(leg.texts, new_labels): t.set_text(l)
        #sns.plt.show()
        #for t, l in zip(ax.axes, new_labels): t.set_text(l)
    
        #sns.plt.show()

    
    
        

```

    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:11: SettingWithCopyWarning: 
    A value is trying to be set on a copy of a slice from a DataFrame
    
    See the caveats in the documentation: https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#returning-a-view-versus-a-copy
      # This is added back by InteractiveShellApp.init_path()
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
    /home/ondrej/.conda/envs/python3.7/lib/python3.7/site-packages/ipykernel_launcher.py:31: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).



![png](output_4_1.png)



![png](output_4_2.png)



![png](output_4_3.png)



![png](output_4_4.png)



![png](output_4_5.png)



![png](output_4_6.png)



![png](output_4_7.png)



![png](output_4_8.png)



![png](output_4_9.png)



![png](output_4_10.png)



![png](output_4_11.png)



![png](output_4_12.png)



![png](output_4_13.png)



![png](output_4_14.png)



![png](output_4_15.png)



![png](output_4_16.png)



![png](output_4_17.png)



![png](output_4_18.png)



![png](output_4_19.png)



![png](output_4_20.png)



![png](output_4_21.png)



![png](output_4_22.png)



![png](output_4_23.png)



![png](output_4_24.png)



![png](output_4_25.png)



![png](output_4_26.png)



![png](output_4_27.png)



![png](output_4_28.png)



![png](output_4_29.png)



![png](output_4_30.png)



![png](output_4_31.png)



![png](output_4_32.png)



![png](output_4_33.png)



![png](output_4_34.png)



![png](output_4_35.png)



![png](output_4_36.png)



![png](output_4_37.png)



![png](output_4_38.png)



![png](output_4_39.png)



![png](output_4_40.png)



![png](output_4_41.png)



![png](output_4_42.png)

