## Session 1 overview
### Summary
- 312 UK and ~70 (pending) Berlin participants completed the survey
- next session will open on Monday 4/5/2020, here I summarize the findings and some issues, followed by proposed adjustments
- the delivered questions are here: <a href="session1/survey/main_survey.pdf" target="_new">Session 1: Survey questions</a> <!--_-->

### Descriptive stats & basic correlations
- this document contains the entire output (it is quite lengthy): [Session 1: Descriptives](session1/descript/cov_descriptive_and_basic_analyses)

#### A few observations
- all questionnaires strongly correlate (r>.6)
- most people follow the news on COVID-19 multiple times a day, most media are rated as negative, but a fair bit is seen as delivering positive messages
- a fair proportion of participants at least somewhat believe that the virus was made in a lab (~70), a few don't believe that the virus is not dangerous
- most people engage in avoidance behaviour but this doesn't seem to be strongly related to anxiety/bdi/catastrophizing
- participants find it more likely that a close person, rather then them, will get infected. Furthermore, an average person is believed to be more likely to get infected than the participant.
- there are 22 people who got infected and 19 who had someone close infected
- the expected end of the pandemic (mode) is in September 2020, but it has heavy tail going well into 2023
- economic impact is believed to be rather long term (modes at end of 2020 and 2021, but tail going well into 2020's)
- ~70% of people expect second wave later this year
- most covid-related measures correlate positively with anxiety (both state and trait), bdi and catastrophizing
- state anxiety (r=0.39) and catastrophizing (r=0.41) seems to correlated with covid-related worries somewhat stronger than trait personality measures (0.2-0.3)

### Inferential stats
- full output can be found here: <a href="session1/infer/cov_mle_inferential.html" target="_new">Session 1: Models</a> <!--_-->  


##### A quick note on methods, for each model I:
1. Ran a linear mixed effect model only with main effects and age and gender as random effects (to get basic overview)
2. I used a model selection procedure (stepAIC) to find a model that best fits the data, this included interactions.
3. I ran the selected as a LME model.   

#### Some highlights

#### Covid worry
- state anxiety and general catastrophizing affect covid-related worry
- trait anxiety & bdi seem unrelated
[some model scale issues, i.e. not final]

#### Covid-related anxiety
- catastrophizing (+) and media valence (-) predict covid anxiety
- best model also includes state anxiety as a positive predictor

#### Avoidance behaviours
- (+) predicted by whether a close person got seriously ill or died

#### Covid-related probability estimates
- catastrophizing, state anxiety and bdi **positively** predict estimates
- trait anxiety **negatively** predicts estimates, this is really surprising [both in main-effect-only and best selected models]
- in a follow-up analysis I looked at this s bit more closely:
  - when only TA (trait anx) is include in the model (y~b0 + b1\*TA +e), the coefficient is positive [below LEFT], when **state anxiety, catastrophizing and bdi** are added, the residuals have a negative relationships with trait anxiety  
  <table><tr>
  <td><img alt="ta1" src="session1/img/ta1.jpg"></td>
  <td><img alt="ta2" src="session1/img/ta2.jpg"></td>
  </tr></table>

### Feedback from open question at the end
[These are recurrent ideas contained at least 5+ times in the open comment section]
- worried about relatives, especially older
- annoyed that others don't take it seriously
- not too worried about the disease but the econ impact
- other diseases "on hold" worry, unable to get medication, health services will collapse
- uncertainty is the worst, current and future jobs at stake
- lack of vaccine
- media have massive impact on me
- mental health impact
- several people who actually lost a close person
- lock down "calmed me down", I was really scared before
- UK government criticism, anti-Conservative sentiment
- in Germany, most people seem happy with the government's response  
- positive effect on environment
- I find everything outside of the house very stressful and I am not an anxious person - people prefer stores closed
- in UK still high fear, in Germany the sentiment is more relaxed
- the Berlin crowd is clearly different from the UK - often mentions refugees, ethical issues, environmental benefits and boredom [some answers were probably written by philosophy students], in UK people are much more worried for their families and lives.

## Identified issues and proposed changes
### Issues
- we should control for any general tendencies in probability estimates, especially if they are related to trait anxiety
- temporal discounting should be assessed (we might be biasing the sample)
- "age" was not among the risk factors in our survey (I took those from NHS website), the NHS has meanwhile created 2 levels of risk group, one very high and one high, age (70+) is now included  
- we should check for personal experience outside of covid (some people were in distress due to other factors such as divorce)

### Proposed changes for session 2
- add a question to personality measures about the time period they are answering in the given section [i.e. make sure they don't mix state and trait]
- ask about other, non-COVID, stressors
- change email on consent form
- add question about bilingualism 

### Proposed additions for later sessions
- we will assess individual tendencies in probability estimates
- we will ask them to do a short temporal discounting task
