### Overview
- 300 people on-line will complete 15 questionnaires over the next ~10 months
- 100 local participants will complete:
  1. 15 questionnaires over the next ~10 months  
  2. Two in-house sessions during which they perform the aversive learning task (this can't be done on-line exactly because it involves electrical shocks), one session in 'safe' period over summer and one during the virus comeback.
- questionnaire sessions are initially spread 15 days apart until mid-June, then 30 days apart until first signs of comeback occur, and then 15 days apart until ~end of January
- questions try to mimic the lab experiments, asking for *objective* predictions, additionally, we also ask for behaviours and fears
- payment will amount to $60 pp overall, bulk of the amount will be paid out over 4-6 incremental installments (tbd)


### Motivation
In a series of laboratory studies investigating the role of trait anxiety in aversive learning we found that contingency changes were better tracked by high trait anxiety group. Behavioral and physiological (Skin Conductance Response, henceforth: SCR) data revealed that high trait anxious individuals had higher awareness of the current hidden state of the environment, especially in the later stages of the experiment (Fig 1). This was indexed by more distinct expectancy ratings and anticipatory SCR responses between periods of objectively low and high threat. Furthermore, we found that following changes in the underlying shock contingency, high trait anxious individuals were faster at adjusting their expectation to the new reinforcement level, indexed by switch steepness. Computational modeling revealed that there are generally two groups of participants: those who update their expectations gradually in a trial-by-trial manner, and those that learn that there are two contingency states and switch between them in abrupt manner. Trait anxiety was associated with tendency to employ the state switching, rather than gradual learning strategy.

*Fig. 1: Data show trial-by-trial adjustment of shock expectancy after a switch from high to low (blue) and low to high (red) probability of shock. Lighter colors show high trait anxious group.*
![behavioural_data](behavioural_data.png)

The above findings may potentially explain why we see higher rates of fear relapse in highly anxious individuals. Our group is currently conducting a study investigating whether state switching and high trait anxiety also lead to higher rates of relapse on a session one week later.

The ongoing global crisis related to the COVID-19 pandemic resembles our experimental design in a number of ways: There is an initial fear period which will be followed by a phase of fear extinction and most likely (see below) by a period of fear reinstatement. We therefore see the situation as a unique opportunity to investigate fear learning in a natural setting.

### Hypotheses

#### Core
1. High trait anxiety participants will recognize a period of safety faster and adjust their *objective* aversive event expectations sooner, their fear and avoidance behaviours will remain high.
2. High  trait anxiety will be associated with faster threat probability (based on our lab results) and fear (based on literature) reinstatement once signs of the second wave of the pandemic occur.

#### Exploratory
3. There will be a dissociation between affective and objective assessment of the situation that will be particularly pronounced in highly anxious individuals.
4. Task performance will be modulated by covid-induced state anxiety.   


*Fig. 2: Simulated time-course of threat probability estimates for high and low trait anxiety groups. Blue rectangles highlight periods where theoretical predictions for state-switchers and gradual learners diverge.*    
![fear](fear.png)

In Fig. 2 we show the main predictions based on our lab results. The predictions concern specifically threat-related probability judgments, not subjective fear.


### Design
We propose a longitudinal study lasting 10-12 months in which an initial sample of 400 English speakers in the UK (300) and Germany (100) will complete 15 on-line questionnaire sessions. Additionally, subjects in Germany will be invited for two in-lab sessions during which they will be asked to perform the aversive learning task used in previous experiments. During the on-line sessions we will collect questionnaire responses assessing personality traits (trait anxiety, catastrophizing, etc.) current level of anxiety, stress, covid-related fears and behaviours and event probability estimates. Participants will be strongly incentivised to complete the entire study.    

#### Materials
##### Demographic measures (DMs)
- age
- gender
- sort code of current location

##### Personality measures (PMs)
1. STAI-TRAIT  
2. STICSA-TRAIT
3. BDI-II
4. General Catastrophizing (new questionnaire by Pike et al.)

##### Current state measures (CSMs)
1. STAI-STATE
2. STICSA-STATE
4. Factual-COVID
5. Affective-COVID
6. Probability estimates


#### Session types
**1. First+Full Session (LONG)**
 - DMs, PMs and CSMs
 - 20-25 min **

**2. Check-in session (SHORT)**
   - only  DMs and CSMs
   - 10-15 min **

** all timings are rough estimates

#### Timing
There is a plan to conduct 15 sessions in total, 3 of which will be first/full sessions while the other 12 will be the shorter check-in sessions. The temporal sampling will be uneven, reflecting the expected progress of the global pandemic. This is to ensure that the critical periods (e.g. resurgence in number of cases expected in Nov/Dec 2020) are sampled with higher frequency. Crucially, the increased sampling frequency will start as soon as the news start reporting a COVID-19 comeback.    

*Fig. 3: Predicted health care demands, UK, next 12 months (based on Anderson et al. 2020, The Lancet)*  
![Sampling](sampling.png)
#### Payment structure

##### Scheme 1
- Completion of the entire study earns $60 for an overall of 225 min (3 hrs and 45 min), that is $16/hr.
- They will be paid $1 after each session plus a bonus on session 3 ($5), session 6 ($10), session 9 ($5), session 12 ($5) and 15 ($20).
- the 100 people that come to lab will be paid $20-25 per visit (~$45)
- if all 400 completed, then the cost would be (300 * $60)\*1.33 [prolific fee] + (100 * $105) = $34 440 [but I still expect 25-40% attrition, so \*0.75 = $25 830, **Nico** let me know if you want me to push this down a bit]

[\*] To retain participants in the study, we would like to only pay them on visit 2/3 for the first time. [Here](https://researcher-help.prolific.co/hc/en-gb/articles/360009223033-Delaying-payments-for-a-long-longitudinal-study) is an overview of Prolific rules about longitudinal payments. In short, the delay in payment can only be 21 days max. Participant IDs must be recorded for next wave, and they need to be manually added to whitelist. It is also possible to include only subjects with at least 10 previous submissions and 95% approval rate.
From Prolific website: *We tend to see very low attrition (drop-out) rates in longitudinal studies if the requirements of the full study are made clear up front, and the time between follow-up stages is not too long.*

##### Backup scheme
Participants will be paid at a rate of $9/hr. On sessions 3, 7 and 11 they will be paid a $5 interim bonus and on the last session an $10 overall bonus. A participant that completes all 15 sessions will earn $43. An attrition rate is expected. Approximately 20% of the participants are expected to complete the study (attrition shown on figure below), so the estimated cost for the on-line sample is $18 500. Adding 100 in-person recruited participants extends the cost by further $8 800, bringing the total to $27 205. Again, this could be lowered.


*Fig. 4: Payment structure of the experiment*  
![Payment](payment.png)



#### Questions
This section contains all questionnaires newly designed for this study.

[Questionnaires are in this google-doc](https://docs.google.com/document/d/1J7cZjyEO8Qx71jTSu84RwjIU7sSIqvTEKv5cio9EuE0/edit#heading=h.p63fqgnl9nb6) (Claire, request access when you want to have a look)


#### Procedure

#### Participants
##### Inclusion criteria [leave ~45k potential participants on Prolific]
- 18 - 40 y.o.
- UK resident [for online]
- fluent in English
- currently not taking any medication prescribed for mental health condition [**Prolific** doesn't have this in their screening, do we need it? I can add it to our own screening.]
- no current mental health condition influencing everyday life
- approval rate on Prolific at least 95%

### Data
1. Questionnaires
2. Task performance: trial-by-trial ratings, pupil dilation
3. Demographic indicators, location
4. Objective measures: number of cases, news articles content



### Outstanding decisions and issues
1. Finalize questions [here](https://docs.google.com/document/d/1J7cZjyEO8Qx71jTSu84RwjIU7sSIqvTEKv5cio9EuE0/edit#heading=h.p63fqgnl9nb6)
2. Decide how to best monitor the 'objective' news. (there are a few sources that do this, plus we could just scrape main news outlets)
3. Randomize order of **whole questions**, or **individual items** or **both**.
4. Question about expecting return of the pandemic - should it be only included in some, I am worried about biasing the participants.
5. Most of the Oxford sample can be re-contacted once I get hold of their consent forms [it's tricky because they are locked in Katja's desk, she is in Leipzig and there is no one at work]. The task involves primary punishment (shocks) which can't be done on-line, so we should decide whether to do the task with monetary losses or drop that route.





---

### Other
#### COVID-19 questionnaire by Anet, Pike and Robinson
1. How worried are you about the novel coronavirus (COVID-19) outbreak?
2. How likely do you think you are to catch the virus?
3. How do you think your health will be a!ected if you do catch the virus?
4. How likely do you think it is that a loved one will catch the virus?
5. How do you think a loved one’s health will be a!ected if they do catch thevirus?
6. Are you in the risk group (according to your country)?
7. Are any of your loved ones in the risk group (according to your country)?
8. How do you think you will be a!ected by the global e!ects of the virus (forexample economic recession, reduced health capacity)?  
  *Please answer questions 9 to 16 according to your thoughts and behaviours in the __last week__.*
9. How much is the following statement true: "I wash my hands more often andlonger than necessary".
10. How much have you been social distancing?
11. How would you rate your social-distancing experience?
12. How many times in a day do you think about the outcomes of the coronavirus outbreak?
13. How many months do you think the measurements taken by yourgovernment (e.g. lockdown, social distancing) will last for?
14. What is your current employment status?
15. How many people live in your house at the present moment (including you)?
16. Do you have any "u-like symptoms?
