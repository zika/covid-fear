---
title: ""
author: "Ondrej Zika"
date: "7/12/2022"
output: html_document
---


```r
here::i_am(".root_dir_covid")
```

```
## here() starts at /data/drive/postdoc/Project4_covid
```

```r
here::here()
```

```
## [1] "/data/drive/postdoc/Project4_covid"
```

```r
renv::load(here::here("covid-fear"))
```

```
## - Project '/data/drive/postdoc/Project4_covid/covid-fear' loaded. [renv 1.0.3]
```

```r
required_packages = c("lmerTest",  "emmeans", "ggplot2", "parameters", "reshape2", "PupillometryR",  "plyr", "tidyr", "ggpubr", "corrplot", "patchwork", "broom", "plotrix", "PupillometryR","Hmisc", "performance", "see", "gtools", "car", "ggpubr" , "RColorBrewer", "effectsize", "lmtest", "patchwork", "datawizard")
invisible(lapply(required_packages, require, character.only = TRUE))

#detach("package:Matrix", unload = TRUE)

source(here::here('covid-fear', 'lib', 'shared', 'r', 'r_stone_lib.R'))

#factors <- c("F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism")
factors <- c("F1_CovidAnxietyWorry", "F2_CovidProbabilityEsts", "F3_Economic")
# will be z-scored
add_vars <- c( "q6_apply_soc_dist", "q6_risk_group" , "q6_media_freq_num",  "q6_media_valence",  "prob_est", "avoid_beh", "q6_houshold_membs", "postcode_density", "q6_apply_soc_dist", "q6_work_home", "q6_risk_group", "sr_age", "stai_sa", "skepticism")
# won't be zscored
add_vars2 <- c( "q7_period_rel_danger",  "GROUP", "deaths", "cases")

trait_factors <- c("TF1_CognAnxDepr", "TF2_PhysiolAnx", "TF3_NegativeAffect")

df <- read.csv(here::here("data","full_dataset_only_complete_based_on_sess_avg.csv"))

# filter for weird ages 
df <- df[df$sr_age>18 & df$sr_age<42,]

df$deaths <- df$deaths14_norm_unsmooth
df$cases <- (df$cases14_norm_unsmooth)

tdf <- df[,c("PROLIFICID", "session", factors, trait_factors, add_vars, add_vars2)]

## zscore predictors
for (v in c(trait_factors, factors,add_vars, "q6_media_freq_num",  "q6_media_valence")) {
  #tdf[v] <- scale(tdf[v])
  tdf[v] <- datawizard::standardize(tdf[v])
  
}

tdf$sess_str <- paste0("sess", tdf$session)

## zscore severity by session
for (v in c("deaths", "cases")) {
  for (s in unique(tdf$sess_str)) {
    tdf[tdf$sess_str %in% s,v] <- datawizard::standardize(tdf[tdf$sess_str %in% s,v])
  }
}

tdf<- tdf %>% 
  mutate(
    wave = case_when(
        session < 6 ~ "first_wave",   
        session >= 6 & session < 14 ~ "summer_nowave", 
        session >= 14 ~ "second_wave"
    )
  )
tdf$wave <- ordered(tdf$wave, levels = c("first_wave", "summer_nowave", "second_wave"))
tdf$sess_str <- ordered(tdf$sess_str, levels = c("sess1","sess2","sess3","sess4","sess5","sess6","sess7","sess8","sess9","sess10","sess11","sess12","sess13","sess14","sess15","sess16","sess17","sess18","sess19","sess20"))

tdf <- tdf %>% dplyr::rename(TF1 = TF1_CognAnxDepr,
                      TF2 = TF2_PhysiolAnx, 
                      TF3 = TF3_NegativeAffect, 
                      AvoidBeh = avoid_beh,
                      MediaVal = q6_media_valence,
                      InforSeek = q6_media_freq_num, 
                      ProbEst = prob_est,
                      CurrThreat = q7_period_rel_danger,
                      Cases = cases,
                      Deaths = deaths, 
                      HouseholdMembers = q6_houshold_membs, 
                      PopulationDensity = postcode_density, 
                      Age = sr_age,
                      SocialDist = q6_apply_soc_dist,
                      WorkHome= q6_work_home, 
                      RiskGr = q6_risk_group     )



tdf$Deaths_q <- gtools::quantcut(tdf$Deaths, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$Cases_q <- gtools::quantcut(tdf$Cases, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF1_3 <- gtools::quantcut(tdf$TF1, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF2_3 <- gtools::quantcut(tdf$TF2, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF3_3 <- gtools::quantcut(tdf$TF3, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF1_2 <- gtools::quantcut(tdf$TF1, q = c(0, 0.5, 1), 
                                 labels = c("low","high"))
tdf$TF2_2 <- gtools::quantcut(tdf$TF2, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))
tdf$TF3_2 <- gtools::quantcut(tdf$TF3, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))

tdf$InforSeek_3 <- gtools::quantcut(tdf$InforSeek, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$InforSeek_2 <- gtools::quantcut(tdf$InforSeek, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))

vars <- c("CurrThreat", "ProbEst", "InforSeek", "AvoidBeh")

tdf <- tdf %>% drop_na(CurrThreat, ProbEst, InforSeek, AvoidBeh, Cases, Deaths, TF1, TF2, TF3)
```

### Wave 1 

```r
df <- read.csv(here::here("output", "prepost", "first_wave_full_prepost_data.csv"))
vars <- c("ThreatPerc", "RiskEsts", "InforSeek", "AvoidBeh")
eq = "~ prepost_Cases*TF1 + prepost_Cases*TF2 + prepost_Cases*TF3"
#eq = "~ prepost_Deaths*TF1 + prepost_Deaths*TF2 + prepost_Deaths*TF3"
eq = "~ prepost_Cases*TF1 + prepost_Cases*TF2 + prepost_Cases*TF3 + prepost_Deaths*TF1 + prepost_Deaths*TF2 + prepost_Deaths*TF3"
for (v in vars) {
    print(v)
    m<- lm(data = df, paste0("prepost_", v, eq) )
    print(summary(m))
    print(anova(m))
}
```

```
## [1] "ThreatPerc"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -3.8898 -1.3267  0.0713  1.2828  4.8895 
## 
## Coefficients:
##                    Estimate Std. Error t value Pr(>|t|)    
## (Intercept)        -1.10594    0.33065  -3.345 0.000938 ***
## prepost_Cases       0.25471    0.52207   0.488 0.626028    
## TF1                 0.59314    0.55806   1.063 0.288775    
## TF2                 0.56890    0.40209   1.415 0.158237    
## TF3                -1.14356    0.50136  -2.281 0.023317 *  
## prepost_Deaths      0.13063    0.12927   1.011 0.313117    
## prepost_Cases:TF1  -0.39327    0.89600  -0.439 0.661064    
## prepost_Cases:TF2   0.89203    0.70303   1.269 0.205566    
## prepost_Cases:TF3   0.04553    0.78008   0.058 0.953501    
## TF1:prepost_Deaths  0.27073    0.21878   1.237 0.216970    
## TF2:prepost_Deaths  0.11355    0.15809   0.718 0.473215    
## TF3:prepost_Deaths -0.46696    0.18680  -2.500 0.013010 *  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.758 on 276 degrees of freedom
##   (12 observations deleted due to missingness)
## Multiple R-squared:  0.05631,	Adjusted R-squared:  0.0187 
## F-statistic: 1.497 on 11 and 276 DF,  p-value: 0.1319
## 
## Analysis of Variance Table
## 
## Response: prepost_ThreatPerc
##                     Df Sum Sq Mean Sq F value  Pr(>F)  
## prepost_Cases        1   6.90  6.8953  2.2323 0.13630  
## TF1                  1   0.53  0.5268  0.1706 0.67994  
## TF2                  1   1.11  1.1073  0.3585 0.54984  
## TF3                  1   0.91  0.9147  0.2961 0.58677  
## prepost_Deaths       1   5.10  5.0972  1.6502 0.20001  
## prepost_Cases:TF1    1   0.04  0.0398  0.0129 0.90966  
## prepost_Cases:TF2    1  11.60 11.6015  3.7558 0.05364 .
## prepost_Cases:TF3    1   1.67  1.6739  0.5419 0.46227  
## TF1:prepost_Deaths   1   0.01  0.0082  0.0027 0.95887  
## TF2:prepost_Deaths   1   3.70  3.7048  1.1994 0.27440  
## TF3:prepost_Deaths   1  19.30 19.3014  6.2486 0.01301 *
## Residuals          276 852.54  3.0889                  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## [1] "RiskEsts"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -43.979  -8.508   1.171   9.454  51.134 
## 
## Coefficients:
##                    Estimate Std. Error t value Pr(>|t|)   
## (Intercept)         -8.5640     2.8975  -2.956  0.00339 **
## prepost_Cases        5.6222     4.5750   1.229  0.22016   
## TF1                  3.0203     4.8904   0.618  0.53735   
## TF2                  0.9891     3.5236   0.281  0.77916   
## TF3                 -3.1659     4.3935  -0.721  0.47178   
## prepost_Deaths       0.1356     1.1328   0.120  0.90482   
## prepost_Cases:TF1    5.7947     7.8518   0.738  0.46114   
## prepost_Cases:TF2    3.7366     6.1608   0.607  0.54467   
## prepost_Cases:TF3   -5.2552     6.8360  -0.769  0.44269   
## TF1:prepost_Deaths  -0.1206     1.9172  -0.063  0.94989   
## TF2:prepost_Deaths   0.3106     1.3854   0.224  0.82279   
## TF3:prepost_Deaths  -0.4586     1.6370  -0.280  0.77958   
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 15.4 on 276 degrees of freedom
##   (12 observations deleted due to missingness)
## Multiple R-squared:  0.02057,	Adjusted R-squared:  -0.01847 
## F-statistic: 0.5269 on 11 and 276 DF,  p-value: 0.8845
## 
## Analysis of Variance Table
## 
## Response: prepost_RiskEsts
##                     Df Sum Sq Mean Sq F value Pr(>F)
## prepost_Cases        1    354  354.32  1.4937 0.2227
## TF1                  1    143  142.59  0.6011 0.4388
## TF2                  1     53   53.17  0.2242 0.6363
## TF3                  1     44   44.29  0.1867 0.6660
## prepost_Deaths       1     29   28.98  0.1222 0.7269
## prepost_Cases:TF1    1    246  246.24  1.0381 0.3092
## prepost_Cases:TF2    1    262  262.02  1.1046 0.2942
## prepost_Cases:TF3    1    195  195.23  0.8230 0.3651
## TF1:prepost_Deaths   1     12   12.35  0.0521 0.8197
## TF2:prepost_Deaths   1     17   17.03  0.0718 0.7889
## TF3:prepost_Deaths   1     19   18.62  0.0785 0.7796
## Residuals          276  65470  237.21               
## [1] "InforSeek"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -3.5611 -0.7685  0.2168  0.9579  2.5939 
## 
## Coefficients:
##                    Estimate Std. Error t value Pr(>|t|)    
## (Intercept)        -1.51059    0.21404  -7.058 1.37e-11 ***
## prepost_Cases       0.19837    0.33795   0.587   0.5577    
## TF1                 0.12971    0.36125   0.359   0.7198    
## TF2                -0.09639    0.26028  -0.370   0.7114    
## TF3                -0.19571    0.32455  -0.603   0.5470    
## prepost_Deaths     -0.15412    0.08368  -1.842   0.0666 .  
## prepost_Cases:TF1   0.59817    0.58001   1.031   0.3033    
## prepost_Cases:TF2  -0.24222    0.45509  -0.532   0.5950    
## prepost_Cases:TF3  -0.70198    0.50497  -1.390   0.1656    
## TF1:prepost_Deaths -0.13948    0.14162  -0.985   0.3256    
## TF2:prepost_Deaths  0.04300    0.10234   0.420   0.6747    
## TF3:prepost_Deaths  0.09937    0.12092   0.822   0.4119    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.138 on 276 degrees of freedom
##   (12 observations deleted due to missingness)
## Multiple R-squared:  0.03685,	Adjusted R-squared:  -0.001536 
## F-statistic:  0.96 on 11 and 276 DF,  p-value: 0.4834
## 
## Analysis of Variance Table
## 
## Response: prepost_InforSeek
##                     Df Sum Sq Mean Sq F value Pr(>F)
## prepost_Cases        1   0.54  0.5408  0.4178 0.5186
## TF1                  1   1.75  1.7453  1.3484 0.2466
## TF2                  1   1.36  1.3632  1.0532 0.3057
## TF3                  1   3.51  3.5148  2.7155 0.1005
## prepost_Deaths       1   3.20  3.1995  2.4719 0.1170
## prepost_Cases:TF1    1   0.02  0.0201  0.0155 0.9009
## prepost_Cases:TF2    1   0.00  0.0018  0.0014 0.9701
## prepost_Cases:TF3    1   1.97  1.9656  1.5185 0.2189
## TF1:prepost_Deaths   1   0.32  0.3240  0.2503 0.6172
## TF2:prepost_Deaths   1   0.12  0.1191  0.0920 0.7618
## TF3:prepost_Deaths   1   0.87  0.8741  0.6753 0.4119
## Residuals          276 357.24  1.2944               
## [1] "AvoidBeh"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -4.3196 -0.6460  0.0389  0.7668  4.4229 
## 
## Coefficients:
##                    Estimate Std. Error t value Pr(>|t|)    
## (Intercept)        -1.06532    0.23701  -4.495 1.03e-05 ***
## prepost_Cases       0.11401    0.37423   0.305   0.7609    
## TF1                 0.66251    0.40003   1.656   0.0988 .  
## TF2                -0.64652    0.28823  -2.243   0.0257 *  
## TF3                -0.11642    0.35939  -0.324   0.7462    
## prepost_Deaths     -0.11598    0.09266  -1.252   0.2118    
## prepost_Cases:TF1  -0.21718    0.64227  -0.338   0.7355    
## prepost_Cases:TF2   0.48120    0.50394   0.955   0.3405    
## prepost_Cases:TF3  -0.01400    0.55918  -0.025   0.9800    
## TF1:prepost_Deaths  0.28664    0.15682   1.828   0.0687 .  
## TF2:prepost_Deaths -0.29378    0.11332  -2.592   0.0100 *  
## TF3:prepost_Deaths -0.13662    0.13390  -1.020   0.3085    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.26 on 276 degrees of freedom
##   (12 observations deleted due to missingness)
## Multiple R-squared:  0.08114,	Adjusted R-squared:  0.04452 
## F-statistic: 2.216 on 11 and 276 DF,  p-value: 0.01391
## 
## Analysis of Variance Table
## 
## Response: prepost_AvoidBeh
##                     Df Sum Sq Mean Sq F value  Pr(>F)  
## prepost_Cases        1   0.15  0.1473  0.0928 0.76086  
## TF1                  1  10.32 10.3226  6.5037 0.01131 *
## TF2                  1   3.10  3.0957  1.9504 0.16366  
## TF3                  1   9.98  9.9785  6.2869 0.01274 *
## prepost_Deaths       1   2.63  2.6307  1.6575 0.19903  
## prepost_Cases:TF1    1   1.03  1.0311  0.6497 0.42093  
## prepost_Cases:TF2    1   0.03  0.0340  0.0214 0.88368  
## prepost_Cases:TF3    1   0.10  0.1019  0.0642 0.80020  
## TF1:prepost_Deaths   1   0.01  0.0067  0.0042 0.94842  
## TF2:prepost_Deaths   1   9.68  9.6820  6.1001 0.01412 *
## TF3:prepost_Deaths   1   1.65  1.6521  1.0409 0.30850  
## Residuals          276 438.06  1.5872                  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

### Wave 2 

```r
df <- read.csv(here::here("output", "prepost", "second_wave_full_prepost_data.csv"))
vars <- c("ThreatPerc", "RiskEsts", "InforSeek", "AvoidBeh")
eq = "~ prepost_Cases*TF1 + prepost_Cases*TF2 + prepost_Cases*TF3 + prepost_Deaths*TF1 + prepost_Deaths*TF2 + prepost_Deaths*TF3"
eq = "~ prepost_Deaths*TF1 + prepost_Deaths*TF2 + prepost_Deaths*TF3"
for (v in vars) {
    print(v)
    m<- lm(data = df, paste0("prepost_", v, eq) )
    print(summary(m))
    print(anova(m))
}
```

```
## [1] "ThreatPerc"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -4.1523 -0.8617 -0.1408  0.7348  4.8002 
## 
## Coefficients:
##                     Estimate Std. Error t value Pr(>|t|)    
## (Intercept)         1.381430   0.147321   9.377  < 2e-16 ***
## prepost_Deaths     -0.181531   0.107877  -1.683  0.09359 .  
## TF1                 0.353922   0.259431   1.364  0.17364    
## TF2                -0.001681   0.180480  -0.009  0.99257    
## TF3                -0.657489   0.227609  -2.889  0.00418 ** 
## prepost_Deaths:TF1 -0.178831   0.181348  -0.986  0.32497    
## prepost_Deaths:TF2 -0.040524   0.126822  -0.320  0.74957    
## prepost_Deaths:TF3  0.386722   0.162136   2.385  0.01777 *  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.242 on 268 degrees of freedom
##   (24 observations deleted due to missingness)
## Multiple R-squared:  0.04425,	Adjusted R-squared:  0.01928 
## F-statistic: 1.772 on 7 and 268 DF,  p-value: 0.0929
## 
## Analysis of Variance Table
## 
## Response: prepost_ThreatPerc
##                     Df Sum Sq Mean Sq F value  Pr(>F)  
## prepost_Deaths       1   3.74  3.7408  2.4261 0.12051  
## TF1                  1   0.87  0.8743  0.5670 0.45210  
## TF2                  1   0.04  0.0423  0.0275 0.86853  
## TF3                  1   4.35  4.3550  2.8244 0.09401 .
## prepost_Deaths:TF1   1   0.58  0.5817  0.3772 0.53961  
## prepost_Deaths:TF2   1   0.77  0.7651  0.4962 0.48180  
## prepost_Deaths:TF3   1   8.77  8.7722  5.6890 0.01777 *
## Residuals          268 413.24  1.5419                  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## [1] "RiskEsts"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -25.510  -4.910  -1.195   3.759  34.902 
## 
## Coefficients:
##                    Estimate Std. Error t value Pr(>|t|)    
## (Intercept)          3.5513     0.9654   3.679 0.000283 ***
## prepost_Deaths      -0.3125     0.7069  -0.442 0.658804    
## TF1                  3.1744     1.7000   1.867 0.062950 .  
## TF2                  0.5995     1.1826   0.507 0.612624    
## TF3                 -3.6552     1.4915  -2.451 0.014896 *  
## prepost_Deaths:TF1  -1.3659     1.1883  -1.149 0.251408    
## prepost_Deaths:TF2  -0.2988     0.8310  -0.360 0.719451    
## prepost_Deaths:TF3   1.7730     1.0624   1.669 0.096317 .  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 8.137 on 268 degrees of freedom
##   (24 observations deleted due to missingness)
## Multiple R-squared:  0.03095,	Adjusted R-squared:  0.005642 
## F-statistic: 1.223 on 7 and 268 DF,  p-value: 0.2902
## 
## Analysis of Variance Table
## 
## Response: prepost_RiskEsts
##                     Df  Sum Sq Mean Sq F value  Pr(>F)  
## prepost_Deaths       1    10.8  10.848  0.1638 0.68596  
## TF1                  1    48.2  48.227  0.7284 0.39416  
## TF2                  1    39.9  39.934  0.6031 0.43806  
## TF3                  1   244.7 244.661  3.6953 0.05563 .
## prepost_Deaths:TF1   1    12.4  12.392  0.1872 0.66563  
## prepost_Deaths:TF2   1    26.3  26.326  0.3976 0.52886  
## prepost_Deaths:TF3   1   184.4 184.394  2.7850 0.09632 .
## Residuals          268 17744.1  66.209                  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## [1] "InforSeek"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -3.3685 -0.4689 -0.0719  0.5251  2.5908 
## 
## Coefficients:
##                     Estimate Std. Error t value Pr(>|t|)    
## (Intercept)         0.492877   0.099634   4.947 1.33e-06 ***
## prepost_Deaths     -0.040313   0.072957  -0.553   0.5810    
## TF1                 0.006140   0.175454   0.035   0.9721    
## TF2                -0.121157   0.122059  -0.993   0.3218    
## TF3                 0.041196   0.153932   0.268   0.7892    
## prepost_Deaths:TF1 -0.025766   0.122646  -0.210   0.8338    
## prepost_Deaths:TF2  0.158390   0.085770   1.847   0.0659 .  
## prepost_Deaths:TF3  0.004203   0.109653   0.038   0.9695    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.8398 on 268 degrees of freedom
##   (24 observations deleted due to missingness)
## Multiple R-squared:  0.02512,	Adjusted R-squared:  -0.0003459 
## F-statistic: 0.9864 on 7 and 268 DF,  p-value: 0.4414
## 
## Analysis of Variance Table
## 
## Response: prepost_InforSeek
##                     Df  Sum Sq Mean Sq F value  Pr(>F)  
## prepost_Deaths       1   0.089 0.08881  0.1259 0.72297  
## TF1                  1   0.816 0.81608  1.1571 0.28303  
## TF2                  1   0.730 0.72954  1.0344 0.31004  
## TF3                  1   0.120 0.12039  0.1707 0.67981  
## prepost_Deaths:TF1   1   0.664 0.66360  0.9409 0.33292  
## prepost_Deaths:TF2   1   2.450 2.45030  3.4743 0.06342 .
## prepost_Deaths:TF3   1   0.001 0.00104  0.0015 0.96945  
## Residuals          268 189.010 0.70526                  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## [1] "AvoidBeh"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -4.1948 -0.5093 -0.0550  0.5540  3.0509 
## 
## Coefficients:
##                    Estimate Std. Error t value Pr(>|t|)    
## (Intercept)         0.39955    0.11826   3.378 0.000837 ***
## prepost_Deaths     -0.05652    0.08660  -0.653 0.514535    
## TF1                 0.26606    0.20826   1.278 0.202518    
## TF2                -0.06669    0.14488  -0.460 0.645652    
## TF3                -0.39265    0.18271  -2.149 0.032533 *  
## prepost_Deaths:TF1 -0.06736    0.14558  -0.463 0.643975    
## prepost_Deaths:TF2  0.08034    0.10181   0.789 0.430702    
## prepost_Deaths:TF3  0.09932    0.13016   0.763 0.446087    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.9968 on 268 degrees of freedom
##   (24 observations deleted due to missingness)
## Multiple R-squared:  0.04344,	Adjusted R-squared:  0.01845 
## F-statistic: 1.739 on 7 and 268 DF,  p-value: 0.1002
## 
## Analysis of Variance Table
## 
## Response: prepost_AvoidBeh
##                     Df  Sum Sq Mean Sq F value   Pr(>F)   
## prepost_Deaths       1   0.490  0.4904  0.4936 0.482957   
## TF1                  1   0.020  0.0197  0.0198 0.888198   
## TF2                  1   1.281  1.2806  1.2888 0.257289   
## TF3                  1   8.946  8.9461  9.0032 0.002949 **
## prepost_Deaths:TF1   1   0.322  0.3216  0.3237 0.569872   
## prepost_Deaths:TF2   1   0.456  0.4559  0.4588 0.498770   
## prepost_Deaths:TF3   1   0.579  0.5786  0.5823 0.446087   
## Residuals          268 266.301  0.9937                    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```
