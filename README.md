# Internal documentation for the project

https://zika.mpib.berlin/covid-fear/documentation/#/

# Project proposal for COVID & fear study
https://zika.mpib.berlin/covid-fear/shared/#/proposal

# Analysis summaries
- [Session 1](https://zika.mpib.berlin/covid-fear/shared/#/session1)
- [Session 5 - temporal](https://zika.mpib.berlin/covid-fear/shared/#/session5/temporal/cov_temporal_analyses)
- [Session 8 - temporal](https://zika.mpib.berlin/covid-fear/shared/#/session8/temporal/cov_temporal_analyses)
- [Session 12 - temporal](https://zika.mpib.berlin/covid-fear/shared/#/session12/temporal/cov_temporal_analyses)



