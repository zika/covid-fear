import sys
from groo.groo import get_root
root_dir = get_root(".root_dir_covid")
import os
os.chdir(root_dir)
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))
import numpy as np
import pandas as pd
from cov_functions import *
from scipy import stats
import warnings
warnings.filterwarnings('ignore')
import bambi as bmb
import json
import pickle

import bammm.bammm as mm


# data import
df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete.csv"))
df = df.loc[df["session"].isin([15,16,17,18,19]),:]

model_constant = "severity_noslope_q4"
folder = "severity_timewindow"

# hyperparameters
corr_type = "spearman"
factors =[ "F1_Close_Person_Worry", "F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism"]
trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing","TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"]

from scipy.stats import zscore
for tr in trait_factor_names+["state_severity"]:
    df.loc[:,tr] = zscore(df.loc[:,tr], nan_policy='omit')




for f_idx, f in enumerate(factors):
    dfl = df.loc[:,[f, "state_severity", "PROLIFICID", "session"]+trait_factor_names].dropna()
    #models = json.load(open(os.path.join(root_dir, "output", "models", "model_database.json"), "r"))
    models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"), "r"))
    if "F"+str(f_idx+1)+"_"+model_constant+"_4_3000" in models.keys():
        mod = models["F"+str(f_idx+1)+"_"+model_constant+"_4_3000"]
    else:
        mod = mm.get_template()
    mod["type"] = "lmm"
    mod["lmm"]["dep_var"] = f
    mod["lmm"]["fxeff"] = ["state_severity"]+trait_factor_names+['state_severity*'+i for i in trait_factor_names]+["session"]
    mod["lmm"]["rneff"] = ["session|PROLIFICID"]
    mod["est"]["nchains"] = 4
    mod["est"]["nsamples"] = 3000
    mod["est"]["ncores"] = 4
    mod["name"] = "F"+str(f_idx+1)+"_"+model_constant+"_"+str(mod["est"]["nchains"])+"_"+str(mod["est"]["nsamples"])
    mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"])
    mod["location"] = os.path.join("output", "models", "model_data", folder, mod["name"]+".dic" )
    mod["current_sys_location"] = os.path.join(root_dir ,mod["location"])
    models[mod["name"]] = mod
    mm.save_model_info(models, os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"))

    mod, res, m = mm.estimate_lmm(mod=mod, data=dfl, override=1)
    
    mm.update_model_entry(models, mod, os.path.join(root_dir, "covid-fear", "model_dbs", "model_database.json"))