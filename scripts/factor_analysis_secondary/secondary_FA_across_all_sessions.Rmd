---
title: "fa trait"
author: "Ondrej Zika"
date: "5/30/2022"
output:
  html_document: default
  pdf_document: default
---
```{r}
# install core package venv and let it handle the rest
if (!requireNamespace("pacman")) install.packages("pacman")
#packages_cran <- c("renv", "dplyr")
#library(pacman)
#pacman::p_install(dplyr)

# load packages
library(psych)
library(GPArotation)
library(corrplot)
#library(data.table)
library(ggplot2)
library(dplyr)
library(tidyr)
library(dplyr)
library(reshape2)
library(textshape)
library(sjmisc)
library(nFactors)
library(tidyverse)
library(parameters)
 library(xtable)
library(see)
library(polycor)
here::i_am("flag_project_root.R")
mainf <- here::here()
rootf <- here::here("")
dataf <- here::here("data/")
outputf <- here::here("output/")
source(here::here("covid-fear/scripts/factor_analysis/lib/utils.R"))
```

```{r}
corr_type = "spearman"
nFactors = 3
`%nin%` = Negate(`%in%`)

data <- read.csv(here::here("data", "summarized_data_for_secondary_factor_analysis.csv"))
#data <- data[, colnames(data)[ colnames(data) %nin% c("PROLIFICID", "worry.prob","affheallth.econ.worry", "state_severity", "obj_severity", "covid_worry", "avoid_beh", "avoid_anx", "prob_est", "covid_thoughts", "stai_ta",  "sticsa_cog_ta", "sticsa_som_ta") ]]

data <- data[, colnames(data)[ colnames(data) %nin% c("PROLIFICID") ]]
#data <- data[, colnames(data)[!grepl("^r_*", colnames(data)) ]]
#data <- data[, colnames(data)[!grepl("^TF*", colnames(data)) ]]
#data <- data[, colnames(data)[!grepl("^F*", colnames(data)) ]]
#data <- data[,colnames(data)["state_severity","TF3_NegativeAffect","TF2_PhysiolAnx", "TF1_CognAnxDepr", "F1_CovidAnxietyWorry", "F2_CovidProbabilityEsts", "F3_Leftoever",  "q6_close_person_infdied", "q6_work_home",
#                             "q6_apply_soc_dist",  "q6_risk_group",  "q6_houshold_membs", "q6_media_freq_num",  "q6_media_valence",  "q7_worry_infdie", "q7_worry_econ_impact", "q7_worry_insuf_help", "q7_worry_shortage",
#                             "q7_period_rel_danger","q7_period_rel_safety", "q7_safety_danger_diff"   "q7_people_overreact"     "q7_vir_not_as_dangerous" "q7_avoid"                "q7_covid_anxiety"        "q7_anx_touching_surf"    #"q8_prob_inf_me"         
#[28]           
#[37] "deaths7_norm"            "worry.prob"              "worryhealth"             "affheallth.econ.worry"   "covid_cases_est_diff"   ] ]


for(i in  colnames(data)){
    data[is.na(data[,i]), i] <- median(data[,i], na.rm = TRUE)
  }

#for(i in colnames(data)){
#    data[,i] <- (data[,i] - mean(data[,i],na.rm = TRUE)) / sd(data[,i],na.rm = TRUE)
#    #df.allsess[,i] <- (df.allsess[,i] - min(df.allsess[,i]))  / (max(df.allsess[,i]) - min(df.allsess[,i])) 
#}



```

```{r}
#cdf = cor(data, method = c(corr_type)) #how does cor2 deal with missing values?
# Polychor correlation
cordf <- polycor::hetcor(data)
cdf <- cordf$correlations
cp <- corrplot(cdf, tl.pos = 'n')


```

```{r}
# a/ Eigenvalues
evals <- eigen(cdf)$values 
scree(cdf, factors = TRUE)
```



```{r}
# Cattell-Nelson-Gorsuch indices

m <- nCng(data, cor = TRUE, model = "factors", details = TRUE)
print(m)
m$detail
plotuScree(data, main=paste(m$nFactors,
                          " factors retained by the CNG procedure",
                          sep=""))
```


### Method agreement procedure 
```{r, results='asis'}
# https://www.r-bloggers.com/2018/05/how-many-factors-to-retain-in-factor-analysis/

res = parameters::n_factors(data, type="FA", rotation="oblimin")
#see::plots(res)
print(res)

print(xtable(data.frame(res)), type = "html")
dfrs <- data.frame(res)
```
```{r}
# Run factor analysis
fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "lm", scores = "regression")
fa3.load <- data.frame(unclass(fa3$loadings))
fa3.load$varname <- rownames(fa3.load)
plot(fa3)
```

### Method 1: Get trait factor scores

```{r, fig.width = 14, fig.asp =0.5, results='asis' }

do.th1 <- 1

  if (do.th1 == 1) {
  

  ## run FA on session 1 (make sure to use df.allsess which uses data standardized across sessions)
  df.reduced <- data
  cordf <- polycor::hetcor(df.reduced)
  cdf <- cordf$correlations
  
  #cdf = cor(df.reduced, method = c(corr_type)) #how does cor2 deal with missing values?

  #df.reduced  <- df.allsess[df.allsess$SESSIONID %in% "1",]
  #cdf = cor(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)] , method = c(cor_type")) #how does cor2 deal with missing values?
  fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "lm")
  fa3.load <- data.frame(unclass(fa3$loadings))
  fa3.load$sessid <- 1
  fa3.load$varname <- rownames(fa3.load)
  #fa3.load <- merge(fa3.load, vardf, by="varname")
  
  orig_factors = colnames(fa3.load)[grepl("MR",colnames(fa3.load))]
    
  #fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])] #[,c("varname",sprintf("ML%s",seq(1:nFactors)))]
  #fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])][,c("varname",sprintf("ML%s",seq(1:nFactors)))]
  fa3.load$manual_factor <- apply( abs(fa3.load[,colnames(fa3.load)[grepl("MR",colnames(fa3.load))]]),  1, which.max)
  
  fa3.load$idx <- seq(nrow(fa3.load))
  library("textshape")
  temp <- fa3.load[,c("idx","manual_factor","varname")] %>%
    pivot_wider(names_from = "manual_factor", values_from = "varname")
  temp <- temp[,!(colnames(temp) %in% c("idx"))]
  temp <- temp[,paste(sort(as.integer(colnames(temp))))]
  # !!! the below assumes ordering of columns by manual factor 1,2,3
  temp <- plyr::ldply(apply(temp, 2, sort, decreasing=F),rbind)
  #temp = t(column_to_rownames(temp))
  temp = data.frame(t(temp))
  temp <- temp[!(rownames(temp) %in% c(".id")), ]
  colnames(temp) <- sprintf("F%s",seq(nFactors))
  temp <- temp[,sprintf("F%s",seq(nFactors))]
  colnames(temp) <- orig_factors
  print(xtable(temp), type = "html")
  # export factors from session 1
  #write.csv(data.frame(temp), here::here(outputf, "questionnaires", paste0("trait_factors_overview_f", toString(nFactors),".csv"))) 
  
  
  fa3.load.long <- fa3.load %>% 
       melt(id.vars = c("varname",  "manual_factor"),  # variables to carry forward
            measure.vars = colnames(fa3.load)[grepl("MR",colnames(fa3.load))],  #variables to stack
            value.name = "loading",     # name of the value variable 
            variable.name = "factor" ) # name of the variable   
  
    
    # Visualize - needs to match the `temp` table
     for (i in orig_factors) {
      fa3.load.long$current_fac <- 0
      fa3.load.long$current_fac[fa3.load.long$factor %in% i & abs(fa3.load.long$loading) > 0.2] <- 1
      
      tdf <- fa3.load.long[fa3.load.long$factor %in% i,]
      g <- ggplot(tdf, aes(x=varname, y=loading, color=as.factor(manual_factor), fill=as.factor(current_fac))) +
      geom_bar(stat="identity", lwd=1) +
      ggtitle(paste0("Factor ",i, " \""), "\"")  +
      scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
      theme(axis.text.x = element_text(angle = 45, vjust = 1, hjust=1)) + 
      ylim(-0.5,1)
      print(g)
    }



  }
```