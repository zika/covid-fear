from tslearn.utils import to_time_series
from tslearn.clustering import TimeSeriesKMeans, silhouette_score
import numpy as np
import pandas as pd
import seaborn as sns
from cov_functions import *
import matplotlib.pyplot as plt
import warnings

warnings.filterwarnings('ignore')


def estimate_clusters(ts, tdf, n_clusters, metric="euclidean"):

    # 'dtw' stands for dynamic time warping, i.e. it aligns multiple sources that are temporally misaligned 
    # 'euclidean' 
  
    # specify model
    from tslearn.clustering import TimeSeriesKMeans, silhouette_score

    model = TimeSeriesKMeans(n_clusters=n_clusters, metric=metric, max_iter=50, verbose=0, n_init=15) #specify model
    # fit model 
    model.fit(ts) #fit

    ## not sur ewhat's the silhouette score
    #labels = model.fit_predict(ts) # ??
    #silhouette_score(ts, labels, metric=metric) #?? 

    #which cluster each participant belongs to - awesome!
    cl_ids = model.predict(ts) 

    ## back to pandas
    tdf["cl"] = cl_ids
    tdf["id"] = tdf.index

    # claculate centroids to data frame
    #df_centroids = pd.DataFrame()
    #for clid, cl in enumerate(m.cluster_centers_):
    #    df_centroids["cl"+str(clid)] = pd.Series(cl[:,0]) 


    
    return tdf, model

def visualize_clusters(df, tdf, model, dep_var, huevar, n_clusters=10):
    # df - overall data set 
    # tdf - data in wide format, output of estimate_clusters() 
    # model - estimated model
    # dep_var - which variable are we clustering
    # huevar - second variable to color clusters by
    
    # get information from the model
    ts = to_time_series(tdf.drop(columns=["cl", "id"]))
    cl_ids = model.predict(ts) 
    clusters = np.transpose(np.squeeze(model.cluster_centers_))
    (unique, counts) = np.unique(cl_ids, return_counts=True)
    
    # where will labels be shown 
    y = clusters[18,:]
    me=tdf[["sess18", "cl"]].groupby("cl").mean()
    yme=np.array(me["sess18"])
    
    # turn back to long format
    tdf2 = tdf.melt(id_vars=["id", "cl"], value_vars=["sess"+str(x) for x in range(1,21)]).set_index("id")

    
    # plot 
    plt.figure(figsize=(7,4))
    pal=sns.color_palette("rocket_r", 4)
    f = sns.lineplot(data=tdf2, x="session", y="value", hue="cl", palette=pal, legend=False)
    f.set_xticklabels([str(i) for i in range(20)])
    f.set_ylabel(dep_var) 

    #fig, ax = plt.subplots(1,len(huevar), figsize=(len(huevar)*4, 5))
    #for f_idx, hv in enumerate(huevar):

        # add anxiety to the dataset
    stdf = tdf2.join(df.loc[:,["PROLIFICID"]+huevar].groupby("PROLIFICID").mean())
        # Set your custom color palette
    #    pal=sns.color_palette("rocket_r", n_clusters)
        
        #get mean anxiety per bin
    anx_cluster = stdf.loc[:,["cl"]+huevar].groupby(["cl"]).mean()
    #    plt.ylabel(dep_var)
    x = 18
    for i,v,c,y in zip(np.array(anx_cluster.index),unique, counts,yme):
        f.annotate("n="+str(c)+" cluster="+str(v), (x,y),   bbox=dict(boxstyle="round", fc="w", edgecolor=pal[i]))
    

    ### show second plot
    stdf = tdf.loc[:,["id", "cl"]].join(df.loc[:,["PROLIFICID"]+huevar].groupby("PROLIFICID").mean())
    stdf = stdf.melt(id_vars=["id", "cl"], value_vars=huevar).set_index("id")

    f, ax = plt.subplots(1,2,figsize=(10,5) ) 
    axx=sns.barplot(data=stdf, x="cl", y="value", hue="variable", errorbar="se", ax=ax[0],  palette=pal)
    axx.set_ylabel(dep_var) 
    #for b in axx.bar:
    #    print(b)

    axx=sns.barplot(data=stdf, x="cl", y="value", hue="variable", errorbar="se", ax=ax[1],  palette=pal)
    for b1, b2 in zip(axx.containers[0], axx.containers[1]):
        b1.set_alpha(0.6)
        b2.set_alpha(0.6)
    axx=sns.swarmplot(legend=False, data=stdf, x="cl", y="value", hue="variable", palette=pal, dodge=True, color="black", alpha=0.3)