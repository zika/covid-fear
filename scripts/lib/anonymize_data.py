# crucial script that removes IP addressess and renames all PROLIFICIDs
# be very careful before using this

import numpy as np
import os
import random, string
import pandas as pd
print(os.getcwd())
def generate_new_keys(df, var_id='PROLIFICID', length=16):
    old_keys = df[var_id].unique()
    new_keys = [''.join(random.choices(string.ascii_letters + string.digits, k=length)) for k in range(old_keys.shape[0])]
    df_keys = pd.DataFrame({'old_key': old_keys, 'new_key': new_keys})
    return df_keys

def anonymize_ids(df, df_keys, var_id):
    # check that all PIDs are in the key file
    df_keys[var_id] = df_keys["old_key"]
    tdf = df.merge(df_keys, on=var_id, how='outer', suffixes=['_file', '_key'], indicator=True)
    if tdf["_merge"].value_counts()["left_only"] > 0:
        id = tdf["_merge"].isin(["left_only"])
        tdf = tdf.loc[id,:]
        app_keys= generate_new_keys(tdf, var_id=var_id)
        df_keys = pd.concat([df_keys, app_keys]).reset_index(drop=True).drop(columns=[var_id])
        # udate keys file
        df_keys.to_csv('data/main_key.csv',index=False)
    df[var_id] = df[var_id].replace(np.array(df_keys["old_key"]), np.array(df_keys["new_key"]))
    return df


# generate base key (this should be based on a file that contains all IDs)
generate_key = 0 #SHOULD be 0 otherwise previous key gets overwritten
if generate_key == 1:
    raws1 = pd.read_csv("data/session1/main_data.csv")
    df_keys=generate_new_keys(raws1)
    df_keys.to_csv('data/main_key.csv',index=False)
elif generate_key == 0:
    df_keys = pd.read_csv('data/main_key.csv')


full_sweep = 0
if full_sweep == 1:
    # build list of files to anonymize
    sess = ["1","2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15"]
    #sess = ["2"]
    stype = ["extra" if sess[sidx].count("extra") else s for sidx, s in enumerate(["long" if np.isin(s, ["8", "15" ]) else "short"  for s in sess]) ]
    sstr = ["extra_" if s.count("extra") else "" for s in stype]
    sess_num = [int(s) if stype[sidx] not in "extra" else int(s[-1]) for sidx, s in enumerate(sess)]
    for se, st, ss in zip(sess_num, stype, sstr):
            conf = {"session": se, #
                    "session_type": st, #long/short/extra
                    "sess_str": ss #"extra_" #empty if normal session
                   }
            data_path = "data/"+conf["sess_str"]+"session"+str(conf["session"])+"/"
            file_name = ["prolific_data_uk", "prolific_data_be", "main_data"]
            var_name = ["participant_id", "participant_id", "PROLIFICID"]
            for f,v in zip(file_name, var_name):
                dt  = pd.read_csv(data_path+f+".csv", delimiter=",")
                dt = anonymize_ids(dt, df_keys, v)
                if any(dt.columns.isin(["ipaddr"])):
                    dt = dt.drop(columns=["ipaddr"])
                dt  = dt.to_csv(data_path+f+".csv", index=False)

standalone = 1
file_path = "data/session1/"
file_name = "screening_data"
var_id = "PROLIFICID"
if standalone == 1:
    df  = pd.read_csv(file_path+file_name+".csv", delimiter=",")
    df = anonymize_ids(df, df_keys, var_id)
    if any(df.columns.isin(["ipaddr"])):
        df = df.drop(columns=["ipaddr"])
    df.to_csv(file_path+file_name+".csv", index=False)
