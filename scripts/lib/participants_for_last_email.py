import pandas as pd
import ptitprince as pt
import os
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import scipy as sp
os.chdir("/data/drive/postdoc/Project4_covid/")
## Load lab ID and prolific ID link db
link_db = pd.read_csv('data/berlin_lab_participants.csv')
link_db = link_db.loc[:,link_db.columns.intersection(['email', 'Name', 'subID'])]
link_db = link_db.set_index('subID')

lastdata = pd.read_csv('data/lastemail_data.csv')
lastdata = lastdata.loc[lastdata.Status.isin(['A', 'B']),:].set_index('Participant')
data = lastdata.join(link_db).to_csv('data/lastemail_emails.csv')
