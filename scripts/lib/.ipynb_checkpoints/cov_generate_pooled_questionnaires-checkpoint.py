import pandas as pd
import os
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
os.chdir("/data/drive/postdoc/Project4_covid/covid-fear/scripts/")


## Eligible participants
prev_sess_dir = "../../data/session7/"
chosen_pcs = pd.read_csv(prev_sess_dir+"prolific_data_be.csv")
chosen_pcs.index = chosen_pcs.participant_id
chosen_pcs = chosen_pcs.loc[chosen_pcs.status=="APPROVED", :]
subs = chosen_pcs.index;

## Trait anxiety at the beginning (Session1)
s1ta = pd.read_csv("../../data/session1/individual_questionnaires/stai_trait.csv")
s1ta.index = s1ta.PROLIFICID
s1ta=s1ta.loc[s1ta.index.intersection(subs),:]
s1ta = s1ta.rename(columns={"ta": "ta_s1"})

## Trait anxiety session 8
s8ta = pd.read_csv("../../data/session8/individual_questionnaires/stai_trait.csv")
s8ta.index = s8ta.PROLIFICID
s8ta=s8ta.loc[s8ta.index.intersection(subs),:]
s8ta = s8ta.rename(columns={"ta": "ta_s8"})

## Trait anxiety STICSA at the beginning (Session1)
s1_stics = pd.read_csv("../../data/session1/individual_questionnaires/sticsa_trait.csv")
s1_stics.index = s1_stics.PROLIFICID
s1_stics=s1_stics.loc[s1_stics.index.intersection(subs),:]
s1_stics = s1_stics.rename(columns={"stta": "stta_s1"})

## Trait anxiety STICSA session 8
s8_stics = pd.read_csv("../../data/session8/individual_questionnaires/sticsa_trait.csv")
s8_stics.index = s8_stics.PROLIFICID
s8_stics=s8_stics.loc[s8_stics.index.intersection(subs),:]
s8_stics = s8_stics.rename(columns={"stta": "stta_s8"})


## Quickly compare
data_sticsa = s1_stics.join(s8_stics, lsuffix = "_s1", rsuffix="_s8")
data_sticsa = data_sticsa.loc[:,data_sticsa.columns.intersection(["stta_s1", "stta_s8"])]
data_sticsa["stta_overall"] = data_sticsa.mean(axis=1)

data = s1ta.join(s8ta, lsuffix = "_s1", rsuffix="_s8")
data = data.loc[:,data.columns.intersection(["ta_s1", "ta_s8"])]

data["ta_overall"] = data.mean(axis=1)
data = data.join(data_sticsa)
data["pid"] = data.index

plt.figure()
a=sns.scatterplot(data.ta_s1, data.ta_s8)
plt.title('sess1-sess8 correlation');
plt.savefig("../../output/figures/ta_reliability.png")
plt.figure()
a=sns.scatterplot(data.stta_s1, data.stta_s8)
plt.title('sess1-sess8 correlation');
plt.savefig("../../output/figures/stta_reliability.png")
#b=sns.distplot(data.ta_overall)

data.to_csv("../../output/questionnaires/pooled_anxiety.csv")


em = pd.read_csv("../../data/session2/working_data/participants_germany.csv")
em.index = em.ID
em=em.loc[em.index.intersection(subs),:]
data=data.join(em)
data.to_csv("../../learning-task/behavioural_participants.csv")


################################################################3333
## Trait anxiety STICSA at the beginning (Session1)
s1_stics = pd.read_csv("../../data/session1/individual_questionnaires/sticsa_trait_som_subscale.csv")
s1_stics.index = s1_stics.PROLIFICID
s1_stics=s1_stics.loc[s1_stics.index.intersection(subs),:]
s1_stics = s1_stics.rename(columns={"stta_som": "stta_s1"})

## Trait anxiety STICSA session 8
s8_stics = pd.read_csv("../../data/session8/individual_questionnaires/sticsa_trait_som_subscale.csv")
s8_stics.index = s8_stics.PROLIFICID
s8_stics=s8_stics.loc[s8_stics.index.intersection(subs),:]
s8_stics = s8_stics.rename(columns={"stta_som": "stta_s8"})

data_sticsa = s1_stics.join(s8_stics, lsuffix = "_s1", rsuffix="_s8")
data_sticsa = data_sticsa.loc[:,data_sticsa.columns.intersection(["stta_s1", "stta_s8"])]
data_sticsa["stta_overall"] = data_sticsa.mean(axis=1)
plt.figure()
a=sns.scatterplot(data_sticsa.stta_s1, data_sticsa.stta_s8)
plt.title('Somatic');
plt.savefig("../../output/figures/stta_som_reliability.png")

################################################################3333
## Trait anxiety STICSA at the beginning (Session1)
s1_stics = pd.read_csv("../../data/session1/individual_questionnaires/sticsa_trait_cog_subscale.csv")
s1_stics.index = s1_stics.PROLIFICID
s1_stics=s1_stics.loc[s1_stics.index.intersection(subs),:]
s1_stics = s1_stics.rename(columns={"stta_cog": "stta_s1"})

## Trait anxiety STICSA session 8
s8_stics = pd.read_csv("../../data/session8/individual_questionnaires/sticsa_trait_cog_subscale.csv")
s8_stics.index = s8_stics.PROLIFICID
s8_stics=s8_stics.loc[s8_stics.index.intersection(subs),:]
s8_stics = s8_stics.rename(columns={"stta_cog": "stta_s8"})

data_sticsa = s1_stics.join(s8_stics, lsuffix = "_s1", rsuffix="_s8")
data_sticsa = data_sticsa.loc[:,data_sticsa.columns.intersection(["stta_s1", "stta_s8"])]
data_sticsa["stta_overall"] = data_sticsa.mean(axis=1)
plt.figure()
a=sns.scatterplot(data_sticsa.stta_s1, data_sticsa.stta_s8)
plt.title('Cognitive');
plt.savefig("../../output/figures/stta_cog_reliability.png")
