import pandas as pd
import numpy as np
import seaborn as sns
import scipy as sp
import ptitprince as pt
import matplotlib.pyplot as plt

from scipy import stats
data_path = "/data/drive/postdoc/Project4_covid/data/task_data/visit1/questionnaires/"
visit = "1"
df=pd.read_csv(data_path+"full_questionnaire_data.csv", sep=';')
print(df)
masterdb = pd.read_csv("/data/drive/postdoc/Project4_covid/learning-task/task/master_db.csv")
df=df.set_index("subID", drop=False)
masterdb = masterdb.set_index("subid", drop=False)
df2=pd.read_csv("/data/drive/postdoc/Project4_covid/output/selection_bias_data.csv")
df2=df2.loc[:,df2.columns.intersection(["subID", "ta_s1",	"ta_s8",	"ta_overall",	"stta_s1",	"stta_s8",  "stta_overall"])].set_index("subID", drop=True)

data = masterdb.join(df).join(df2)



ivs = ['stai_ta', 'ta_overall', 'covid_worry'];
#ivs = ['ta_overall']
labels = ['STAI trait anxiety', 'STAI trait anxiety (from online)', 'Covid worry']


for (v,l) in zip(ivs, labels):
    print(l)
    t,p = sp.stats.ttest_ind(data[v].loc[data.schedule_type=="A"], data[v].loc[data.schedule_type=="B"], nan_policy='omit');
    print(p,t)
    fig, axs = plt.subplots(ncols=2, figsize=(14,5))
    sns.histplot(data=data, x=v, bins=12, ax=axs[0], stat='count', kde=True, element="step",
                color=sns.color_palette("rocket")[2])
    axs[0].set_title('Overall distribution')
    axs[0].set_xlabel(l)

    sns.histplot(data=data, x=v, bins=12, hue='schedule_type', ax=axs[1], stat='count', kde=True,
                 kde_kws=dict(bw_method='scott', bw_adjust=0.7),
                 alpha=0.35, element="step",
                palette=list( sns.color_palette("rocket")[i] for i in [1,4] ), common_norm=False)
    axs[1].set_title('By schedule type')
    axs[1].set_xlabel(l)

plt.figure()
sns.violinplot(data=data, x='schedule_type', y='ta_overall')

## use of specific schedules
print(masterdb['schedule'].value_counts())
print(masterdb['schedule_type'].value_counts())
0
