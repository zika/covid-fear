import pandas as pd
import os
import numpy as np
os.chdir("/data/drive/postdoc/Project4_covid/covid-fear/scripts/")

# current session
sess_type = ""
sess = 15
generate_bonus_files = 1
bonus = "14.87" # in pounds

curr_sess_dir = "../../data/"+sess_type+"session"+str(sess)+"/"
prev_sess_dir = "../../data/session15/working_data/"
outp_dir =  curr_sess_dir+"working_data/"
em = pd.read_csv("../../data/session2/working_data/participants_germany.csv")
em.index = em.ID

types = ["uk", "be"]
for t in types:
    prev = pd.read_csv(prev_sess_dir+"sess15_invited_"+t+".csv")
    # check if any manual appends exist!
    if os.path.exists(curr_sess_dir+"working_data/"+t+"_manually_added.csv"):
        newids = pd.read_csv(curr_sess_dir+"working_data/"+t+"_manually_added.csv")
        newids = newids.assign(status="APPROVED")
        prev = pd.concat([prev, newids])

    if os.path.exists(curr_sess_dir+"prolific_data_"+t+".csv"):

        curr = pd.read_csv(curr_sess_dir+"prolific_data_"+t+".csv")
        curr.index = curr.participant_id
        prev.index = prev.participant_id
        prevsubs = np.array(prev.participant_id)
        currsubs = np.array(curr.participant_id)
        different = np.setdiff1d(prevsubs, currsubs)
        common = np.intersect1d(prevsubs, currsubs)

        diff = prev.loc[prev.index.intersection(different),prev.columns.intersection(["participant_id", "status"])]
        diff = diff[diff.status=="APPROVED"]
        diff.to_csv(outp_dir+t+"_participants_who_didnt_fill_yet.csv")

        comm = curr.loc[curr.index.intersection(common),curr.columns.intersection(["session_id", "participant_id", "status"])]


        comm.to_csv(outp_dir+t+"_participants_who_completed.csv")

    else:
        prev = prev[prev.status=="APPROVED"]
        prev.to_csv(outp_dir+t+"_participants_who_didnt_fill_yet.csv")




    ## Export payment file
    if generate_bonus_files==1:
        paym = comm[comm.status=="APPROVED"]
        paym = paym.assign(amount=bonus).drop(["session_id", "status"], axis=1)
        paym.to_csv(outp_dir+t+"_bonus_payment.csv", header=False, index=False)

    if t == "be":
        if os.path.exists(curr_sess_dir+"prolific_data_"+t+".csv"):

            be_em = em.loc[em.index.intersection(common),:]
            be_em.to_csv(outp_dir+t+"_participants_who_completed_emails.csv")

            be_em = em.loc[em.index.intersection(diff.participant_id),:]
            be_em.to_csv(outp_dir+t+"_participants_who_didnt_fill_yet_emails.csv")
        else:
            be_em = em.loc[em.index.intersection(prev.participant_id),:]
            be_em.to_csv(outp_dir+t+"_participants_who_didnt_fill_yet_emails.csv")
