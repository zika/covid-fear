#!/usr/bin/env python3
import urllib3
import pandas as pd
import numpy as np
import datetime as dt
import os
import subprocess
import requests
import json
import urllib.request, urllib.error, urllib.parse
from newsapi import NewsApiClient

os.chdir('/data/drive/postdoc/Project4_covid/covid-fear/scripts/')



# SCRAPE MAIN SITES OF MEDIA
urls = pd.read_csv('../../data/covid_online_news_dataset/news/unique_media_list.csv')
urls=urls.dropna().reset_index()
urls['name'] = urls['name'].str.replace(' ', '').str.replace('.', '')
urls['identifier'] = urls.index.astype('str')+'_'+urls['name']

now = dt.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')


#df['Name'] = df['First'].str.cat(df['Last'],sep=" ")
#df
for iux, ur in enumerate(urls.url):
    print(ur)
    ## Make dir
    path = '../../data/covid_online_news_dataset/news/'+urls.identifier[iux]+'/'
    bashCommand = 'mkdir '+path
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    response = urllib.request.urlopen(ur)
    webContent = response.read()
    f = open(path+now+urls.identifier[iux]+'.html', 'wb')
    f.write(webContent)
    f.close



# SCRAPE ARTICLES VIE NEWS API
srcs = pd.read_csv('../../data/covid_online_news_dataset/news_via_newsapi/sources.csv')
now = dt.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
# Init
newsapi = NewsApiClient(api_key='480d105c9f3f44869570ee0da7b4ea0d')
kwds = ['coronavirus', 'covid']
for kw in kwds:
    for sidx, src in enumerate(srcs.src):
        if srcs.lang[sidx]=='en':
            keywords =kw
            language = 'en'
        elif srcs.lang[sidx]=='de':
            keywords =kw
            language = 'de'
        print('Keyword: '+keywords)
        print('Source: '+src+' '+srcs.domain[sidx])
        print((dt.datetime.now()-dt.timedelta(1)).strftime('%Y-%m-%d')+' - '+dt.datetime.now().strftime('%Y-%m-%d'))
        da = newsapi.get_everything(q=keywords,
                                      sources=src,
                                      domains=srcs.domain[sidx],
                                      from_param=(dt.datetime.now()-dt.timedelta(1)).strftime('%Y-%m-%d'),
                                      to=dt.datetime.now().strftime('%Y-%m-%d'),
                                      language=language,
                                      sort_by='popularity',
                                      page=5)

        data = pd.DataFrame(da['articles'])
        ident = str(sidx)+'_'+src
        path = '../../data/covid_online_news_dataset/news_via_newsapi/'+kw+'/'+ident+'/'
        bashCommand = 'mkdir '+path
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        data.to_csv(path+ident+'_'+now+'.csv')
