---
title: "Untitled"
author: "Ondrej Zika"
date: "2024-08-13"
output: html_document
---





```r
here::i_am(".root_dir_covid")
```

```
## here() starts at /data/drive/postdoc/Project4_covid
```

```r
here::here()
```

```
## [1] "/data/drive/postdoc/Project4_covid"
```

```r
renv::load(here::here("covid-fear"))
```

```
## - Project '/data/drive/postdoc/Project4_covid/covid-fear' loaded. [renv 1.0.3]
```


```r
knight_it = 0
if (knight_it == 1) {
  knitr::knit2html(input=here::here("covid-fear", "scripts","focused_analyses", "paper_GLMs_focused.Rmd"), 
              output=here::here("covid-fear", "scripts","focused_analyses", "paper_GLMs_focused.html"))
}
```



```r
required_packages = c("lmerTest",  "emmeans", "ggplot2", "parameters", "reshape2", "PupillometryR",  "plyr", "tidyr", "ggpubr", "corrplot", "patchwork", "broom", "plotrix", "PupillometryR","Hmisc", "performance", "see", "gtools", "car", "ggpubr" , "RColorBrewer", "effectsize", "lmtest", "patchwork", "datawizard")
invisible(lapply(required_packages, require, character.only = TRUE))
```

```
## Loading required package: lmerTest
```

```
## Loading required package: lme4
```

```
## Loading required package: Matrix
```

```
## 
## Attaching package: 'lmerTest'
```

```
## The following object is masked from 'package:lme4':
## 
##     lmer
```

```
## The following object is masked from 'package:stats':
## 
##     step
```

```
## Loading required package: emmeans
```

```
## Loading required package: ggplot2
```

```
## Loading required package: parameters
```

```
## Loading required package: reshape2
```

```
## Loading required package: PupillometryR
```

```
## Loading required package: dplyr
```

```
## 
## Attaching package: 'dplyr'
```

```
## The following objects are masked from 'package:stats':
## 
##     filter, lag
```

```
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```
## Loading required package: rlang
```

```
## Loading required package: plyr
```

```
## ----------------------------------------------------------------------------------------------------------------------------------------------------------
```

```
## You have loaded plyr after dplyr - this is likely to cause problems.
## If you need functions from both plyr and dplyr, please load plyr first, then dplyr:
## library(plyr); library(dplyr)
```

```
## ----------------------------------------------------------------------------------------------------------------------------------------------------------
```

```
## 
## Attaching package: 'plyr'
```

```
## The following objects are masked from 'package:dplyr':
## 
##     arrange, count, desc, failwith, id, mutate, rename, summarise, summarize
```

```
## Loading required package: tidyr
```

```
## 
## Attaching package: 'tidyr'
```

```
## The following object is masked from 'package:reshape2':
## 
##     smiths
```

```
## The following objects are masked from 'package:Matrix':
## 
##     expand, pack, unpack
```

```
## Loading required package: ggpubr
```

```
## 
## Attaching package: 'ggpubr'
```

```
## The following object is masked from 'package:plyr':
## 
##     mutate
```

```
## Loading required package: corrplot
```

```
## corrplot 0.92 loaded
```

```
## Loading required package: patchwork
```

```
## Loading required package: broom
```

```
## Loading required package: plotrix
```

```
## Loading required package: Hmisc
```

```
## Registered S3 method overwritten by 'htmlwidgets':
##   method           from         
##   print.htmlwidget tools:rstudio
```

```
## Registered S3 method overwritten by 'data.table':
##   method           from
##   print.data.table
```

```
## 
## Attaching package: 'Hmisc'
```

```
## The following objects are masked from 'package:plyr':
## 
##     is.discrete, summarize
```

```
## The following objects are masked from 'package:dplyr':
## 
##     src, summarize
```

```
## The following objects are masked from 'package:base':
## 
##     format.pval, units
```

```
## Loading required package: performance
```

```
## Loading required package: see
```

```
## Loading required package: gtools
```

```
## 
## Attaching package: 'gtools'
```

```
## The following object is masked from 'package:rlang':
## 
##     chr
```

```
## Loading required package: car
```

```
## Loading required package: carData
```

```
## 
## Attaching package: 'car'
```

```
## The following object is masked from 'package:gtools':
## 
##     logit
```

```
## The following object is masked from 'package:dplyr':
## 
##     recode
```

```
## Loading required package: RColorBrewer
```

```
## Loading required package: effectsize
```

```
## Loading required package: lmtest
```

```
## Loading required package: zoo
```

```
## 
## Attaching package: 'zoo'
```

```
## The following objects are masked from 'package:base':
## 
##     as.Date, as.Date.numeric
```

```
## Loading required package: datawizard
```

```
## 
## Attaching package: 'datawizard'
```

```
## The following object is masked from 'package:plotrix':
## 
##     rescale
```

```
## The following objects are masked from 'package:ggpubr':
## 
##     mean_sd, median_mad
```

```r
#detach("package:Matrix", unload = TRUE)

source(here::here('covid-fear', 'lib', 'shared', 'r', 'r_stone_lib.R'))

#factors <- c("F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism")
factors <- c("F1_CovidAnxietyWorry", "F2_CovidProbabilityEsts", "F3_Economic")
# will be z-scored
add_vars <- c( "q6_apply_soc_dist", "q6_risk_group" , "q6_media_freq_num",  "q6_media_valence", "covid_worry",  "prob_est", "avoid_beh", "q6_houshold_membs", "postcode_density", "q6_apply_soc_dist", "q6_work_home", "q6_risk_group", "sr_age", "stai_sa", "skepticism")
# won't be zscored
add_vars2 <- c( "q7_period_rel_danger",  "GROUP", "deaths", "cases")

trait_factors <- c("TF1_CognAnxDepr", "TF2_PhysiolAnx", "TF3_NegativeAffect")

df <- read.csv(here::here("data","full_dataset_only_complete_based_on_sess_avg.csv"))

# filter for weird ages 
df <- df[df$sr_age>18 & df$sr_age<42,]

df$deaths <- df$deaths14_norm_unsmooth
df$cases <- (df$cases14_norm_unsmooth)

tdf <- df[,c("PROLIFICID", "session", factors, trait_factors, add_vars, add_vars2)]

## zscore predictors
for (v in c(trait_factors, factors,add_vars, "q6_media_freq_num",  "q6_media_valence")) {
  #tdf[v] <- scale(tdf[v])
  tdf[v] <- datawizard::standardize(tdf[v])
  
}

tdf$sess_str <- paste0("sess", tdf$session)

## zscore severity by session
for (v in c("deaths", "cases")) {
  for (s in unique(tdf$sess_str)) {
    tdf[tdf$sess_str %in% s,v] <- datawizard::standardize(tdf[tdf$sess_str %in% s,v])
  }
}

tdf<- tdf %>% 
  mutate(
    wave = dplyr::case_when(
        session <= 6 ~ "first_wave",   
        session > 6 & session < 13 ~ "summer_nowave", 
        session >= 13 ~ "second_wave"
    )
  )
tdf$wave <- ordered(tdf$wave, levels = c("first_wave", "summer_nowave", "second_wave"))
tdf$sess_str <- ordered(tdf$sess_str, levels = c("sess1","sess2","sess3","sess4","sess5","sess6","sess7","sess8","sess9","sess10","sess11","sess12","sess13","sess14","sess15","sess16","sess17","sess18","sess19","sess20"))

tdf <- tdf %>% dplyr::rename(TF1 = TF1_CognAnxDepr,
                      TF2 = TF2_PhysiolAnx, 
                      TF3 = TF3_NegativeAffect, 
                      AvoidBeh = avoid_beh,
                      MediaVal = q6_media_valence,
                      InforSeek = q6_media_freq_num, 
                      ProbEst = prob_est,
                      Worry = covid_worry,
                      CurrThreat = q7_period_rel_danger,
                      Cases = cases,
                      Deaths = deaths, 
                      HouseholdMembers = q6_houshold_membs, 
                      PopulationDensity = postcode_density, 
                      Age = sr_age,
                      SocialDist = q6_apply_soc_dist,
                      WorkHome= q6_work_home, 
                      RiskGr = q6_risk_group)



tdf$Deaths_q <- gtools::quantcut(tdf$Deaths, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$Cases_q <- gtools::quantcut(tdf$Cases, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF1_3 <- gtools::quantcut(tdf$TF1, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF2_3 <- gtools::quantcut(tdf$TF2, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF3_3 <- gtools::quantcut(tdf$TF3, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF1_2 <- gtools::quantcut(tdf$TF1, q = c(0, 0.5, 1), 
                                 labels = c("low","high"))
tdf$TF2_2 <- gtools::quantcut(tdf$TF2, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))
tdf$TF3_2 <- gtools::quantcut(tdf$TF3, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))

tdf$InforSeek_3 <- gtools::quantcut(tdf$InforSeek, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$InforSeek_2 <- gtools::quantcut(tdf$InforSeek, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))

vars <- c("CurrThreat", "ProbEst", "Worry",  "AvoidBeh", "InforSeek")

tdf <- tdf %>% drop_na(Worry, CurrThreat, ProbEst, InforSeek, AvoidBeh, Cases, Deaths, TF1, TF2, TF3)
```

### Run time-resolved GLM - wave MAIN


```r
library(stringr)
library(car)
library(formattable)
```

```
## 
## Attaching package: 'formattable'
```

```
## The following object is masked from 'package:datawizard':
## 
##     normalize
```

```
## The following object is masked from 'package:patchwork':
## 
##     area
```

```r
library(lmerTest)
vars
```

```
## [1] "CurrThreat" "ProbEst"    "Worry"      "AvoidBeh"   "InforSeek"
```

```r
plist <- list()
plist2 <- list()
mlist <- list()
for (v in vars) {
  print("++++++START++++++")
  print(v)
  #v <- "CurrThreat"
  #dvs <- " ~ TF1*Cases*wave  + TF2*Cases*wave + TF3*Cases*wave + TF1*Deaths*wave  + TF2*Deaths*wave + TF3*Deaths*wave + CurrThreat*wave + InforSeek*wave + AvoidBeh*wave + ProbEst*wave + HouseholdMembers + Age + WorkHome + MediaVal + skepticism + RiskGr + SocialDist+ PopulationDensity + (1|PROLIFICID)"
  
  #dvs <- " ~ TF1*Cases*wave  + TF2*Cases*wave + TF3*Cases*wave + TF1*Deaths*wave  + TF2*Deaths*wave + TF3*Deaths*wave +  (1+wave|PROLIFICID)"
  #dvs <- " ~ TF1*Cases*wave  + TF2*Cases*wave + TF3*Cases*wave +  (1+wave|PROLIFICID)"
  paste(paste0(setdiff(vars, v), "*wave"), collapse=" + ")
  #CurrThreat*wave + InforSeek*wave + AvoidBeh*wave + ProbEst*wave +  +
  
   dvs <- paste0(" ~ TF1*Cases*wave  + TF2*Cases*wave + TF3*Cases*wave + HouseholdMembers + Age + WorkHome + MediaVal + skepticism + RiskGr + SocialDist+ PopulationDensity +  (1+wave|PROLIFICID)")
    
  pattern_to_remove <- paste0(v, "\\*wave\\s*\\+")
  dvs <- gsub(pattern_to_remove, "", dvs)
  eq <- paste0(v, dvs)
  
  m <- lmerTest::lmer(data=tdf,formula=eq )
   mlist <- append(mlist, list(m))
   print(anova(m))
   print(summary(m))
  #an<- car::Anova(m)
  #an$sign <- ifelse(an$`Pr(>Chisq)`<0.05, "nonsig", "sig")
  #print(formattable(an) )

  mps <- parameters::parameters(m)
  mps2 <-parameters::parameters(m)
  #mps <- mps[!grepl("sess_", mps$Parameter) & !grepl("SD", mps$Parameter) & !grepl(".Q", mps$Parameter),] 
  
  #"TF1:wave.L" "wave.L:TF2"  "wave.L:TF3" 
  mps <- mps[!grepl("wave", mps$Parameter) & !grepl("SD", mps$Parameter) & !grepl(".Q", mps$Parameter),] 
  
 # (grepl("TF1:wave.L", mps$Parameter) |
#               grepl("wave.L:TF2", mps$Parameter) | 
 #              grepl("wave.L:TF3", mps$Parameter))
  
  g <- change_color_aes_mapping_to_sig(mps, c("gray", "dodgerblue")) + 
       ggtitle(paste0(v, " ~")) + 
       xlab("")
  plist <- append(plist, list(g))
  
  mps2 <- mps2[(grepl("TF1:wave.L", mps2$Parameter) | grepl("wave.L:TF2", mps2$Parameter) | grepl("wave.L:TF3", mps2$Parameter)),] 
  g2<- change_color_aes_mapping_to_sig(mps2, c("gray", "dodgerblue")) + 
       ggtitle(paste0(v, "")) + 
       xlab("")
  plist2 <- append(plist2, list(g2))
  
  
}
```

```
## [1] "++++++START++++++"
## [1] "CurrThreat"
## Type III Analysis of Variance Table with Satterthwaite's method
##                    Sum Sq Mean Sq NumDF  DenDF  F value    Pr(>F)    
## TF1                 1.532   1.532     1  260.1   1.9523 0.1635280    
## Cases              34.922  34.922     1 2411.3  44.5013 3.142e-11 ***
## wave              195.163  97.581     2  283.5 124.3479 < 2.2e-16 ***
## TF2                 2.057   2.057     1  255.0   2.6206 0.1067190    
## TF3                 0.009   0.009     1  265.0   0.0110 0.9164788    
## HouseholdMembers    2.798   2.798     1  705.8   3.5653 0.0594095 .  
## Age                 7.082   7.082     1  270.4   9.0248 0.0029131 ** 
## WorkHome            1.297   1.297     1 2308.9   1.6527 0.1987187    
## MediaVal          268.076 268.076     1 4001.6 341.6086 < 2.2e-16 ***
## skepticism         98.924  98.924     1 1600.9 126.0587 < 2.2e-16 ***
## RiskGr              9.325   9.325     1 1347.2  11.8827 0.0005840 ***
## SocialDist         40.076  40.076     1 3887.9  51.0683 1.060e-12 ***
## PopulationDensity   0.272   0.272     1  259.9   0.3464 0.5566658    
## TF1:Cases           0.226   0.226     1 2817.6   0.2874 0.5919557    
## TF1:wave            0.842   0.421     2  265.4   0.5367 0.5852929    
## Cases:wave         11.158   5.579     2 1235.4   7.1095 0.0008512 ***
## Cases:TF2           0.403   0.403     1 2352.9   0.5139 0.4735163    
## wave:TF2            1.546   0.773     2  261.1   0.9849 0.3748637    
## Cases:TF3          10.088  10.088     1 2747.4  12.8548 0.0003425 ***
## wave:TF3            6.577   3.288     2  270.3   4.1905 0.0161342 *  
## TF1:Cases:wave      0.030   0.015     2 1258.8   0.0194 0.9808108    
## Cases:wave:TF2      0.749   0.375     2 1140.2   0.4774 0.6205487    
## Cases:wave:TF3      3.777   1.889     2 1377.7   2.4067 0.0904866 .  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## Linear mixed model fit by REML. t-tests use Satterthwaite's method ['lmerModLmerTest']
## Formula: eq
##    Data: tdf
## 
## REML criterion at convergence: 12647.9
## 
## Scaled residuals: 
##     Min      1Q  Median      3Q     Max 
## -5.1888 -0.5166  0.0356  0.5865  4.7714 
## 
## Random effects:
##  Groups     Name        Variance Std.Dev. Corr       
##  PROLIFICID (Intercept) 0.8457   0.9196              
##             wave.L      0.4553   0.6748   -0.37      
##             wave.Q      0.2372   0.4871   -0.44  0.15
##  Residual               0.7847   0.8859              
## Number of obs: 4324, groups:  PROLIFICID, 267
## 
## Fixed effects:
##                     Estimate Std. Error         df t value Pr(>|t|)    
## (Intercept)        4.512e+00  5.851e-02  2.554e+02  77.104  < 2e-16 ***
## TF1                1.523e-01  1.090e-01  2.601e+02   1.397 0.163528    
## Cases              2.004e-01  3.004e-02  2.411e+03   6.671 3.14e-11 ***
## wave.L             7.302e-01  5.104e-02  2.923e+02  14.306  < 2e-16 ***
## wave.Q             2.628e-01  3.932e-02  2.774e+02   6.684 1.27e-10 ***
## TF2                1.224e-01  7.562e-02  2.550e+02   1.619 0.106719    
## TF3                9.965e-03  9.494e-02  2.650e+02   0.105 0.916479    
## HouseholdMembers  -7.925e-02  4.197e-02  7.058e+02  -1.888 0.059409 .  
## Age                1.552e-01  5.167e-02  2.704e+02   3.004 0.002913 ** 
## WorkHome          -4.106e-02  3.194e-02  2.309e+03  -1.286 0.198719    
## MediaVal          -3.180e-01  1.721e-02  4.002e+03 -18.483  < 2e-16 ***
## skepticism        -3.886e-01  3.461e-02  1.601e+03 -11.228  < 2e-16 ***
## RiskGr             1.214e-01  3.523e-02  1.347e+03   3.447 0.000584 ***
## SocialDist         1.841e-01  2.576e-02  3.888e+03   7.146 1.06e-12 ***
## PopulationDensity  3.174e-02  5.393e-02  2.599e+02   0.589 0.556666    
## TF1:Cases          2.399e-02  4.476e-02  2.818e+03   0.536 0.591956    
## TF1:wave.L         5.776e-02  9.151e-02  2.667e+02   0.631 0.528496    
## TF1:wave.Q         5.870e-02  7.130e-02  2.658e+02   0.823 0.411050    
## Cases:wave.L      -7.439e-02  4.959e-02  1.445e+03  -1.500 0.133860    
## Cases:wave.Q      -1.158e-01  4.511e-02  1.002e+03  -2.568 0.010378 *  
## Cases:TF2          2.700e-02  3.766e-02  2.353e+03   0.717 0.473516    
## wave.L:TF2         6.235e-02  6.371e-02  2.618e+02   0.979 0.328665    
## wave.Q:TF2        -4.960e-02  4.968e-02  2.609e+02  -0.998 0.319017    
## Cases:TF3         -1.293e-01  3.605e-02  2.747e+03  -3.585 0.000342 ***
## wave.L:TF3        -1.860e-01  7.917e-02  2.700e+02  -2.350 0.019506 *  
## wave.Q:TF3        -1.040e-01  6.202e-02  2.722e+02  -1.677 0.094698 .  
## TF1:Cases:wave.L  -9.188e-03  7.304e-02  1.453e+03  -0.126 0.899918    
## TF1:Cases:wave.Q  -6.965e-03  6.534e-02  1.042e+03  -0.107 0.915128    
## Cases:wave.L:TF2  -1.890e-02  5.967e-02  1.308e+03  -0.317 0.751424    
## Cases:wave.Q:TF2  -4.333e-02  5.768e-02  8.692e+02  -0.751 0.452768    
## Cases:wave.L:TF3   2.850e-02  6.082e-02  1.625e+03   0.469 0.639366    
## Cases:wave.Q:TF3   9.982e-02  5.296e-02  1.164e+03   1.885 0.059701 .  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```
## 
## Correlation matrix not shown by default, as p = 32 > 12.
## Use print(summary(m), correlation=TRUE)  or
##     vcov(summary(m))        if you need it
```

```
## Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.
## Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.
```

```
## [1] "++++++START++++++"
## [1] "ProbEst"
## Type III Analysis of Variance Table with Satterthwaite's method
##                   Sum Sq Mean Sq NumDF  DenDF F value    Pr(>F)    
## TF1               0.0440  0.0440     1  263.7  0.4454   0.50510    
## Cases             0.6120  0.6120     1 2456.1  6.1959   0.01287 *  
## wave              5.9750  2.9875     2  290.1 30.2473 1.171e-12 ***
## TF2               0.5514  0.5514     1  259.9  5.5828   0.01887 *  
## TF3               0.0278  0.0278     1  264.3  0.2814   0.59624    
## HouseholdMembers  0.0598  0.0598     1 3400.6  0.6051   0.43669    
## Age               0.0021  0.0021     1  429.2  0.0216   0.88314    
## WorkHome          0.3573  0.3573     1 3833.4  3.6176   0.05724 .  
## MediaVal          3.9716  3.9716     1 3897.2 40.2111 2.540e-10 ***
## skepticism        0.4123  0.4123     1 4246.3  4.1742   0.04111 *  
## RiskGr            0.6490  0.6490     1 4228.0  6.5708   0.01040 *  
## SocialDist        4.2837  4.2837     1 4047.3 43.3703 5.111e-11 ***
## PopulationDensity 0.2121  0.2121     1  265.7  2.1479   0.14395    
## TF1:Cases         0.3771  0.3771     1 2895.8  3.8178   0.05081 .  
## TF1:wave          0.4676  0.2338     2  268.3  2.3671   0.09571 .  
## Cases:wave        0.0616  0.0308     2 1133.9  0.3120   0.73202    
## Cases:TF2         0.0691  0.0691     1 2292.7  0.6999   0.40290    
## wave:TF2          0.1352  0.0676     2  262.7  0.6843   0.50534    
## Cases:TF3         0.1274  0.1274     1 2836.8  1.2895   0.25624    
## wave:TF3          0.3736  0.1868     2  273.2  1.8915   0.15282    
## TF1:Cases:wave    0.2041  0.1021     2 1202.7  1.0334   0.35610    
## Cases:wave:TF2    0.2348  0.1174     2 1042.6  1.1889   0.30498    
## Cases:wave:TF3    0.1201  0.0601     2 1313.3  0.6081   0.54454    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## Linear mixed model fit by REML. t-tests use Satterthwaite's method ['lmerModLmerTest']
## Formula: eq
##    Data: tdf
## 
## REML criterion at convergence: 4296
## 
## Scaled residuals: 
##     Min      1Q  Median      3Q     Max 
## -6.1863 -0.4602 -0.0333  0.4060  6.7658 
## 
## Random effects:
##  Groups     Name        Variance Std.Dev. Corr       
##  PROLIFICID (Intercept) 0.76873  0.8768              
##             wave.L      0.09043  0.3007    0.05      
##             wave.Q      0.01559  0.1249   -0.28 -0.20
##  Residual               0.09877  0.3143              
## Number of obs: 4324, groups:  PROLIFICID, 267
## 
## Fixed effects:
##                     Estimate Std. Error         df t value Pr(>|t|)    
## (Intercept)       -3.426e-02  5.404e-02  2.598e+02  -0.634   0.5266    
## TF1                6.705e-02  1.005e-01  2.637e+02   0.667   0.5051    
## Cases              2.684e-02  1.078e-02  2.456e+03   2.489   0.0129 *  
## wave.L            -1.013e-01  2.142e-02  2.929e+02  -4.730 3.50e-06 ***
## wave.Q             8.296e-02  1.188e-02  2.845e+02   6.985 2.01e-11 ***
## TF2                1.655e-01  7.003e-02  2.599e+02   2.363   0.0189 *  
## TF3                4.629e-02  8.727e-02  2.643e+02   0.530   0.5962    
## HouseholdMembers  -1.654e-02  2.126e-02  3.401e+03  -0.778   0.4367    
## Age                6.981e-03  4.746e-02  4.292e+02   0.147   0.8831    
## WorkHome           2.448e-02  1.287e-02  3.833e+03   1.902   0.0572 .  
## MediaVal          -3.909e-02  6.164e-03  3.897e+03  -6.341 2.54e-10 ***
## skepticism        -3.042e-02  1.489e-02  4.246e+03  -2.043   0.0411 *  
## RiskGr             4.008e-02  1.563e-02  4.228e+03   2.563   0.0104 *  
## SocialDist         6.354e-02  9.648e-03  4.047e+03   6.586 5.11e-11 ***
## PopulationDensity  7.991e-02  5.452e-02  2.657e+02   1.466   0.1440    
## TF1:Cases          3.170e-02  1.622e-02  2.896e+03   1.954   0.0508 .  
## TF1:wave.L         5.237e-03  3.848e-02  2.690e+02   0.136   0.8919    
## TF1:wave.Q         4.494e-02  2.143e-02  2.671e+02   2.097   0.0370 *  
## Cases:wave.L      -1.168e-02  1.854e-02  1.797e+03  -0.630   0.5289    
## Cases:wave.Q       1.073e-02  1.500e-02  8.730e+02   0.715   0.4746    
## Cases:TF2         -1.123e-02  1.342e-02  2.293e+03  -0.837   0.4029    
## wave.L:TF2        -7.566e-03  2.682e-02  2.644e+02  -0.282   0.7781    
## wave.Q:TF2        -1.569e-02  1.489e-02  2.607e+02  -1.054   0.2928    
## Cases:TF3         -1.484e-02  1.307e-02  2.837e+03  -1.136   0.2562    
## wave.L:TF3        -2.037e-02  3.326e-02  2.717e+02  -0.612   0.5408    
## wave.Q:TF3        -3.128e-02  1.869e-02  2.749e+02  -1.674   0.0953 .  
## TF1:Cases:wave.L  -3.782e-02  2.737e-02  1.801e+03  -1.382   0.1673    
## TF1:Cases:wave.Q   1.895e-02  2.183e-02  8.598e+02   0.868   0.3857    
## Cases:wave.L:TF2   3.326e-02  2.236e-02  1.647e+03   1.488   0.1370    
## Cases:wave.Q:TF2  -1.896e-02  1.901e-02  7.419e+02  -0.997   0.3189    
## Cases:wave.L:TF3   1.935e-02  2.271e-02  1.986e+03   0.852   0.3945    
## Cases:wave.Q:TF3  -1.719e-02  1.777e-02  9.799e+02  -0.967   0.3337    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```
## 
## Correlation matrix not shown by default, as p = 32 > 12.
## Use print(summary(m), correlation=TRUE)  or
##     vcov(summary(m))        if you need it
## 
## Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.
```

```
## [1] "++++++START++++++"
## [1] "Worry"
## Type III Analysis of Variance Table with Satterthwaite's method
##                   Sum Sq Mean Sq NumDF  DenDF F value    Pr(>F)    
## TF1               0.3066  0.3066     1  260.3  2.3033 0.1303120    
## Cases             1.7148  1.7148     1 2032.8 12.8825 0.0003395 ***
## wave              7.1023  3.5511     2  296.5 26.6786 2.217e-11 ***
## TF2               0.9333  0.9333     1  256.9  7.0113 0.0086009 ** 
## TF3               0.5033  0.5033     1  261.5  3.7811 0.0529073 .  
## HouseholdMembers  0.0897  0.0897     1 2627.7  0.6740 0.4117245    
## Age               0.7679  0.7679     1  359.0  5.7688 0.0168209 *  
## WorkHome          0.0179  0.0179     1 3593.0  0.1341 0.7142034    
## MediaVal          7.7952  7.7952     1 3981.6 58.5632 2.454e-14 ***
## skepticism        0.4863  0.4863     1 4056.7  3.6533 0.0560281 .  
## RiskGr            0.7318  0.7318     1 3804.7  5.4976 0.0190939 *  
## SocialDist        9.2547  9.2547     1 4059.2 69.5282 < 2.2e-16 ***
## PopulationDensity 0.1580  0.1580     1  264.0  1.1873 0.2768674    
## TF1:Cases         0.0775  0.0775     1 2476.3  0.5820 0.4455898    
## TF1:wave          0.7647  0.3823     2  268.0  2.8724 0.0583074 .  
## Cases:wave        0.6751  0.3376     2  940.8  2.5361 0.0797181 .  
## Cases:TF2         0.0000  0.0000     1 1956.9  0.0001 0.9908533    
## wave:TF2          0.1637  0.0818     2  262.3  0.6147 0.5415587    
## Cases:TF3         0.0494  0.0494     1 2411.5  0.3712 0.5423969    
## wave:TF3          1.7266  0.8633     2  273.2  6.4856 0.0017708 ** 
## TF1:Cases:wave    0.0590  0.0295     2  964.2  0.2218 0.8011264    
## Cases:wave:TF2    0.2564  0.1282     2  861.1  0.9632 0.3820647    
## Cases:wave:TF3    0.1821  0.0910     2 1064.6  0.6840 0.5048367    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## Linear mixed model fit by REML. t-tests use Satterthwaite's method ['lmerModLmerTest']
## Formula: eq
##    Data: tdf
## 
## REML criterion at convergence: 5255.7
## 
## Scaled residuals: 
##     Min      1Q  Median      3Q     Max 
## -6.5944 -0.4949 -0.0163  0.4860  5.5557 
## 
## Random effects:
##  Groups     Name        Variance Std.Dev. Corr       
##  PROLIFICID (Intercept) 0.59173  0.7692              
##             wave.L      0.05422  0.2328    0.07      
##             wave.Q      0.01483  0.1218   -0.25  0.01
##  Residual               0.13311  0.3648              
## Number of obs: 4324, groups:  PROLIFICID, 267
## 
## Fixed effects:
##                     Estimate Std. Error         df t value Pr(>|t|)    
## (Intercept)       -7.223e-02  4.759e-02  2.570e+02  -1.518  0.13033    
## TF1                1.345e-01  8.863e-02  2.603e+02   1.518  0.13031    
## Cases              4.205e-02  1.172e-02  2.033e+03   3.589  0.00034 ***
## wave.L            -8.914e-02  1.892e-02  3.068e+02  -4.712 3.73e-06 ***
## wave.Q             7.847e-02  1.286e-02  2.883e+02   6.101 3.39e-09 ***
## TF2                1.633e-01  6.168e-02  2.569e+02   2.648  0.00860 ** 
## TF3                1.498e-01  7.702e-02  2.615e+02   1.944  0.05291 .  
## HouseholdMembers  -1.863e-02  2.269e-02  2.628e+03  -0.821  0.41172    
## Age                1.053e-01  4.386e-02  3.590e+02   2.402  0.01682 *  
## WorkHome          -5.220e-03  1.425e-02  3.593e+03  -0.366  0.71420    
## MediaVal          -5.411e-02  7.071e-03  3.982e+03  -7.653 2.45e-14 ***
## skepticism        -3.160e-02  1.653e-02  4.057e+03  -1.911  0.05603 .  
## RiskGr             4.027e-02  1.717e-02  3.805e+03   2.345  0.01909 *  
## SocialDist         9.079e-02  1.089e-02  4.059e+03   8.338  < 2e-16 ***
## PopulationDensity -5.276e-02  4.842e-02  2.640e+02  -1.090  0.27687    
## TF1:Cases          1.352e-02  1.772e-02  2.476e+03   0.763  0.44559    
## TF1:wave.L         4.019e-02  3.357e-02  2.683e+02   1.197  0.23225    
## TF1:wave.Q         4.421e-02  2.318e-02  2.699e+02   1.907  0.05755 .  
## Cases:wave.L      -1.572e-02  1.955e-02  1.221e+03  -0.804  0.42144    
## Cases:wave.Q       3.703e-02  1.649e-02  7.890e+02   2.245  0.02502 *  
## Cases:TF2         -1.678e-04  1.463e-02  1.957e+03  -0.011  0.99085    
## wave.L:TF2         2.555e-02  2.334e-02  2.629e+02   1.095  0.27469    
## wave.Q:TF2        -5.017e-03  1.610e-02  2.633e+02  -0.312  0.75558    
## Cases:TF3         -8.698e-03  1.428e-02  2.412e+03  -0.609  0.54240    
## wave.L:TF3        -6.579e-02  2.906e-02  2.719e+02  -2.264  0.02437 *  
## wave.Q:TF3        -4.988e-02  2.022e-02  2.776e+02  -2.466  0.01426 *  
## TF1:Cases:wave.L  -6.120e-03  2.878e-02  1.246e+03  -0.213  0.83164    
## TF1:Cases:wave.Q   1.601e-02  2.404e-02  7.811e+02   0.666  0.50563    
## Cases:wave.L:TF2  -1.781e-02  2.342e-02  1.094e+03  -0.760  0.44716    
## Cases:wave.Q:TF2  -1.635e-02  2.091e-02  6.721e+02  -0.782  0.43458    
## Cases:wave.L:TF3   1.049e-02  2.406e-02  1.402e+03   0.436  0.66304    
## Cases:wave.Q:TF3  -2.287e-02  1.958e-02  8.905e+02  -1.168  0.24310    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```
## 
## Correlation matrix not shown by default, as p = 32 > 12.
## Use print(summary(m), correlation=TRUE)  or
##     vcov(summary(m))        if you need it
## 
## Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.
```

```
## [1] "++++++START++++++"
## [1] "AvoidBeh"
## Type III Analysis of Variance Table with Satterthwaite's method
##                   Sum Sq Mean Sq NumDF  DenDF  F value    Pr(>F)    
## TF1                0.136   0.136     1  251.2   0.7439 0.3892328    
## Cases              0.953   0.953     1 1866.4   5.2151 0.0225036 *  
## wave               8.215   4.108     2  286.8  22.4735 8.572e-10 ***
## TF2                0.324   0.324     1  247.7   1.7729 0.1842532    
## TF3                1.558   1.558     1  254.0   8.5235 0.0038207 ** 
## HouseholdMembers   0.078   0.078     1 1529.6   0.4286 0.5127629    
## Age                0.896   0.896     1  297.5   4.9008 0.0276023 *  
## WorkHome           2.671   2.671     1 3472.9  14.6129 0.0001343 ***
## MediaVal           1.240   1.240     1 3974.4   6.7852 0.0092261 ** 
## skepticism         8.669   8.669     1 3515.1  47.4301 6.724e-12 ***
## RiskGr             0.000   0.000     1 3001.6   0.0002 0.9883971    
## SocialDist        86.929  86.929     1 4152.0 475.5926 < 2.2e-16 ***
## PopulationDensity  3.492   3.492     1  259.9  19.1025 1.793e-05 ***
## TF1:Cases          0.525   0.525     1 2308.2   2.8710 0.0903260 .  
## TF1:wave           0.077   0.039     2  264.6   0.2115 0.8095007    
## Cases:wave         0.339   0.169     2  909.0   0.9273 0.3960035    
## Cases:TF2          0.156   0.156     1 1720.8   0.8508 0.3564589    
## wave:TF2           0.403   0.202     2  258.9   1.1032 0.3333520    
## Cases:TF3          0.031   0.031     1 2263.8   0.1716 0.6787163    
## wave:TF3           0.429   0.215     2  269.6   1.1746 0.3105140    
## TF1:Cases:wave     0.179   0.090     2  935.0   0.4904 0.6125200    
## Cases:wave:TF2     0.092   0.046     2  823.1   0.2514 0.7777645    
## Cases:wave:TF3     0.134   0.067     2 1038.1   0.3662 0.6934852    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## Linear mixed model fit by REML. t-tests use Satterthwaite's method ['lmerModLmerTest']
## Formula: eq
##    Data: tdf
## 
## REML criterion at convergence: 6486.3
## 
## Scaled residuals: 
##     Min      1Q  Median      3Q     Max 
## -5.7698 -0.4534  0.0617  0.4948  4.7019 
## 
## Random effects:
##  Groups     Name        Variance Std.Dev. Corr       
##  PROLIFICID (Intercept) 0.41118  0.6412              
##             wave.L      0.11705  0.3421    0.28      
##             wave.Q      0.01527  0.1236   -0.43  0.14
##  Residual               0.18278  0.4275              
## Number of obs: 4324, groups:  PROLIFICID, 267
## 
## Fixed effects:
##                     Estimate Std. Error         df t value Pr(>|t|)    
## (Intercept)       -5.652e-02  4.002e-02  2.477e+02  -1.412 0.159102    
## TF1               -6.436e-02  7.462e-02  2.512e+02  -0.863 0.389233    
## Cases              3.074e-02  1.346e-02  1.866e+03   2.284 0.022504 *  
## wave.L            -1.065e-01  2.545e-02  2.879e+02  -4.185 3.79e-05 ***
## wave.Q             7.827e-02  1.440e-02  2.886e+02   5.435 1.17e-07 ***
## TF2                6.902e-02  5.184e-02  2.477e+02   1.331 0.184253    
## TF3                1.894e-01  6.488e-02  2.540e+02   2.919 0.003821 ** 
## HouseholdMembers   1.596e-02  2.437e-02  1.530e+03   0.655 0.512763    
## Age                8.274e-02  3.737e-02  2.975e+02   2.214 0.027602 *  
## WorkHome           6.241e-02  1.633e-02  3.473e+03   3.823 0.000134 ***
## MediaVal          -2.161e-02  8.296e-03  3.974e+03  -2.605 0.009226 ** 
## skepticism        -1.291e-01  1.874e-02  3.515e+03  -6.887 6.72e-12 ***
## RiskGr            -2.799e-04  1.925e-02  3.002e+03  -0.015 0.988397    
## SocialDist         2.764e-01  1.268e-02  4.152e+03  21.808  < 2e-16 ***
## PopulationDensity -1.737e-01  3.975e-02  2.599e+02  -4.371 1.79e-05 ***
## TF1:Cases          3.456e-02  2.040e-02  2.308e+03   1.694 0.090326 .  
## TF1:wave.L        -1.459e-02  4.569e-02  2.630e+02  -0.319 0.749702    
## TF1:wave.Q         1.508e-02  2.592e-02  2.689e+02   0.582 0.561073    
## Cases:wave.L       1.429e-02  2.265e-02  1.231e+03   0.631 0.528275    
## Cases:wave.Q       1.587e-02  1.866e-02  7.170e+02   0.850 0.395384    
## Cases:TF2         -1.544e-02  1.674e-02  1.721e+03  -0.922 0.356459    
## wave.L:TF2         2.756e-02  3.185e-02  2.585e+02   0.865 0.387648    
## wave.Q:TF2         2.097e-02  1.800e-02  2.622e+02   1.165 0.245152    
## Cases:TF3         -6.811e-03  1.644e-02  2.264e+03  -0.414 0.678716    
## wave.L:TF3         5.996e-03  3.950e-02  2.657e+02   0.152 0.879440    
## wave.Q:TF3        -3.465e-02  2.262e-02  2.766e+02  -1.532 0.126777    
## TF1:Cases:wave.L  -2.859e-02  3.349e-02  1.287e+03  -0.854 0.393388    
## TF1:Cases:wave.Q   1.939e-02  2.717e-02  7.127e+02   0.714 0.475692    
## Cases:wave.L:TF2   1.484e-02  2.715e-02  1.104e+03   0.546 0.584913    
## Cases:wave.Q:TF2  -1.450e-02  2.356e-02  6.108e+02  -0.615 0.538477    
## Cases:wave.L:TF3  -1.315e-02  2.799e-02  1.438e+03  -0.470 0.638473    
## Cases:wave.Q:TF3  -1.217e-02  2.221e-02  8.131e+02  -0.548 0.583896    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```
## 
## Correlation matrix not shown by default, as p = 32 > 12.
## Use print(summary(m), correlation=TRUE)  or
##     vcov(summary(m))        if you need it
## 
## Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.
```

```
## [1] "++++++START++++++"
## [1] "InforSeek"
## Type III Analysis of Variance Table with Satterthwaite's method
##                    Sum Sq Mean Sq NumDF  DenDF F value    Pr(>F)    
## TF1                0.8732  0.8732     1  264.5  4.3479  0.038014 *  
## Cases              2.0831  2.0831     1 2063.2 10.3719  0.001299 ** 
## wave              30.4087 15.2043     2  281.8 75.7025 < 2.2e-16 ***
## TF2                0.0924  0.0924     1  260.8  0.4600  0.498236    
## TF3                1.2176  1.2176     1  267.2  6.0622  0.014442 *  
## HouseholdMembers   0.6730  0.6730     1 1903.7  3.3510  0.067320 .  
## Age                0.0806  0.0806     1  324.7  0.4014  0.526795    
## WorkHome           0.6242  0.6242     1 3847.2  3.1078  0.077999 .  
## MediaVal          13.6543 13.6543     1 3935.6 67.9848 2.221e-16 ***
## skepticism         0.6869  0.6869     1 3858.9  3.4201  0.064482 .  
## RiskGr             0.0369  0.0369     1 3380.6  0.1839  0.668100    
## SocialDist        12.9488 12.9488     1 4206.4 64.4722 1.260e-15 ***
## PopulationDensity  0.0230  0.0230     1  273.9  0.1147  0.735095    
## TF1:Cases          0.0179  0.0179     1 2474.9  0.0889  0.765624    
## TF1:wave           0.0590  0.0295     2  263.6  0.1468  0.863515    
## Cases:wave         0.7140  0.3570     2 1025.5  1.7776  0.169566    
## Cases:TF2          0.8146  0.8146     1 1843.3  4.0560  0.044161 *  
## wave:TF2           0.3223  0.1611     2  258.6  0.8022  0.449435    
## Cases:TF3          0.1453  0.1453     1 2404.7  0.7236  0.395055    
## wave:TF3           1.1257  0.5628     2  268.3  2.8024  0.062442 .  
## TF1:Cases:wave     0.7976  0.3988     2 1049.0  1.9856  0.137817    
## Cases:wave:TF2     0.2446  0.1223     2  922.9  0.6089  0.544171    
## Cases:wave:TF3     0.6567  0.3283     2 1162.7  1.6348  0.195444    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## Linear mixed model fit by REML. t-tests use Satterthwaite's method ['lmerModLmerTest']
## Formula: eq
##    Data: tdf
## 
## REML criterion at convergence: 7095.9
## 
## Scaled residuals: 
##     Min      1Q  Median      3Q     Max 
## -4.2295 -0.4880  0.0104  0.4871  5.9842 
## 
## Random effects:
##  Groups     Name        Variance Std.Dev. Corr       
##  PROLIFICID (Intercept) 0.67274  0.8202              
##             wave.L      0.16576  0.4071    0.26      
##             wave.Q      0.03781  0.1944   -0.55  0.13
##  Residual               0.20084  0.4482              
## Number of obs: 4324, groups:  PROLIFICID, 267
## 
## Fixed effects:
##                     Estimate Std. Error         df t value Pr(>|t|)    
## (Intercept)       -8.544e-02  5.090e-02  2.600e+02  -1.679   0.0944 .  
## TF1                1.975e-01  9.470e-02  2.645e+02   2.085   0.0380 *  
## Cases              4.735e-02  1.470e-02  2.063e+03   3.221   0.0013 ** 
## wave.L            -1.420e-01  2.923e-02  2.854e+02  -4.857 1.97e-06 ***
## wave.Q             1.995e-01  1.757e-02  2.820e+02  11.352  < 2e-16 ***
## TF2               -4.469e-02  6.590e-02  2.608e+02  -0.678   0.4982    
## TF3               -2.025e-01  8.224e-02  2.672e+02  -2.462   0.0144 *  
## HouseholdMembers   4.990e-02  2.726e-02  1.904e+03   1.831   0.0673 .  
## Age                2.832e-02  4.469e-02  3.247e+02   0.634   0.5268    
## WorkHome           3.141e-02  1.782e-02  3.847e+03   1.763   0.0780 .  
## MediaVal          -7.233e-02  8.772e-03  3.936e+03  -8.245 2.22e-16 ***
## skepticism        -3.762e-02  2.034e-02  3.859e+03  -1.849   0.0645 .  
## RiskGr             8.994e-03  2.097e-02  3.381e+03   0.429   0.6681    
## SocialDist         1.093e-01  1.362e-02  4.206e+03   8.029 1.26e-15 ***
## PopulationDensity -1.623e-02  4.791e-02  2.739e+02  -0.339   0.7351    
## TF1:Cases         -6.589e-03  2.210e-02  2.475e+03  -0.298   0.7656    
## TF1:wave.L         2.770e-02  5.264e-02  2.639e+02   0.526   0.5992    
## TF1:wave.Q        -4.264e-03  3.176e-02  2.668e+02  -0.134   0.8933    
## Cases:wave.L       1.273e-02  2.435e-02  1.344e+03   0.523   0.6013    
## Cases:wave.Q      -3.911e-02  2.086e-02  7.782e+02  -1.875   0.0612 .  
## Cases:TF2          3.682e-02  1.828e-02  1.843e+03   2.014   0.0442 *  
## wave.L:TF2         3.893e-02  3.673e-02  2.597e+02   1.060   0.2902    
## wave.Q:TF2         1.519e-02  2.210e-02  2.609e+02   0.687   0.4926    
## Cases:TF3         -1.513e-02  1.779e-02  2.405e+03  -0.851   0.3951    
## wave.L:TF3        -1.073e-01  4.548e-02  2.663e+02  -2.360   0.0190 *  
## wave.Q:TF3         6.375e-03  2.766e-02  2.738e+02   0.230   0.8179    
## TF1:Cases:wave.L  -4.995e-02  3.601e-02  1.395e+03  -1.387   0.1656    
## TF1:Cases:wave.Q  -3.130e-02  3.029e-02  7.946e+02  -1.033   0.3017    
## Cases:wave.L:TF2   1.814e-02  2.919e-02  1.199e+03   0.621   0.5345    
## Cases:wave.Q:TF2   1.741e-02  2.646e-02  6.669e+02   0.658   0.5109    
## Cases:wave.L:TF3   4.489e-02  3.005e-02  1.549e+03   1.494   0.1355    
## Cases:wave.Q:TF3   1.411e-02  2.471e-02  8.984e+02   0.571   0.5681    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```
## 
## Correlation matrix not shown by default, as p = 32 > 12.
## Use print(summary(m), correlation=TRUE)  or
##     vcov(summary(m))        if you need it
## 
## Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.Scale for colour is already present.
## Adding another scale for colour, which will replace the existing scale.
```

```r
print(patchwork::wrap_plots(plist, ncol=length(vars), heights=c(0.5, 0.5)))
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-1.png)

```r
print(patchwork::wrap_plots(plist2, ncol=length(vars), heights=c(0.5, 0.5)))
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-2.png)

```r
for (x in c(1,2,3,4)) {
print(car::vif(mlist[[x]]))
}
```

```
##                        GVIF Df GVIF^(1/(2*Df))
## TF1                4.499555  1        2.121215
## Cases              2.032035  1        1.425495
## wave               1.134956  2        1.032155
## TF2                2.044512  1        1.429864
## TF3                3.439248  1        1.854521
## HouseholdMembers   1.047526  1        1.023487
## Age                1.071165  1        1.034971
## WorkHome           1.056609  1        1.027915
## MediaVal           1.074552  1        1.036606
## skepticism         1.043513  1        1.021525
## RiskGr             1.031667  1        1.015710
## SocialDist         1.120139  1        1.058366
## PopulationDensity  1.115031  1        1.055950
## TF1:Cases          5.122043  1        2.263193
## TF1:wave          15.580881  2        1.986772
## Cases:wave         2.958692  2        1.311520
## Cases:TF2          3.586446  1        1.893791
## wave:TF2           3.262343  2        1.343948
## Cases:TF3          3.066501  1        1.751143
## wave:TF3           8.965295  2        1.730379
## TF1:Cases:wave    22.041980  2        2.166769
## Cases:wave:TF2    11.077248  2        1.824349
## Cases:wave:TF3     6.595423  2        1.602547
##                        GVIF Df GVIF^(1/(2*Df))
## TF1                3.595524  1        1.896187
## Cases              2.009248  1        1.417480
## wave               1.153692  2        1.036388
## TF2                1.650582  1        1.284750
## TF3                2.730711  1        1.652486
## HouseholdMembers   1.018489  1        1.009202
## Age                1.061740  1        1.030408
## WorkHome           1.049044  1        1.024228
## MediaVal           1.073768  1        1.036228
## skepticism         1.023370  1        1.011618
## RiskGr             1.013171  1        1.006564
## SocialDist         1.094163  1        1.046023
## PopulationDensity  1.074236  1        1.036454
## TF1:Cases          5.214829  1        2.283600
## TF1:wave          12.708774  2        1.888104
## Cases:wave         2.863479  2        1.300839
## Cases:TF2          3.517124  1        1.875400
## wave:TF2           2.645220  2        1.275309
## Cases:TF3          3.120923  1        1.766613
## wave:TF3           7.305634  2        1.644048
## TF1:Cases:wave    22.343956  2        2.174153
## Cases:wave:TF2    10.731037  2        1.809924
## Cases:wave:TF3     6.717454  2        1.609908
##                        GVIF Df GVIF^(1/(2*Df))
## TF1                3.565919  1        1.888364
## Cases              1.856533  1        1.362547
## wave               1.196260  2        1.045819
## TF2                1.631709  1        1.277384
## TF3                2.710497  1        1.646359
## HouseholdMembers   1.023814  1        1.011837
## Age                1.066196  1        1.032567
## WorkHome           1.049177  1        1.024293
## MediaVal           1.097189  1        1.047468
## skepticism         1.026743  1        1.013283
## RiskGr             1.015647  1        1.007793
## SocialDist         1.100591  1        1.049091
## PopulationDensity  1.079589  1        1.039033
## TF1:Cases          4.859776  1        2.204490
## TF1:wave          12.689227  2        1.887378
## Cases:wave         2.696795  2        1.281480
## Cases:TF2          3.270095  1        1.808340
## wave:TF2           2.627609  2        1.273181
## Cases:TF3          2.902667  1        1.703722
## wave:TF3           7.332290  2        1.645546
## TF1:Cases:wave    21.046348  2        2.141875
## Cases:wave:TF2    10.147564  2        1.784804
## Cases:wave:TF3     6.322057  2        1.585677
##                        GVIF Df GVIF^(1/(2*Df))
## TF1                3.820508  1        1.954612
## Cases              1.799182  1        1.341336
## wave               1.162800  2        1.038428
## TF2                1.743665  1        1.320479
## TF3                2.908376  1        1.705396
## HouseholdMembers   1.033446  1        1.016586
## Age                1.066758  1        1.032840
## WorkHome           1.051906  1        1.025625
## MediaVal           1.078404  1        1.038462
## skepticism         1.028256  1        1.014029
## RiskGr             1.021180  1        1.010535
## SocialDist         1.113112  1        1.055041
## PopulationDensity  1.100296  1        1.048950
## TF1:Cases          4.722945  1        2.173234
## TF1:wave          13.450893  2        1.915084
## Cases:wave         2.654172  2        1.276387
## Cases:TF2          3.154956  1        1.776220
## wave:TF2           2.795639  2        1.293065
## Cases:TF3          2.823957  1        1.680463
## wave:TF3           7.764676  2        1.669286
## TF1:Cases:wave    20.942652  2        2.139232
## Cases:wave:TF2     9.966625  2        1.776794
## Cases:wave:TF3     6.291453  2        1.583754
```

```r
#print(patchwork::wrap_plots(plist, ncol=length(vars), heights=c(0.5, 0.5)))
```

### Wave model posthocs 
### Threat perception 
[1] "CurrThreat"
Type III Analysis of Variance Table with Satterthwaite's method
                   Sum Sq Mean Sq NumDF  DenDF  F value    Pr(>F)    
TF1                 1.532   1.532     1  260.1   1.9523 0.1635280    
Cases              34.922  34.922     1 2411.3  44.5013 3.142e-11 ***
wave              195.163  97.581     2  283.5 124.3479 < 2.2e-16 ***
TF2                 2.057   2.057     1  255.0   2.6206 0.1067190    
TF3                 0.009   0.009     1  265.0   0.0110 0.9164788    
HouseholdMembers    2.798   2.798     1  705.8   3.5653 0.0594095 .  
Age                 7.082   7.082     1  270.4   9.0248 0.0029131 ** 
WorkHome            1.297   1.297     1 2308.9   1.6527 0.1987187    
MediaVal          268.076 268.076     1 4001.6 341.6086 < 2.2e-16 ***
skepticism         98.924  98.924     1 1600.9 126.0587 < 2.2e-16 ***
RiskGr              9.325   9.325     1 1347.2  11.8827 0.0005840 ***
SocialDist         40.076  40.076     1 3887.9  51.0683 1.060e-12 ***
PopulationDensity   0.272   0.272     1  259.9   0.3464 0.5566658    
TF1:Cases           0.226   0.226     1 2817.6   0.2874 0.5919557    
TF1:wave            0.842   0.421     2  265.4   0.5367 0.5852929    
Cases:wave         11.158   5.579     2 1235.4   7.1095 0.0008512 ***
Cases:TF2           0.403   0.403     1 2352.9   0.5139 0.4735163    
wave:TF2            1.546   0.773     2  261.1   0.9849 0.3748637    
Cases:TF3          10.088  10.088     1 2747.4  12.8548 0.0003425 ***
wave:TF3            6.577   3.288     2  270.3   4.1905 0.0161342 *  
TF1:Cases:wave      0.030   0.015     2 1258.8   0.0194 0.9808108    
Cases:wave:TF2      0.749   0.375     2 1140.2   0.4774 0.6205487    
Cases:wave:TF3      3.777   1.889     2 1377.7   2.4067 0.0904866 . 


```r
emm_options(lmerTest.limit = 5571)
emm_options(pbkrtest.limit = 5570)

## Threat ~ Cases
slopes <- modelbased::estimate_slopes(mlist[[1]], trend="Cases") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |       95% CI | t(2407.93) |      p
## -------------------------------------------------------
## 0.20        | 0.03 | [0.14, 0.26] |       6.71 | < .001
## Marginal effects estimated for Cases
```

```r
## Threat ~ Cases * wave
slopes <- modelbased::estimate_slopes(mlist[[1]], trend="Cases", at = "wave") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          | Coefficient |   SE |       95% CI |    t |      df |      p
## ---------------------------------------------------------------------------
## first_wave    |        0.21 | 0.07 | [0.08, 0.34] | 3.14 | 1219.65 | 0.002 
## summer_nowave |        0.30 | 0.05 | [0.20, 0.39] | 6.29 |  959.61 | < .001
## second_wave   |        0.10 | 0.03 | [0.05, 0.15] | 3.87 | 3357.82 | < .001
## Marginal effects estimated for Cases
```

```r
print(plot(slopes) + ggtitle(paste0("Threat ~ Cases*wave")))
```

![plot of chunk unnamed-chunk-7](figure/unnamed-chunk-7-1.png)

```r
emtr<-emtrends(mlist[[1]], pairwise ~ wave, var = "Cases", ,  adjust = "holm")
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(emtr)
```

```
## $emtrends
##  wave          Cases.trend     SE   df lower.CL upper.CL
##  first_wave          0.207 0.0660 1220   0.0778    0.337
##  summer_nowave       0.298 0.0474  960   0.2050    0.391
##  second_wave         0.102 0.0264 3358   0.0504    0.154
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                    estimate     SE   df t.ratio p.value
##  first_wave - summer_nowave   -0.0909 0.0772 1135  -1.178  0.2752
##  first_wave - second_wave      0.1050 0.0707 1470   1.486  0.2752
##  summer_nowave - second_wave   0.1959 0.0525 1137   3.735  0.0006
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```

```r
## Threat ~ T3 * wave
slopes <- modelbased::estimate_slopes(mlist[[1]], trend="TF3", at = "wave") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          | Coefficient |   SE |        95% CI |     t |     df |     p
## ---------------------------------------------------------------------------
## first_wave    |        0.10 | 0.12 | [-0.14, 0.34] |  0.82 | 276.08 | 0.415
## summer_nowave |        0.10 | 0.12 | [-0.14, 0.33] |  0.81 | 273.10 | 0.418
## second_wave   |       -0.16 | 0.09 | [-0.34, 0.01] | -1.86 | 267.39 | 0.064
## Marginal effects estimated for TF3
```

```r
print(plot(slopes) + ggtitle(paste0("Threat ~ TF3*wave")))
```

![plot of chunk unnamed-chunk-7](figure/unnamed-chunk-7-2.png)

```r
emtr<-emtrends(mlist[[1]], pairwise ~ wave, var = "TF3", ,  adjust = "holm")
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(emtr)
```

```
## $emtrends
##  wave          TF3.trend     SE  df lower.CL upper.CL
##  first_wave       0.1003 0.1228 276   -0.142  0.34208
##  summer_nowave    0.0972 0.1199 273   -0.139  0.33336
##  second_wave     -0.1633 0.0878 267   -0.336  0.00948
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                    estimate     SE  df t.ratio p.value
##  first_wave - summer_nowave   0.00305 0.0946 274   0.032  0.9743
##  first_wave - second_wave     0.26355 0.1119 271   2.354  0.0385
##  summer_nowave - second_wave  0.26050 0.0942 269   2.767  0.0182
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```

```r
## Threat ~ TF3 * Cases
slopes <- modelbased::estimate_slopes(mlist[[1]], trend="TF3", at = "Cases") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Cases | Coefficient |   SE |         95% CI |     t |      df |      p
## ----------------------------------------------------------------------
## -1.97 |        0.26 | 0.12 | [ 0.03,  0.50] |  2.23 |  585.99 | 0.026 
## -0.24 |        0.04 | 0.10 | [-0.15,  0.23] |  0.43 |  275.39 | 0.664 
## 1.48  |       -0.18 | 0.11 | [-0.40,  0.03] | -1.66 |  451.32 | 0.097 
## 3.20  |       -0.40 | 0.15 | [-0.70, -0.11] | -2.69 | 1181.38 | 0.007 
## 4.93  |       -0.63 | 0.20 | [-1.02, -0.23] | -3.10 | 2024.15 | 0.002 
## 6.65  |       -0.85 | 0.26 | [-1.36, -0.34] | -3.28 | 2468.55 | 0.001 
## 8.38  |       -1.07 | 0.32 | [-1.70, -0.45] | -3.37 | 2640.97 | < .001
## 10.10 |       -1.30 | 0.38 | [-2.04, -0.55] | -3.43 | 2705.66 | < .001
## 11.82 |       -1.52 | 0.44 | [-2.38, -0.66] | -3.46 | 2730.83 | < .001
## 13.55 |       -1.74 | 0.50 | [-2.72, -0.76] | -3.48 | 2740.92 | < .001
## Marginal effects estimated for TF3
```

```r
print(plot(slopes) + ggtitle(paste0("Threat ~ TF3*Cases")))
```

![plot of chunk unnamed-chunk-7](figure/unnamed-chunk-7-3.png)

```r
emtr<-emtrends(mlist[[1]], pairwise ~ Cases, var = "TF3", ,  adjust = "holm")
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(emtr)
```

```
## $emtrends
##    Cases TF3.trend    SE  df lower.CL upper.CL
##  -0.0111    0.0114 0.095 272   -0.176    0.198
## 
## Results are averaged over the levels of: wave 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast  estimate SE df z.ratio p.value
##  (nothing)   nonEst NA NA      NA      NA
## 
## Results are averaged over the levels of: wave 
## Degrees-of-freedom method: kenward-roger
```

## Risk

[1] "ProbEst"
Type III Analysis of Variance Table with Satterthwaite's method
                  Sum Sq Mean Sq NumDF  DenDF F value    Pr(>F)    
TF1               0.0440  0.0440     1  263.7  0.4454   0.50510    
Cases             0.6120  0.6120     1 2456.1  6.1959   0.01287 *  
wave              5.9750  2.9875     2  290.1 30.2473 1.171e-12 ***
TF2               0.5514  0.5514     1  259.9  5.5828   0.01887 *  
TF3               0.0278  0.0278     1  264.3  0.2814   0.59624    
HouseholdMembers  0.0598  0.0598     1 3400.6  0.6051   0.43669    
Age               0.0021  0.0021     1  429.2  0.0216   0.88314    
WorkHome          0.3573  0.3573     1 3833.4  3.6176   0.05724 .  
MediaVal          3.9716  3.9716     1 3897.2 40.2111 2.540e-10 ***
skepticism        0.4123  0.4123     1 4246.3  4.1742   0.04111 *  
RiskGr            0.6490  0.6490     1 4228.0  6.5708   0.01040 *  
SocialDist        4.2837  4.2837     1 4047.3 43.3703 5.111e-11 ***
PopulationDensity 0.2121  0.2121     1  265.7  2.1479   0.14395    
TF1:Cases         0.3771  0.3771     1 2895.8  3.8178   0.05081 .  
TF1:wave          0.4676  0.2338     2  268.3  2.3671   0.09571 .  
Cases:wave        0.0616  0.0308     2 1133.9  0.3120   0.73202    
Cases:TF2         0.0691  0.0691     1 2292.7  0.6999   0.40290    
wave:TF2          0.1352  0.0676     2  262.7  0.6843   0.50534    
Cases:TF3         0.1274  0.1274     1 2836.8  1.2895   0.25624    
wave:TF3          0.3736  0.1868     2  273.2  1.8915   0.15282    
TF1:Cases:wave    0.2041  0.1021     2 1202.7  1.0334   0.35610    
Cases:wave:TF2    0.2348  0.1174     2 1042.6  1.1889   0.30498    
Cases:wave:TF3    0.1201  0.0601     2 1313.3  0.6081   0.54454    
  


```r
## Risk ~ TF2
slopes <- modelbased::estimate_slopes(mlist[[2]], trend="TF2") 
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |       95% CI | t(261.06) |     p
## -----------------------------------------------------
## 0.17        | 0.07 | [0.03, 0.30] |      2.36 | 0.019
## Marginal effects estimated for TF2
```

```r
emtr<-emtrends(mlist[[2]], pairwise ~ 1, var = "TF2",  adjust = "holm")
print(emtr)
```

```
## $emtrends
##  1       TF2.trend   SE  df lower.CL upper.CL
##  overall     0.166 0.07 261   0.0277    0.304
## 
## Results are averaged over the levels of: wave 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast  estimate SE df z.ratio p.value
##  (nothing)   nonEst NA NA      NA      NA
## 
## Results are averaged over the levels of: wave 
## Degrees-of-freedom method: kenward-roger
```

```r
## Risk ~ Cases
slopes <- modelbased::estimate_slopes(mlist[[2]], trend="Cases") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |       95% CI | t(2457.17) |     p
## ------------------------------------------------------
## 0.03        | 0.01 | [0.01, 0.05] |       2.46 | 0.014
## Marginal effects estimated for Cases
```

```r
emtr<-emtrends(mlist[[2]], pairwise ~ 1, var = "Cases",  adjust = "holm")
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(emtr)
```

```
## $emtrends
##  1       Cases.trend     SE   df lower.CL upper.CL
##  overall      0.0266 0.0108 2457  0.00536   0.0478
## 
## Results are averaged over the levels of: wave 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast  estimate SE df z.ratio p.value
##  (nothing)   nonEst NA NA      NA      NA
## 
## Results are averaged over the levels of: wave 
## Degrees-of-freedom method: kenward-roger
```
### Worry
[1] "Worry"
Type III Analysis of Variance Table with Satterthwaite's method
                  Sum Sq Mean Sq NumDF  DenDF F value    Pr(>F)    
TF1               0.3066  0.3066     1  260.3  2.3033 0.1303120    
Cases             1.7148  1.7148     1 2032.8 12.8825 0.0003395 ***
wave              7.1023  3.5511     2  296.5 26.6786 2.217e-11 ***
TF2               0.9333  0.9333     1  256.9  7.0113 0.0086009 ** 
TF3               0.5033  0.5033     1  261.5  3.7811 0.0529073 .  
HouseholdMembers  0.0897  0.0897     1 2627.7  0.6740 0.4117245    
Age               0.7679  0.7679     1  359.0  5.7688 0.0168209 *  
WorkHome          0.0179  0.0179     1 3593.0  0.1341 0.7142034    
MediaVal          7.7952  7.7952     1 3981.6 58.5632 2.454e-14 ***
skepticism        0.4863  0.4863     1 4056.7  3.6533 0.0560281 .  
RiskGr            0.7318  0.7318     1 3804.7  5.4976 0.0190939 *  
SocialDist        9.2547  9.2547     1 4059.2 69.5282 < 2.2e-16 ***
PopulationDensity 0.1580  0.1580     1  264.0  1.1873 0.2768674    
TF1:Cases         0.0775  0.0775     1 2476.3  0.5820 0.4455898    
TF1:wave          0.7647  0.3823     2  268.0  2.8724 0.0583074 .  
Cases:wave        0.6751  0.3376     2  940.8  2.5361 0.0797181 .  
Cases:TF2         0.0000  0.0000     1 1956.9  0.0001 0.9908533    
wave:TF2          0.1637  0.0818     2  262.3  0.6147 0.5415587    
Cases:TF3         0.0494  0.0494     1 2411.5  0.3712 0.5423969    
wave:TF3          1.7266  0.8633     2  273.2  6.4856 0.0017708 ** 
TF1:Cases:wave    0.0590  0.0295     2  964.2  0.2218 0.8011264    
Cases:wave:TF2    0.2564  0.1282     2  861.1  0.9632 0.3820647    
Cases:wave:TF3    0.1821  0.0910     2 1064.6  0.6840 0.5048367    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
Linear mixed model fit by REML. t-tests use Satterthwaite's method   

```r
## Worry ~ TF2
slopes <- modelbased::estimate_slopes(mlist[[3]], trend="TF2") 
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |       95% CI | t(260.76) |     p
## -----------------------------------------------------
## 0.16        | 0.06 | [0.04, 0.28] |      2.65 | 0.009
## Marginal effects estimated for TF2
```

```r
emtr<-emtrends(mlist[[3]], pairwise ~ 1, var = "TF2",  adjust = "holm")
print(emtr)
```

```
## $emtrends
##  1       TF2.trend     SE  df lower.CL upper.CL
##  overall     0.163 0.0617 261   0.0419    0.285
## 
## Results are averaged over the levels of: wave 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast  estimate SE df z.ratio p.value
##  (nothing)   nonEst NA NA      NA      NA
## 
## Results are averaged over the levels of: wave 
## Degrees-of-freedom method: kenward-roger
```

```r
## Worry ~ TF1
slopes <- modelbased::estimate_slopes(mlist[[3]], trend="TF1") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |        95% CI | t(264.26) |     p
## ------------------------------------------------------
## 0.13        | 0.09 | [-0.04, 0.31] |      1.52 | 0.131
## Marginal effects estimated for TF1
```

```r
## Worry ~ TF3
slopes <- modelbased::estimate_slopes(mlist[[3]], trend="TF3") 
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |        95% CI | t(265.53) |     p
## ------------------------------------------------------
## 0.15        | 0.08 | [ 0.00, 0.30] |      1.95 | 0.053
## Marginal effects estimated for TF3
```

```r
## Worry ~ T3 * wave
slopes <- modelbased::estimate_slopes(mlist[[3]], trend="TF3", at = "wave") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          | Coefficient |   SE |        95% CI |    t |     df |     p
## --------------------------------------------------------------------------
## first_wave    |        0.18 | 0.08 | [ 0.02, 0.33] | 2.23 | 268.30 | 0.027
## summer_nowave |        0.19 | 0.08 | [ 0.03, 0.35] | 2.36 | 267.06 | 0.019
## second_wave   |        0.08 | 0.08 | [-0.07, 0.24] | 1.05 | 266.12 | 0.296
## Marginal effects estimated for TF3
```

```r
print(plot(slopes) + ggtitle(paste0("Worry ~ TF3*wave")))
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9-1.png)

```r
emtr<-emtrends(mlist[[3]], pairwise ~ wave, var = "TF3", ,  adjust = "holm")
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(emtr)
```

```
## $emtrends
##  wave          TF3.trend     SE  df lower.CL upper.CL
##  first_wave        0.176 0.0790 268   0.0206    0.332
##  summer_nowave     0.190 0.0808 267   0.0313    0.349
##  second_wave       0.083 0.0793 266  -0.0731    0.239
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                    estimate     SE  df t.ratio p.value
##  first_wave - summer_nowave   -0.0142 0.0343 274  -0.414  0.6795
##  first_wave - second_wave      0.0932 0.0411 271   2.269  0.0482
##  summer_nowave - second_wave   0.1074 0.0300 270   3.582  0.0012
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```

```r
## Worry ~ Cases * wave
slopes <- modelbased::estimate_slopes(mlist[[3]], trend="Cases", at = "wave") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          | Coefficient |   SE |        95% CI |    t |      df |      p
## ----------------------------------------------------------------------------
## first_wave    |        0.07 | 0.03 | [ 0.02, 0.12] | 2.62 | 1036.66 | 0.009 
## summer_nowave |        0.01 | 0.02 | [-0.02, 0.04] | 0.65 |  683.04 | 0.515 
## second_wave   |        0.05 | 0.01 | [ 0.02, 0.07] | 4.23 | 3531.09 | < .001
## Marginal effects estimated for Cases
```

```r
print(plot(slopes) + ggtitle(paste0("Worry ~ Cases*wave")))
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9-2.png)

```r
emtr<-emtrends(mlist[[3]], pairwise ~ wave, var = "Cases", ,  adjust = "holm")
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(emtr)
```

```
## $emtrends
##  wave          Cases.trend     SE   df lower.CL upper.CL
##  first_wave         0.0680 0.0259 1037   0.0171   0.1189
##  summer_nowave      0.0111 0.0170  683  -0.0223   0.0444
##  second_wave        0.0465 0.0110 3531   0.0249   0.0680
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                    estimate     SE   df t.ratio p.value
##  first_wave - summer_nowave    0.0569 0.0293  940   1.945  0.1564
##  first_wave - second_wave      0.0215 0.0279 1254   0.772  0.4402
##  summer_nowave - second_wave  -0.0354 0.0191  834  -1.857  0.1564
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```



## Avoidance
[1] "AvoidBeh"
Type III Analysis of Variance Table with Satterthwaite's method
                  Sum Sq Mean Sq NumDF  DenDF  F value    Pr(>F)    
TF1                0.136   0.136     1  251.2   0.7439 0.3892328    
Cases              0.953   0.953     1 1866.4   5.2151 0.0225036 *  
wave               8.215   4.108     2  286.8  22.4735 8.572e-10 ***
TF2                0.324   0.324     1  247.7   1.7729 0.1842532    
TF3                1.558   1.558     1  254.0   8.5235 0.0038207 ** 
HouseholdMembers   0.078   0.078     1 1529.6   0.4286 0.5127629    
Age                0.896   0.896     1  297.5   4.9008 0.0276023 *  
WorkHome           2.671   2.671     1 3472.9  14.6129 0.0001343 ***
MediaVal           1.240   1.240     1 3974.4   6.7852 0.0092261 ** 
skepticism         8.669   8.669     1 3515.1  47.4301 6.724e-12 ***
RiskGr             0.000   0.000     1 3001.6   0.0002 0.9883971    
SocialDist        86.929  86.929     1 4152.0 475.5926 < 2.2e-16 ***
PopulationDensity  3.492   3.492     1  259.9  19.1025 1.793e-05 ***
TF1:Cases          0.525   0.525     1 2308.2   2.8710 0.0903260 .  
TF1:wave           0.077   0.039     2  264.6   0.2115 0.8095007    
Cases:wave         0.339   0.169     2  909.0   0.9273 0.3960035    
Cases:TF2          0.156   0.156     1 1720.8   0.8508 0.3564589    
wave:TF2           0.403   0.202     2  258.9   1.1032 0.3333520    
Cases:TF3          0.031   0.031     1 2263.8   0.1716 0.6787163    
wave:TF3           0.429   0.215     2  269.6   1.1746 0.3105140    
TF1:Cases:wave     0.179   0.090     2  935.0   0.4904 0.6125200    
Cases:wave:TF2     0.092   0.046     2  823.1   0.2514 0.7777645    
Cases:wave:TF3     0.134   0.067     2 1038.1   0.3662 0.6934852    

```r
## Avoidance ~ Cases
slopes <- modelbased::estimate_slopes(mlist[[4]], trend="Cases") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |       95% CI | t(1875.56) |     p
## ------------------------------------------------------
## 0.03        | 0.01 | [0.00, 0.06] |       2.24 | 0.025
## Marginal effects estimated for Cases
```

```r
## Avoidance ~ TF3
slopes <- modelbased::estimate_slopes(mlist[[4]], trend="TF3") 
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |       95% CI | t(267.45) |     p
## -----------------------------------------------------
## 0.19        | 0.06 | [0.06, 0.32] |      2.92 | 0.004
## Marginal effects estimated for TF3
```

```r
## Avoidance ~ Cases * wave
slopes <- modelbased::estimate_slopes(mlist[[4]], trend="Cases", at = "wave") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          | Coefficient |   SE |        95% CI |    t |      df |      p
## ----------------------------------------------------------------------------
## first_wave    |        0.03 | 0.03 | [-0.03, 0.08] | 0.88 |  970.57 | 0.378 
## summer_nowave |        0.02 | 0.02 | [-0.02, 0.06] | 0.88 |  659.88 | 0.377 
## second_wave   |        0.05 | 0.01 | [ 0.02, 0.07] | 3.65 | 3679.64 | < .001
## Marginal effects estimated for Cases
```

```r
print(plot(slopes) + ggtitle(paste0("Avoid ~ Cases*wave")))
```

![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-10-1.png)

```r
emtr<-emtrends(mlist[[4]], pairwise ~ wave, var = "Cases",  adjust = "holm")
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(emtr)
```

```
## $emtrends
##  wave          Cases.trend     SE   df lower.CL upper.CL
##  first_wave         0.0261 0.0296  971   -0.032   0.0843
##  summer_nowave      0.0172 0.0194  660   -0.021   0.0553
##  second_wave        0.0475 0.0130 3680    0.022   0.0731
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                    estimate     SE   df t.ratio p.value
##  first_wave - summer_nowave   0.00895 0.0332  837   0.270  1.0000
##  first_wave - second_wave    -0.02143 0.0323 1279  -0.663  1.0000
##  summer_nowave - second_wave -0.03037 0.0221  861  -1.374  0.5096
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```

```r
## Avoidance ~ TF3 * wave
slopes <- modelbased::estimate_slopes(mlist[[4]], trend="TF3", at = "wave") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          | Coefficient |   SE |       95% CI |    t |     df |     p
## -------------------------------------------------------------------------
## first_wave    |        0.17 | 0.06 | [0.04, 0.30] | 2.64 | 270.48 | 0.009
## summer_nowave |        0.22 | 0.07 | [0.08, 0.36] | 3.07 | 269.67 | 0.002
## second_wave   |        0.18 | 0.07 | [0.03, 0.33] | 2.43 | 269.41 | 0.016
## Marginal effects estimated for TF3
```

```r
print(plot(slopes) + ggtitle(paste0("Avoid ~ TF3*wave")))
```

![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-10-2.png)

```r
emtr<-emtrends(mlist[[4]], pairwise ~ wave, var = "TF3",  adjust = "holm")
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(emtr)
```

```
## $emtrends
##  wave          TF3.trend     SE  df lower.CL upper.CL
##  first_wave        0.171 0.0648 270   0.0435    0.299
##  summer_nowave     0.218 0.0709 270   0.0781    0.357
##  second_wave       0.180 0.0741 269   0.0339    0.326
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                    estimate     SE  df t.ratio p.value
##  first_wave - summer_nowave  -0.04661 0.0405 275  -1.151  0.7524
##  first_wave - second_wave    -0.00869 0.0558 270  -0.156  0.8765
##  summer_nowave - second_wave  0.03793 0.0382 269   0.994  0.7524
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```

```r
## Avoidance ~ TF3 * Cases * wave
slopes <- modelbased::estimate_slopes(mlist[[4]], trend="Cases", at = c("wave", "TF3") )
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          |   TF3 | Coefficient |   SE |        95% CI |     t |      df |      p
## -------------------------------------------------------------------------------------
## first_wave    | -1.94 |        0.03 | 0.08 | [-0.14, 0.20] |  0.36 | 1000.49 | 0.715 
## summer_nowave | -1.94 |        0.01 | 0.05 | [-0.10, 0.12] |  0.21 |  714.53 | 0.837 
## second_wave   | -1.94 |        0.09 | 0.04 | [ 0.00, 0.17] |  2.06 | 3633.76 | 0.039 
## first_wave    | -1.43 |        0.03 | 0.07 | [-0.10, 0.16] |  0.43 | 1012.79 | 0.664 
## summer_nowave | -1.43 |        0.01 | 0.04 | [-0.07, 0.10] |  0.29 |  688.82 | 0.769 
## second_wave   | -1.43 |        0.08 | 0.03 | [ 0.01, 0.14] |  2.34 | 3645.33 | 0.019 
## first_wave    | -0.93 |        0.03 | 0.05 | [-0.07, 0.13] |  0.54 | 1025.26 | 0.589 
## summer_nowave | -0.93 |        0.01 | 0.03 | [-0.05, 0.08] |  0.43 |  656.72 | 0.666 
## second_wave   | -0.93 |        0.07 | 0.02 | [ 0.02, 0.11] |  2.79 | 3664.27 | 0.005 
## first_wave    | -0.42 |        0.03 | 0.04 | [-0.05, 0.10] |  0.71 | 1022.69 | 0.478 
## summer_nowave | -0.42 |        0.02 | 0.02 | [-0.03, 0.06] |  0.66 |  631.17 | 0.511 
## second_wave   | -0.42 |        0.06 | 0.02 | [ 0.02, 0.09] |  3.45 | 3689.20 | < .001
## first_wave    |  0.09 |        0.03 | 0.03 | [-0.03, 0.08] |  0.93 |  936.40 | 0.352 
## summer_nowave |  0.09 |        0.02 | 0.02 | [-0.02, 0.05] |  0.95 |  689.97 | 0.345 
## second_wave   |  0.09 |        0.05 | 0.01 | [ 0.02, 0.07] |  3.48 | 3663.31 | < .001
## first_wave    |  0.60 |        0.02 | 0.03 | [-0.03, 0.08] |  0.94 |  761.79 | 0.349 
## summer_nowave |  0.60 |        0.02 | 0.02 | [-0.02, 0.06] |  1.00 |  917.38 | 0.319 
## second_wave   |  0.60 |        0.03 | 0.02 | [ 0.00, 0.07] |  2.03 | 3588.61 | 0.043 
## first_wave    |  1.11 |        0.02 | 0.03 | [-0.05, 0.09] |  0.67 |  736.51 | 0.504 
## summer_nowave |  1.11 |        0.02 | 0.03 | [-0.03, 0.07] |  0.81 | 1039.09 | 0.421 
## second_wave   |  1.11 |        0.02 | 0.02 | [-0.03, 0.07] |  0.95 | 3569.89 | 0.342 
## first_wave    |  1.61 |        0.02 | 0.05 | [-0.07, 0.12] |  0.46 |  774.24 | 0.649 
## summer_nowave |  1.61 |        0.02 | 0.04 | [-0.05, 0.09] |  0.64 | 1034.58 | 0.524 
## second_wave   |  1.61 |        0.01 | 0.03 | [-0.05, 0.08] |  0.38 | 3570.25 | 0.705 
## first_wave    |  2.12 |        0.02 | 0.06 | [-0.10, 0.15] |  0.33 |  807.15 | 0.745 
## summer_nowave |  2.12 |        0.02 | 0.05 | [-0.07, 0.11] |  0.53 | 1006.38 | 0.598 
## second_wave   |  2.12 |    2.19e-03 | 0.04 | [-0.08, 0.09] |  0.05 | 3573.38 | 0.960 
## first_wave    |  2.63 |        0.02 | 0.08 | [-0.14, 0.18] |  0.24 |  830.23 | 0.808 
## summer_nowave |  2.63 |        0.03 | 0.06 | [-0.08, 0.14] |  0.45 |  980.88 | 0.650 
## second_wave   |  2.63 |   -8.52e-03 | 0.05 | [-0.11, 0.10] | -0.16 | 3576.40 | 0.874 
## Marginal effects estimated for Cases
```

```r
print(plot(slopes) + ggtitle(paste0("Avoidance ~ TF3*Cases*wave")))
```

```
## Warning: Using alpha for a discrete variable is not advised.
```

![plot of chunk unnamed-chunk-10](figure/unnamed-chunk-10-3.png)

```r
emtr<-emtrends(mlist[[4]], pairwise ~ wave*Cases, var = "TF3",  adjust = "holm")
print(emtr)
```

```
## $emtrends
##  wave            Cases TF3.trend     SE  df lower.CL upper.CL
##  first_wave    -0.0111     0.171 0.0648 270   0.0435    0.299
##  summer_nowave -0.0111     0.218 0.0709 270   0.0781    0.357
##  second_wave   -0.0111     0.180 0.0741 269   0.0339    0.326
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                                                                          estimate     SE  df t.ratio p.value
##  (first_wave Cases-0.0110942026407387) - (summer_nowave Cases-0.0110942026407387)  -0.04661 0.0405 275  -1.151  0.7524
##  (first_wave Cases-0.0110942026407387) - (second_wave Cases-0.0110942026407387)    -0.00869 0.0558 270  -0.156  0.8765
##  (summer_nowave Cases-0.0110942026407387) - (second_wave Cases-0.0110942026407387)  0.03793 0.0382 269   0.994  0.7524
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```

### Information seeking
[1] "InforSeek"
Type III Analysis of Variance Table with Satterthwaite's method
                   Sum Sq Mean Sq NumDF  DenDF F value    Pr(>F)    
TF1                0.8732  0.8732     1  264.5  4.3479  0.038014 *  
Cases              2.0831  2.0831     1 2063.2 10.3719  0.001299 ** 
wave              30.4087 15.2043     2  281.8 75.7025 < 2.2e-16 ***
TF2                0.0924  0.0924     1  260.8  0.4600  0.498236    
TF3                1.2176  1.2176     1  267.2  6.0622  0.014442 *  
HouseholdMembers   0.6730  0.6730     1 1903.7  3.3510  0.067320 .  
Age                0.0806  0.0806     1  324.7  0.4014  0.526795    
WorkHome           0.6242  0.6242     1 3847.2  3.1078  0.077999 .  
MediaVal          13.6543 13.6543     1 3935.6 67.9848 2.221e-16 ***
skepticism         0.6869  0.6869     1 3858.9  3.4201  0.064482 .  
RiskGr             0.0369  0.0369     1 3380.6  0.1839  0.668100    
SocialDist        12.9488 12.9488     1 4206.4 64.4722 1.260e-15 ***
PopulationDensity  0.0230  0.0230     1  273.9  0.1147  0.735095    
TF1:Cases          0.0179  0.0179     1 2474.9  0.0889  0.765624    
TF1:wave           0.0590  0.0295     2  263.6  0.1468  0.863515    
Cases:wave         0.7140  0.3570     2 1025.5  1.7776  0.169566    
Cases:TF2          0.8146  0.8146     1 1843.3  4.0560  0.044161 *  
wave:TF2           0.3223  0.1611     2  258.6  0.8022  0.449435    
Cases:TF3          0.1453  0.1453     1 2404.7  0.7236  0.395055    
wave:TF3           1.1257  0.5628     2  268.3  2.8024  0.062442 .  
TF1:Cases:wave     0.7976  0.3988     2 1049.0  1.9856  0.137817    
Cases:wave:TF2     0.2446  0.1223     2  922.9  0.6089  0.544171    
Cases:wave:TF3     0.6567  0.3283     2 1162.7  1.6348  0.195444    
 


```r
## InforSeek ~ TF1
slopes <- modelbased::estimate_slopes(mlist[[5]], trend="TF1") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |       95% CI | t(265.42) |     p
## -----------------------------------------------------
## 0.20        | 0.09 | [0.01, 0.38] |      2.09 | 0.038
## Marginal effects estimated for TF1
```

```r
## InforSeek ~ TF3
slopes <- modelbased::estimate_slopes(mlist[[5]], trend="TF3") 
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |         95% CI | t(268.12) |     p
## -------------------------------------------------------
## -0.20       | 0.08 | [-0.36, -0.04] |     -2.46 | 0.015
## Marginal effects estimated for TF3
```

```r
## InforSeek ~ Cases
slopes <- modelbased::estimate_slopes(mlist[[5]], trend="Cases") 
```

```
## NOTE: Results may be misleading due to involvement in interactions
```

```r
print(slopes)
```

```
## Estimated Marginal Effects
## 
## Coefficient |   SE |       95% CI | t(2073.77) |     p
## ------------------------------------------------------
## 0.05        | 0.01 | [0.02, 0.08] |       3.17 | 0.002
## Marginal effects estimated for Cases
```

```r
tdf$TF1_cases <- tdf$TF1 * tdf$Cases 
tdf$TF2_cases <- tdf$TF2 * tdf$Cases 
tdf$TF3_cases <- tdf$TF3 * tdf$Cases 
## Three-way interactions - stimate models with TF1/2/3*cases as a separate term (for easier contrasts) 
eq <- paste0("InforSeek ~ TF1 +  TF1_cases + TF1:wave + TF1_cases:wave  + 
                           TF2 +  TF2_cases + TF2:wave + TF2_cases:wave  + 
                           TF3 +  TF3_cases + TF3:wave + TF3_cases:wave  + 
                           Cases*wave + HouseholdMembers + Age + WorkHome + MediaVal + skepticism + RiskGr + SocialDist+ PopulationDensity +  (1+wave|PROLIFICID)")
m <- lmerTest::lmer(data=tdf,formula=eq )

anova(m)
```

```
## Type III Analysis of Variance Table with Satterthwaite's method
##                    Sum Sq Mean Sq NumDF  DenDF F value    Pr(>F)    
## TF1                0.8732  0.8732     1  264.5  4.3479  0.038014 *  
## TF1_cases          0.0179  0.0179     1 2474.9  0.0889  0.765624    
## TF2                0.0924  0.0924     1  260.8  0.4600  0.498236    
## TF2_cases          0.8146  0.8146     1 1843.3  4.0560  0.044161 *  
## TF3                1.2176  1.2176     1  267.2  6.0622  0.014442 *  
## TF3_cases          0.1453  0.1453     1 2404.7  0.7236  0.395055    
## Cases              2.0831  2.0831     1 2063.2 10.3719  0.001299 ** 
## wave              30.4087 15.2043     2  281.8 75.7025 < 2.2e-16 ***
## HouseholdMembers   0.6730  0.6730     1 1903.7  3.3510  0.067320 .  
## Age                0.0806  0.0806     1  324.7  0.4014  0.526795    
## WorkHome           0.6242  0.6242     1 3847.2  3.1078  0.077999 .  
## MediaVal          13.6543 13.6543     1 3935.6 67.9848 2.221e-16 ***
## skepticism         0.6869  0.6869     1 3858.9  3.4201  0.064482 .  
## RiskGr             0.0369  0.0369     1 3380.6  0.1839  0.668100    
## SocialDist        12.9488 12.9488     1 4206.4 64.4722 1.260e-15 ***
## PopulationDensity  0.0230  0.0230     1  273.9  0.1147  0.735095    
## TF1:wave           0.0590  0.0295     2  263.6  0.1468  0.863515    
## TF1_cases:wave     0.7976  0.3988     2 1049.0  1.9856  0.137817    
## wave:TF2           0.3223  0.1611     2  258.6  0.8022  0.449435    
## wave:TF2_cases     0.2446  0.1223     2  922.9  0.6089  0.544171    
## wave:TF3           1.1257  0.5628     2  268.3  2.8024  0.062442 .  
## wave:TF3_cases     0.6567  0.3283     2 1162.7  1.6348  0.195444    
## wave:Cases         0.7140  0.3570     2 1025.5  1.7776  0.169566    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

```r
emm_options(lmerTest.limit = 4324, pbkrtest.limit = 4324)
## InforSeek ~ TF1 * Cases * wave
slopes <- modelbased::estimate_slopes(m, trend="TF1_cases", at = c("wave") )
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          | Coefficient |   SE |         95% CI |     t |      df |     p
## -----------------------------------------------------------------------------
## first_wave    |        0.02 | 0.04 | [-0.07,  0.10] |  0.36 |  931.02 | 0.721
## summer_nowave |        0.02 | 0.03 | [-0.05,  0.08] |  0.57 | 1078.06 | 0.566
## second_wave   |       -0.05 | 0.03 | [-0.10, -0.01] | -2.17 | 3704.70 | 0.030
## Marginal effects estimated for TF1_cases
```

```r
print(plot(slopes) + ggtitle(paste0("InforSeek ~ TF1*Cases*wave")))
```

![plot of chunk unnamed-chunk-11](figure/unnamed-chunk-11-1.png)

```r
emtr<-emtrends(m, pairwise ~ wave, var = "TF1_cases",  adjust = "holm")
print(emtr)
```

```
## $emtrends
##  wave          TF1_cases.trend     SE   df lower.CL upper.CL
##  first_wave             0.0160 0.0446  931  -0.0717  0.10356
##  summer_nowave          0.0190 0.0330 1078  -0.0458  0.08378
##  second_wave           -0.0547 0.0252 3705  -0.1040 -0.00538
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                    estimate     SE   df t.ratio p.value
##  first_wave - summer_nowave  -0.00302 0.0503  740  -0.060  0.9522
##  first_wave - second_wave     0.07064 0.0512 1430   1.380  0.3354
##  summer_nowave - second_wave  0.07366 0.0395 1386   1.864  0.1876
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```

```r
## InforSeek ~ TF3 * Cases * wave

slopes <- modelbased::estimate_slopes(m, trend="TF3_cases", at = c("wave") )
print(slopes)
```

```
## Estimated Marginal Effects
## 
## wave          | Coefficient |   SE |        95% CI |     t |      df |     p
## ----------------------------------------------------------------------------
## first_wave    |       -0.04 | 0.04 | [-0.11, 0.03] | -1.12 | 1001.15 | 0.265
## summer_nowave |       -0.03 | 0.03 | [-0.08, 0.02] | -1.03 | 1034.62 | 0.302
## second_wave   |        0.02 | 0.02 | [-0.02, 0.06] |  1.04 | 3715.48 | 0.300
## Marginal effects estimated for TF3_cases
```

```r
print(plot(slopes) + ggtitle(paste0("InforSeek ~ TF3*Cases*wave")))
```

![plot of chunk unnamed-chunk-11](figure/unnamed-chunk-11-2.png)

```r
emtr<-emtrends(m, pairwise ~ wave, var = "TF3_cases",  adjust = "holm")
print(emtr)
```

```
## $emtrends
##  wave          TF3_cases.trend     SE   df lower.CL upper.CL
##  first_wave            -0.0411 0.0368 1001  -0.1134   0.0312
##  summer_nowave         -0.0267 0.0258 1035  -0.0772   0.0239
##  second_wave            0.0224 0.0216 3715  -0.0200   0.0647
## 
## Degrees-of-freedom method: kenward-roger 
## Confidence level used: 0.95 
## 
## $contrasts
##  contrast                    estimate     SE   df t.ratio p.value
##  first_wave - summer_nowave   -0.0145 0.0417  865  -0.347  0.7290
##  first_wave - second_wave     -0.0635 0.0427 1587  -1.487  0.3775
##  summer_nowave - second_wave  -0.0490 0.0320 1457  -1.532  0.3775
## 
## Degrees-of-freedom method: kenward-roger 
## P value adjustment: holm method for 3 tests
```



