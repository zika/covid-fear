---
title: ""
author: "Ondrej Zika"
date: "7/12/2022"
output: html_document
---

```{r}
here::i_am(".root_dir_covid")
here::here()
#renv::init(here::here("covid-fear"))
#renv::snapshot()
renv::load(here::here("covid-fear"))
```
```{r}
required_packages = c("lme4", "lmerTest", "emmeans", "sjPlot", "sjmisc","ggplot2", "Jmisc", "parameters", "reshape2", "Jmisc", "PupillometryR",  "plyr", "tidyr", "ggpubr", "corrplot", "patchwork", "broom", "plotrix", "PupillometryR","Hmisc", "performance", "see", "insight", "gtools", "car", "MASS", "ggpubr" , "RColorBrewer", "glmmTMB", "effectsize", "lmtest")
invisible(lapply(required_packages, require, character.only = TRUE))
emm_options(pbkrtest.limit = 5012)

source(here::here('covid-fear', 'lib', 'shared', 'r', 'r_stone_lib.R'))
```

```{r}
#factors <- c("F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism")
factors <- c("F1_CovidAnxietyWorry", "F2_CovidProbabilityEsts", "F3_Economic")
# will be z-scored
add_vars <- c( "q6_apply_soc_dist", "q6_risk_group" , "q6_media_freq_num",  "q6_media_valence",  "covid_worry", "prob_est", "worryhealth", "q7_worry_econ_impact" ,"affheallth.econ.worry")
# won't be zscored
add_vars2 <- c( "q7_period_rel_danger", "q7_period_rel_safety", "deaths", "cases", "GROUP")
trait_factors <- c("TF1_CognAnxDepr", "TF2_PhysiolAnx", "TF3_NegativeAffect")

df <- read.csv(here::here("data","full_dataset_only_complete.csv"))
#df <- df[df$GROUP %in% "UK", ]
df$deaths <- df$deaths14_norm_unsmooth
df$cases <- (df$cases14_norm_unsmooth)
#df$state_severity <- (df$cases14_std_unsmooth)
df$state_severity <- df$deaths14_std_unsmooth
tdf <- df[,c("state_severity", "PROLIFICID", "session", factors, trait_factors, add_vars, add_vars2)]
#tdf <- tdf[tdf$session %in% c(seq(0,4), seq(15,19)),]

## zscore predictors
for (v in c(trait_factors, factors,add_vars, "q6_media_freq_num",  "q6_media_valence")) {
  tdf[v] <- scale(tdf[v])
}

tdf$sess <- as.numeric(tdf$session+1)
tdf$sess_str <- paste0("sess", tdf$sess)

hist(tdf$state_severity)

## zscore severity by session
for (v in c("deaths", "cases", "state_severity")) {
  for (s in unique(tdf$sess_str)) {
    tdf[tdf$sess_str %in% s,v] <- scale(tdf[tdf$sess_str %in% s,v])
  }
}

hist(tdf$state_severity)

tdf<- tdf %>% 
  mutate(
    wave = case_when(
        session < 6 ~ "first_wave",   
        session >= 6 & session < 14 ~ "summer_nowave", 
        session >= 14 ~ "second_wave"
    )
  )
tdf$wave <- ordered(tdf$wave, levels = c("first_wave", "summer_nowave", "second_wave"))
tdf$sess_str <- ordered(tdf$sess_str, levels = c("sess1","sess2","sess3","sess4","sess5","sess6","sess7","sess8","sess9","sess10","sess11","sess12","sess13","sess14","sess15","sess16","sess17","sess18","sess19","sess20"))


```

```{r}
tdf <- tdf %>% dplyr::rename(TF1 = TF1_CognAnxDepr,
                      TF2 = TF2_PhysiolAnx, 
                      TF3 = TF3_NegativeAffect, 
                      F1 = F1_CovidAnxietyWorry,
                      F2 = F2_CovidProbabilityEsts, 
                      F3 = F3_Economic,
                      StSev  = state_severity,
                      MedVal = q6_media_valence,
                      MedFreq = q6_media_freq_num, 
                      HealthWorry = worryhealth, 
                      EconWorry = q7_worry_econ_impact,
                      CovidWorry = covid_worry, 
                      ProbEst = prob_est,
                      HealthEconWorry = affheallth.econ.worry, 
                      Cases = cases,
                      Deaths = deaths)
```
### split vars by terciles
```{r}


tdf$StSev_q <- gtools::quantcut(tdf$StSev, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$Deaths_q <- gtools::quantcut(tdf$Deaths, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$Cases_q <- gtools::quantcut(tdf$Cases, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF1_3 <- gtools::quantcut(tdf$TF1, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF2_3 <- gtools::quantcut(tdf$TF2, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF3_3 <- gtools::quantcut(tdf$TF3, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))

tdf$MedFreq_3 <- gtools::quantcut(tdf$MedFreq, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$MedFreq_2 <- gtools::quantcut(tdf$MedFreq, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))

```

```{r fig.width = 20, fig.height=3}
ggplot(tdf, aes(x=StSev)) + 
  geom_histogram(binwidth=1) + 
  facet_grid(cols=vars(sess_str))
```

```{r}
hist(tdf$q7_period_rel_danger)
```

### Full complex model with session - mostly additive
Notice the better fit of the death model - deaths overall predict perception of danger better. 

```{r fig.height=30, fig.width=7}
#m2 <- lmer(data=tdf, q7_period_rel_danger ~ TF1_CognAnxDepr * TF2_PhysiolAnx * TF3_NegativeAffect * F1_CovidAnxietyWorry * F2_CovidProbabilityEsts * F3_Economic *sess *q6_media_freq_num * #q6_media_valence   +     q6_close_person_infdied + q6_apply_soc_dist + q6_risk_group +  sess + (1|PROLIFICID)  )
eq <- "q7_period_rel_danger ~ TF1*Deaths*MedFreq  + TF2*Deaths*MedFreq + TF3*Deaths*MedFreq  + sess_str + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
eq2 <- "q7_period_rel_danger ~ TF1*Cases*MedFreq  + TF2*Cases*MedFreq + TF3*Cases*MedFreq  + sess_str + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"

m <- lmer(data=tdf,formula=eq )
m2 <- lmer(data=tdf,formula=eq2 )

#gm <- glmmTMB::glmmTMB(data=tdf,
#                       formula = as.formula(eq), 
#                       family=poisson())

mc <- compare_performance(m, m2)
mc
#car::Anova(m)
#plot(parameters(m))
#effectsize(m)
anova(m)



g <- change_color_aes_mapping_to_sig(parameters::parameters(m), c("gray", "dodgerblue"))
print(g)


anova(m2)
g <- change_color_aes_mapping_to_sig(parameters::parameters(m2), c("gray", "dodgerblue"))
print(g)



library(modelbased)

tdf$danger_pred_dmodel <- predict(m, tdf, allow.new.levels = TRUE)
tdf$danger_pred_cmodel <- predict(m2, tdf, allow.new.levels = TRUE)


#vif(m2)
#anova(m2)
#bestM <- cAIC4::stepcAIC(m2, groupCandidates=c("PROLIFICID", "sess"))

#see::plots(p1, p2, p3, n_columns = 2)
#ggsave(here::here("output", "regressions", "model_output.pdf"), width = 4.25, height = 30, dpi = 120)
```

### Role of TF1
```{r}
eq.full <- "q7_period_rel_danger ~ TF1*Deaths*MedFreq  + TF2*Deaths*MedFreq + TF3*Deaths*MedFreq  + sess_str + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
eq.dropTF1 <- "q7_period_rel_danger ~ TF2*Deaths*MedFreq + TF3*Deaths*MedFreq  + sess_str + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
eq.dropTF2 <- "q7_period_rel_danger ~ TF1*Deaths*MedFreq  + TF3*Deaths*MedFreq  + sess_str + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
eq.dropTF3 <- "q7_period_rel_danger ~ TF1*Deaths*MedFreq  + TF3*Deaths*MedFreq  + sess_str + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"


m.full <- lmer(data=tdf,formula=eq.full )
m.withoutTF1 <- lmer(data=tdf,formula=eq.dropTF1 )
m.withoutTF2 <- lmer(data=tdf,formula=eq.dropTF2 )
m.withoutTF3 <- lmer(data=tdf,formula=eq.dropTF3 )


anova(m.full,m.withoutTF1, m.withoutTF2, m.withoutTF3)

lmtest::lrtest(m.full,m.withoutTF1)
lmtest::lrtest(m.full, m.withoutTF2)
lmtest::lrtest(m.full, m.withoutTF3)


```

### main effect of somatic anxiety
```{r}
tdf2 <- tdf %>% 
  group_by(PROLIFICID) %>% 
  summarise_at(c("q7_period_rel_danger","danger_pred_dmodel", "TF2"), mean)

g <-ggplot(aes(x = TF2, y = q7_period_rel_danger), data = tdf2) +
  geom_point(alpha=0.4) +
    geom_smooth(aes(x = TF2, y = danger_pred_dmodel), method = 'lm', se = TRUE)  + 
  scale_colour_brewer(palette="Spectral") + 
  ggtitle("per_danger ~ TF2 ")
g
```

### main effect of deaths
```{r}
tdf2 <- tdf %>% 
  group_by(PROLIFICID) %>% 
  summarise_at(c("q7_period_rel_danger","danger_pred_dmodel","Deaths"), mean)

g <-ggplot(aes(x = Deaths, y = q7_period_rel_danger), data = tdf2) +
  geom_point(alpha=0.4) +
    geom_smooth(aes(x = Deaths, y = danger_pred_dmodel), method = 'lm', se = TRUE)  + 
  scale_colour_brewer(palette="Spectral") + 
  ggtitle("per_danger ~ Deaths ") + 
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  
g
```
### main effect of deaths
```{r fig.width=25, fig.height=3}
tdf2 <- tdf %>% 
  group_by(PROLIFICID, sess_str) %>% 
  summarise_at(c("q7_period_rel_danger","danger_pred_dmodel","Deaths", "Cases"), mean)

g <-ggplot(aes(x = Cases, y = q7_period_rel_danger), data = tdf2) +
  geom_point(alpha=0.4) +
    geom_smooth(aes(x = Cases, y = danger_pred_dmodel), method = 'lm', se = TRUE)  + 
  scale_colour_brewer(palette="Spectral") + 
  ggtitle("per_danger ~ Cases ") + 
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  +
  facet_grid(cols=vars(sess_str))
g

g <-ggplot(aes(x = Deaths, y = q7_period_rel_danger), data = tdf2) +
  geom_point(alpha=0.4) +
    geom_smooth(aes(x = Deaths, y = danger_pred_dmodel), method = 'lm', se = TRUE)  + 
  scale_colour_brewer(palette="Spectral") + 
  ggtitle("per_danger ~ Deaths ") + 
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  +
  facet_grid(cols=vars(sess_str))
g
```
### main effect of information seeking
```{r}
tdf2 <- tdf %>% 
  group_by(PROLIFICID) %>% 
  summarise_at(c("q7_period_rel_danger","danger_pred_dmodel","MedFreq"), mean)

g <-ggplot(aes(x = MedFreq, y = q7_period_rel_danger), data = tdf2) +
  geom_point(alpha=0.4) +
    geom_smooth(aes(x = MedFreq, y = danger_pred_dmodel), method = 'lm', se = TRUE)  + 
  scale_colour_brewer(palette="Spectral") + 
  ggtitle("per_danger ~ MedFreq ")
g
```



# media freq and deaths
```{r}

tdf2 <- tdf %>% 
  group_by(PROLIFICID, MedFreq_2) %>% 
  summarise_at(c("q7_period_rel_danger","danger_pred_dmodel","Deaths"), mean)
tdf2<- tdf2[!is.na(tdf2$MedFreq_2),]


g <-ggplot(aes(x = Deaths, y = q7_period_rel_danger, color = MedFreq_2), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(aes(x = Deaths, y = q7_period_rel_danger, color = MedFreq_2), method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ Deaths * Media Frequency")
g

tdf2 <- tdf %>% 
  group_by(PROLIFICID, Deaths_q) %>% 
  summarise_at(c("q7_period_rel_danger","danger_pred_dmodel","MedFreq"), mean)
tdf2<- tdf2[!is.na(tdf2$Deaths_q),]

g <-ggplot(aes(x = MedFreq, y = q7_period_rel_danger, color = Deaths_q), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(aes(x = MedFreq, y = danger_pred_dmodel, color = Deaths_q), method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ Deaths * Media Frequency")
g

slopes <- modelbased::estimate_slopes(m, trend = "MedFreq", at = c("Deaths"))
plot(slopes)

slopes <- modelbased::estimate_slopes(m, trend = "Deaths", at = c("MedFreq"))
plot(slopes)

emtr<-emtrends(m, pairwise ~ MedFreq, var = "Deaths", at=list(MedFreq=c(-2,-1,0,1)))
emtr

eqt <- "q7_period_rel_danger ~ TF1*Deaths*MedFreq_2  + TF2*Deaths*MedFreq_2 + TF3*Deaths*MedFreq_2  + sess_str + MedFreq_2 + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
m <- lmer(data=tdf,formula=eqt )
emtr<-emtrends(m, pairwise ~ MedFreq_2, var = "Deaths")
emtr
```

# media freq, TF1, cases
```{r fig.width=10, fig.height=4}
tdf2 <- tdf %>% 
  group_by(PROLIFICID, MedFreq_2, TF1_3) %>% 
  summarise_at(c("q7_period_rel_danger","danger_pred_cmodel", "Cases"), mean)
tdf2<- tdf2[!is.na(tdf2$MedFreq_2),]


g <-ggplot(aes(x = Cases, y = q7_period_rel_danger, color = MedFreq_2), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(aes(x = Cases, y = danger_pred_cmodel, color = MedFreq_2), method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ Cases * Media Frequency * TF1: Cog anx/ depression") + 
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)   + 
  facet_grid(cols=vars(TF1_3)) + 
  xlim(-2.5, 2.5)
g
#danger_pred_dmodel
```

# media freq, TF3 deaths 
```{r fig.width=10, fig.height=4}
tdf2 <- tdf %>% 
  group_by(PROLIFICID, MedFreq_2, TF3_3) %>% 
  summarise_at(c("q7_period_rel_danger","Deaths"), mean)
tdf2<- tdf2[!is.na(tdf2$MedFreq_2),]


g <-ggplot(aes(x = Deaths, y = q7_period_rel_danger, color = MedFreq_2), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ Deaths * Media Frequency * TF3: Unhapinness") + 
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)   + 
  facet_grid(cols=vars(TF3_3))
g

```



# media freq and TF2
```{r}

tdf2 <- tdf %>% 
  group_by(PROLIFICID, TF2_3) %>% 
  summarise_at(c("q7_period_rel_danger","MedFreq"), mean)
tdf2<- tdf2[!is.na(tdf2$TF2_3),]


g <-ggplot(aes(x = MedFreq, y = q7_period_rel_danger, color = TF2_3), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ TF2 * Media Frequency")
g


```


### Model with categorical session
```{r fig.height=30, fig.width=7}
#m2 <- lmer(data=tdf, q7_period_rel_danger ~ TF1_CognAnxDepr * TF2_PhysiolAnx * TF3_NegativeAffect * F1_CovidAnxietyWorry * F2_CovidProbabilityEsts * F3_Economic *sess *q6_media_freq_num * #q6_media_valence   +     q6_close_person_infdied + q6_apply_soc_dist + q6_risk_group +  sess + (1|PROLIFICID)  )
eq <- "q7_period_rel_danger ~ TF1*Deaths*MedFreq*sess_str  + TF2*Deaths*MedFreq*sess_str + TF3*Deaths*MedFreq*sess_str  + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
m <- lmer(data=tdf, formula=eq )

eq2 <- "q7_period_rel_danger ~ TF1*Cases*MedFreq*sess_str  + TF2*Cases*MedFreq*sess_str + TF3*Cases*MedFreq*sess_str  + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
m2 <- lmer(data=tdf, formula=eq2 )
#gm <- glmmTMB::glmmTMB(data=tdf,
#                       formula = as.formula(eq), 
#                       family=poisson())

mc <- compare_performance(m, m2)
mc
#car::Anova(m)
#plot(parameters(m))
#effectsize(m)

anova(m)
plot(parameters(m))

anova(m2)
plot(parameters(m2))

#emm_options(lmerTest.limit = 5814)
#em = emmeans(m, specs = pairwise ~ TF2*MedFreq)
#em
#vif(m2)
#anova(m2)
#bestM <- cAIC4::stepcAIC(m2, groupCandidates=c("PROLIFICID", "sess"))

#see::plots(p1, p2, p3, n_columns = 2)
#ggsave(here::here("output", "regressions", "model_output.pdf"), width = 4.25, height = 30, dpi = 120)
```
# Deaths by sess
```{r fig.width=28, fig.height=4}

tdf2 <- tdf %>% 
  group_by(PROLIFICID, sess_str) %>% 
  summarise_at(c("q7_period_rel_danger", "Deaths"), mean)
#tdf2<- tdf2[!is.na(tdf2$TF3_3),]
#tdf2<- tdf2[!is.na(tdf2$Deaths_q),]

g <-ggplot(aes(x = Deaths, y = q7_period_rel_danger), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ Deaths * sess") + 
  xlim(-2,2) +
  facet_grid(cols=vars(sess_str))
g
```

# TF3 by sess
```{r fig.width=28, fig.height=4}

tdf2 <- tdf %>% 
  group_by(PROLIFICID, sess_str) %>% 
  summarise_at(c("q7_period_rel_danger", "TF3"), mean)
#tdf2<- tdf2[!is.na(tdf2$TF3_3),]
#tdf2<- tdf2[!is.na(tdf2$Deaths_q),]

g <-ggplot(aes(x = TF3, y = q7_period_rel_danger), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ TF3 * sess") + 
  #xlim(-2,2) +
  facet_grid(cols=vars(sess_str))
g
```
# Deaths and TF3 by sess
```{r fig.width=28, fig.height=4}

tdf2 <- tdf %>% 
  group_by(PROLIFICID, sess_str, TF3_3) %>% 
  summarise_at(c("q7_period_rel_danger", "Deaths"), mean)
#tdf2<- tdf2[!is.na(tdf2$TF3_3),]
#tdf2<- tdf2[!is.na(tdf2$Deaths_q),]

g <-ggplot(aes(x = Deaths, y = q7_period_rel_danger, fill=TF3_3, color=TF3_3), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) +
  scale_fill_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) +
  ggtitle("per_danger ~ Deaths * sess") + 
  xlim(-2,2) +
  facet_grid(cols=vars(sess_str))
g
```

# TF3 and deathsby session
```{r fig.width=28, fig.height=4}

tdf2 <- tdf %>% 
  group_by(PROLIFICID, sess_str, TF3_3, Deaths_q) %>% 
  summarise_at(c("q7_period_rel_danger","Deaths", "TF3"), mean)
tdf2<- tdf2[!is.na(tdf2$TF3_3),]
tdf2<- tdf2[!is.na(tdf2$Deaths_q),]

g <-ggplot(aes(x = Deaths, y = q7_period_rel_danger, color = TF3_3), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ TF3 * Cases") + 
  xlim(-2,2) +
  facet_grid(cols=vars(sess_str))
g

g <-ggplot(aes(x = TF3, y = q7_period_rel_danger, color = Deaths_q), data = tdf2) +
  geom_point(alpha=0.2) +
    geom_smooth(method = 'lm', se = TRUE)  + 
  scale_color_manual(values= c("dodgerblue", "gold4", "red1", "darkorange", "midnightblue")) + 
  ggtitle("per_danger ~ TF3 * Cases") +
  xlim(-2,2) +
  facet_grid(cols=vars(sess_str))
g
```




### Model with categorical wave
```{r fig.height=30, fig.width=7}
tdw <- tdf#[tdf$wave %in% c("first_wave", "second_wave"),]

#m2 <- lmer(data=tdf, q7_period_rel_danger ~ TF1_CognAnxDepr * TF2_PhysiolAnx * TF3_NegativeAffect * F1_CovidAnxietyWorry * F2_CovidProbabilityEsts * F3_Economic *sess *q6_media_freq_num * #q6_media_valence   +     q6_close_person_infdied + q6_apply_soc_dist + q6_risk_group +  sess + (1|PROLIFICID)  )
eq <- "q7_period_rel_danger ~ TF1*Deaths*wave*MedFreq  + TF2*Deaths*wave*MedFreq + TF3*Deaths*wave*MedFreq  + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
eq2 <- "q7_period_rel_danger ~ TF1*Cases*wave*MedFreq  + TF2*Cases*wave*MedFreq + TF3*Cases*wave*MedFreq  + MedFreq + MedVal + q6_apply_soc_dist + q6_risk_group  + (1|PROLIFICID)"
m <- lmer(data=tdw, formula=eq )
m2 <- lmer(data=tdw, formula=eq2 )

mc <- compare_performance(m, m2)
mc
#car::Anova(m)
#plot(parameters(m))
#effectsize(m)

anova(m)
plot(parameters(m))

anova(m2)
plot(parameters(m2))

library(modelbased)

tdw$danger_pred_dmodel <- predict(m, tdw, allow.new.levels = TRUE)
tdw$danger_pred_cmodel <- predict(m2, tdw, allow.new.levels = TRUE)

#emm_options(lmerTest.limit = 5814)
#em = emmeans(m, specs = pairwise ~ TF2*MedFreq)
#em
#vif(m2)
#anova(m2)
#bestM <- cAIC4::stepcAIC(m2, groupCandidates=c("PROLIFICID", "sess"))

#see::plots(p1, p2, p3, n_columns = 2)
#ggsave(here::here("output", "regressions", "model_output.pdf"), width = 4.25, height = 30, dpi = 120)
```
# state severity and deaths
```{r}


tdw$danger_pred <- predict(m, tdw, allow.new.levels = TRUE)
tdf2 <- tdw %>% 
  group_by(wave, PROLIFICID) %>%
  summarise_at("q7_period_rel_danger", funs(mean),na.rm = TRUE)
tdf3 <- tdf2 %>% 
  group_by(wave) %>%
  summarise_at("q7_period_rel_danger", funs(mean,std.error),na.rm = TRUE)
  
  #dplyr::summarize(danger_pred=mean(danger_pred, na.rm=TRUE), dp_se=se_mean(danger_pred, na.rm=TRUE))

g <-ggplot(aes(x = wave, y = mean), data = tdf3) +
  geom_bar(, stat="identity", fill="skyblue", alpha=0.7) +
    geom_linerange( aes(x=wave,y=mean, ymin=mean-std.error, ymax=mean+std.error), width=0.1, colour="black", alpha=0.9, size=0.5) + 
  labs(y="Danger perception")
  #geom_point(alpha=0.2) +
  #  geom_smooth(method = 'lm', se = TRUE)  + 
  #scale_colour_brewer(palette="Spectral") +
  #facet_grid(cols=vars(TF1_3), rows=vars(TF3_3))
g


tdf2 <- tdw %>% 
  group_by(wave, PROLIFICID) %>%
  summarise_at(c("q7_period_rel_danger","danger_pred_dmodel", "danger_pred_cmodel", "Cases" ,"Deaths", "MedFreq", "TF3"), funs(mean),na.rm = TRUE)


g <- ggplot(data=tdf2, aes(x=Deaths, y= q7_period_rel_danger))  + 
geom_point() +
  facet_grid(cols=vars(wave)) + 
  ylim(0,8) +
  geom_smooth(aes(x=Deaths, y= danger_pred_dmodel), method='lm', formula= y~x, alpha=0.3, show.legend = FALSE, color="black") +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  + 
  ggtitle("Deaths")
g

g <- ggplot(data=tdf2, aes(x=Cases, y= q7_period_rel_danger))  + 
geom_point() +
  facet_grid(cols=vars(wave)) + 
  ylim(0,8) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = FALSE, color="black") +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  + 
  ggtitle("Cases")
g

g <- ggplot(data=tdf2, aes(x=MedFreq, y= q7_period_rel_danger))  + 
geom_point() +
  facet_grid(cols=vars(wave)) + 
  ylim(0,8) +
  geom_smooth( aes(x=MedFreq, y= danger_pred_dmodel), method='lm', formula= y~x, alpha=0.3, show.legend = FALSE, color="black") +
  stat_cor(method = "pearson",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  + 
  ggtitle("MedFreq * wave")
g
emm_options(lmerTest.limit = 5634)
em <- emtrends(m, pairwise ~ wave, var = "MedFreq")
em
#emtr


g <- ggplot(data=tdf2, aes(x=TF3, y= q7_period_rel_danger))  + 
geom_point() +
  facet_grid(cols=vars(wave)) + 
  ylim(0,8) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = FALSE, color="black") +
  stat_cor(method = "pearson",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  + 
  ggtitle("TF3 * wave")
g

```

# 

```{r fig.height=4, fig.width=8}
tdf2 <- tdw %>% 
  group_by(wave, PROLIFICID) %>%
  summarise_at(c("q7_period_rel_danger", "TF1", "TF2", "TF3"), funs(mean),na.rm = TRUE)


g <- ggplot(data=tdf2, aes(x=TF1, y= q7_period_rel_danger))  + 
geom_point() +
  ggtitle("TF1 - cog anx and dep") +
  facet_grid(cols=vars(wave)) + 
  ylim(0,8) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = FALSE, color="black") +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  

g

g <- ggplot(data=tdf2, aes(x=TF2, y= q7_period_rel_danger))  + 
geom_point() +
  ggtitle("TF2 - somatic anxiety") +
  facet_grid(cols=vars(wave)) + 
  ylim(0,8) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = FALSE, color="black") +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  

g

g <- ggplot(data=tdf2, aes(x=TF3, y= q7_period_rel_danger))  + 
geom_point() +
  facet_grid(cols=vars(wave)) + 
  ggtitle("TF3 - Unhappiness") +
  ylim(0,8) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = FALSE, color="black") +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  

g



```

## Death*TFs
```{r fig.width=12, fig.heigh=5}

## zscore severity by wave
#for (s in unique(tdw$wave)) {
#  tdf[tdw$tdw %in% s,"StSev_norm"] <- scale(tdw[tdw$wave %in% s,"StSev"])
#}
#tdw$StSev_q <-  gtools::quantcut(tdf$StSev_norm, q = c(0, 0.33, 0.66, 1), 
#                                 labels = c("low", "mid", "high"))

tdf2 <- tdw %>% 
  group_by(wave, PROLIFICID, TF1_3) %>%
  summarise_at(c("q7_period_rel_danger", "Deaths"), funs(mean),na.rm = TRUE)
tdf2<- tdf2[!is.na(tdf2$TF1_3),]

g <- ggplot(data=tdf2, aes(x=Deaths, y= q7_period_rel_danger, fill=TF1_3, color=TF1_3))  + 
geom_point(alpha=0.2,show.legend = FALSE) +
  ggtitle("TF1 - cog anx and dep") +
  facet_grid(cols=vars(wave)) + 
  ylim(0,10) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = TRUE) +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  

g


## TF2 
tdf2 <- tdw %>% 
  group_by(wave, PROLIFICID, TF2_3) %>%
  summarise_at(c("q7_period_rel_danger", "Deaths"), funs(mean),na.rm = TRUE)
tdf2<- tdf2[!is.na(tdf2$TF2_3),]

g <- ggplot(data=tdf2, aes(x=Deaths, y= q7_period_rel_danger, fill=TF2_3, color=TF2_3))  + 
geom_point(alpha=0.2,show.legend = FALSE) +
  ggtitle("TF2") +
  facet_grid(cols=vars(wave)) + 
  ylim(0,10) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = TRUE) +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  
g

## TF3
tdf2 <- tdw %>% 
  group_by(wave, PROLIFICID, TF3_3) %>%
  summarise_at(c("q7_period_rel_danger", "Deaths"), funs(mean),na.rm = TRUE)
tdf2<- tdf2[!is.na(tdf2$TF3_3),]

g <- ggplot(data=tdf2, aes(x=Deaths, y= q7_period_rel_danger, fill=TF3_3, color=TF3_3))  + 
geom_point(alpha=0.2,show.legend = FALSE) +
  ggtitle("TF3") +
  facet_grid(cols=vars(wave)) + 
  ylim(0,10) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = TRUE) +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  

g

```
### Formally test the differences between the factors 
```{r fig.width=8, fig.height=4}

tdw2 <- tdw %>% 
  group_by(wave, PROLIFICID) %>%
  summarise_at(c("q7_period_rel_danger", "Deaths", "TF1", "TF2", "TF3"), funs(mean),na.rm = TRUE)

tdw2 <- tdw2 %>% 
     melt(id.vars = c("Deaths", "q7_period_rel_danger", "wave", "PROLIFICID"),  # variables to carry forward
          measure.vars = c("TF1", "TF2", "TF3"),  #variables to stack
          value.name = "TFscore",     # name of the value variable 
          variable.name = "TFname" ) # name of the variable  
tdw2 <- tdw2[tdw2$wave %in% c("first_wave", "second_wave"),]

m<-lmer(data=tdw2, q7_period_rel_danger ~ Deaths*wave*TFscore*TFname  + (1|PROLIFICID))
anova(m)

sjPlot::plot_model(m, type="pred", terms=c("TFscore", "TFname","wave"))

em <- emtrends(m, pairwise ~ wave*TFname, var = "TFscore")
em
### add to paper
lincombs <- contrast(em$contrasts,
               list(TF_2_3_first_second=c(0,0,0,0,0, 0,0,0,0,0, 1,0,0,-1,0),
                    TF_1_3_first_second=c(0,0,0,1,0, 0,0,0,-1,0, 0,0,0,0,0),
                    TF_1_2_first_second=c(0,1,0,0,0, 0,-1,0,0,0, 0,0,0,0,0)
                    ), adjust="holm") 


g <- ggplot(data=tdw2, aes(x=TFscore, y= q7_period_rel_danger, fill=TFname, color=TFname))  + 
geom_point(alpha=0.1,show.legend = FALSE) +
  ggtitle("") +
  facet_grid(cols=vars(wave)) + 
  labs(x="factor score") +
  ylim(0,10) +
  geom_smooth(method='lm', formula= y~x, alpha=0.3, show.legend = TRUE) +
  stat_cor(method = "spearman",  alternative = "two.sided",  cor.coef.name = c("r"),  label.sep = ", ",  label.x.npc = "left",  label.y.npc = "top", digits = 3, r.digits = 3, p.digits = 3)  

g

```
 first_wave TF1 - first_wave TF2        0.08488 0.0400 Inf   2.122  0.4579
 first_wave TF1 - first_wave TF3       -0.04652 0.0393 Inf  -1.183  0.9602
 first_wave TF2 - first_wave TF3       -0.13140 0.0408 Inf  -3.219  0.0349
 
 second_wave TF1 - second_wave TF2     -0.05914 0.0410 Inf  -1.444  0.8807
 second_wave TF1 - second_wave TF3      0.07887 0.0400 Inf   1.972  0.5630
 second_wave TF2 - second_wave TF3      0.13801 0.0416 Inf   3.317  0.0255







