---
title: ""
author: "Ondrej Zika"
date: "7/12/2022"
output: html_document
---


```r
knight_it = 1
if (knight_it == 1) {
  knitr::knit2html(input=here::here("covid-fear", "scripts","focused_analyses", "prepost_GLMs.Rmd"), 
              output=here::here("covid-fear", "scripts","focused_analyses", "prepost_GLMs.html"))
}
```

```
## Warning: It seems you should call rmarkdown::render() instead of knitr::knit2html() because
## /data/drive/postdoc/Project4_covid/covid-fear/scripts/focused_analyses/prepost_GLMs.Rmd appears to be an R Markdown v2 document.
```

```
## 
## 
## processing file: /data/drive/postdoc/Project4_covid/covid-fear/scripts/focused_analyses/prepost_GLMs.Rmd
```

```
##   |                                                                                                                        |                                                                                                                |   0%  |                                                                                                                        |............                                                                                                    |  11%                     |                                                                                                                        |.........................                                                                                       |  22% [unnamed-chunk-21]  |                                                                                                                        |.....................................                                                                           |  33%                     |                                                                                                                        |..................................................                                                              |  44% [unnamed-chunk-22]  |                                                                                                                        |..............................................................                                                  |  56% [unnamed-chunk-23]  |                                                                                                                        |...........................................................................                                     |  67%                     |                                                                                                                        |.......................................................................................                         |  78% [unnamed-chunk-24]  |                                                                                                                        |....................................................................................................            |  89%                     |                                                                                                                        |................................................................................................................| 100% [unnamed-chunk-25]
```

```
## output file: prepost_GLMs.md
```


```r
here::i_am(".root_dir_covid")
```

```
## here() starts at /data/drive/postdoc/Project4_covid
```

```r
here::here()
```

```
## [1] "/data/drive/postdoc/Project4_covid"
```

```r
renv::load(here::here("covid-fear"))
```

```
## - Project '/data/drive/postdoc/Project4_covid/covid-fear' loaded. [renv 1.0.3]
```

```r
required_packages = c("lmerTest",  "emmeans", "ggplot2", "parameters", "reshape2", "PupillometryR",  "plyr", "tidyr", "ggpubr", "corrplot", "patchwork", "broom", "plotrix", "PupillometryR","Hmisc", "performance", "see", "gtools", "car", "ggpubr" , "RColorBrewer", "effectsize", "lmtest", "patchwork", "datawizard")
invisible(lapply(required_packages, require, character.only = TRUE))

#detach("package:Matrix", unload = TRUE)

source(here::here('covid-fear', 'lib', 'shared', 'r', 'r_stone_lib.R'))

#factors <- c("F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism")
factors <- c("F1_CovidAnxietyWorry", "F2_CovidProbabilityEsts", "F3_Economic")
# will be z-scored
add_vars <- c( "q6_apply_soc_dist", "q6_risk_group" , "q6_media_freq_num",  "q6_media_valence",  "prob_est", "avoid_beh", "q6_houshold_membs", "postcode_density", "q6_apply_soc_dist", "q6_work_home", "q6_risk_group", "sr_age", "stai_sa", "skepticism")
# won't be zscored
add_vars2 <- c( "q7_period_rel_danger",  "GROUP", "deaths", "cases")

trait_factors <- c("TF1_CognAnxDepr", "TF2_PhysiolAnx", "TF3_NegativeAffect")

df <- read.csv(here::here("data","full_dataset_only_complete_based_on_sess_avg.csv"))

# filter for weird ages 
df <- df[df$sr_age>18 & df$sr_age<42,]

df$deaths <- df$deaths14_norm_unsmooth
df$cases <- (df$cases14_norm_unsmooth)

tdf <- df[,c("PROLIFICID", "session", factors, trait_factors, add_vars, add_vars2)]

## zscore predictors
for (v in c(trait_factors, factors,add_vars, "q6_media_freq_num",  "q6_media_valence")) {
  #tdf[v] <- scale(tdf[v])
  tdf[v] <- datawizard::standardize(tdf[v])
  
}

tdf$sess_str <- paste0("sess", tdf$session)

## zscore severity by session
for (v in c("deaths", "cases")) {
  for (s in unique(tdf$sess_str)) {
    tdf[tdf$sess_str %in% s,v] <- datawizard::standardize(tdf[tdf$sess_str %in% s,v])
  }
}

tdf<- tdf %>% 
  mutate(
    wave = case_when(
        session < 6 ~ "first_wave",   
        session >= 6 & session < 14 ~ "summer_nowave", 
        session >= 14 ~ "second_wave"
    )
  )
tdf$wave <- ordered(tdf$wave, levels = c("first_wave", "summer_nowave", "second_wave"))
tdf$sess_str <- ordered(tdf$sess_str, levels = c("sess1","sess2","sess3","sess4","sess5","sess6","sess7","sess8","sess9","sess10","sess11","sess12","sess13","sess14","sess15","sess16","sess17","sess18","sess19","sess20"))

tdf <- tdf %>% dplyr::rename(TF1 = TF1_CognAnxDepr,
                      TF2 = TF2_PhysiolAnx, 
                      TF3 = TF3_NegativeAffect, 
                      AvoidBeh = avoid_beh,
                      MediaVal = q6_media_valence,
                      InforSeek = q6_media_freq_num, 
                      ProbEst = prob_est,
                      CurrThreat = q7_period_rel_danger,
                      Cases = cases,
                      Deaths = deaths, 
                      HouseholdMembers = q6_houshold_membs, 
                      PopulationDensity = postcode_density, 
                      Age = sr_age,
                      SocialDist = q6_apply_soc_dist,
                      WorkHome= q6_work_home, 
                      RiskGr = q6_risk_group     )



tdf$Deaths_q <- gtools::quantcut(tdf$Deaths, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$Cases_q <- gtools::quantcut(tdf$Cases, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF1_3 <- gtools::quantcut(tdf$TF1, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF2_3 <- gtools::quantcut(tdf$TF2, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF3_3 <- gtools::quantcut(tdf$TF3, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$TF1_2 <- gtools::quantcut(tdf$TF1, q = c(0, 0.5, 1), 
                                 labels = c("low","high"))
tdf$TF2_2 <- gtools::quantcut(tdf$TF2, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))
tdf$TF3_2 <- gtools::quantcut(tdf$TF3, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))

tdf$InforSeek_3 <- gtools::quantcut(tdf$InforSeek, q = c(0, 0.33, 0.66, 1), 
                                 labels = c("low", "mid", "high"))
tdf$InforSeek_2 <- gtools::quantcut(tdf$InforSeek, q = c(0, 0.5, 1), 
                                 labels = c("low", "high"))

vars <- c("CurrThreat", "ProbEst", "InforSeek", "AvoidBeh")

tdf <- tdf %>% drop_na(CurrThreat, ProbEst, InforSeek, AvoidBeh, Cases, Deaths, TF1, TF2, TF3)
```

### Wave 1 

```r
df <- read.csv(here::here("output", "prepost", "first_wave_full_prepost_data.csv"))
vars <- c("ThreatPerc", "RiskEsts", "InforSeek", "AvoidBeh")
eq = "~ prepost_Cases*TF1 + prepost_Cases*TF2 + prepost_Cases*TF3"
#eq = "~ prepost_Deaths*TF1 + prepost_Deaths*TF2 + prepost_Deaths*TF3"
#eq = "~ prepost_Cases*TF1 + prepost_Cases*TF2 + prepost_Cases*TF3 + prepost_Deaths*TF1 + prepost_Deaths*TF2 + prepost_Deaths*TF3"
for (v in vars) {
    print(v)
    m<- lm(data = df, paste0("prepost_", v, eq) )
    print(summary(m))
    print(anova(m))
}
```

```
## [1] "ThreatPerc"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -3.6736 -1.3841  0.2259  1.3841  4.4685 
## 
## Coefficients:
##                   Estimate Std. Error t value Pr(>|t|)    
## (Intercept)       -1.23479    0.19397  -6.366 7.57e-10 ***
## prepost_Cases      0.61244    0.45378   1.350    0.178    
## TF1                0.01105    0.34108   0.032    0.974    
## TF2                0.35659    0.25332   1.408    0.160    
## TF3               -0.28185    0.28068  -1.004    0.316    
## prepost_Cases:TF1 -0.04974    0.78358  -0.063    0.949    
## prepost_Cases:TF2  0.88018    0.62336   1.412    0.159    
## prepost_Cases:TF3 -0.63489    0.69377  -0.915    0.361    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.765 on 290 degrees of freedom
##   (2 observations deleted due to missingness)
## Multiple R-squared:  0.02293,	Adjusted R-squared:  -0.0006536 
## F-statistic: 0.9723 on 7 and 290 DF,  p-value: 0.4516
## 
## Analysis of Variance Table
## 
## Response: prepost_ThreatPerc
##                    Df Sum Sq Mean Sq F value  Pr(>F)  
## prepost_Cases       1   6.26  6.2606  2.0105 0.15729  
## TF1                 1   0.31  0.3104  0.0997 0.75245  
## TF2                 1   1.42  1.4235  0.4571 0.49950  
## TF3                 1   1.41  1.4057  0.4514 0.50220  
## prepost_Cases:TF1   1   0.19  0.1943  0.0624 0.80292  
## prepost_Cases:TF2   1   8.99  8.9911  2.8874 0.09035 .
## prepost_Cases:TF3   1   2.61  2.6078  0.8375 0.36088  
## Residuals         290 903.03  3.1139                  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## [1] "RiskEsts"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -46.014  -8.443   1.220   9.447  51.280 
## 
## Coefficients:
##                   Estimate Std. Error t value Pr(>|t|)    
## (Intercept)         -9.023      1.671  -5.400 1.39e-07 ***
## prepost_Cases        4.668      3.909   1.194    0.233    
## TF1                  2.978      2.938   1.013    0.312    
## TF2                  0.449      2.182   0.206    0.837    
## TF3                 -2.035      2.418  -0.842    0.401    
## prepost_Cases:TF1    4.724      6.749   0.700    0.485    
## prepost_Cases:TF2    2.961      5.369   0.551    0.582    
## prepost_Cases:TF3   -4.541      5.976  -0.760    0.448    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 15.2 on 290 degrees of freedom
##   (2 observations deleted due to missingness)
## Multiple R-squared:  0.01425,	Adjusted R-squared:  -0.009548 
## F-statistic: 0.5987 on 7 and 290 DF,  p-value: 0.7569
## 
## Analysis of Variance Table
## 
## Response: prepost_RiskEsts
##                    Df Sum Sq Mean Sq F value Pr(>F)
## prepost_Cases       1    246 246.122  1.0653 0.3029
## TF1                 1    177 177.228  0.7671 0.3818
## TF2                 1     14  13.894  0.0601 0.8065
## TF3                 1     56  56.087  0.2428 0.6226
## prepost_Cases:TF1   1    205 204.907  0.8869 0.3471
## prepost_Cases:TF2   1    137 136.603  0.5913 0.4426
## prepost_Cases:TF3   1    133 133.401  0.5774 0.4479
## Residuals         290  66999 231.033               
## [1] "InforSeek"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -3.5011 -0.8081  0.1725  1.0226  2.5554 
## 
## Coefficients:
##                   Estimate Std. Error t value Pr(>|t|)    
## (Intercept)       -1.24737    0.12400 -10.060   <2e-16 ***
## prepost_Cases     -0.15111    0.29009  -0.521    0.603    
## TF1                0.29515    0.21804   1.354    0.177    
## TF2               -0.11376    0.16194  -0.702    0.483    
## TF3               -0.26582    0.17943  -1.482    0.140    
## prepost_Cases:TF1  0.11837    0.50091   0.236    0.813    
## prepost_Cases:TF2 -0.01016    0.39849  -0.026    0.980    
## prepost_Cases:TF3 -0.30839    0.44350  -0.695    0.487    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.128 on 290 degrees of freedom
##   (2 observations deleted due to missingness)
## Multiple R-squared:  0.02039,	Adjusted R-squared:  -0.003255 
## F-statistic: 0.8624 on 7 and 290 DF,  p-value: 0.5367
## 
## Analysis of Variance Table
## 
## Response: prepost_InforSeek
##                    Df Sum Sq Mean Sq F value Pr(>F)
## prepost_Cases       1   0.63  0.6307  0.4956 0.4820
## TF1                 1   1.83  1.8289  1.4373 0.2316
## TF2                 1   1.12  1.1189  0.8793 0.3492
## TF3                 1   3.40  3.3986  2.6708 0.1033
## prepost_Cases:TF1   1   0.06  0.0583  0.0458 0.8307
## prepost_Cases:TF2   1   0.03  0.0309  0.0242 0.8764
## prepost_Cases:TF3   1   0.62  0.6153  0.4835 0.4874
## Residuals         290 369.03  1.2725               
## [1] "AvoidBeh"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -4.2780 -0.6720  0.0944  0.7669  4.5815 
## 
## Coefficients:
##                   Estimate Std. Error t value Pr(>|t|)    
## (Intercept)       -0.77899    0.13816  -5.638 4.07e-08 ***
## prepost_Cases      0.04857    0.32322   0.150    0.881    
## TF1                0.07082    0.24294   0.292    0.771    
## TF2               -0.06731    0.18043  -0.373    0.709    
## TF3                0.31965    0.19992   1.599    0.111    
## prepost_Cases:TF1  0.16316    0.55812   0.292    0.770    
## prepost_Cases:TF2 -0.03035    0.44400  -0.068    0.946    
## prepost_Cases:TF3  0.14085    0.49415   0.285    0.776    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.257 on 290 degrees of freedom
##   (2 observations deleted due to missingness)
## Multiple R-squared:  0.04845,	Adjusted R-squared:  0.02548 
## F-statistic: 2.109 on 7 and 290 DF,  p-value: 0.04258
## 
## Analysis of Variance Table
## 
## Response: prepost_AvoidBeh
##                    Df Sum Sq Mean Sq F value   Pr(>F)   
## prepost_Cases       1   0.04  0.0404  0.0256 0.873038   
## TF1                 1  10.90 10.8951  6.8965 0.009095 **
## TF2                 1   2.15  2.1516  1.3620 0.244158   
## TF3                 1   9.39  9.3934  5.9460 0.015351 * 
## prepost_Cases:TF1   1   0.69  0.6856  0.4340 0.510555   
## prepost_Cases:TF2   1   0.03  0.0333  0.0211 0.884584   
## prepost_Cases:TF3   1   0.13  0.1283  0.0812 0.775824   
## Residuals         290 458.14  1.5798                    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```

### Wave 2 

```r
df <- read.csv(here::here("output", "prepost", "second_wave_full_prepost_data.csv"))
vars <- c("ThreatPerc", "RiskEsts", "InforSeek", "AvoidBeh")
#eq = "~ prepost_Cases*TF1 + prepost_Cases*TF2 + prepost_Cases*TF3 + prepost_Deaths*TF1 + prepost_Deaths*TF2 + prepost_Deaths*TF3"
eq = "~ prepost_Cases*TF1 + prepost_Cases*TF2 + prepost_Cases*TF3"
for (v in vars) {
    print(v)
    m<- lm(data = df, paste0("prepost_", v, eq) )
    print(summary(m))
    print(anova(m))
}
```

```
## [1] "ThreatPerc"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -4.7482 -0.7709 -0.1536  0.7054  4.6977 
## 
## Coefficients:
##                    Estimate Std. Error t value Pr(>|t|)    
## (Intercept)        1.273438   0.261926   4.862 1.94e-06 ***
## prepost_Cases     -0.049103   0.120850  -0.406    0.685    
## TF1                0.047940   0.444788   0.108    0.914    
## TF2                0.339698   0.302925   1.121    0.263    
## TF3               -0.160583   0.399845  -0.402    0.688    
## prepost_Cases:TF1  0.017755   0.207179   0.086    0.932    
## prepost_Cases:TF2 -0.185959   0.142193  -1.308    0.192    
## prepost_Cases:TF3  0.001622   0.181133   0.009    0.993    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.253 on 279 degrees of freedom
##   (13 observations deleted due to missingness)
## Multiple R-squared:  0.01824,	Adjusted R-squared:  -0.006388 
## F-statistic: 0.7406 on 7 and 279 DF,  p-value: 0.6377
## 
## Analysis of Variance Table
## 
## Response: prepost_ThreatPerc
##                    Df Sum Sq Mean Sq F value Pr(>F)
## prepost_Cases       1   0.40 0.40284  0.2566 0.6129
## TF1                 1   1.27 1.27306  0.8108 0.3687
## TF2                 1   0.04 0.03611  0.0230 0.8796
## TF3                 1   2.44 2.44475  1.5570 0.2131
## prepost_Cases:TF1   1   1.21 1.20534  0.7677 0.3817
## prepost_Cases:TF2   1   2.78 2.77824  1.7694 0.1845
## prepost_Cases:TF3   1   0.00 0.00013  0.0001 0.9929
## Residuals         279 438.07 1.57015               
## [1] "RiskEsts"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -26.543  -5.062  -1.479   3.566  34.861 
## 
## Coefficients:
##                   Estimate Std. Error t value Pr(>|t|)  
## (Intercept)         3.6704     1.7224   2.131    0.034 *
## prepost_Cases      -0.1422     0.7947  -0.179    0.858  
## TF1                 1.4270     2.9248   0.488    0.626  
## TF2                 0.1887     1.9920   0.095    0.925  
## TF3                 0.7978     2.6293   0.303    0.762  
## prepost_Cases:TF1  -0.2820     1.3624  -0.207    0.836  
## prepost_Cases:TF2   0.1200     0.9350   0.128    0.898  
## prepost_Cases:TF3  -0.8602     1.1911  -0.722    0.471  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 8.24 on 279 degrees of freedom
##   (13 observations deleted due to missingness)
## Multiple R-squared:  0.01914,	Adjusted R-squared:  -0.00547 
## F-statistic: 0.7777 on 7 and 279 DF,  p-value: 0.6065
## 
## Analysis of Variance Table
## 
## Response: prepost_RiskEsts
##                    Df  Sum Sq Mean Sq F value Pr(>F)
## prepost_Cases       1    24.2  24.223  0.3568 0.5508
## TF1                 1    30.0  30.031  0.4423 0.5066
## TF2                 1    73.5  73.490  1.0824 0.2991
## TF3                 1   126.1 126.076  1.8569 0.1741
## prepost_Cases:TF1   1    75.8  75.815  1.1166 0.2916
## prepost_Cases:TF2   1     4.6   4.578  0.0674 0.7953
## prepost_Cases:TF3   1    35.4  35.413  0.5216 0.4708
## Residuals         279 18942.8  67.895               
## [1] "InforSeek"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -3.3330 -0.4618 -0.2633  0.5662  2.5420 
## 
## Coefficients:
##                     Estimate Std. Error t value Pr(>|t|)  
## (Intercept)        0.3202825  0.1758418   1.821   0.0696 .
## prepost_Cases      0.0563660  0.0811314   0.695   0.4878  
## TF1               -0.0007791  0.2986042  -0.003   0.9979  
## TF2               -0.1721214  0.2033656  -0.846   0.3981  
## TF3                0.1460448  0.2684325   0.544   0.5868  
## prepost_Cases:TF1 -0.0064752  0.1390877  -0.047   0.9629  
## prepost_Cases:TF2  0.1066575  0.0954600   1.117   0.2648  
## prepost_Cases:TF3 -0.0495203  0.1216023  -0.407   0.6842  
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 0.8412 on 279 degrees of freedom
##   (13 observations deleted due to missingness)
## Multiple R-squared:  0.01309,	Adjusted R-squared:  -0.01167 
## F-statistic: 0.5287 on 7 and 279 DF,  p-value: 0.8126
## 
## Analysis of Variance Table
## 
## Response: prepost_InforSeek
##                    Df  Sum Sq Mean Sq F value Pr(>F)
## prepost_Cases       1   0.253 0.25283  0.3573 0.5505
## TF1                 1   0.687 0.68736  0.9713 0.3252
## TF2                 1   0.306 0.30608  0.4325 0.5113
## TF3                 1   0.132 0.13235  0.1870 0.6657
## prepost_Cases:TF1   1   0.091 0.09051  0.1279 0.7209
## prepost_Cases:TF2   1   1.033 1.03262  1.4592 0.2281
## prepost_Cases:TF3   1   0.117 0.11736  0.1658 0.6842
## Residuals         279 197.438 0.70766               
## [1] "AvoidBeh"
## 
## Call:
## lm(formula = paste0("prepost_", v, eq), data = df)
## 
## Residuals:
##     Min      1Q  Median      3Q     Max 
## -4.2753 -0.4665 -0.0627  0.5353  3.1006 
## 
## Coefficients:
##                   Estimate Std. Error t value Pr(>|t|)
## (Intercept)        0.11634    0.20622   0.564    0.573
## prepost_Cases      0.10123    0.09515   1.064    0.288
## TF1                0.44477    0.35018   1.270    0.205
## TF2               -0.24964    0.23849  -1.047    0.296
## TF3               -0.13971    0.31480  -0.444    0.658
## prepost_Cases:TF1 -0.11908    0.16311  -0.730    0.466
## prepost_Cases:TF2  0.12566    0.11195   1.122    0.263
## prepost_Cases:TF3 -0.06824    0.14261  -0.479    0.633
## 
## Residual standard error: 0.9865 on 279 degrees of freedom
##   (13 observations deleted due to missingness)
## Multiple R-squared:  0.04777,	Adjusted R-squared:  0.02388 
## F-statistic:     2 on 7 and 279 DF,  p-value: 0.05524
## 
## Analysis of Variance Table
## 
## Response: prepost_AvoidBeh
##                    Df  Sum Sq Mean Sq F value   Pr(>F)   
## prepost_Cases       1   0.278  0.2780  0.2856 0.593459   
## TF1                 1   0.020  0.0196  0.0201 0.887267   
## TF2                 1   0.699  0.6985  0.7177 0.397624   
## TF3                 1  10.105 10.1050 10.3826 0.001423 **
## prepost_Cases:TF1   1   0.837  0.8370  0.8600 0.354549   
## prepost_Cases:TF2   1   1.463  1.4627  1.5029 0.221255   
## prepost_Cases:TF3   1   0.223  0.2229  0.2290 0.632654   
## Residuals         279 271.541  0.9733                    
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
```
