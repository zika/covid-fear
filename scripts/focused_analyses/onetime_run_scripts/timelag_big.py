from groo.groo import get_root
root_dir = get_root(".root_dir_covid")

import os

print(root_dir)
import sys
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))


import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import warnings
import itertools
warnings.filterwarnings('ignore')
from sklearn.linear_model import LinearRegression as lm
from scipy.stats import zscore
import bambi as bmb
import arviz as az
import bammm.bammm as mm
import matplotlib.pyplot as plt
import networkx as nx
import json
from itertools import product, combinations



# load main dataset
df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete_based_on_sess_avg.csv"))
df["cases"] = df["cases14_std_unsmooth"]
df["deaths"] = df["deaths14_std_unsmooth"]

no_trait_factors = 4
if no_trait_factors == 6:
        trait_factor_names = ["TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing",  "TF4_Physiological_Anx",  "TF5_Depression",  "TF6_Cognitive_Anx"]
elif no_trait_factors==5:
    trait_factor_names = ["TF1_Worry", "TF2_Self_Worth", "TF3_Catastrophizing",  "TF4_Anxiety",  "TF5_Depression"]    
elif no_trait_factors==3:
        trait_factor_names = ["TF3_NegativeAffect", "TF2_PhysiolAnx", "TF1_CognAnxDepr"]
elif no_trait_factors==4:
        trait_factor_names = ["TF3_NegativeAffect", "TF2_PhysiolAnx", "TF1_CognAnxDepr","stai_ta"]
        #trait_factor_names_short = ["TF3", "TF2", "TF1","TA"]
        trait_factor_names_short = ["TF3", "TF2", "TF1"]
elif no_trait_factors==99:
    trait_factor_names = ["stai_ta", "sticsa_ta", "cat", "bdi"] 

mapdict = {'TF3_NegativeAffect': 'TF3',
        'TF2_PhysiolAnx': 'TF2', 
        'TF1_CognAnxDepr': 'TF1',
        'stai_ta': "TA",
        'covid_worry': 'Worry', 
        'prob_est': 'Probest',
        'q7_worry_econ_impact': 'EconWorr',
        'q6_apply_soc_dist': 'SocialDist',
        'q6_risk_group': 'RiskGr',
        'avoid_beh': 'AvoidBeh',
        'avoid_anx': 'AvoidAnx',
        'q6_media_freq_num': 'InforSeek',
        'q6_media_valence': 'MediaVal',
        'q7_period_rel_danger': 'DangerSate',
        'deaths': 'Deaths',
        'cases': 'Cases',
        'worryhealth': 'HealthWorry',
        'q6_work_home': "WorkHome",
        'sticsa_som_sa_currsess': "StateSomAnx",
    #    "q6_close_person_infdied": "ClosePDied",
        "covid_thoughts": "CovThoughts", 
        "memory_worry_infected_baselined": "MemWorry", 
        "memory_prob_infected_baselined": "MemProb", 

        #"affheallth-econ-worry": "Health>Econ \n worry", 
        #"covid_cases_est_diff": "Estimate of \n covid cases"
        }

vars = ["Worry", "Probest", "SocialDist", 
 "AvoidBeh", "AvoidAnx", "InforSeek", 
 "DangerSate", "Deaths", "WorkHome"]

vars = ["AvoidBeh","InforSeek", 
 "DangerSate", "Deaths", "Cases"]


vars = ["Worry", "Probest", 
 "AvoidBeh", "AvoidAnx", "InforSeek", 
 "DangerSate", "Deaths","Cases"]


vars= ["F1_exposure_anxiety","F2_covid_worry", "F3_infection_probability",  
                "F4_mixed_IS_closep", "F5_danger_perception", "F6_econ_worry", 
                "F7_skepticism", "F8_time_end_estimates", "F9_avoidance", "Deaths","Cases"]



df=df.rename(columns=mapdict)
dfall = df.copy()

df.loc[:,vars] = df.loc[:,vars].apply(zscore, nan_policy='omit')
df = df.loc[:,vars+["PROLIFICID","session"]]
tdf = df.pivot(columns="session", index="PROLIFICID", values=vars)


id_df = dfall.loc[:,["PROLIFICID","TF3", "TF2", "TF1", "TA"]].groupby(by=["PROLIFICID"]).mean()

#v = "Worry"
#vs = list(set(vars) - set([v]))
win= 2
model_family = "timelag_wo_traits_singleM_EFA"
samples = 1000
chains = 2
quickloadI = 0


ddf = pd.DataFrame()
for s in np.arange(win,20)+1:
    for vidx, v in enumerate(vars):
        y = demultindex(tdf.loc[:,(v,np.arange(s-win,s+1))])
        y = y.rename(columns=dict(zip(y.columns, [v+"_lag"+str(x) for x in np.arange(win,-1, -1)])))
        if vidx > 0: 
            yy = yy.join(y)
        else:
            yy = y
    yy["session"] = "sess"+str(s)
    ddf = pd.concat([ddf, yy])

ddf = ddf.join(id_df).reset_index()

if quickloadI == 0:
    fitdf = pd.DataFrame()
    for dv in vars: 
        print(dv)
        ivs = list(set(vars) - set([dv]))
        fxeff = [i+"_lag"+str(lg)+"*"+tt for i in ivs for tt in ["TF1" ,"TF2", "TF3"] for lg in [0,1,2]]
        dv_fxeff = [dv+"_lag"+str(lg)+"*"+tt for tt in ["TF1" ,"TF2", "TF3"] for lg in [1,2]]

        models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "timelag_model_db.json"), "r"))             
        model_key = model_family+"_window"+str(win)+"_"+dv+"_"+str(chains)+"_"+str(samples)

        if model_key in models.keys():
            mod = models[model_key]
        else:
            mod = mm.get_template()

        mod["type"] = "lmm"
        mod["lmm"]["dep_var"] = dv+"_lag0"
        mod["lmm"]["fxeff"] = fxeff + dv_fxeff + ["session"]
        #mod["lmm"]["fxeff"] = ["dv_lag1", "dv_lag2", "dv_lag3", "iv_lag1", "iv_lag2", "iv_lag3", "session"]
        mod["lmm"]["rneff"] = ["1|PROLIFICID"]
        mod["est"]["nchains"] = chains
        mod["est"]["nsamples"] = samples
        mod["est"]["tune"] = 500
        mod["family"] = model_family
        mod["name"] = model_key
        mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"], "reducedrank") 
        mod["location"] = os.path.join("output", "models", "model_data", model_family, mod["name"]+".dic" )
        mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
        mod, res, m = mm.estimate_lmm(mod=mod, data=ddf.dropna(), override=0)
        models[mod["name"]] = mod

        # save model
        mm.save_model_info(models, os.path.join(root_dir, "output", "models", "timelag_model_db.json"))


        ### gather into structure and save
        v2p1 = [i+"_lag"+str(lg)+":"+tt for i in ivs for tt in ["TF1" ,"TF2", "TF3"] for lg in [0,1,2]]
        v2p2 = [dv+"_lag"+str(lg)+":"+tt for tt in ["TF1" ,"TF2", "TF3"] for lg in [1,2]]
        v2p3 = [i+"_lag"+str(lg) for i in ivs for lg in [0,1,2]]
        v2p4 = [dv+"_lag"+str(lg) for lg in [1,2]]
        vars2plot = v2p1 + v2p2 + v2p3 + v2p4


        ttdf = pd.DataFrame()
        for tf in vars2plot:
            #print(tf+" "+lbl)
            d = res.posterior[tf].stack(draws=("chain", "draw"))
            dt = pd.DataFrame(d, columns=["sample"])
            dt.loc[:,"var"] = tf
            dt.loc[:,"dv"] = dv
            ttdf = pd.concat([ttdf, dt])

        #fitdf = pd.concat([fitdf, ttdf])


        #fitdf.to_csv(os.path.join(root_dir, "output",  "models", "model_data", "lagmodels_window"+str(win)+".csv"))
        ttdf.to_csv(os.path.join(root_dir, "output",  "models", "model_data", model_family +"_"+dv+"_"+str(chains)+"_"+str(samples)+"_window"+str(win)+".csv"))
        fitdf = pd.concat([fitdf, pd.read_csv(os.path.join(root_dir, "output",  "models", "model_data", model_family +"_"+dv+"_"+str(chains)+"_"+str(samples)+"_window"+str(win)+".csv"))])
    fitdf.to_csv(os.path.join(root_dir, "output",  "models", "model_data", model_family +"_all_"+str(chains)+"_"+str(samples)+"_window"+str(win)+".csv"))
elif quickloadI==1:
    fitdf = pd.DataFrame()
    for dv in vars: 
        fitdf = pd.concat([fitdf, pd.read_csv(os.path.join(root_dir, "output",  "models", "model_data", model_family +"_"+dv+"_"+str(chains)+"_"+str(samples)+"_window"+str(win)+".csv"))])