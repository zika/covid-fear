# Packges to use:

setwd("/data/drive/postdoc/Project4_covid/covid-fear")
here::i_am(".root_dir_covid")
here::here()
renv::load(here::here("covid-fear"))

library("foreign")
library("graphicalVAR")
library("qgraph")
library("dplyr")
#library("lvnet")
#library("WriteXLS")
### Patient 1 ###

# Load the data:
Data <- read.csv(here::here("covid-fear","scripts", "focused_analyses", "network_analyses", "epskamp_example", "epskampsupplementary2_data.csv"),header=TRUE, stringsAsFactors = FALSE)

# Variables to include in analysis:
Vars <- c("relaxed","sad","nervous","concentration","tired","rumination","bodily.discomfort")

# Time varible:
Data$time <- as.POSIXct(Data$time)

# Day variable:
Data$date <- as.numeric(as.Date(Data$time))

# Detrending significant linear trends:
for (v in seq_along(Vars)){
  ff <- as.formula(paste0(Vars[[v]]," ~ time"))
  fit <- lm(ff, data = Data)
  if (anova(fit)$P[1] < 0.05){
    message(paste("Detrending variable",v))
    Data[[Vars[v]]][!is.na(Data[[Vars[[v]]]])] <- residuals(fit)
  }
}

# # Resample using spline (uncomment to use spline method, see https://osf.io/zefbc/):
# for (v in seq_along(Vars)){
#   Data[[Vars[v]]] <- (spline(as.numeric(Data$time),Data[[Vars[v]]],nrow(Data),method='fmm'))$y
# }

# Run analysis (excluding nights using dayvar = 'date'):
Res <- graphicalVAR(Data, gamma = 0, vars = Vars, dayvar = "date")

# Layout:
L <- structure(c(0.230674900867366, -0.919938715422483, 1, 0.887122558704986, 
                 -0.364338699795459, -1, 0.00855894184815797, 0.15256150738994, 
                 0.603479309271783, -0.579484678415291, 0.957494045519005, 1, 
                 -0.965354607229001, -1), .Dim = c(7L, 2L))

# Plot results:
contemporaneous <- qgraph(Res$PCC, fade = FALSE, labels = gsub("\\.","\n",Vars), layout = L, theme = "colorblind")
temporal <- qgraph(Res$PDC, fade = FALSE, labels = gsub("\\.","\n",Vars), layout = L, theme = "colorblind")

# Node placement might differ depending on operating system.

### Fitting the model in lvnet ###
# Extract augmented data:
laggedData <- Res$data$data_l[,-1]
names(laggedData) <- paste0(names(laggedData),"_lag1")
currentData <- Res$data$data_c
# Combine:
augData <- cbind(laggedData, currentData)

# Remove periods (required for OpenMx):
names(augData) <- gsub("\\.","_",names(augData))

## construct model matrices ##
nVar <- ncol(currentData)

# Lambda (identity):
Lambda <- diag(nVar*2)

# Theta (zeros):
Theta <- matrix(0, nVar*2, nVar*2)

# Beta (block)
gvarBeta <- ifelse(Res$beta[,-1]==0,0,NA)
O <- matrix(0, nVar, nVar)
Beta <- rbind(
  cbind(O, O),
  cbind(gvarBeta, O)
)

# Latent network (exo block for t-1, cont network for t):
gvarKappa <- ifelse(Res$kappa==0,0,NA)
Omega_psi <- rbind(
  cbind(matrix(NA,nVar,nVar), O),
  cbind(O, gvarKappa)
)
diag(Omega_psi) <- 0

# Free latent scale:
delta_psi <- diag(NA, nVar*2)

# Fit model:
lvfit <- lvnet(augData, lambda = Lambda, theta = Theta,
               omega_psi = Omega_psi, beta = Beta,
               delta_psi = delta_psi, fitFunction = "ML",
               exogenous = 1:nVar)

# Judge fit:
lvfit
summary(lvfit)

# Compare to latent variable model:
# Lambda (equality constrained factor loadings):
Lambda <- rbind(
  cbind(
    c(1,paste0("l",2:nVar)),0
  ),
  cbind(
    0,c(1,paste0("l",2:nVar))
  )
)

# Equality constrained residuals:
Theta <- diag(nVar*2)
for (i in 1:nVar){
  Theta[i,nVar+i] <- Theta[nVar+i,i] <- NA
}
diag(Theta) <- c(paste0("t",1:nVar),paste0("t",1:nVar))

# Beta (regression t-1 -> t1):
Beta <- matrix(c(
  0,0,
  NA,0
),2,2,byrow=TRUE)

# Psi (exo var and residual)
Psi <- diag(NA, 2)

# Equality constrained residuals:
Theta[1:nVar,1:nVar] <- NA_character_

# Fit model:
lvfit2 <- lvnet(augData, lambda = Lambda, theta = Theta,
                beta = Beta, psi = Psi, fitFunction = "ML",
                exogenous = 1:nVar)

# Compare the models:
tab <- lvnetCompare(lvfit, lvfit2)
tab

# Create the plot in paper:
loopRot <- c(NA,NA,NA,NA,1.5*pi,NA,NA)
pdf("Figure.pdf",width=8,height=4)
layout(t(1:2))
qgraph(Res$PDC, layout = L, vsize = 13, mar = c(5,5,5,5), title = "(a) Temporal network", asize = 6, labels = gsub("\\.","\n",Vars), directed = TRUE, loopRotation = loopRot, theme = "colorblind")
par(mar=c(0,0,0,0))
box()
qgraph(Res$PCC, layout = L, vsize = 13, mar = c(5,5,5,5), title = "(b) Contemporaneous network", labels = gsub("\\.","\n",Vars), theme = "colorblind")
par(mar=c(0,0,0,0))
box()
dev.off()

# Store results in excel file:
WriteXLS(list(
  contemporaneous = as.data.frame(Res$PCC),
  temporal = as.data.frame(Res$PDC),
  layout = as.data.frame(L),
  lvnetResults = tab
), ExcelFileName = "Networks.xls")
