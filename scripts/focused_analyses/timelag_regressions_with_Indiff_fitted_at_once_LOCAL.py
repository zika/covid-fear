


samples=1000
chains=2
win=2
model_family="timelag_w_traits_fitatonce"


from groo.groo import get_root
root_dir = get_root(".root_dir_covid", verbose=False)
import os
import sys
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))

import numpy as np
import pandas as pd
from cov_functions import *
import warnings
warnings.filterwarnings('ignore')
from scipy.stats import zscore
import bammm.bammm as mm
#import bammm_local as mm
import json
from itertools import product


# load main dataset
df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete_based_on_sess_avg.csv"))
df["cases"] = df["cases14_std_unsmooth"]
df["deaths"] = df["deaths14_std_unsmooth"]
trait_factor_names = ["TF3_NegativeAffect", "TF2_PhysiolAnx", "TF1_CognAnxDepr","stai_ta"]
mapdict = {
           'covid_worry': 'Worry', 
           'prob_est': 'Probest',
           'q7_worry_econ_impact': 'EconWorr',
           'q6_apply_soc_dist': 'SocialDist',
           'q6_risk_group': 'RiskGr',
           'avoid_beh': 'AvoidBeh',
           'avoid_anx': 'AvoidAnx',
           'q6_media_freq_num': 'InforSeek',
           'q6_media_valence': 'MediaVal',
           'q7_period_rel_danger': 'DangerSate',
           'deaths': 'Deaths',
           'cases': 'Cases',
           'worryhealth': 'HealthWorry',
           'q6_work_home': "WorkHome",
            "covid_thoughts": "CovThoughts",
            "TF3_NegativeAffect": "TF3",
            "TF2_PhysiolAnx": "TF2",
            "TF1_CognAnxDepr": "TF1"
           }

df = df.rename(columns=mapdict)
vars_factors = ["F1_exposure_anxiety","F2_covid_worry", "F3_infection_probability",  
                "F4_mixed_IS_closep", "F5_danger_perception", "F6_econ_worry", 
                "F7_skepticism", "F8_time_end_estimates", "F9_avoidance", "Deaths","Cases"]


vars = ["Worry", "Probest", 
 "AvoidBeh", "AvoidAnx", "InforSeek", 
 "DangerSate", "Deaths","Cases"]#  + vars_factors

vars=vars_factors

df.loc[:,vars] = df.loc[:,vars].apply(zscore, nan_policy='omit')
df = df.loc[:,vars+["PROLIFICID","session", "TF1", "TF2", "TF3"]]
tdf = df.pivot(columns="session", index="PROLIFICID", values=vars)

id_df = df.loc[:,["PROLIFICID", "TF1", "TF2", "TF3"]].groupby(by="PROLIFICID").mean()

def demultindex(df):
    df.columns = [ x+"_"+str(y) for x,y in df.columns ]
    return df

fitdf = pd.DataFrame()
combs = pd.DataFrame(list(product(vars, vars)), columns=['dv', 'iv'])
idx = np.array(combs["dv"] != combs["iv"])
combs = combs.loc[idx]

#for dv, iv in zip(combs.dv, combs.iv):
for idxx, (dv, iv) in enumerate(zip(combs.dv, combs.iv)):
    print(str(idxx)+"/"+str(combs.shape[0]))
    ## Prepare data set 
    regdf = pd.DataFrame()
    for s in np.arange(win,20)+1:
        y = demultindex(tdf.loc[:,(dv,[s])])
        y= y.rename(columns={y.columns[0]:"dv"}) 

        #tdf.loc[:,(dv,[1,2])]
        x_dv = demultindex(tdf.loc[:,(dv,np.arange(s-win,s))])
        x_dv = x_dv.rename(columns=dict(zip(x_dv.columns, ["dv_lag"+str(x) for x in np.arange(win,0, -1)])))
        x_iv = demultindex(tdf.loc[:,(iv,np.arange(s-win,s+1))])
        x_iv = x_iv.rename(columns=dict(zip(x_iv.columns, ["iv_lag"+str(x) for x in np.arange(win,-1, -1)])))

        qdf = y.join(x_dv).join(x_iv)
        qdf["session"] = "sess"+str(s)
        regdf = pd.concat([regdf, qdf], axis=0)
    regdf = regdf.join(id_df)
    regdf = regdf.dropna().reset_index()

    ## specify and fit model
    #model_family = "timelag_w_traits"
    models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "timelag_model_db.json"), "r")) 
    model_key = model_family+"_window"+str(win)+"_"+dv+"~"+iv+"_"+str(chains)+"_"+str(samples)

    if model_key in models.keys():
        mod = models[model_key]
    else:
        mod = mm.get_template()

    mod["type"] = "lmm"
    mod["lmm"]["dep_var"] = "dv"
    mod["lmm"]["fxeff"] = ["dv_lag1*TF1", "dv_lag1*TF2", "dv_lag1*TF3", 
                        "dv_lag2*TF1", "dv_lag2*TF2", "dv_lag2*TF3",
                        "iv_lag0*TF1", "iv_lag0*TF2", "iv_lag0*TF3",
                        "iv_lag1*TF1", "iv_lag1*TF2", "iv_lag1*TF3",
                        "iv_lag2*TF1", "iv_lag2*TF2", "iv_lag2*TF3", "session"]
    #mod["lmm"]["fxeff"] = ["dv_lag1", "dv_lag2", "dv_lag3", "iv_lag1", "iv_lag2", "iv_lag3", "session"]
    mod["lmm"]["rneff"] = ["1|PROLIFICID"]
    mod["est"]["nchains"] = chains
    mod["est"]["nsamples"] = samples
    mod["est"]["tune"] = 500
    mod["family"] = model_family
    mod["name"] = model_key
    mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"], "reducedrank") 
    mod["location"] = os.path.join("output", "models", "model_data", model_family, mod["name"]+".dic" )
    mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
    mod, res, m = mm.estimate_lmm(mod=mod, data=regdf, override=0)
    models[mod["name"]] = mod

    # save model
    mm.save_model_info(models, os.path.join(root_dir, "output", "models", "timelag_model_db.json"))


    ### gather into structure and save
    vars2plot = ["dv_lag1", "dv_lag2","iv_lag0", "iv_lag1", "iv_lag2", 
                    "dv_lag1:TF1", "dv_lag1:TF2", "dv_lag1:TF3", 
                "dv_lag2:TF1", "dv_lag2:TF2", "dv_lag2:TF3",
                "iv_lag0:TF1", "iv_lag0:TF2", "iv_lag0:TF3",
                "iv_lag1:TF1", "iv_lag1:TF2", "iv_lag1:TF3",
                "iv_lag2:TF1", "iv_lag2:TF2", "iv_lag2:TF3"]
    #vars2plot = ["dv_lag1", "dv_lag2", "dv_lag3", "iv_lag1", "iv_lag2", "iv_lag3"]
    list_base = [dv+" ~ "+dv+"(lag"+str(x+1)+")" for x in  range(win)]+[dv+" ~ "+iv+"(lag"+str(x)+")" for x in  range(win+1)]
    list_dv = [[dv+" ~ "+dv+"*"+fac+"(lag"+str(x+1)+")" for fac in ["TF1", "TF2", "TF3"]] for x in  range(win)] 
    list_iv  = [[dv+" ~ "+iv+"*"+fac+"(lag"+str(x)+")" for fac in ["TF1", "TF2", "TF3"]] for x in  range(win+1)]
    labels = list_base + list_dv[0] +  list_dv[1] + list_iv[0] + list_iv[1] + list_iv[2]

    ttdf = pd.DataFrame()
    for tf, lbl in zip(vars2plot, labels):
        #print(tf+" "+lbl)
        d = res.posterior[tf].stack(draws=("chain", "draw"))
        dt = pd.DataFrame(d, columns=["sample"])
        dt.loc[:,"var"] = tf
        dt.loc[:,"label"] = lbl
        dt.loc[:,"iv"] = iv
        dt.loc[:,"dv"] = dv
        ttdf = pd.concat([ttdf, dt])



    ttdf.to_csv(os.path.join(root_dir, "output",  "models", "model_data", model_family, iv+"_"+dv+"_"+str(chains)+"_"+str(samples)+"_window"+str(win)+".csv"))
