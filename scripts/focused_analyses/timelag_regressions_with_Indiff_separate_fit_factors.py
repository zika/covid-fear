from groo.groo import get_root
root_dir = get_root(".root_dir_covid")

import os

print(root_dir)
import sys
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts"))


import numpy as np
import pandas as pd
from cov_functions import *
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
import warnings
import itertools
warnings.filterwarnings('ignore')
from sklearn.linear_model import LinearRegression as lm
from scipy.stats import zscore
import bambi as bmb
import arviz as az
import bammm_local as mm
import json
from itertools import product


# load main dataset
df = pd.read_csv(os.path.join(root_dir, "data", "full_dataset_only_complete_based_on_sess_avg.csv"))
df["cases"] = df["cases14_std_unsmooth"]
df["deaths"] = df["deaths14_std_unsmooth"]
trait_factor_names = ["TF3_NegativeAffect", "TF2_PhysiolAnx", "TF1_CognAnxDepr","stai_ta"]
mapdict = {
           'covid_worry': 'Worry', 
           'prob_est': 'Probest',
           'q7_worry_econ_impact': 'EconWorr',
           'q6_apply_soc_dist': 'SocialDist',
           'q6_risk_group': 'RiskGr',
           'avoid_beh': 'AvoidBeh',
           'avoid_anx': 'AvoidAnx',
           'q6_media_freq_num': 'InforSeek',
           'q6_media_valence': 'MediaVal',
           'q7_period_rel_danger': 'DangerSate',
           'deaths': 'Deaths',
           'cases': 'Cases',
           'worryhealth': 'HealthWorry',
           'q6_work_home': "WorkHome",
            "covid_thoughts": "CovThoughts",
            "TF3_NegativeAffect": "TF3",
            "TF2_PhysiolAnx": "TF2",
            "TF1_CognAnxDepr": "TF1"
           }

df = df.rename(columns=mapdict)
vars = ["Worry", "Probest", 
 "AvoidBeh", "AvoidAnx", "InforSeek", 
 "DangerSate", "Deaths","Cases"]


def demultindex(df):
    df.columns = [ x+"_"+str(y) for x,y in df.columns ]
    return df

df.loc[:,vars] = df.loc[:,vars].apply(zscore, nan_policy='omit')
df = df.loc[:,vars+["PROLIFICID","session", "TF1", "TF2", "TF3"]]
tdf = df.pivot(columns="session", index="PROLIFICID", values=vars)

id_df = df.loc[:,["PROLIFICID", "TF1", "TF2", "TF3"]].groupby(by="PROLIFICID").mean()



fitdf = pd.DataFrame()
model_family = "timelag_w_traits_bytrait"
samples = 1000
chains = 2
win = 2

combs = pd.DataFrame(list(product(vars, vars)), columns=['dv', 'iv'])
idx = np.array(combs["dv"] != combs["iv"])
combs = combs.loc[idx]
ii = 0
for dv, iv in zip(combs.dv, combs.iv):
    for trait in ["TF1", "TF2", "TF3"]:
        print(str(ii)+"/"+str(combs.shape[0]*3))
        ii = ii + 1
        ## Prepare data set 
        regdf = pd.DataFrame()
        for s in np.arange(win,20)+1:
            y = demultindex(tdf.loc[:,(dv,[s])])
            y= y.rename(columns={y.columns[0]:"dv"}) 

            #tdf.loc[:,(dv,[1,2])]
            x_dv = demultindex(tdf.loc[:,(dv,np.arange(s-win,s))])
            x_dv = x_dv.rename(columns=dict(zip(x_dv.columns, ["dv_lag"+str(x) for x in np.arange(win,0, -1)])))
            x_iv = demultindex(tdf.loc[:,(iv,np.arange(s-win,s+1))])
            x_iv = x_iv.rename(columns=dict(zip(x_iv.columns, ["iv_lag"+str(x) for x in np.arange(win,-1, -1)])))

            qdf = y.join(x_dv).join(x_iv)
            qdf["session"] = "sess"+str(s)
            regdf = pd.concat([regdf, qdf], axis=0)
        regdf = regdf.join(id_df)
        regdf = regdf.dropna().reset_index()

        ## specify and fit model
        models = json.load(open(os.path.join(root_dir, "covid-fear", "model_dbs", "timelag_model_db.json"), "r")) 
        
        model_key = model_family+"_window"+str(win)+"_"+dv+"~"+iv+"_"+str(chains)+"_"+str(samples)+"_"+trait

        if model_key in models.keys():
            mod = models[model_key]
        else:
            mod = mm.get_template()

        mod["type"] = "lmm"
        mod["lmm"]["dep_var"] = "dv"
        mod["lmm"]["fxeff"] = [v+trait for v in ["dv_lag1*", "dv_lag2*","iv_lag0*", "iv_lag1*", "iv_lag2*"]] + ["session"]
        #mod["lmm"]["fxeff"] = ["dv_lag1", "dv_lag2", "dv_lag3", "iv_lag1", "iv_lag2", "iv_lag3", "session"]
        mod["lmm"]["rneff"] = ["1|PROLIFICID"]
        mod["est"]["nchains"] = chains
        mod["est"]["nsamples"] = samples
        mod["est"]["tune"] = 500
        mod["family"] = model_family
        mod["name"] = model_key
        mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"], "reducedrank") 
        mod["location"] = os.path.join("output", "models", "model_data", model_family, mod["name"]+".dic" )
        mod["current_sys_location"] = os.path.join(root_dir, mod["location"])
        mod, res, m = mm.estimate_lmm(mod=mod, data=regdf, override=1)
        models[mod["name"]] = mod

        # save model
        mm.save_model_info(models, os.path.join(root_dir, "covid-fear", "model_dbs", "timelag_model_db.json"))


        ### plot key result
        vars2plot = ["dv_lag1", "dv_lag2", "iv_lag0", "iv_lag1", "iv_lag2"] + [v+trait for v in ["dv_lag1:", "dv_lag2:", "iv_lag0:", "iv_lag1:", "iv_lag2:"]]
        labels = [dv+" ~ "+dv+"(lag"+str(x+1)+")" for x in  range(win)]+[dv+" ~ "+iv+"(lag"+str(x)+")" for x in  range(win+1)]
        labels = labels + [dv+" ~ "+dv+"(lag"+str(x+1)+")*"+trait for x in  range(win)]+[dv+" ~ "+iv+"(lag"+str(x)+")*"+trait for x in  range(win+1)]

        ttdf = pd.DataFrame()
        for tf, lbl in zip(vars2plot, labels):
            d = res.posterior[tf].stack(draws=("chain", "draw"))
            dt = pd.DataFrame(d, columns=["sample"])
            dt.loc[:,"var"] = tf
            dt.loc[:,"label"] = lbl
            dt.loc[:,"iv"] = iv
            dt.loc[:,"dv"] = dv
            dt.loc[:,"model"] = trait
            ttdf = pd.concat([ttdf, dt])

        #fitdf = pd.concat([fitdf, ttdf])


        ttdf.to_csv(os.path.join(root_dir, "output",  "models", "model_data", model_family, trait + "_"+iv+"_"+dv+"_"+str(chains)+"_"+str(samples)+"_window"+str(win)+".csv"))
#fitdf.to_csv(os.path.join(root_dir, "output",  "models", "model_data", "lagmodels_"+str(chains)+"_"+str(samples)+"_window"+str(win)+"_separateTFs.csv"))

