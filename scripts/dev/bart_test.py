from pathlib import Path
import arviz as az
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pymc as pm
import pymc_bart as pmb

RANDOM_SEED = 5781
np.random.seed(RANDOM_SEED)

bikes = pd.read_csv(pm.get_data("bikes.csv"))
X = bikes[["hour", "temperature", "humidity", "workingday"]]
Y = bikes["count"]

with pm.Model() as model_bikes:
    α = pm.Exponential("α", 1 / 10)
    μ = pmb.BART("μ", X, Y)
    y = pm.NegativeBinomial("y", mu=np.abs(μ), alpha=α, observed=Y)
    idata_bikes = pm.sample(random_seed=RANDOM_SEED)


y

