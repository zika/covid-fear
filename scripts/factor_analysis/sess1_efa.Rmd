---
title: 'EFA: Session 1'
author: "Ondrej Zika"
date: "1/10/2022"
output: html_document
---

```{r setup, include=FALSE}
# install core package venv and let it handle the rest
#if (!requireNamespace("pacman")) install.packages("pacman")
#packages_cran <- c("renv")
#pacman::p_install(packages_cran)

#required_packages = c("reshape2", "sjmisc", "sjPlot", "purrr", "ggpubr", "corrplot", "dplyr","ggplot2", "ggsignif", "parameters", "extdplyr", "Jmisc", "PupillometryR",  "plyr", "lme4", #"lmerTest", "emmeans", "tidyr", "ggpubr", "corrplot", "patchwork", "broom", "plotrix", "PupillometryR","glmmTMB", "Hmisc", "boot", "data.table", "devtools", "bmsR", "performance", #"rstan", "dplyr", "utils", "loo", "jtools", "heplots")



source("lib/utils.R")
here::i_am("flag_project_root.R")
mainf <- here::here()
rootf <- here::here("")
renv.dir <- here::here("covid-fear")
dataf <- here::here("data/")
outputf <- here::here("output/")
#renv::init(renv.dir)
#renv::load(renv.dir)
#renv::snapshot()
required_packages = c("rlang", "ggplot2", "psych", "GPArotation", "corrplot", "dplyr", "tidyr", "parameters", "nFactors", "xtable")
invisible(lapply(required_packages, require, character.only = TRUE))
```


```{r}
corr_type = "spearman"
nFactors = 9
```
## EFA: Session 1

### Analysis description 
- only session 1
- only continuous variables
- just to check how things are running, e.g. how to select factor numbers 
- steps roughly based on [Modern Psychometrics in R: Chapter 2](https://link.springer.com/book/10.1007/978-3-319-93177-7)

### Overview of variables
```{r}
# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
print(vardf)

```

### Preparatory steps
```{r }
# Preparatory steps

#vars2use <- vardf[vardf$fa_incl==1,["varname"]]

vars2use <- vardf[vardf$fa_incl==1,"varname"]

# read preprocessed data from session 1 
df<-read.csv(paste0(dataf, "/session1/clean_dataset.csv"))
df.orig <- df

# filter only relevant columns
df <- df[,colnames(df) %in% vars2use]

# replace missing values with median (controversial?, cor() can't deal with it but it seems that the fa() package does the same)
for(i in 1:ncol(df)){
  # replace with median
  df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
  # scale
  # df[,i] <- (df[,i] - min(df[,i]))  / (max(df[,i]) - min(df[,i])) 
  df[,i] <- (df[,i] - mean(df[,i]))  / (sd(df[,i])) 
}
```


### Correlation matrix 
```{r, fig, fig.width = 15, fig.asp = 1}
# Create correlation matrix 
cdf = cor(df, method = c(corr_type)) #how does cor2 deal with missing values?
cp <- corrplot(cdf) 

```

### Check for how many factors
#### Eigenvalues
```{r}
  ## Factor number assessment: 
# a/ Eigenvalues
evals <- eigen(cdf)$values 
scree(cdf, factors = FALSE)
```

#### Parallel analysis
```{r}

# b/ Parallel analysis
set.seed(123) 
resPA <- fa.parallel(df, fa = "both", cor = "cor",fm = "ml")
```

```{r}

# Cattell-Nelson-Gorsuch indices

m <- nCng(df, cor = TRUE, model = "factors", details = TRUE)
print(m)
m$detail
plotuScree(df, main=paste(m$nFactors,
                          " factors retained by the CNG procedure",
                          sep=""))
```
```{r}

# Multiple Regression Procedure
# https://rdrr.io/cran/nFactors/man/nMreg.html
m <- nMreg(df, cor = TRUE, model = "factors", details = TRUE)
print(m)
m$detail
plotuScree(df, main=paste(m$nFactors[1], ", ",
                            m$nFactors[2], " or ",
                            m$nFactors[3],
                            " factors retained by the MREG procedures",
                            sep=""))
```


#### Very simple structure
```{r}
# c/ Very simple srtucutre
resvss <- vss(cdf, fm = "ml", n.obs = nrow(df), plot = TRUE) 
resvss
```
### Method agreement procedure 
```{r}
# https://www.r-bloggers.com/2018/05/how-many-factors-to-retain-in-factor-analysis/

do.comparison = 1
if (do.comparison==1) {
  res = parameters::n_factors(df, type="FA", rotation="oblimin")
  
  print(res)
  q = data.frame(res)
  print(q)
}
```

### Run EFA
```{r}


# Run factor analysis
fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores = "regression")
fa3.load <- data.frame(unclass(fa3$loadings))
fa3.load$varname <- rownames(fa3.load)
fa3.load <- merge(fa3.load, vardf, by="varname")

# save the estimated loadings
save(list = c("df", "fa3", "fa3.load"), file = paste0(outputf, "/fa/fa_sess1_loadings.Rdata"))
```

### Visualize EFA: psych package diagram
```{r, fig2, fig.width = 15, fig.asp = 1}
# I don't like this plot very much so better plots below
fa.diagram(fa3)
```
```{r}
fa3

```
```{r}
# I don't like this plot very much so better plots below


```

### Loadings and pre-defined categories
- we have created the questionnaire to roughly reflect: objective measures, affective measures (worries and avoidance behaviour), probability estimates of aversive events, time estimates)
- here I just show how the estimated loadings map onto our guesses

```{r, fig3, fig.width = 10, fig.asp = 0.8}
library(reshape2)
fa3.load.long <- fa3.load %>% 
         melt(id.vars = c("varname", "agg_manual"),  # variables to carry forward
              measure.vars = colnames(fa3.load)[grepl("ML",colnames(fa3.load))],  #variables to stack
              value.name = "loading",     # name of the value variable 
              variable.name = "factor" ) # name of the variable   

## Question 1: How do the pre-defined categories                      
pal<-assign_colors("manual-cats")
g <- ggplot(fa3.load.long, aes(x=varname, y=loading, fill=agg_manual)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values =pal, name = "Manual labels (pre-FA)") +
  facet_grid(rows=vars(factor),  scales="free") + 
  scale_x_discrete() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
g
```

### Loadings by factor
- I just highlighted indicators with higher loading than 0.2
- most seem to make sense (yay)

```{r, fig.width = 10, fig.asp =0.5 }
## Question 2: Which questions factor together
n.fac <- length(unique(fa3.load.long$factor))
# just some manual labelling
#factor_names <- c("F1: close-person-threat", "F2: economic", "F3: worry", "F4: infection-prob", "F5: state", "F6: anx-avoidance", "F7: time-pred", "F8: skeptics", "F9: successful-avoidance")
factor_names <- unique(fa3.load.long$factor)
# print a quick overview of what indicators are groupped together 
fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])][,c("varname",sprintf("ML%s",seq(1:nFactors)))]
fa3.load$factor <- apply( abs(fa3.load[,colnames(fa3.load)[grepl("ML",colnames(fa3.load))]]),  1, which.max)
#cfa.str <- ""
#for (i in seq(nFactors)) {cfa.str <- paste(cfa.str, "Factor ", toString(i), " (", factor_names[i], "):\n", paste(fa3.load[fa3.load$factor==i,"varname"], collapse = " + "), "\n\n", sep="")}
#cat(cfa.str)

## better way to display
fa3.load$idx <- seq(nrow(fa3.load))
library("textshape")
temp <- fa3.load[,c("idx","factor","varname")] %>%
  pivot_wider(names_from = "factor", values_from = "varname")
temp <- temp[,!(colnames(temp) %in% c("idx"))]
temp <- temp[,paste(sort(as.integer(colnames(temp))))] #Order!
temp <- plyr::ldply(apply(temp, 2, sort, decreasing=F),rbind)
temp = data.frame(t(temp))
colnames(temp) <- gsub(pattern = "*X*", replacement = "", x = colnames(temp))
temp <- temp[!(rownames(temp) %in% c(".id")), ]
colnames(temp) <- sprintf("F%s",colnames(temp))

temp <- temp[,sprintf("F%s",seq(nFactors))]
colnames(temp) <- factor_names
print(xtable(temp))

for (i in 1:n.fac) {
  fa3.load.long$current_fac <- 0
  fa3.load.long$current_fac[fa3.load.long$factor %in% paste0("ML",toString(i)) & abs(fa3.load.long$loading) > 0.2] <- 1
  
  tdf <- fa3.load.long[fa3.load.long$factor %in% paste0("ML",toString(i)),]
  g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
  geom_bar(stat="identity") +
  ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
  print(g)
}

write.csv(data.frame(temp), here::here(outputf, "questionnaires", "covid_factors_overview.csv")) 
```


### Factor scores correlation with individual measures
- not very informative, the results are very similar to when we group the variables based on our pre-defined categories 


```{r, fig4, fig.width = 10, fig.asp = 1}
## Factor scores
fa3.scores <- data.frame(fa(df, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores="regression", missing=TRUE, impute="median")$scores)
fa3.df <- cbind(df.orig, fa3.scores )
psych.vars <- c("stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", colnames(temp))

# filter only relevant columns
fa3.df <- fa3.df[,colnames(fa3.df) %in% psych.vars]

# replace missing values with median 
for(i in 1:ncol(fa3.df)){
  fa3.df[is.na(fa3.df[,i]), i] <- median(fa3.df[,i], na.rm = TRUE)
}


# Create correlation matrix 
fa3.cdf = cor(fa3.df, method = c("pearson")) #how does cor2 deal with missing values?
cp2 <- corrplot(fa3.cdf, method = "number") 

```




