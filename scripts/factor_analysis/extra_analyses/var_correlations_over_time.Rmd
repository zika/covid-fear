---
title: 'EFA: Factor stability'
author: "Ondrej Zika"
date: "1/10/2022"
output: html_document
---

```{r setup, include=FALSE}
# install core package venv and let it handle the rest
if (!requireNamespace("pacman")) install.packages("pacman")
packages_cran <- c("renv")
pacman::p_install(packages_cran)

# load packages
library(psych)
library(GPArotation)
library(corrplot)
#library(data.table)
library(ggplot2)
library(dplyr)
library(tidyr)
library(reshape2)

here::i_am("flag_project_root.R")
mainf <- here::here()
rootf <- here::here("")
dataf <- here::here("data/")
outputf <- here::here("output/")
source(here::here("covid-fear/scripts/factor_analysis/lib/utils.R"))
```

## Assesing data stability
### Correlate the same question across sessions
#### Gather data
```{r}
# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
vars2use <- vardf[vardf$fa_incl==1,"varname"]

# read preprocessed data from session 1 
df<-read.csv(paste0(dataf, "/session1/clean_dataset.csv"))
df.orig <- df
# filter only relevant columns
df <- df[,colnames(df) %in% vars2use]
# replace missing values with median (controversial?, cor() can't deal with it but it seems that the fa() package does the same)

sessions = c("1", "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")
df.all <- data.frame()
sno <- 1
for (s in sessions) {
  prefix = "";sid = s
  if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
  df<-read.csv(paste0(dataf, "/", prefix, "session", sid, "/clean_dataset.csv"))
  tdf<-df
  df <- df[,colnames(df) %in% vars2use]
  for(i in 1:ncol(df)){
    df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
  }
  df$id <- tdf$PROLIFICID
  df$sess <- s
  df$sessid <- sno
  df.all <- rbind(df.all,df)

  sno <- sno +1

}

pids <- unique(df.all[df.all$sessid==19,])["id"]
df.all <- df.all[df.all$id %in% pids$id,]
```

#### correlations for each questions

```{r  fig, fig.width = 10, fig.asp = 1}
var <- as.vector( vardf[vardf$fa_incl==1,"varname"])
for (v in var) {
  tdf <- select(df.all, c(v,"sessid", "id"))
  tdf$sessid <- paste0("sess", as.character(df.all$sessid))
  df <- tdf %>%
    pivot_wider(names_from = "sessid", values_from = v)
  df<-as.data.frame(df[,grepl( "sess" , names( df ) )])
  for(i in 1:ncol(df)){
      df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
    }
  cdf = cor(df, method = c("spearman")) #how does cor2 deal with missing values?
  cp <- corrplot(cdf, title=v,  mar=c(0,0,1,0), col.lim = c(-1,1)) 
    
}


 

```


```{r}
load(file = paste0(outputf, "/fa/fa_sess1_loadings.Rdata"))
#https://rdrr.io/cran/psych/src/R/fa.pooled.r
nFactors = 6
fa.pooled(fa3, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores = "regression")



```