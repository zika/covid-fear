---
title: 'EFA: Session 8'
author: "Ondrej Zika"
date: "1/10/2022"
output: html_document
---

```{r setup, include=FALSE}
# install core package venv and let it handle the rest
if (!requireNamespace("pacman")) install.packages("pacman")
packages_cran <- c("renv")
pacman::p_install(packages_cran)

# load packages
library(psych)
library(GPArotation)
library(corrplot)
#library(data.table)
library(ggplot2)
library(dplyr)

source("lib/utils.R")

mainf <- here::here()
rootf <- here::here("../../../")
dataf <- here::here("../../../data")
outputf <- here::here("../../../output")
```
### Analysis description 
- only session 8 (~half-way through)
- only continuous variables
- just to check how things are running, e.g. how to select factor numbers 
- steps roughly based on [Modern Psychometrics in R: Chapter 2](https://link.springer.com/book/10.1007/978-3-319-93177-7)

### Overview of variables
```{r}
# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
print(vardf)
```

### Preparatory steps
```{r }
# Preparatory steps

#vars2use <- vardf[vardf$fa_incl==1,["varname"]]

vars2use <- vardf[vardf$fa_incl==1,"varname"]

# read preprocessed data from session 1 
df<-read.csv(paste0(dataf, "/session8/clean_dataset.csv"))
df.orig <- df

# filter only relevant columns
df <- df[,colnames(df) %in% vars2use]

# replace missing values with median (controversial?, cor() can't deal with it)
for(i in 1:ncol(df)){
  df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
}
```


### Correlation matrix (Spearman)
```{r, fig, fig.width = 15, fig.asp = 1}
# Create correlation matrix 
cdf = cor(df, method = c("spearman")) #how does cor2 deal with missing values?
cp <- corrplot(cdf) 

```

### Check for how many factors
#### Eigenvalues
```{r}
  ## Factor number assessment: 
# a/ Eigenvalues
evals <- eigen(cdf)$values 
scree(cdf, factors = FALSE)
```

#### Parallel analysis
```{r}

# b/ Parallel analysis
set.seed(123) 
resPA <- fa.parallel(df, fa = "pc", cor = "cor",fm = "ml")
```

#### Very simple structure
```{r}
# c/ Very simple srtucutre
resvss <- vss(cdf, fm = "ml", n.obs = nrow(df), plot = TRUE) 
resvss
```

### Run EFA
```{r}
# Run factor analysis
fa3 <- fa(cdf, nfactors = 6, rotate = "oblimin", fm = "ml")
fa3.load <- data.frame(unclass(fa3$loadings))
fa3.load$varname <- rownames(fa3.load)
fa3.load <- merge(fa3.load, vardf, by="varname")
```

### Visualize EFA: psych package diagram
```{r, fig2, fig.width = 15, fig.asp = 1}
# I don't like this plot very much so better plots below
fa.diagram(fa3)
```

### Loadings and pre-defined categories
- we have created the questionnaire to roughly reflect: objective measures, affective measures (worries and avoidance behaviour), probability estimates of aversive events, time estimates)
- here I just show how the estimated loadings map onto our guesses

```{r, fig3, fig.width = 10, fig.asp = 0.8}
library(reshape2)
fa3.load.long <- fa3.load %>% 
         melt(id.vars = c("varname", "agg_manual"),  # variables to carry forward
              measure.vars = colnames(fa3.load)[grepl("ML",colnames(fa3.load))],  #variables to stack
              value.name = "loading",     # name of the value variable 
              variable.name = "factor" ) # name of the variable   

## Question 1: How do the pre-defined categories                      
pal<-assign_colors("manual-cats")
g <- ggplot(fa3.load.long, aes(x=varname, y=loading, fill=agg_manual)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values =pal, name = "Manual labels (pre-FA)") +
  facet_grid(rows=vars(factor),  scales="free") + 
  scale_x_discrete() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
g
```

### Loadings by factor
- I just highlighted indicators with higher loading than 0.2
- most seem to make sense (yay)

```{r, fig.width = 10, fig.asp =0.5 }
## Question 2: Which questions factor together
n.fac <- length(unique(fa3.load.long$factor))
factor_names <- c("F1: high-worry", "F2: infection-prob", "F3: economics", "F4: avoidance", "F5: close-person-threat", "F6: state-aware")

for (i in 1:n.fac) {
  fa3.load.long$current_fac <- 0
  fa3.load.long$current_fac[fa3.load.long$factor %in% paste0("ML",toString(i)) & abs(fa3.load.long$loading) > 0.2] <- 1
  
  tdf <- fa3.load.long[fa3.load.long$factor %in% paste0("ML",toString(i)),]
  g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
  geom_bar(stat="identity") +
  ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
  print(g)
}


```


### Factor scores correlation with individual measures


```{r, fig4, fig.width = 10, fig.asp = 1}
## Factor scores
fa3.scores <- data.frame(fa(df, nfactors = 6, rotate = "oblimin", fm = "ml", scores="regression", missing=TRUE, impute="median")$scores)
colnames(fa3.scores) <- factor_names
fa3.df <- cbind(df.orig, fa3.scores )
psych.vars <- c("stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", factor_names)

# filter only relevant columns
fa3.df <- fa3.df[,colnames(fa3.df) %in% psych.vars]

# replace missing values with median 
for(i in 1:ncol(fa3.df)){
  fa3.df[is.na(fa3.df[,i]), i] <- median(fa3.df[,i], na.rm = TRUE)
}


# Create correlation matrix 
fa3.cdf = cor(fa3.df, method = c("pearson")) #how does cor2 deal with missing values?
cp2 <- corrplot(fa3.cdf, method = "number") 

```




