

library(lme4) # for lmer
library(memisc) # for mtable-lme4.R code
library(ggplot2) # for decent plotting
library(plyr) # for collapse-and-mean functions like ddply
library(psych)
library(GPArotation)
library(paran)
library(reshape2)

setwd('~/Dropbox/NYU/Dimensional_Psychiatry/Analysis/efa/')


## plot factor analysis output
melted <- melt(load_3[,1:3])# load_3 is output for a 3 factor model where you get the loadings via: fa$loadings
melted$group_class <- substr(melted$X1, 1,3)

melted$Questionnaire <- ifelse(melted$group_class == "OCI", "OCD", ifelse(melted$group_class == "AES","Apathy", ifelse(melted$group_class == "AUD","Alcohol Misuse",ifelse(melted$group_class == "SDS","Depression",ifelse(melted$group_class == "STA","Trait Anxiety",ifelse(melted$group_class == "EAT","Eating Disorders", ifelse(melted$group_class == "BIS","Impulsivity",ifelse(melted$group_class == "LSA","Social Anxiety",ifelse(melted$group_class == "SCZ","Schizotypy",NA)))))))))

melted$Order <- ifelse(melted$group_class == "OCI", 3, ifelse(melted$group_class == "AES",8, ifelse(melted$group_class == "AUD",4,ifelse(melted$group_class == "SDS",6,ifelse(melted$group_class == "STA",7,ifelse(melted$group_class == "EAT",1, ifelse(melted$group_class == "BIS",2,ifelse(melted$group_class == "LSA",9,ifelse(melted$group_class == "SCZ",5,NA)))))))))

row.names(melted) = 1:627
melted$num <- row.names(melted)
melted = melted[order(melted$X2,melted$Order),]
melted$X1 <- factor(melted$X1, levels = subset(melted$X1,melted$X2=="ML1"))
melted$Questionnaire <- factor(melted$Questionnaire, levels = c("Eating Disorders", "Impulsivity","OCD", "Alcohol Misuse", "Schizotypy","Depression","Trait Anxiety","Apathy","Social Anxiety"))


barplot <- ggplot(data=melted)+ geom_bar(aes(x=X1, y=value, fill=Questionnaire, color="black"), stat="identity", binwidth=0, width=1.3)+facet_wrap(~X2, ncol=1) +theme(axis.ticks.x=element_blank(),legend.text = element_text(size = 16), strip.text = element_blank(),strip.background = element_rect(fill="white"), legend.title = element_text(size = 16, face = "bold"),axis.text.x=element_blank(), axis.title.x=element_blank(),axis.title.y=element_blank(),panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.title.y = element_text(size = rel(1.8), angle = 90),
panel.background = element_rect(fill="white"))+ scale_fill_discrete(breaks=c("Eating Disorders", "Impulsivity","OCD", "Alcohol Misuse", "Schizotypy","Depression","Trait Anxiety","Apathy","Social Anxiety"))

#change the colours. probably a better way to do this lol
cbbPalette <- c("mediumTurquoise", "deepPink","DodgerBlue","tomato", "palegoldenrod", "mediumseagreen",  "DarkGoldenRod", "mediumblue","MediumOrchid")
borders <- c("gray14","gray14","gray14","gray14","gray14","gray14","gray14","gray14","gray14","gray14")
barplot+ scale_fill_manual(values=cbbPalette)+ scale_colour_manual(values=borders)+ guides(color= FALSE)

ggsave(filename="figures/study2_efa_loadings.png")

