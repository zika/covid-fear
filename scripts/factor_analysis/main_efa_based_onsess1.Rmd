---
title: 'EFA: Factor stability'
author: "Ondrej Zika"
date: "1/10/2022"
output: html_document
---

```{r setup, include=FALSE}
# install core package venv and let it handle the rest
source("lib/utils.R")
here::i_am("flag_project_root.R")
mainf <- here::here()
rootf <- here::here("")
renv.dir <- here::here("covid-fear")
dataf <- here::here("data")
outputf <- here::here("output/")

#renv::init(renv.dir)
#renv::load(renv.dir)
#renv::snapshot()

required_packages = c("rlang", "ggplot2", "psych", "GPArotation", "corrplot", "dplyr", "tidyr", "parameters", "nFactors", "textshape", "reshape2", "sjmisc")
invisible(lapply(required_packages, require, character.only = TRUE))

source(here::here("covid-fear/scripts/factor_analysis/lib/utils.R"))
```

```{r}
corr_type = "spearman"
nFactors = 3
```
## Assesing data stability
### Suggested number of factorss
```{r}
# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
vars2use <- vardf[vardf$fa_incl==1,"varname"]

sessions = c("1", "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")
#sessions = c("1", "5");#, "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")

```

```{r}
### first the data need to be z-scored across sessions
for (s in sessions) {
  prefix = "";sid = s
  if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
  if (s == "1") {
      dftemp <- read.csv(paste0(dataf, "/", prefix, "session", sid, "/clean_dataset.csv"))
      #dftemp<-dftemp[unique(dftemp$PROLIFICID),]
      dftemp$sid <- s
      for(i in vars2use){
        dftemp[is.na(dftemp[,i]), i] <- median(dftemp[,i], na.rm = TRUE)
      }
      df.allsess <- dftemp[,colnames(dftemp) %in% c(as.character(vars2use), "PROLIFICID", "SESSIONID", "sid")]
  } else {
      dftemp <- read.csv(paste0(dataf, "/", prefix, "session", sid, "/clean_dataset.csv"))
      #dftemp<-dftemp[unique(dftemp$PROLIFICID),]
      dftemp$sid <- s
      for(i in vars2use){
        dftemp[is.na(dftemp[,i]), i] <- median(dftemp[,i], na.rm = TRUE)
      }
      df.allsess <- rbind(df.allsess, dftemp[,colnames(dftemp) %in% c(as.character(vars2use), "PROLIFICID", "SESSIONID","sid")])
  }
}
for(i in vars2use){
    #df.allsess[,i] <- (df.allsess[,i] - mean(df.allsess[,i],na.rm = TRUE)) / sd(df.allsess[,i],na.rm = TRUE)
    df.allsess[,i] <- (df.allsess[,i] - mean(df.allsess[,i],na.rm = TRUE)) / sd(df.allsess[,i],na.rm = TRUE)
    #print(unique(df.allsess[,i]))
    #df.allsess[,i] <- (df.allsess[,i] - min(df.allsess[,i]))  / (max(df.allsess[,i]) - min(df.allsess[,i])) 
}
```

```{r}


df.all <- data.frame()
sno <- 1


for (s in sessions) {
  prefix = "";sid = s
  if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
  df.reduced  <- df.allsess[df.allsess$sid %in% s,]
  for(i in vars2use){
    df.reduced[is.na(df.reduced[,i]), i] <- median(df.reduced[,i], na.rm = TRUE)
  }
  
  df <- df.reduced[,colnames(df.reduced) %in% as.character(vars2use)]
  set.seed(123) 
  #resPA <- fa.parallel(df, fa = "pc", cor = "cor",fm = "ml")

  # Run factor analysis
  cdf = cor(df, method = c("pearson")) #how does cor2 deal with missing values?
  fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores = "regression")
  fa3.load <- data.frame(unclass(fa3$loadings))
  if (s=="1") {
    factors.s1 <- fa3.load
  } else {
    # loop over session 1 factors 
    pre <- fa3.load
    fa3.load.temp <- fa3.load
    for (c in colnames(factors.s1)) {
      fa3.load.temp$s1fac <- factors.s1[c]
      # calculate correlations
      cdf = cor(fa3.load.temp, method = c(corr_type))
      # get the row with max correlation - this is how factors are marched
      id = which.max(cdf[nFactors+1,1:nFactors])
      fa3.load[c] <- fa3.load.temp[,id]
    }
  }
  fa3.load$varname <- rownames(fa3.load)
  fa3.load$sessid <- sno
  df.all <- rbind(df.all,fa3.load)
  sno <- sno +1

}

df.all.long <- df.all %>% 
          melt(id.vars = c("varname", "sessid"),  # variables to carry forward
          measure.vars = c(sprintf("ML%d",seq(1:nFactors))),  #variables to stack
          value.name = "loading",     # name of the value variable 
          variable.name = "factor" ) # name of the variable  


  
```



```{r fi2, fix.width=10, fig.height=10}

for (c in colnames(factors.s1)) {
  tdf <- select(df.all, c(c,"sessid", "varname"))
  tdf$sessid <- paste0("sess", as.character(df.all$sessid))
  df <- tdf %>%
      pivot_wider(names_from = "sessid", values_from = c)
  df<-as.data.frame(df[,grepl( "sess" , names( df ) )])
  for(i in 1:ncol(df)){
      df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
    }
  cdf = cor(df, method = c("pearson")) #how does cor2 deal with missing values?
  cp <- corrplot(cdf, method="number", title=c,  mar=c(0,0,1,0), col.lim = c(-0.5,1)) 
  mean(cp$corr)
}

```


### Plot distribution of loadings per session

```{r fig3, fig.height=15, fig.width=8}
i<-1
for (i in 1:nFactors) {
  # Label questions loading higer than 0.1
  df.all.long$current_fac <- 0
  df.all.long$current_fac[df.all.long$factor %in% paste0("ML",toString(i)) & abs(df.all.long$loading) > 0.2] <- 1
  
  # Select data
  tdf <- df.all.long[df.all.long$factor %in% paste0("ML",toString(i)),]
    

  g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
  geom_bar(stat="identity") +
  #ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) + 
    facet_grid(rows=vars(sessid))
  print(g)
  
  
}

```
### Loadings session 1 - also save factor names

```{r}
i<-1



```

### Average loadings per factor
```{r}
i<-1
loadings.mean <- df.all.long %>%
  group_by(factor, varname) %>%
  summarise_at("loading", mean, na.rm = TRUE)


for (i in 1:nFactors) {
  # Select data
  tdf <- loadings.mean[loadings.mean$factor %in% paste0("ML",toString(i)),]
    

  g <- ggplot(tdf, aes(x=varname, y=loading)) +
  geom_bar(stat="identity") +
  #ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  #scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))  
    #facet_grid(rows=vars(sessid))
  print(g)
  
  
}

library(tidyr)
lds <- loadings.mean %>%
        pivot_wider(names_from = "factor", values_from = "loading")
cdf = cor(as.matrix(lds[,grepl( "ML",colnames(lds))]), method = c("pearson")) #how does cor2 deal with missing values?

cp <- corrplot(cdf,method = "number") 
```
### Method 1: Get factor scores using Thurstone based on session 1


```{r}
library(xtable)
do.th1 <- 1

  if (do.th1 == 1) {
  sessions = c("1", "2","3", "4","5", "6", "7","8","extra1", "9", "extra2", "10", "11", "12", "extra3", "13", "extra4", "14", "extra5", "15")

  ## run FA on session 1 (make sure to use df.allsess which uses data standardized across sessions)
  df.reduced  <- df.allsess[df.allsess$sid %in% "1",]
  cdf = cor(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)] , method = c(corr_type)) #how does cor2 deal with missing values?
  fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "ml")
  fa3.load <- data.frame(unclass(fa3$loadings))
  fa3.load$sessid <- 1
  fa3.load$varname <- rownames(fa3.load)
  fa3.load <- merge(fa3.load, vardf, by="varname")
  fa3.load.long <- fa3.load %>% 
         melt(id.vars = c("varname", "agg_manual"),  # variables to carry forward
              measure.vars = colnames(fa3.load)[grepl("ML",colnames(fa3.load))],  #variables to stack
              value.name = "loading",     # name of the value variable 
              variable.name = "factor" ) # name of the variable   
  orig_factors = unique(fa3.load.long$factor)
  for (i in orig_factors) {
    fa3.load.long$current_fac <- 0
    fa3.load.long$current_fac[fa3.load.long$factor %in% i & abs(fa3.load.long$loading) > 0.2] <- 1
    
    tdf <- fa3.load.long[fa3.load.long$factor %in% i,]
    g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
    geom_bar(stat="identity") +
    ggtitle(paste0("Factor ",i, " \""), "\"")  +
    scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
    print(g)
  }
  
  #fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])] #[,c("varname",sprintf("ML%s",seq(1:nFactors)))]
  #fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])][,c("varname",sprintf("ML%s",seq(1:nFactors)))]
  fa3.load$manual_factor <- apply( abs(fa3.load[,colnames(fa3.load)[grepl("ML",colnames(fa3.load))]]),  1, which.max)
  
  fa3.load$idx <- seq(nrow(fa3.load))
  library("textshape")
  temp <- fa3.load[,c("idx","manual_factor","varname")] %>%
  pivot_wider(names_from = "manual_factor", values_from = "varname")
  temp <- temp[,!(colnames(temp) %in% c("idx"))]
  temp <- temp[,paste(sort(as.integer(colnames(temp))))] #Order!
  temp <- plyr::ldply(apply(temp, 2, sort, decreasing=F),rbind)
  temp = data.frame(t(temp))
  colnames(temp) <- gsub(pattern = "*X*", replacement = "", x = colnames(temp))
  temp <- temp[!(rownames(temp) %in% c(".id")), ]
  colnames(temp) <- sprintf("F%s",colnames(temp))
  
  temp <- temp[,sprintf("F%s",seq(nFactors))]
  colnames(temp) <- orig_factors
  print(xtable(temp))
  # export factors from session 1
  write.csv(data.frame(temp), here::here(outputf, "questionnaires", "covid_factors_overview.csv")) 
  
  

  
  

  ## usig FA from Sess 1 - get factor scores for all sessions
  
  for (s in sessions) {
    prefix = "";sid = s
    if (str_contains(s, "extra")) { prefix="extra_"; sid=substr(s, nchar(s), nchar(s)) } 
    df.reduced  <- df.allsess[df.allsess$sid %in% s,] #on standardized data
    
    # This line gets the factor scores using FA from sess 1 for the current session 
    # factor.scores(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)], fa3)$scores
    # vars2use is a vector of questions to be used in the analysis 
    
    # Using regression (z-scores output betas)
    #fa3.scores <- cbind(df.reduced[,c("SESSIONID", "PROLIFICID")],  data.frame(factor.scores(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)], fa3, method="Thurstone")$scores)) #https://www.personality-project.org/r/html/factor.scores.html
    
    
    # using component loadings (without estimation of weights - I assume this is just multiplication) because it preserves scale
    
    fa3.scores <- cbind(df.reduced[,c("SESSIONID", "PROLIFICID")],  data.frame(factor.scores(as.matrix(df.reduced[,colnames(df.reduced ) %in% as.character(vars2use)]), fa3, method="components")$scores) )
    hist(fa3.scores$ML1)
    #print(summary(fa3.scores[,c("ML1", "ML2", "ML3")]))

    # uncoment to actually write
    write.csv(fa3.scores, paste0(dataf, "/", prefix, "session", sid, "/factor_scores_", toString(nFactors),".csv"), row.names = FALSE)
    
    
  }

}
```


```{r fig4, fig.height=10, fig.width=8}

i<-1
for (i in 1:nFactors) {
 # Label questions loading higer than 0.1
 df.all.long$current_fac <- 0
 df.all.long$current_fac[df.all.long$factor %in% paste0("ML",toString(i)) & abs(df.all.long$loading) > 0.2] <- 1
 
 # Select data
 tdf <- df.all.long[df.all.long$factor %in% paste0("ML",toString(i)),]
 g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(sessid))) +
 geom_bar(position="stack", stat="identity", colour="black") +
# ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"") +
 #scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
 theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
 scale_colour_distiller(
  type = "seq",
  palette = 1,
  direction = -1,
  values = NULL,
  space = "Lab",
  na.value = "grey50",
  guide = "colourbar",
  aesthetics = "colour"
 )
 # facet_grid(rows=vars(sessid))
 print(g)
 
 
}

```