

## EFA: Session 15

### Analysis description 
- only session 15
- only continuous variables
- just to check how things are running, e.g. how to select factor numbers 
- steps roughly based on [Modern Psychometrics in R: Chapter 2](https://link.springer.com/book/10.1007/978-3-319-93177-7)

### Overview of variables

```r
# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
```

### Preparatory steps

```r
# Preparatory steps

#vars2use <- vardf[vardf$fa_incl==1,["varname"]]

vars2use <- vardf[vardf$fa_incl==1,"varname"]

# read preprocessed data from session 1 
df<-read.csv(paste0(dataf, "/session15/clean_dataset.csv"))
df.orig <- df

# filter only relevant columns
df <- df[,colnames(df) %in% vars2use]

# replace missing values with median (controversial?, cor() can't deal with it but it seems that the fa() package does the same)
for(i in 1:ncol(df)){
  df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
}
```


### Correlation matrix (Spearman)

```r
# Create correlation matrix 
cdf = cor(df, method = c("spearman")) #how does cor2 deal with missing values?
cp <- corrplot(cdf) 
```

<img src="sess15_efa_files/figure-html/fig-1.png" width="1440" />

### Check for how many factors
#### Eigenvalues

```r
  ## Factor number assessment: 
# a/ Eigenvalues
evals <- eigen(cdf)$values 
scree(cdf, factors = FALSE)
```

<img src="sess15_efa_files/figure-html/unnamed-chunk-3-1.png" width="672" />

#### Parallel analysis

```r
# b/ Parallel analysis
set.seed(123) 
resPA <- fa.parallel(df, fa = "pc", cor = "cor",fm = "ml")
```

<img src="sess15_efa_files/figure-html/unnamed-chunk-4-1.png" width="672" />

```
## Parallel analysis suggests that the number of factors =  NA  and the number of components =  6
```

#### Very simple structure

```r
# c/ Very simple srtucutre
resvss <- vss(cdf, fm = "ml", n.obs = nrow(df), plot = TRUE) 
```

<img src="sess15_efa_files/figure-html/unnamed-chunk-5-1.png" width="672" />

```r
resvss
```

```
## 
## Very Simple Structure
## Call: vss(x = cdf, fm = "ml", n.obs = nrow(df), plot = TRUE)
## VSS complexity 1 achieves a maximimum of 0.73  with  1  factors
## VSS complexity 2 achieves a maximimum of 0.82  with  4  factors
## 
## The Velicer MAP achieves a minimum of 0.01  with  8  factors 
## BIC achieves a minimum of  -2427.57  with  8  factors
## Sample Size adjusted BIC achieves a minimum of  -340.78  with  8  factors
## 
## Statistics by number of factors 
##   vss1 vss2   map dof chisq     prob sqresid  fit RMSEA   BIC SABIC complex
## 1 0.73 0.00 0.025 945  4835  0.0e+00      52 0.73 0.117  -555  2442     1.0
## 2 0.55 0.79 0.022 901  3922  0.0e+00      40 0.79 0.106 -1217  1640     1.3
## 3 0.54 0.81 0.017 858  3147 9.2e-258      29 0.85 0.094 -1747   974     1.6
## 4 0.49 0.82 0.016 816  2657 2.5e-193      24 0.87 0.087 -1998   590     1.7
## 5 0.48 0.79 0.016 775  2184 3.0e-134      22 0.89 0.078 -2237   221     1.8
## 6 0.44 0.74 0.014 735  1832  4.8e-95      20 0.89 0.070 -2360   -29     1.8
## 7 0.43 0.72 0.014 696  1552  4.0e-67      18 0.90 0.064 -2418  -211     2.0
## 8 0.44 0.73 0.014 658  1326  2.8e-47      15 0.92 0.058 -2428  -341     1.9
##   eChisq  SRMR eCRMS  eBIC
## 1   7411 0.112 0.114  2020
## 2   4936 0.091 0.096  -203
## 3   2787 0.068 0.074 -2107
## 4   2009 0.058 0.064 -2645
## 5   1559 0.051 0.058 -2862
## 6   1331 0.047 0.055 -2862
## 7   1076 0.043 0.051 -2894
## 8    714 0.035 0.043 -3039
```

### Run EFA

```r
nFactors = 4
# Run factor analysis
fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "ml")
fa3.load <- data.frame(unclass(fa3$loadings))
fa3.load$varname <- rownames(fa3.load)
fa3.load <- merge(fa3.load, vardf, by="varname")

# save the estimated loadings
save(list = c("df", "fa3", "fa3.load"), file = paste0(outputf, "/fa/fa_sess1_loadings.Rdata"))
```

### Visualize EFA: psych package diagram

```r
# I don't like this plot very much so better plots below
fa.diagram(fa3)
```

<img src="sess15_efa_files/figure-html/fig2-1.png" width="1440" />

### Loadings and pre-defined categories
- we have created the questionnaire to roughly reflect: objective measures, affective measures (worries and avoidance behaviour), probability estimates of aversive events, time estimates)
- here I just show how the estimated loadings map onto our guesses


```r
library(reshape2)
```

```
## Warning: package 'reshape2' was built under R version 3.6.3
```

```r
fa3.load.long <- fa3.load %>% 
         melt(id.vars = c("varname", "agg_manual"),  # variables to carry forward
              measure.vars = colnames(fa3.load)[grepl("ML",colnames(fa3.load))],  #variables to stack
              value.name = "loading",     # name of the value variable 
              variable.name = "factor" ) # name of the variable   

## Question 1: How do the pre-defined categories                      
pal<-assign_colors("manual-cats")
g <- ggplot(fa3.load.long, aes(x=varname, y=loading, fill=agg_manual)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values =pal, name = "Manual labels (pre-FA)") +
  facet_grid(rows=vars(factor),  scales="free") + 
  scale_x_discrete() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
g
```

<img src="sess15_efa_files/figure-html/fig3-1.png" width="960" />

### Loadings by factor
- I just highlighted indicators with higher loading than 0.2
- most seem to make sense (yay)


```r
## Question 2: Which questions factor together
n.fac <- length(unique(fa3.load.long$factor))
# just some manual labelling
#factor_names <- c("F1: close-person-threat", "F2: economic", "F3: worry", "F4: infection-prob", "F5: state", "F6: anx-avoidance", "F7: time-pred", "F8: skeptics", "F9: successful-avoidance")
factor_names <- sprintf("F%s",seq(1:nFactors))
# print a quick overview of what indicators are groupped together 
fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])][,c("varname",sprintf("ML%s",seq(1:nFactors)))]
fa3.load$factor <- apply( abs(fa3.load[,colnames(fa3.load)[grepl("ML",colnames(fa3.load))]]),  1, which.max)
cfa.str <- ""
for (i in seq(nFactors)) {cfa.str <- paste(cfa.str, "Factor ", toString(i), " (", factor_names[i], "):\n", paste(fa3.load[fa3.load$factor==i,"varname"], collapse = " + "), "\n\n", sep="")}
cat(cfa.str)
```

```
## Factor 1 (F1):
## q6_apply_soc_dist + q6_work_home + q7_anx_another_beh + q7_anx_eating_food_out + q7_anx_public_transp + q7_anx_stand_close_to_ppl + q7_anx_touching_surf + q7_anx_visit_doc + q7_beh_avoid_ppl + q7_beh_avoid_public_places + q7_beh_wash_hands + q7_initial_surprise + q7_people_overreact + q8_t_secondw_when_days
## 
## Factor 2 (F2):
## q6_econ_impact_closep + q6_econ_impact_me + q7_worry_econ_impact + q8_prob_econ_imp_me
## 
## Factor 3 (F3):
## q6_houshold_membs + q6_media_freq_num + q6_media_valence + q7_diff_beh_freq_num + q7_diff_beh_freq_num + q7_period_rel_danger + q7_period_rel_safety + q7_vir_made_lab + q7_vir_not_as_dangerous + q8_prob_inf_avgp + q8_prob_inf_closep + q8_prob_inf_me + q8_t_econ_back_norm_days + q8_t_pand_end_days
## 
## Factor 4 (F4):
## q6_risk_group + q6_risk_group_closep + q7_closep_die + q7_inf_worry_frequency_num + q7_inf_worry_frequency_num + q7_initial_scared + q7_worry_closep_inf + q7_worry_die + q7_worry_infected + q7_worry_insuf_help + q7_worry_shortage + q7_worry_sthg_bad + q8_prob_die_closep + q8_prob_die_me + q8_t_life_back_norm_days
```

```r
for (i in 1:n.fac) {
  fa3.load.long$current_fac <- 0
  fa3.load.long$current_fac[fa3.load.long$factor %in% paste0("ML",toString(i)) & abs(fa3.load.long$loading) > 0.2] <- 1
  
  tdf <- fa3.load.long[fa3.load.long$factor %in% paste0("ML",toString(i)),]
  g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
  geom_bar(stat="identity") +
  ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
  print(g)
}
```

<img src="sess15_efa_files/figure-html/unnamed-chunk-7-1.png" width="960" /><img src="sess15_efa_files/figure-html/unnamed-chunk-7-2.png" width="960" /><img src="sess15_efa_files/figure-html/unnamed-chunk-7-3.png" width="960" /><img src="sess15_efa_files/figure-html/unnamed-chunk-7-4.png" width="960" />


### Factor scores correlation with individual measures
- not very informative, the results are very similar to when we group the variables based on our pre-defined categories 



```r
## Factor scores
fa3.scores <- data.frame(fa(df, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores="regression", missing=TRUE, impute="median")$scores)
colnames(fa3.scores) <- factor_names
fa3.df <- cbind(df.orig, fa3.scores )
psych.vars <- c("stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", factor_names)

# filter only relevant columns
fa3.df <- fa3.df[,colnames(fa3.df) %in% psych.vars]

# replace missing values with median 
for(i in 1:ncol(fa3.df)){
  fa3.df[is.na(fa3.df[,i]), i] <- median(fa3.df[,i], na.rm = TRUE)
}


# Create correlation matrix 
fa3.cdf = cor(fa3.df, method = c("pearson")) #how does cor2 deal with missing values?
cp2 <- corrplot(fa3.cdf, method = "number") 
```

<img src="sess15_efa_files/figure-html/fig4-1.png" width="960" />



