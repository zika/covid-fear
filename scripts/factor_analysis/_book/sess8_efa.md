---
title: 'EFA: Session 8'
author: "Ondrej Zika"
date: "1/10/2022"
output: html_document
---


### Analysis description 
- only session 8 (~half-way through)
- only continuous variables
- just to check how things are running, e.g. how to select factor numbers 
- steps roughly based on [Modern Psychometrics in R: Chapter 2](https://link.springer.com/book/10.1007/978-3-319-93177-7)

### Overview of variables

```r
# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
print(vardf)
```

```
##                       varname        type          agg_manual fa_incl fulltext
## 1                   q6_me_inf      binary     objective-covid       0       NA
## 2         q6_close_person_inf      binary     objective-covid       0       NA
## 3        q6_close_person_died      binary     objective-covid       0       NA
## 4           q6_econ_impact_me       scale      objective-econ       1       NA
## 5       q6_econ_impact_closep       scale      objective-econ       1       NA
## 6                q6_work_home       scale           objective       1       NA
## 7           q6_apply_soc_dist       scale     objective-covid       1       NA
## 8               q6_risk_group       scale     objective-covid       1       NA
## 9        q6_risk_group_closep       scale     objective-covid       1       NA
## 10          q6_houshold_membs       scale           objective       1       NA
## 11              q6_media_freq       scale     objective-media       0       NA
## 12           q6_media_valence       scale     objective-media       1       NA
## 13          q7_worry_infected       scale     affective-worry       1       NA
## 14               q7_worry_die       scale     affective-worry       1       NA
## 15       q7_worry_econ_impact       scale     affective-worry       1       NA
## 16          q7_worry_sthg_bad       scale     affective-worry       1       NA
## 17        q7_worry_insuf_help       scale     affective-worry       1       NA
## 18        q7_worry_closep_inf       scale     affective-worry       1       NA
## 19              q7_closep_die       scale     affective-worry       1       NA
## 20          q7_worry_shortage       scale     affective-worry       1       NA
## 21       q7_period_rel_danger       scale           affective       1       NA
## 22       q7_period_rel_safety       scale           affective       1       NA
## 23        q7_initial_surprise       scale           affective       1       NA
## 24          q7_initial_scared       scale           affective       1       NA
## 25        q7_people_overreact       scale           affective       1       NA
## 26    q7_vir_not_as_dangerous       scale           affective       1       NA
## 27            q7_vir_made_lab       scale           affective       1       NA
## 28 q7_inf_worry_frequency_num       scale     affective-worry       1       NA
## 29       q7_diff_beh_freq_num       scale affective-behaviour       1       NA
## 30          q7_beh_wash_hands       scale affective-behaviour       1       NA
## 31           q7_beh_avoid_ppl       scale affective-behaviour       1       NA
## 32 q7_beh_avoid_public_places       scale affective-behaviour       1       NA
## 33       q7_anx_touching_surf       scale affective-behaviour       1       NA
## 34  q7_anx_stand_close_to_ppl       scale affective-behaviour       1       NA
## 35     q7_anx_eating_food_out       scale affective-behaviour       1       NA
## 36       q7_anx_public_transp       scale affective-behaviour       1       NA
## 37           q7_anx_visit_doc       scale affective-behaviour       1       NA
## 38         q7_anx_another_beh       scale affective-behaviour       1       NA
## 39             q8_prob_inf_me       scale             probest       1       NA
## 40             q8_prob_die_me       scale             probest       1       NA
## 41        q8_prob_econ_imp_me       scale             probest       1       NA
## 42         q8_prob_inf_closep       scale             probest       1       NA
## 43         q8_prob_die_closep       scale             probest       1       NA
## 44           q8_prob_inf_avgp       scale             probest       1       NA
## 45              q8_t_pand_end        date             probest       0       NA
## 46        q8_t_life_back_norm        date             probest       0       NA
## 47                 q8_secondw      binary             probest       0       NA
## 48          q8_t_secondw_when        date             probest       0       NA
## 49        q8_t_econ_back_norm        date             probest       0       NA
## 50          q6_media_freq_num       scale     objective-media       1       NA
## 51 q7_inf_worry_frequency_num       scale     affective-worry       1       NA
## 52       q7_diff_beh_freq_num       scale affective-behaviour       1       NA
## 53         q8_t_pand_end_days       scale             probest       1       NA
## 54   q8_t_life_back_norm_days       scale             probest       1       NA
## 55     q8_t_secondw_when_days       scale             probest       1       NA
## 56   q8_t_econ_back_norm_days       scale             probest       1       NA
## 57             q6_me_inf_resc        <NA>                <NA>       0       NA
## 58   q6_close_person_inf_resc        <NA>                <NA>       0       NA
## 59  q6_close_person_died_resc        <NA>                <NA>       0       NA
## 60            q8_secondw_resc        <NA>                <NA>       0       NA
## 61               obj_severity     agg_man                <NA>       0       NA
## 62                covid_worry     agg_man                <NA>       0       NA
## 63                  avoid_beh     agg_man                <NA>       0       NA
## 64                  avoid_anx     agg_man                <NA>       0       NA
## 65                  avoid_all     agg_man                           0       NA
## 66                   prob_est     agg_man                           0       NA
## 67              covid_end_est     agg_man                           0       NA
## 68                    stai_ta stand_quest                           0       NA
## 69                    stai_sa stand_quest                           0       NA
## 70                  sticsa_ta stand_quest                           0       NA
## 71              sticsa_cog_ta stand_quest                           0       NA
## 72              sticsa_som_ta stand_quest                           0       NA
## 73                  sticsa_sa stand_quest                           0       NA
## 74              sticsa_cog_sa stand_quest                           0       NA
## 75              sticsa_som_sa stand_quest                           0       NA
## 76                        bdi stand_quest                           0       NA
## 77                        cat stand_quest                           0       NA
## 78                        PID        <NA>                           0       NA
## 79                 PROLIFICID        <NA>                           0       NA
```

### Preparatory steps

```r
# Preparatory steps

#vars2use <- vardf[vardf$fa_incl==1,["varname"]]

vars2use <- vardf[vardf$fa_incl==1,"varname"]

# read preprocessed data from session 1 
df<-read.csv(paste0(dataf, "/session8/clean_dataset.csv"))
df.orig <- df

# filter only relevant columns
df <- df[,colnames(df) %in% vars2use]

# replace missing values with median (controversial?, cor() can't deal with it)
for(i in 1:ncol(df)){
  df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
}
```


### Correlation matrix (Spearman)

```r
# Create correlation matrix 
cdf = cor(df, method = c("spearman")) #how does cor2 deal with missing values?
cp <- corrplot(cdf) 
```

<img src="sess8_efa_files/figure-html/fig-1.png" width="1440" />

### Check for how many factors
#### Eigenvalues

```r
  ## Factor number assessment: 
# a/ Eigenvalues
evals <- eigen(cdf)$values 
scree(cdf, factors = FALSE)
```

<img src="sess8_efa_files/figure-html/unnamed-chunk-3-1.png" width="672" />

#### Parallel analysis

```r
# b/ Parallel analysis
set.seed(123) 
resPA <- fa.parallel(df, fa = "pc", cor = "cor",fm = "ml")
```

<img src="sess8_efa_files/figure-html/unnamed-chunk-4-1.png" width="672" />

```
## Parallel analysis suggests that the number of factors =  NA  and the number of components =  6
```

#### Very simple structure

```r
# c/ Very simple srtucutre
resvss <- vss(cdf, fm = "ml", n.obs = nrow(df), plot = TRUE) 
```

<img src="sess8_efa_files/figure-html/unnamed-chunk-5-1.png" width="672" />

```r
resvss
```

```
## 
## Very Simple Structure
## Call: vss(x = cdf, fm = "ml", n.obs = nrow(df), plot = TRUE)
## VSS complexity 1 achieves a maximimum of 0.73  with  1  factors
## VSS complexity 2 achieves a maximimum of 0.8  with  4  factors
## 
## The Velicer MAP achieves a minimum of 0.01  with  7  factors 
## BIC achieves a minimum of  -2344.34  with  8  factors
## Sample Size adjusted BIC achieves a minimum of  -257.15  with  8  factors
## 
## Statistics by number of factors 
##   vss1 vss2   map dof chisq     prob sqresid  fit RMSEA   BIC SABIC complex
## 1 0.73 0.00 0.025 945  5389  0.0e+00      51 0.73 0.119   -91  2906     1.0
## 2 0.58 0.79 0.021 901  4379  0.0e+00      39 0.79 0.108  -846  2012     1.4
## 3 0.50 0.79 0.019 858  3536 1.1e-320      33 0.83 0.097 -1439  1282     1.6
## 4 0.51 0.80 0.017 816  2845 3.9e-222      26 0.86 0.087 -1887   701     1.6
## 5 0.49 0.80 0.015 775  2454 2.2e-173      22 0.89 0.081 -2040   418     1.7
## 6 0.45 0.75 0.014 735  2055 3.8e-125      20 0.89 0.074 -2208   124     1.8
## 7 0.43 0.71 0.013 696  1745  1.9e-91      18 0.91 0.068 -2291   -83     1.9
## 8 0.43 0.71 0.014 658  1471  4.0e-64      15 0.92 0.061 -2344  -257     2.0
##   eChisq  SRMR eCRMS  eBIC
## 1   8001 0.111 0.113  2521
## 2   5290 0.090 0.094    65
## 3   3800 0.076 0.082 -1175
## 4   2469 0.061 0.068 -2263
## 5   1699 0.051 0.058 -2795
## 6   1482 0.048 0.055 -2780
## 7   1134 0.042 0.050 -2902
## 8    766 0.034 0.042 -3049
```

### Run EFA

```r
# Run factor analysis
fa3 <- fa(cdf, nfactors = 6, rotate = "oblimin", fm = "ml")
fa3.load <- data.frame(unclass(fa3$loadings))
fa3.load$varname <- rownames(fa3.load)
fa3.load <- merge(fa3.load, vardf, by="varname")
```

### Visualize EFA: psych package diagram

```r
# I don't like this plot very much so better plots below
fa.diagram(fa3)
```

<img src="sess8_efa_files/figure-html/fig2-1.png" width="1440" />

### Loadings and pre-defined categories
- we have created the questionnaire to roughly reflect: objective measures, affective measures (worries and avoidance behaviour), probability estimates of aversive events, time estimates)
- here I just show how the estimated loadings map onto our guesses


```r
library(reshape2)
```

```
## Warning: package 'reshape2' was built under R version 3.6.3
```

```r
fa3.load.long <- fa3.load %>% 
         melt(id.vars = c("varname", "agg_manual"),  # variables to carry forward
              measure.vars = colnames(fa3.load)[grepl("ML",colnames(fa3.load))],  #variables to stack
              value.name = "loading",     # name of the value variable 
              variable.name = "factor" ) # name of the variable   

## Question 1: How do the pre-defined categories                      
pal<-assign_colors("manual-cats")
g <- ggplot(fa3.load.long, aes(x=varname, y=loading, fill=agg_manual)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values =pal, name = "Manual labels (pre-FA)") +
  facet_grid(rows=vars(factor),  scales="free") + 
  scale_x_discrete() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
g
```

<img src="sess8_efa_files/figure-html/fig3-1.png" width="960" />

### Loadings by factor
- I just highlighted indicators with higher loading than 0.2
- most seem to make sense (yay)


```r
## Question 2: Which questions factor together
n.fac <- length(unique(fa3.load.long$factor))
factor_names <- c("F1: high-worry", "F2: infection-prob", "F3: economics", "F4: avoidance", "F5: close-person-threat", "F6: state-aware")

for (i in 1:n.fac) {
  fa3.load.long$current_fac <- 0
  fa3.load.long$current_fac[fa3.load.long$factor %in% paste0("ML",toString(i)) & abs(fa3.load.long$loading) > 0.2] <- 1
  
  tdf <- fa3.load.long[fa3.load.long$factor %in% paste0("ML",toString(i)),]
  g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
  geom_bar(stat="identity") +
  ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
  print(g)
}
```

<img src="sess8_efa_files/figure-html/unnamed-chunk-7-1.png" width="960" /><img src="sess8_efa_files/figure-html/unnamed-chunk-7-2.png" width="960" /><img src="sess8_efa_files/figure-html/unnamed-chunk-7-3.png" width="960" /><img src="sess8_efa_files/figure-html/unnamed-chunk-7-4.png" width="960" /><img src="sess8_efa_files/figure-html/unnamed-chunk-7-5.png" width="960" /><img src="sess8_efa_files/figure-html/unnamed-chunk-7-6.png" width="960" />


### Factor scores correlation with individual measures



```r
## Factor scores
fa3.scores <- data.frame(fa(df, nfactors = 6, rotate = "oblimin", fm = "ml", scores="regression", missing=TRUE, impute="median")$scores)
colnames(fa3.scores) <- factor_names
fa3.df <- cbind(df.orig, fa3.scores )
psych.vars <- c("stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", factor_names)

# filter only relevant columns
fa3.df <- fa3.df[,colnames(fa3.df) %in% psych.vars]

# replace missing values with median 
for(i in 1:ncol(fa3.df)){
  fa3.df[is.na(fa3.df[,i]), i] <- median(fa3.df[,i], na.rm = TRUE)
}


# Create correlation matrix 
fa3.cdf = cor(fa3.df, method = c("pearson")) #how does cor2 deal with missing values?
cp2 <- corrplot(fa3.cdf, method = "number") 
```

<img src="sess8_efa_files/figure-html/fig4-1.png" width="960" />




