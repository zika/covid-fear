---
title: 'EFA: Session 1'
author: "Ondrej Zika"
date: "1/10/2022"
output: html_document
---


## EFA: Session 1

### Analysis description 
- only session 1
- only continuous variables
- just to check how things are running, e.g. how to select factor numbers 
- steps roughly based on [Modern Psychometrics in R: Chapter 2](https://link.springer.com/book/10.1007/978-3-319-93177-7)

### Overview of variables

```r
# 1) Load csv with information about variable types 
vardf <- read.csv(paste0(outputf, "/questionnaires/covid_questionnaire_questions_overview.csv"))
print(vardf)
```

```
##                       varname        type          agg_manual fa_incl fulltext
## 1                   q6_me_inf      binary     objective-covid       0       NA
## 2         q6_close_person_inf      binary     objective-covid       0       NA
## 3        q6_close_person_died      binary     objective-covid       0       NA
## 4           q6_econ_impact_me       scale      objective-econ       1       NA
## 5       q6_econ_impact_closep       scale      objective-econ       1       NA
## 6                q6_work_home       scale           objective       1       NA
## 7           q6_apply_soc_dist       scale     objective-covid       1       NA
## 8               q6_risk_group       scale     objective-covid       1       NA
## 9        q6_risk_group_closep       scale     objective-covid       1       NA
## 10          q6_houshold_membs       scale           objective       1       NA
## 11              q6_media_freq       scale     objective-media       0       NA
## 12           q6_media_valence       scale     objective-media       1       NA
## 13          q7_worry_infected       scale     affective-worry       1       NA
## 14               q7_worry_die       scale     affective-worry       1       NA
## 15       q7_worry_econ_impact       scale     affective-worry       1       NA
## 16          q7_worry_sthg_bad       scale     affective-worry       1       NA
## 17        q7_worry_insuf_help       scale     affective-worry       1       NA
## 18        q7_worry_closep_inf       scale     affective-worry       1       NA
## 19              q7_closep_die       scale     affective-worry       1       NA
## 20          q7_worry_shortage       scale     affective-worry       1       NA
## 21       q7_period_rel_danger       scale           affective       1       NA
## 22       q7_period_rel_safety       scale           affective       1       NA
## 23        q7_initial_surprise       scale           affective       1       NA
## 24          q7_initial_scared       scale           affective       1       NA
## 25        q7_people_overreact       scale           affective       1       NA
## 26    q7_vir_not_as_dangerous       scale           affective       1       NA
## 27            q7_vir_made_lab       scale           affective       1       NA
## 28 q7_inf_worry_frequency_num       scale     affective-worry       1       NA
## 29       q7_diff_beh_freq_num       scale affective-behaviour       1       NA
## 30          q7_beh_wash_hands       scale affective-behaviour       1       NA
## 31           q7_beh_avoid_ppl       scale affective-behaviour       1       NA
## 32 q7_beh_avoid_public_places       scale affective-behaviour       1       NA
## 33       q7_anx_touching_surf       scale affective-behaviour       1       NA
## 34  q7_anx_stand_close_to_ppl       scale affective-behaviour       1       NA
## 35     q7_anx_eating_food_out       scale affective-behaviour       1       NA
## 36       q7_anx_public_transp       scale affective-behaviour       1       NA
## 37           q7_anx_visit_doc       scale affective-behaviour       1       NA
## 38         q7_anx_another_beh       scale affective-behaviour       1       NA
## 39             q8_prob_inf_me       scale             probest       1       NA
## 40             q8_prob_die_me       scale             probest       1       NA
## 41        q8_prob_econ_imp_me       scale             probest       1       NA
## 42         q8_prob_inf_closep       scale             probest       1       NA
## 43         q8_prob_die_closep       scale             probest       1       NA
## 44           q8_prob_inf_avgp       scale             probest       1       NA
## 45              q8_t_pand_end        date             probest       0       NA
## 46        q8_t_life_back_norm        date             probest       0       NA
## 47                 q8_secondw      binary             probest       0       NA
## 48          q8_t_secondw_when        date             probest       0       NA
## 49        q8_t_econ_back_norm        date             probest       0       NA
## 50          q6_media_freq_num       scale     objective-media       1       NA
## 51 q7_inf_worry_frequency_num       scale     affective-worry       1       NA
## 52       q7_diff_beh_freq_num       scale affective-behaviour       1       NA
## 53         q8_t_pand_end_days       scale             probest       1       NA
## 54   q8_t_life_back_norm_days       scale             probest       1       NA
## 55     q8_t_secondw_when_days       scale             probest       1       NA
## 56   q8_t_econ_back_norm_days       scale             probest       1       NA
## 57             q6_me_inf_resc        <NA>                <NA>       0       NA
## 58   q6_close_person_inf_resc        <NA>                <NA>       0       NA
## 59  q6_close_person_died_resc        <NA>                <NA>       0       NA
## 60            q8_secondw_resc        <NA>                <NA>       0       NA
## 61               obj_severity     agg_man                <NA>       0       NA
## 62                covid_worry     agg_man                <NA>       0       NA
## 63                  avoid_beh     agg_man                <NA>       0       NA
## 64                  avoid_anx     agg_man                <NA>       0       NA
## 65                  avoid_all     agg_man                           0       NA
## 66                   prob_est     agg_man                           0       NA
## 67              covid_end_est     agg_man                           0       NA
## 68                    stai_ta stand_quest                           0       NA
## 69                    stai_sa stand_quest                           0       NA
## 70                  sticsa_ta stand_quest                           0       NA
## 71              sticsa_cog_ta stand_quest                           0       NA
## 72              sticsa_som_ta stand_quest                           0       NA
## 73                  sticsa_sa stand_quest                           0       NA
## 74              sticsa_cog_sa stand_quest                           0       NA
## 75              sticsa_som_sa stand_quest                           0       NA
## 76                        bdi stand_quest                           0       NA
## 77                        cat stand_quest                           0       NA
## 78                        PID        <NA>                           0       NA
## 79                 PROLIFICID        <NA>                           0       NA
```

### Preparatory steps

```r
# Preparatory steps

#vars2use <- vardf[vardf$fa_incl==1,["varname"]]

vars2use <- vardf[vardf$fa_incl==1,"varname"]

# read preprocessed data from session 1 
df<-read.csv(paste0(dataf, "/session1/clean_dataset.csv"))
df.orig <- df

# filter only relevant columns
df <- df[,colnames(df) %in% vars2use]

# replace missing values with median (controversial?, cor() can't deal with it but it seems that the fa() package does the same)
for(i in 1:ncol(df)){
  df[is.na(df[,i]), i] <- median(df[,i], na.rm = TRUE)
}
```


### Correlation matrix (Spearman)

```r
# Create correlation matrix 
cdf = cor(df, method = c("spearman")) #how does cor2 deal with missing values?
cp <- corrplot(cdf) 
```

<img src="sess1_efa_files/figure-html/fig-1.png" width="1440" />

### Check for how many factors
#### Eigenvalues

```r
  ## Factor number assessment: 
# a/ Eigenvalues
evals <- eigen(cdf)$values 
scree(cdf, factors = FALSE)
```

<img src="sess1_efa_files/figure-html/unnamed-chunk-3-1.png" width="672" />

#### Parallel analysis

```r
# b/ Parallel analysis
set.seed(123) 
resPA <- fa.parallel(df, fa = "pc", cor = "cor",fm = "ml")
```

<img src="sess1_efa_files/figure-html/unnamed-chunk-4-1.png" width="672" />

```
## Parallel analysis suggests that the number of factors =  NA  and the number of components =  8
```

#### Very simple structure

```r
# c/ Very simple srtucutre
resvss <- vss(cdf, fm = "ml", n.obs = nrow(df), plot = TRUE) 
```

<img src="sess1_efa_files/figure-html/unnamed-chunk-5-1.png" width="672" />

```r
resvss
```

```
## 
## Very Simple Structure
## Call: vss(x = cdf, fm = "ml", n.obs = nrow(df), plot = TRUE)
## VSS complexity 1 achieves a maximimum of 0.7  with  2  factors
## VSS complexity 2 achieves a maximimum of 0.78  with  4  factors
## 
## The Velicer MAP achieves a minimum of 0.01  with  8  factors 
## BIC achieves a minimum of  -2657.5  with  8  factors
## Sample Size adjusted BIC achieves a minimum of  -569.61  with  8  factors
## 
## Statistics by number of factors 
##   vss1 vss2   map dof chisq     prob sqresid  fit RMSEA   BIC SABIC complex
## 1 0.70 0.00 0.016 945  4621  0.0e+00      47 0.70 0.098 -1045  1953     1.0
## 2 0.70 0.74 0.015 901  3869  0.0e+00      40 0.74 0.090 -1534  1325     1.2
## 3 0.69 0.78 0.014 858  3120 1.7e-253      31 0.80 0.081 -2025   697     1.4
## 4 0.50 0.78 0.012 816  2541 4.4e-176      27 0.83 0.072 -2352   237     1.7
## 5 0.47 0.74 0.012 775  2101 8.5e-123      25 0.84 0.065 -2546   -87     1.7
## 6 0.43 0.73 0.012 735  1753  6.4e-85      22 0.86 0.059 -2654  -322     1.9
## 7 0.38 0.65 0.012 696  1523  1.1e-63      20 0.87 0.054 -2651  -442     2.1
## 8 0.39 0.66 0.011 658  1288  3.2e-43      18 0.88 0.049 -2658  -570     2.1
##   eChisq  SRMR eCRMS  eBIC
## 1   7107 0.094 0.097  1440
## 2   5432 0.083 0.087    29
## 3   3300 0.064 0.069 -1845
## 4   2377 0.055 0.060 -2516
## 5   2064 0.051 0.058 -2583
## 6   1537 0.044 0.051 -2870
## 7   1250 0.040 0.047 -2923
## 8    919 0.034 0.042 -3027
```

### Run EFA

```r
nFactors = 4
# Run factor analysis
fa3 <- fa(cdf, nfactors = nFactors, rotate = "oblimin", fm = "ml")
fa3.load <- data.frame(unclass(fa3$loadings))
fa3.load$varname <- rownames(fa3.load)
fa3.load <- merge(fa3.load, vardf, by="varname")

# save the estimated loadings
save(list = c("df", "fa3", "fa3.load"), file = paste0(outputf, "/fa/fa_sess1_loadings.Rdata"))
```

### Visualize EFA: psych package diagram

```r
# I don't like this plot very much so better plots below
fa.diagram(fa3)
```

<img src="sess1_efa_files/figure-html/fig2-1.png" width="1440" />

### Loadings and pre-defined categories
- we have created the questionnaire to roughly reflect: objective measures, affective measures (worries and avoidance behaviour), probability estimates of aversive events, time estimates)
- here I just show how the estimated loadings map onto our guesses


```r
library(reshape2)
```

```
## Warning: package 'reshape2' was built under R version 3.6.3
```

```r
fa3.load.long <- fa3.load %>% 
         melt(id.vars = c("varname", "agg_manual"),  # variables to carry forward
              measure.vars = colnames(fa3.load)[grepl("ML",colnames(fa3.load))],  #variables to stack
              value.name = "loading",     # name of the value variable 
              variable.name = "factor" ) # name of the variable   

## Question 1: How do the pre-defined categories                      
pal<-assign_colors("manual-cats")
g <- ggplot(fa3.load.long, aes(x=varname, y=loading, fill=agg_manual)) +
  geom_bar(stat="identity") +
  scale_fill_manual(values =pal, name = "Manual labels (pre-FA)") +
  facet_grid(rows=vars(factor),  scales="free") + 
  scale_x_discrete() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
g
```

<img src="sess1_efa_files/figure-html/fig3-1.png" width="960" />

### Loadings by factor
- I just highlighted indicators with higher loading than 0.2
- most seem to make sense (yay)


```r
## Question 2: Which questions factor together
n.fac <- length(unique(fa3.load.long$factor))
# just some manual labelling
#factor_names <- c("F1: close-person-threat", "F2: economic", "F3: worry", "F4: infection-prob", "F5: state", "F6: anx-avoidance", "F7: time-pred", "F8: skeptics", "F9: successful-avoidance")
factor_names <- sprintf("F%s",seq(1:nFactors))
# print a quick overview of what indicators are groupped together 
fa3.load <- fa3.load[,c("varname", colnames(fa3.load)[grepl("ML",colnames(fa3.load))])][,c("varname",sprintf("ML%s",seq(1:nFactors)))]
fa3.load$factor <- apply( abs(fa3.load[,colnames(fa3.load)[grepl("ML",colnames(fa3.load))]]),  1, which.max)
cfa.str <- ""
for (i in seq(nFactors)) {cfa.str <- paste(cfa.str, "Factor ", toString(i), " (", factor_names[i], "):\n", paste(fa3.load[fa3.load$factor==i,"varname"], collapse = " + "), "\n\n", sep="")}
cat(cfa.str)
```

```
## Factor 1 (F1):
## q6_risk_group + q7_closep_die + q7_diff_beh_freq_num + q7_diff_beh_freq_num + q7_inf_worry_frequency_num + q7_inf_worry_frequency_num + q7_initial_scared + q7_vir_made_lab + q7_worry_closep_inf + q7_worry_die + q7_worry_infected + q7_worry_insuf_help + q7_worry_shortage + q7_worry_sthg_bad + q8_prob_die_me
## 
## Factor 2 (F2):
## q6_econ_impact_closep + q6_econ_impact_me + q7_worry_econ_impact + q8_prob_econ_imp_me
## 
## Factor 3 (F3):
## q6_risk_group_closep + q8_prob_die_closep + q8_prob_inf_avgp + q8_prob_inf_closep + q8_prob_inf_me + q8_t_econ_back_norm_days + q8_t_life_back_norm_days + q8_t_pand_end_days + q8_t_secondw_when_days
## 
## Factor 4 (F4):
## q6_apply_soc_dist + q6_houshold_membs + q6_media_freq_num + q6_media_valence + q6_work_home + q7_anx_another_beh + q7_anx_eating_food_out + q7_anx_public_transp + q7_anx_stand_close_to_ppl + q7_anx_touching_surf + q7_anx_visit_doc + q7_beh_avoid_ppl + q7_beh_avoid_public_places + q7_beh_wash_hands + q7_initial_surprise + q7_people_overreact + q7_period_rel_danger + q7_period_rel_safety + q7_vir_not_as_dangerous
```

```r
for (i in 1:n.fac) {
  fa3.load.long$current_fac <- 0
  fa3.load.long$current_fac[fa3.load.long$factor %in% paste0("ML",toString(i)) & abs(fa3.load.long$loading) > 0.2] <- 1
  
  tdf <- fa3.load.long[fa3.load.long$factor %in% paste0("ML",toString(i)),]
  g <- ggplot(tdf, aes(x=varname, y=loading, fill=as.factor(current_fac))) +
  geom_bar(stat="identity") +
  ggtitle(paste0("Factor ",toString(i), " \"", factor_names[i]), "\"")  +
  scale_fill_manual(values = assign_colors("binary1"), name = "Indicator loading > 0.2") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
  print(g)
}
```

<img src="sess1_efa_files/figure-html/unnamed-chunk-7-1.png" width="960" /><img src="sess1_efa_files/figure-html/unnamed-chunk-7-2.png" width="960" /><img src="sess1_efa_files/figure-html/unnamed-chunk-7-3.png" width="960" /><img src="sess1_efa_files/figure-html/unnamed-chunk-7-4.png" width="960" />


### Factor scores correlation with individual measures
- not very informative, the results are very similar to when we group the variables based on our pre-defined categories 



```r
## Factor scores
fa3.scores <- data.frame(fa(df, nfactors = nFactors, rotate = "oblimin", fm = "ml", scores="regression", missing=TRUE, impute="median")$scores)
colnames(fa3.scores) <- factor_names
fa3.df <- cbind(df.orig, fa3.scores )
psych.vars <- c("stai_ta", "stai_sa", "sticsa_ta", "sticsa_sa", "bdi", "cat", factor_names)

# filter only relevant columns
fa3.df <- fa3.df[,colnames(fa3.df) %in% psych.vars]

# replace missing values with median 
for(i in 1:ncol(fa3.df)){
  fa3.df[is.na(fa3.df[,i]), i] <- median(fa3.df[,i], na.rm = TRUE)
}


# Create correlation matrix 
fa3.cdf = cor(fa3.df, method = c("pearson")) #how does cor2 deal with missing values?
cp2 <- corrplot(fa3.cdf, method = "number") 
```

<img src="sess1_efa_files/figure-html/fig4-1.png" width="960" />



### Points to resolve
1. The specific factors will differ across sessions, what would be the best way to analyse this? EFA on Session 1 and then CFA on each of the others? 
Alternatively, would it make sense to create a "factor template" (mean loading across all sessions) and then computing the distance for each session to make it less sess1 dependent? ( I noticed some interesting additions to some of the factors towards the end of the study) 
Alternatively II, i saw some work about longitudinal CFA (basically structural equations models accounting for residual covariance) which can provide us with i) factors across sessions; ii) change in mean for each factor from session to session -> this is kind of what we want looking at how adjustments are made depending on trait anxiety for example
1b. What would be a common way to assess "factor stability", i.e. how much a factor changes over time? I noticed that the factors are relatively similar (looking at session 8 and 15 as well) but their weight changes (e.g. based on eigenvectors)

2. To test relationship of factors and "standardized questionnaires" such as trait anx, depression etc. would one commonly work with Factor Scores like I did above? (which i understand are numbers how a given factor is expressed in an individual)

3. CFA: At least in the way it's implemented in the `lavaan` package it seems that factors can only by implemented into the design in binary way (either belonging to a factor or not belonging)

4. Are different scales an issue - most variables are scaled 1-7 but some are 1 - 10000 (number of days till the end of the panemic)
