assign_colors <- function(id) {
  if (id == "manual-cats") {
    pal = c(
      #affective      affective-behaviour affective-worry  objective   objective-covid     objective-econ                objective-media        probest NA
      "#637FF1", "#2244CC", "#8D4ED3", "#D9563A", "#D91616",  "#EF5252", "#8F1010",  "#0CB318", "#A1A1A1" 
    )
  } else if (id == "binary1") {
    pal = c(
      "#A1A1A1", "#097be6" 
    )
  }
  return(pal)
}