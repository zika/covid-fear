import pandas as pd
import numpy as np
import statsmodels.api as sm
from scipy.stats import t

# Example dataset
# Assuming data is loaded in a DataFrame df with columns: ParticipantID, Session, CurrentThreat, ProbabilityEstims, InforSeek, AvoidBeh, TF1, TF2, TF3, Deaths, Cases

# Placeholder for your dataset
# df = pd.read_csv('your_data.csv')

# Function to perform regression and get t-values
def regression_t_values(data, dependent_var):
    predictors = ["Cases", "Deaths", "TF1", "TF2", "TF3", 
                  "TF1:Deaths", "TF2:Deaths", "TF3:Deaths", 
                  "TF1:Cases", "TF2:Cases", "TF3:Cases", 
                  "ProbabilityEstims", "InforSeek", "AvoidBeh"]
    
    # Creating interaction terms
    data["TF1:Deaths"] = data["TF1"] * data["Deaths"]
    data["TF2:Deaths"] = data["TF2"] * data["Deaths"]
    data["TF3:Deaths"] = data["TF3"] * data["Deaths"]
    data["TF1:Cases"] = data["TF1"] * data["Cases"]
    data["TF2:Cases"] = data["TF2"] * data["Cases"]
    data["TF3:Cases"] = data["TF3"] * data["Cases"]
    
    X = data[predictors]
    y = data[dependent_var]
    
    X = sm.add_constant(X)
    model = sm.OLS(y, X).fit()
    
    t_values = model.tvalues
    return t_values

# Function to identify clusters and their masses
def identify_clusters(t_values, threshold=2.3):
    clusters = []
    current_cluster = []
    for idx, t_value in enumerate(t_values):
        if abs(t_value) > threshold:
            current_cluster.append(t_value)
        else:
            if current_cluster:
                clusters.append(current_cluster)
                current_cluster = []
    if current_cluster:
        clusters.append(current_cluster)
    
    cluster_masses = [sum(np.abs(cluster)) for cluster in clusters]
    return cluster_masses

# Function to perform the cluster-based permutation test
def cluster_based_permutation_test(df, dependent_var, n_permutations=50, threshold=2.3):
    # Get the actual t-values
    actual_t_values = regression_t_values(df, dependent_var)
    actual_cluster_masses = identify_clusters(actual_t_values, threshold)
    
    # Permutation tests
    permuted_cluster_masses = []
    for _ in range(n_permutations):
        shuffled_df = df.copy()
        shuffled_df["ParticipantID"] = np.random.permutation(shuffled_df["ParticipantID"])
        permuted_t_values = regression_t_values(shuffled_df, dependent_var)
        permuted_cluster_masses.extend(identify_clusters(permuted_t_values, threshold))
    
    # Determine significance thresholds
    lower_threshold = np.percentile(permuted_cluster_masses, 2.5)
    upper_threshold = np.percentile(permuted_cluster_masses, 97.5)
    
    # Check if actual clusters are significant
    significant_clusters = [cluster for cluster in actual_cluster_masses 
                            if cluster < lower_threshold or cluster > upper_threshold]
    
    return significant_clusters, lower_threshold, upper_threshold

# Example usage for "CurrentThreat"
significant_clusters, lower_threshold, upper_threshold = cluster_based_permutation_test(df, "CurrentThreat")
print("Significant Clusters:", significant_clusters)
print("Lower Threshold:", lower_threshold)
print("Upper Threshold:", upper_threshold)
