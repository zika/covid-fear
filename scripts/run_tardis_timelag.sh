PATH_BASE="${HOME}/projects/covid-study"
# Give message to user
#echo "${SUB_LABEL}: ${MODEL}"
N_CPUS=2
# maximum number of threads per process:
N_THREADS=1
# memory demand in *GB*
MEM_MB=2
# data directory
#PATH_DATA="${PATH_BASE}/data/temp"

dvs=("F1_exposure_anxiety" "F2_covid_worry" "F3_infection_probability" "F4_mixed_IS_closep" "F5_danger_perception" "F6_econ_worry" "F7_skepticism" "F8_time_end_estimates" "F9_avoidance" "Worry" "Probest" "SocialDist" "AvoidBeh" "AvoidAnx" "InforSeek" "DangerSate" "Deaths" "Cases" "WorkHome")
ivs=("F1_exposure_anxiety" "F2_covid_worry" "F3_infection_probability" "F4_mixed_IS_closep" "F5_danger_perception" "F6_econ_worry" "F7_skepticism" "F8_time_end_estimates" "F9_avoidance" "Worry" "Probest" "SocialDist" "AvoidBeh" "AvoidAnx" "InforSeek" "DangerSate" "Deaths" "Cases" "WorkHome")
dvs=("F1_exposure_anxiety")
ivs=("F2_covid_worry")
nosamples=2500
nochains=2
win=2
model_family="timelag_w_traits_fitatonce"



for dv in "${dvs[@]}"; do
  for iv in "${ivs[@]}"; do

    JOB_NAME="timelag_${dv}_${iv}"
    # Create job file
    echo "#!/bin/bash" > job.slurm
    # name of the job
    echo "#SBATCH --job-name ${JOB_NAME}" >> job.slurm
    # set the expected maximum running time for the job:
    echo "#SBATCH --time 99:59:00" >> job.slurm
    # determine how much RAM your operation needs:
    echo "#SBATCH --mem ${MEM_MB}GB" >> job.slurm
    # determine number of CPUs
    echo "#SBATCH --cpus-per-task ${N_CPUS}" >> job.slurm
    # write to log folder
    #echo "#SBATCH --output ${PATH_LOG}/slurm-${JOB_NAME}.%j.out" >> job.slurm
    echo "#SBATCH --output /home/mpib/zika/logs/slurm_${JOB_NAME}_%j.out" >> job.slurm
    echo "#SBATCH --partition long" >> job.slurm
    
    # Load R module
    echo "module load conda" >> job.slurm
    echo "conda init bash" >> job.slurm
    echo "conda activate covid_pyenv" >> job.slurm
    #echo "mkdir ${PATH_BASE}/output/models/model_data/${model_family}" >> job.slurm
    #echo "pip install numpy scipy pandas bammm-ozika groo-ozika" >> job.slurm
    echo "python ${PATH_BASE}/covid-fear/scripts/focused_analyses/timelag_regressions_with_Indiff_fitted_at_once.py \
    -f ${model_family} -s ${nosamples} -c ${nochains} -w ${win} --iv ${iv} --dv ${dv}" >> job.slurm

    # submit job to cluster queue and remove it to avoid confusion:
    # sbatch job.slurm
    sbatch job.slurm
    rm -f job.slurm
    sleep 2

  done
done
