import pandas as pd
import numpy as np
from scipy import stats
from groo.groo import get_root
root_dir = get_root(".root_dir_covid")

data_path = os.path.join(root_dir, "data", "task_data", "visit1", "questionnaires") 
visit = "1"
data = pd.read_csv(os.path.join(data_path, "t1_raw_data.csv"))
data.index = data.subID
data= data.loc[~data.index.isna(),:]


df_pt1 = data.loc[:, ["submitdate", "Q15I2","subID",]]
df_pt2 = data.loc[:,"Q6I1[SQ001]":"Q8I5" ]



df = df_pt1.join(df_pt2)
#print(df.index)
df = df.drop(["ct63bad", "ct80bad"])

df = df.rename(columns={"Q6I1[SQ001]":"q6_me_inf",
                        "Q6I1[SQ002]":"q6_close_person_inf", "Q6I1[SQ003]":"q6_close_person_died",
                        "Q6I2[SQ001]":"q6_econ_impact_me", "Q6I2[SQ002]":"q6_econ_impact_closep",
                        "Q6I2[SQ003]": "q6_work_home", "Q6I2[SQ004]":"q6_apply_soc_dist", "Q6I2[SQ005]":"q6_risk_group",
                        "Q6I2[SQ006]": "q6_risk_group_closep", "Q6I3": "q6_houshold_membs", "Q6I4": "q6_media_freq",
                        "Q6I5": "q6_media_name", "Q6I6[SQ001]": "q6_media_valence", "Q7I1[SQ001]": "q7_worry_infected",
                        "Q7I1[SQ002]": "q7_worry_die", "Q7I1[SQ003]": "q7_worry_econ_impact", "Q7I1[SQ004]": "q7_worry_sthg_bad",
                        "Q7I1[SQ005]": "q7_worry_insuf_help", "Q7I1[SQ006]": "q7_worry_closep_inf", "Q7I1[SQ007]": "q7_closep_die",
                        "Q7I1[SQ008]": "q7_worry_shortage", "Q7I2[SQ001]": "q7_period_rel_danger", "Q7I3[SQ001]": "q7_period_rel_safety",
                        "Q7I4[SQ001]": "q7_initial_surprise", "Q7I5[SQ001]": "q7_initial_scared",
                        "Q7I6[SQ001]": "q7_people_overreact", "Q7I7[SQ001]": "q7_vir_not_as_dangerous",
                        "Q7I8[SQ001]": "q7_vir_made_lab", "Q7I9": "q7_inf_worry_frequency", "Q7I10": "q7_diff_beh_freq",
                        "Q7I11[SQ001]": "q7_beh_wash_hands", "Q7I11[SQ002]": "q7_beh_avoid_ppl", "Q7I11[SQ003]": "q7_beh_avoid_public_places",
                        "Q7I12[SQ001]": "q7_anx_touching_surf", "Q7I12[SQ002]": "q7_anx_stand_close_to_ppl",
                        "Q7I12[SQ003]": "q7_anx_eating_food_out", "Q7I12[SQ004]": "q7_anx_public_transp",
                        "Q7I12[SQ005]": "q7_anx_visit_doc", "Q7I12[SQ006]": "q7_anx_another_beh",
                        "Q8I1[SQ001]": "q8_prob_inf_me", "Q8I1[SQ002]": "q8_prob_die_me", "Q8I1[SQ003]": "q8_prob_econ_imp_me",
                        "Q8I1[SQ004]": "q8_prob_inf_closep", "Q8I1[SQ005]": "q8_prob_die_closep", "Q8I1[SQ006]": "q8_prob_inf_avgp",
                        "Q8I2": "q8_t_pand_end", "Q8I3": "q8_t_life_back_norm", "Q8I4": "q8_secondw", "Q8I4b": "q8_t_secondw_when",
                        "Q8I5": "q8_t_econ_back_norm",
                        "Q13I1": "smoke", "Q13I5":"coffee_last24", "Q13I6": "tea_last24"

                    } )
df=df.drop(columns=["q6_media_name"])

# Q6 1 - 7
q_list = ["q6_econ_impact_me", "q6_econ_impact_closep",	"q6_work_home", "q6_apply_soc_dist", "q6_risk_group","q6_risk_group_closep"]
for i in q_list:
    df[i] = df[i].replace({"1 - Does not apply": 1, "2": 2, "3": 3, "4":4, "5":5, "6":6, "7 - Strongly applies":7} )

# Q6: household members
#df["q6_houshold_membs"] = df["q6_houshold_membs"].replace({"5+":5})

# Q6 media cnsumption frequency
df["q6_media_freq_num"] =  df["q6_media_freq"].replace({"Multiple times per day":5, "Once a day": 4, "3-4 times a week":3, "Once a week": 2, "Few times a month": 1, "Less than few times a month":0})

# Q6 media valence
df["q6_media_valence"] = df["q6_media_valence"].replace({"-3 (very negative)":-3, "-2": -2, "-1": 1, "0 (neutral)":0, "1":1, "2":2, "3 (very positive)":3})

# Q7: worries
q_list = ["q7_worry_infected","q7_worry_die","q7_worry_econ_impact","q7_worry_sthg_bad","q7_worry_insuf_help","q7_worry_closep_inf","q7_closep_die","q7_worry_shortage","q7_period_rel_danger","q7_period_rel_safety","q7_initial_surprise","q7_initial_scared","q7_people_overreact","q7_vir_not_as_dangerous","q7_vir_made_lab"]
for i in q_list:
    df[i] = df[i].replace({"1 - Strongly disagree": 1, "2 - Disagree": 2, "3 - Somewhat disagree": 3, "4 - Neither agree nor disagree":4, "5 - Somewhat agree":5, "6 - Agree":6, "7 - Strongly agree":7} )

# Q7: worry q7_inf_worry_frequency
q_list = ["q7_inf_worry_frequency", "q7_diff_beh_freq"]
for i in q_list:
    df[i+"_num"] = df[i].replace({"Never":0, "On one or several days":1, "On about half the days":2, "Nearly every day (more than half the days)":3})

# Q7: behaviours + anxiety
q_list = ["q7_beh_wash_hands", "q7_beh_avoid_ppl", "q7_beh_avoid_public_places","q7_anx_touching_surf", "q7_anx_stand_close_to_ppl", "q7_anx_eating_food_out", "q7_anx_public_transp", 	"q7_anx_visit_doc",	"q7_anx_another_beh"]
for i in q_list:
    df[i] = df[i].replace({"1 - Strongly disagree": 1, "2 - Disagree": 2, "3 - Somewhat disagree": 3, "4 - Neither agree nor disagree":4, "5 - Somewhat agree":5, "6 - Agree":6, "7 - Strongly agree":7} )

# Q8 Probability ratings
q_list = ["q8_prob_inf_me","q8_prob_die_me","q8_prob_econ_imp_me","q8_prob_inf_closep","q8_prob_die_closep","q8_prob_inf_avgp"]
for i in q_list:
    df[i] = df[i].str.replace(r'\D', '').replace("NaN","").astype(float)

# Q8 temporal expectations
q_list = ["q8_t_pand_end",	"q8_t_life_back_norm", "q8_t_secondw_when",	"q8_t_econ_back_norm"]
for i in q_list:
    df[i] = pd.to_datetime(df[i])
    df[i+"_days"] = (df[i]-pd.to_datetime(df["submitdate"])).astype('timedelta64[D]')

# Q6 & Q8 and from YES/NO 10 1/0
q_list = ["q6_me_inf", "q6_close_person_inf", "q6_close_person_died", "q8_secondw"]
for i in q_list:
    df[i] = df[i].replace({"Yes": 1, "No": 0})

# Add collapsed measures
# COVID-related worry
df["covid_worry"] = df.loc[:,"q7_worry_infected":"q7_worry_shortage"].mean(axis=1)

# COVID-related behaviours
df["covid_avoidance_beh"] = df.loc[:,"q7_beh_wash_hands":"q7_beh_avoid_public_places"].mean(axis=1)

# COVID-related anxiety
df["covid_spec_anxiety"] = df.loc[:,"q7_anx_touching_surf":"q7_anx_another_beh"].mean(axis=1)

# COVID-probability estimates
df["covid_prob_estimates"] = df.loc[:,"q8_prob_inf_me":"q8_prob_inf_avgp"].mean(axis=1)

# COVID-end time estimate
df["covid_end_est"] = df.loc[:, df.columns.intersection(["q8_t_pand_end_days", "q8_t_econ_back_norm",	"q8_t_life_back_norm_days"]) ].mean(axis=1)

# Trait anxiety STAI
ta = data.loc[:, "Q9I1[SQ001]":"Q9I20[SQ001]"].dropna()





for c in ta.columns:
    ta[c] = ta[c].replace({"1 - Almost never": 1, "2 - Sometimes": 2,  "3 - Often": 3, "4 - Almost always": 4})
#ta.to_csv(data_path+'temp.csv')
stai_scoring = [-1, 1, -1, 1, 1, -1, -1, 1, 1, -1, 1, 1, -1, -1, 1 ,-1, 1, 1, -1, 1] #1=normal -1=reverse
stai_key = { 'item': np.arange(21,41), 'scoring': stai_scoring}
stai_key = pd.DataFrame.from_dict(stai_key)

idx = np.array(stai_key.scoring.isin([-1])).astype(bool)
for i, sub in enumerate(ta.index):

    d = np.array(ta.loc[sub,])
    d[idx] = 5 - d[idx]
    ta.loc[sub,] = d
ta["ta"] = ta.sum(axis=1,  min_count=1)
ta.to_csv(os.path.join(data_path, "individual_questionnaires", "stai_trait.csv"))
#ta.to_csv(data_path+'individual_questionnaires/stai_trait.csv')
df["stai_ta"] = ta["ta"]


# State anxiety STAI
sa = data.loc[:, "Q4I1[SQ001]":"Q4I20[SQ001]"]
#sa.to_csv(data_path+'working_data/stai_state.csv')
sa = sa.dropna()
for c in sa.columns:
    sa[c] = sa[c].replace({"1 - Not at all": 1, "2 - Somewhat": 2,  "3 - Moderately so": 3, "4 - Very much so": 4})
stai_scoring = [-1, -1, 1, 1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1, -1, -1, 1, 1, -1, -1] #1=normal -1=reverse
stai_key = { 'item': np.arange(1,21), 'scoring': stai_scoring}
stai_key = pd.DataFrame.from_dict(stai_key)

#sa.to_csv(data_path+'working_data/stai_state.csv')
for i, sub in enumerate(sa.index):
    idx2 = np.array(stai_key.scoring.isin([-1])).astype(bool)
    d2 = np.array(sa.loc[sub,])
    sa.loc[sub,idx2] = 5 - d2[idx2]
sa["sa"] = sa.sum(axis=1,  min_count=1)
# np.sum(sa, axis=1)
sa.to_csv(os.path.join(data_path, "individual_questionnaires", "stai_state_t1.csv"))
df["stai_sa_t1"] = sa["sa"]

# Sticsa cognitive and somatic sub-scales
st_subsc = np.array(['s','s','c','c','c','s','s','s','c','c','c','s','c','s','s','c','c','s','c','s','s'])
stta = data.loc[:, "Q10I1[SQ001]":"Q10I21[SQ001]"]
stta = stta.dropna()
for c in stta.columns:
    stta[c] = stta[c].replace({"1. Not at all":1, "2. A little": 2, "3. Moderately": 3,  "4. Very much so": 4})

stta_cog = stta.iloc[:,np.where(st_subsc=='c')[0]]
stta_som = stta.iloc[:,np.where(st_subsc=='s')[0]]

stta["stta"] = stta.sum(axis=1,  min_count=1)

#stta.to_csv(os.path.join(data_path, 'individual_questionnaires','sticsa_trait.csv'))
stta.to_csv(os.path.join(data_path, 'individual_questionnaires','sticsa_trait.csv'))

stta_cog["stta_cog"] = stta_cog.sum(axis=1,  min_count=1)
stta_cog.to_csv(os.path.join(data_path, 'individual_questionnaires','sticsa_trait_cog_subscale.csv'))

stta_som["stta_som"] = stta_som.sum(axis=1,  min_count=1)
stta_som.to_csv(os.path.join(data_path, 'individual_questionnaires', 'sticsa_trait_som_subscale.csv'))

df["sticsa_ta"] = stta["stta"]
df["sticsa_cog_ta"] = stta_cog["stta_cog"]
df["sticsa_som_ta"] = stta_som["stta_som"]


# STICSA STATE
stsa = data.loc[:, "Q5I1[SQ001]":"Q5I21[SQ001]"]
stsa = stsa.dropna()
for c in stsa.columns:
    stsa[c] = stsa[c].replace({"1. Not at all":1, "2. A little": 2, "3. Moderately": 3,  "4. Very much so": 4})

stsa_cog = stsa.iloc[:,np.where(st_subsc=='c')[0]]
stsa_som = stsa.iloc[:,np.where(st_subsc=='s')[0]]

stsa["stsa"] = stsa.sum(axis=1,  min_count=1)
stsa.to_csv(os.path.join(data_path, 'individual_questionnaires','sticsa_state_t1.csv'))

stsa_cog["stsa_cog"] = stsa_cog.sum(axis=1,  min_count=1)
stsa_cog.to_csv(os.path.join(data_path, 'individual_questionnaires', 'sticsa_state_cog_subscale_t1.csv'))

stsa_som["stsa_som"] = stsa_som.sum(axis=1,  min_count=1)
stsa_som.to_csv(os.path.join(data_pathm, 'individual_questionnaires', 'sticsa_state_som_subscale_t1.csv'))

df["sticsa_sa_t1"] = stsa["stsa"]
df["sticsa_cog_sa_t1"] = stsa_cog["stsa_cog"]
df["sticsa_som_sa_t1"] = stsa_som["stsa_som"]



####### T2 #######
data = pd.read_csv(os.path.join(data_path, "t2_raw_data.csv"))
data.index = data.subID
data= data.loc[~data.index.isna(),:]

df2 = data.loc[:, ["Q14I1[SQ001]"]]
df2 = df2.rename(columns={"Q14I1[SQ001]":"stress_t2"})
df2["stress_t2"] = df2["stress_t2"].replace({"0 - Not stressed at all": 0})

df = df.join(df2)
#print(df.index)
#df = df.drop(["ct63bad", "ct80bad"])

# State anxiety STAI
sa = data.loc[:, "Q4I1[SQ001]":"Q4I20[SQ001]"].drop(["ct63bad", "ct80bad"])
#sa.to_csv(data_path+'working_data/stai_state.csv')
sa = sa.dropna()
for c in sa.columns:
    sa[c] = sa[c].replace({"1 - Not at all": 1, "2 - Somewhat": 2,  "3 - Moderately so": 3, "4 - Very much so": 4})
stai_scoring = [-1, -1, 1, 1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1, -1, -1, 1, 1, -1, -1] #1=normal -1=reverse
stai_key = { 'item': np.arange(1,21), 'scoring': stai_scoring}
stai_key = pd.DataFrame.from_dict(stai_key)

#sa.to_csv(data_path+'working_data/stai_state.csv')
for i, sub in enumerate(sa.index):
    idx2 = np.array(stai_key.scoring.isin([-1])).astype(bool)
    d2 = np.array(sa.loc[sub,])
    sa.loc[sub,idx2] = 5 - d2[idx2]
sa["sa"] = sa.sum(axis=1,  min_count=1)
# np.sum(sa, axis=1)
sa.to_csv(os.path.join(data_path, 'individual_questionnaires','stai_state_t2.csv'))
df["stai_sa_t2"] = sa["sa"]

stsa = data.loc[:, "Q5I1[SQ001]":"Q5I21[SQ001]"]
stsa = stsa.dropna()
for c in stsa.columns:
    stsa[c] = stsa[c].replace({"1. Not at all":1, "2. A little": 2, "3. Moderately": 3,  "4. Very much so": 4})

stsa_cog = stsa.iloc[:,np.where(st_subsc=='c')[0]]
stsa_som = stsa.iloc[:,np.where(st_subsc=='s')[0]]

stsa["stsa"] = stsa.sum(axis=1,  min_count=1)
stsa.to_csv(os.path.join(data_path, 'individual_questionnaires', 'sticsa_state_t2.csv'))

stsa_cog["stsa_cog"] = stsa_cog.sum(axis=1,  min_count=1)
stsa_cog.to_csv(os.path.join(data_path, 'individual_questionnaires', 'sticsa_state_cog_subscale_t2.csv'))

stsa_som["stsa_som"] = stsa_som.sum(axis=1,  min_count=1)
stsa_som.to_csv(os.path.join(data_path, 'individual_questionnaires', 'sticsa_state_som_subscale_t2.csv'))

df["sticsa_sa_t2"] = stsa["stsa"]
df["sticsa_cog_sa_t2"] = stsa_cog["stsa_cog"]
df["sticsa_som_sa_t2"] = stsa_som["stsa_som"]



####### T3 #######
data = pd.read_csv(os.path.join(data_path, "t3_raw_data.csv"))
data.index = data.subID
data= data.loc[~data.index.isna(),:]

df3 = data.loc[:, ["Q14I1[SQ001]"]]
df3 = df3.rename(columns={"Q14I1[SQ001]":"stress_t3"})
df3["stress_t3"] = df3["stress_t3"].replace({"0 - Not stressed at all": 0})

df = df.join(df3)

# State anxiety STAI
sa = data.loc[:, "Q4I1[SQ001]":"Q4I20[SQ001]"].drop("ct80bad")
#sa.to_csv(data_path+'working_data/stai_state.csv')
sa = sa.dropna()
for c in sa.columns:
    sa[c] = sa[c].replace({"1 - Not at all": 1, "2 - Somewhat": 2,  "3 - Moderately so": 3, "4 - Very much so": 4})
stai_scoring = [-1, -1, 1, 1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1, -1, -1, 1, 1, -1, -1] #1=normal -1=reverse
stai_key = { 'item': np.arange(1,21), 'scoring': stai_scoring}
stai_key = pd.DataFrame.from_dict(stai_key)

#sa.to_csv(data_path+'working_data/stai_state.csv')
for i, sub in enumerate(sa.index):
    idx2 = np.array(stai_key.scoring.isin([-1])).astype(bool)
    d2 = np.array(sa.loc[sub,])

    sa.loc[sub,idx2] = 5 - d2[idx2]
sa["sa"] = sa.sum(axis=1,  min_count=1)
# np.sum(sa, axis=1)
sa.to_csv(os.path.join(data_path,'individual_questionnaires', 'stai_state_t3.csv'))
df["stai_sa_t3"] = sa["sa"]

## sticsa
stsa = data.loc[:, "Q5I1[SQ001]":"Q5I21[SQ001]"]
stsa = stsa.dropna()
for c in stsa.columns:
    stsa[c] = stsa[c].replace({"1. Not at all":1, "2. A little": 2, "3. Moderately": 3,  "4. Very much so": 4})

stsa_cog = stsa.iloc[:,np.where(st_subsc=='c')[0]]
stsa_som = stsa.iloc[:,np.where(st_subsc=='s')[0]]

stsa["stsa"] = stsa.sum(axis=1,  min_count=1)
stsa.to_csv(os.path.join(data_path, 'individual_questionnaires', 'sticsa_state_t3.csv'))

stsa_cog["stsa_cog"] = stsa_cog.sum(axis=1,  min_count=1)
stsa_cog.to_csv(os.path.join(data_path, 'individual_questionnaires','sticsa_state_cog_subscale_t3.csv'))

stsa_som["stsa_som"] = stsa_som.sum(axis=1,  min_count=1)
stsa_som.to_csv(os.path.join(data_path, 'individual_questionnaires','sticsa_state_som_subscale_t3.csv'))

df["sticsa_sa_t3"] = stsa["stsa"]
df["sticsa_cog_sa_t3"] = stsa_cog["stsa_cog"]
df["sticsa_som_sa_t3"] = stsa_som["stsa_som"]



####### T4 #######
data = pd.read_csv(os.path.join(data_path, "t4_raw_data.csv"))
data.index = data.subID
data= data.loc[~data.index.isna(),:]

df4 = data.loc[:, ["Q14I1[SQ001]"]]
df4 = df4.rename(columns={"Q14I1[SQ001]":"stress_t4"})
df4["stress_t4"] = df4["stress_t4"].replace({"0 - Not stressed at all": 0})
df4b = data.loc[:, "taskQ1a":"taskQ3d[SQ005]"]
df4b = df4b.rename(columns={"Q14I1[SQ001]":"stress_t4"})

# there is some issue with appending T4
#df = df.join(df4).join(df4b)


# State anxiety STAI
sa = data.loc[:, "Q4I1[SQ001]":"Q4I20[SQ001]"].drop("ct80bad")
#sa.to_csv(data_path+'working_data/stai_state.csv')
sa = sa.dropna()
for c in sa.columns:
    sa[c] = sa[c].replace({"1 - Not at all": 1, "2 - Somewhat": 2,  "3 - Moderately so": 3, "4 - Very much so": 4})
stai_scoring = [-1, -1, 1, 1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1, -1, -1, 1, 1, -1, -1] #1=normal -1=reverse
stai_key = { 'item': np.arange(1,21), 'scoring': stai_scoring}
stai_key = pd.DataFrame.from_dict(stai_key)

#sa.to_csv(data_path+'working_data/stai_state.csv')
for i, sub in enumerate(sa.index):
    idx2 = np.array(stai_key.scoring.isin([-1])).astype(bool)
    d2 = np.array(sa.loc[sub,])

    sa.loc[sub,idx2] = 5 - d2[idx2]
sa["sa"] = sa.sum(axis=1,  min_count=1)
# np.sum(sa, axis=1)
sa.to_csv(os.path.join(data_path, 'individual_questionnaires','stai_state_t4.csv'))
df["stai_sa_t4"] = sa["sa"]

## sticsa
stsa = data.loc[:, "Q5I1[SQ001]":"Q5I21[SQ001]"]
stsa = stsa.dropna()
for c in stsa.columns:
    stsa[c] = stsa[c].replace({"1. Not at all":1, "2. A little": 2, "3. Moderately": 3,  "4. Very much so": 4})

stsa_cog = stsa.iloc[:,np.where(st_subsc=='c')[0]]
stsa_som = stsa.iloc[:,np.where(st_subsc=='s')[0]]

stsa["stsa"] = stsa.sum(axis=1,  min_count=1)
stsa.to_csv(os.path.join(data_path, 'individual_questionnaires','sticsa_state_t4.csv'))

stsa_cog["stsa_cog"] = stsa_cog.sum(axis=1,  min_count=1)
stsa_cog.to_csv(os.path.join(data_path, 'individual_questionnaires', 'sticsa_state_cog_subscale_t4.csv'))

stsa_som["stsa_som"] = stsa_som.sum(axis=1,  min_count=1)
stsa_som.to_csv(os.path.join(data_path, 'individual_questionnaires', 'sticsa_state_som_subscale_t4.csv'))

df["sticsa_sa_t4"] = stsa["stsa"]
df["sticsa_cog_sa_t4"] = stsa_cog["stsa_cog"]
df["sticsa_som_sa_t4"] = stsa_som["stsa_som"]

#df=df.drop(index=["ct63bad"])


df.to_csv(os.path.join(data_path,"full_questionnaire_data.csv"), sep=";")
