---
title: "Untitled"
author: "Ondrej Zika"
date: "7/12/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(lme4)
library(lmerTest)
library(emmeans)
library(sjPlot)
library(sjmisc)
library(ggplot2)
library(Jmisc)
library(reshape2)
library(Hmisc)
library(RColorBrewer)
library(PupillometryR)
library(plyr)
library(ggeffects)
library(ggpubr)
library(dplyr)
library(robustlmm)
library(broom)
library(tidyverse)
library(tidyr)
library(car)
library(glmmTMB)
library(boot)
library(glmmLasso)
emm_options(pbkrtest.limit = 5012)
```

```{r}
#factors <- c("F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism")
factors <- c("F1_CovidAnxietyWorry", "F2_CovidProbabilityEsts", "F3_Economic")
add_vars <- c("q6_close_person_infdied", "q6_apply_soc_dist", "q6_risk_group" , "q6_media_freq_num",  "q6_media_valence" )
here::i_am(".root_dir_covid")

trait_factors <- c("TF1_CognAnxDepr", "TF2_PhysiolAnx", "TF3_NegativeAffect")
```

```{r}
df <- read.csv(here::here("data","full_dataset_only_complete.csv"))
#df$state_severity <- df$deaths14_norm_unsmooth
df$state_severity <- df$cases14_std_unsmooth
tdf <- df[,append(append(append(c("state_severity", "PROLIFICID", "session"), factors), trait_factors), add_vars)]
tdf <- tdf[tdf$session %in% c(seq(0,4), seq(15,19)),]

## zscore predictors
for (v in append(c("state_severity"), trait_factors)) {
  tdf[v] <- scale(tdf[v])
  
}
tdf$sess <- as.numeric(tdf$session+1)
```


```{r}
generate_equation <- function(dep_var, vec_to_iterate, vec_interactions, other_terms, reffs) {
   eq <- ""
   eq <- paste0(dep_var, " ~ ")
   eff1 <- paste(vec_to_iterate, sep="", collapse=" + ")
   eq <- paste0(eq, eff1)
   
   for (i in vec_interactions) {
     eff2 <- paste0(paste(vec_to_iterate, sep="+", collapse=paste0("*", i, " + ")), paste0("*", i))
     eq <- paste(eq, eff2, sep=" + ", collapse=" + ")
   }
   
   other_terms <- paste(other_terms, sep=" + ", collapse=" + ")
   eq <- paste(eq, other_terms, sep=" + ", collapse=" + ")
   ref_str <- paste(reffs, sep="", collapse=" + ")
   eq <- paste(eq, ref_str, sep=" + ", collapse=" + ")
  return(eq)
}

e <- generate_equation("dv", c("a1", "a2"), c("b", "c", "b*c"), c("D", "E"), c("(1|sub)", "(n|sub)"))
print(e)
```

```{r}

for (f in factors) {
  eq <- generate_equation(f, trait_factors, c("state_severity"), c("state_severity", "sess", add_vars), "(1|PROLIFICID)")
  print(eq)
  
  m1f1 <- lmer(data=tdf, eq )
  print(anova(m1f1))
  print(summary(m1f1))
  
  g3 <- plot_model(m1f1, terms=c(trait_factors, "state_severity"), 
           show.p=TRUE,
           show.values=TRUE)
  print(g3)
  
  eff2 <- paste0(paste0(trait_factors, ":state_severity"))
  g3 <- plot_model(m1f1,terms=eff2, 
           show.p=TRUE,
           show.values=TRUE)#+ ylim(-0.05, 0.05)
  
  print(g3)
  
}

 
```
```{r}
library(brms)
#c("F1_AnxietyWorry", "F2_ProbabilityEsts", "F3_Leftoever")
model_constant = "basic_model"
for (f in factors) {
  eq <- generate_equation(f, c(trait_factors), c("state_severity"), c("state_severity", "sess"), "(1|PROLIFICID)")
  print(eq)

  m1f1 <- brms::brm(eq, data=tdf, chains = 8, warmup = 500, iter = 1000, cores=8, file=here::here("output", "r_models", "severity", paste0(model_constant, "_x5_", f, ".csv" )))
  summary(m1f1)
  g3 <- plot_model(m1f1,terms=append(trait_factors, "state_severity"), 
           show.p=TRUE,
           show.values=TRUE)#+ ylim(-0.05, 0.05)
  
  print(g3)
    
  eff2 <- paste0(paste0(trait_factors, ":state_severity"))
  g3 <- plot_model(m1f1,terms=eff2, 
           show.p=TRUE,
           show.values=TRUE)#+ ylim(-0.05, 0.05)
  print(g3)

}

```

### With autoregression
```{r}
#c("F1_AnxietyWorry", "F2_ProbabilityEsts", "F3_Leftoever")
model_constant = "arma_model"

for (f in factors) {
  eq <- generate_equation(f, trait_factors, c("state_severity"), c("state_severity"), "(sess|PROLIFICID)")
  print(eq)
  
  m <- brms::brm(eq, data=tdf, 
                    autocor = ~ arma(p=1, q=1),
                    chains = 2, warmup = 1000, iter = 5000, cores=2, 
                    file=here::here("output", "r_models", "severity", paste0(model_constant, "_", f, ".csv" )))
  summary(m)
  g3 <- plot_model(m,terms=append(trait_factors, "state_severity"), 
           show.p=TRUE,
           show.values=TRUE)#+ ylim(-0.05, 0.05)
  
  print(g3)
      
  eff2 <- paste0(paste0(trait_factors, ":state_severity"))
  g3 <- plot_model(m,terms=eff2, 
           show.p=TRUE,
           show.values=TRUE)+ ylim(-0.1, 0.1)
  print(g3)
}
```

```{r}
    
  anova(m1f1)
  summary(m1f1)
  g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  g2<- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + ylim(-0.15, 0.15)
  print(g2)
  
  
  ### full model with slopes
  m2f1 <- lmer(data=tdf, F1_Close_Person_Worry~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + session|PROLIFICID) )
  anova(m2f1)
  summary(m2f1)
  g3 <- plot_model(m2f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g3)
  g4<-plot_model(m2f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  print(g4)
  
  
    m3f1 <- lmer(data=tdf, F1_Close_Person_Worry ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
  state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
  state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
  + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + sess|PROLIFICID) )
    
    summary(m3f1)
  
    g1<-plot_model(m3f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
               show.p=TRUE,
               show.values=TRUE)
    print(g1)
    
    g2 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
                 show.p=TRUE,
                 show.values=TRUE)
    print(g2)
    
    g3 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
                 show.p=TRUE,
                 show.values=TRUE) + ylim(-0.05, 0.05)
    print(g3)
  
  ### MOdel comaprison 
  anova(m1f1, m2f1, m3f1)

```



### F2_Anxiety_Avoidance
```{r}
    m1f1 <- lmer(data=tdf, F2_Anxiety_Avoidance~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (session|PROLIFICID) )
  anova(m1f1)
  summary(m1f1)
  g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  g2<- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  print(g2)
  
  
  ### full model with slopes
  m2f1 <- lmer(data=tdf, F2_Anxiety_Avoidance~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + session|PROLIFICID) )
  anova(m2f1)
  summary(m2f1)
  plot_model(m2f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  plot_model(m2f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  
  m3f1 <- lmer(data=tdf, F2_Anxiety_Avoidance ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
  state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
  state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
  + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + sess|PROLIFICID) )
    
    summary(m3f1)
  
    g1<-plot_model(m3f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
               show.p=TRUE,
               show.values=TRUE)
    print(g1)
    
    g2 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
                 show.p=TRUE,
                 show.values=TRUE)
    print(g2)
    
    g3 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
                 show.p=TRUE,
                 show.values=TRUE) + ylim(-0.05, 0.05)
    print(g3)
  
  ### MOdel comaprison 
  anova(m1f1, m2f1, m3f1)

```


### F3_Economic_Impact_Worry
```{r}
    m1f1 <- lmer(data=tdf, F3_Economic_Impact_Worry~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (session|PROLIFICID) )
  anova(m1f1)
  summary(m1f1)
  g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  g2<- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  print(g2)
  
  
  ### full model with slopes
  m2f1 <- lmer(data=tdf, F3_Economic_Impact_Worry~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + session|PROLIFICID) )
  anova(m2f1)
  summary(m2f1)
  plot_model(m2f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  plot_model(m2f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  
      m3f1 <- lmer(data=tdf, F3_Economic_Impact_Worry ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + sess|PROLIFICID) )
  
  summary(m3f1)

  g1<-plot_model(m3f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  
  g2 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
               show.p=TRUE,
               show.values=TRUE)
  print(g2)
  
  g3 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
               show.p=TRUE,
               show.values=TRUE) + ylim(-0.05, 0.05)
  print(g3)
  
  ### MOdel comaprison 
  anova(m1f1, m2f1, m3f1)

```

### F4_Prob_Estimates
```{r}
    m1f1 <- lmer(data=tdf, F4_Prob_Estimates~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (session|PROLIFICID) )
  anova(m1f1)
  summary(m1f1)
  g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  g2<- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  print(g2)
  
  
  ### full model with slopes
  m2f1 <- lmer(data=tdf, F4_Prob_Estimates~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + session|PROLIFICID) )
  anova(m2f1)
  summary(m2f1)
  plot_model(m2f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  plot_model(m2f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  
    m3f1 <- lmer(data=tdf, F4_Prob_Estimates ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + sess|PROLIFICID) )
  
  summary(m3f1)

  g1<-plot_model(m3f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  
  g2 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
               show.p=TRUE,
               show.values=TRUE)
  print(g2)
  
  g3 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
               show.p=TRUE,
               show.values=TRUE) + ylim(-0.05, 0.05)
  print(g3)
  
  
  ### MOdel comaprison 
  anova(m1f1, m2f1, m3f1)

```

### F5_Worry
```{r}
    m1f1 <- lmer(data=tdf, F5_Worry~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (session|PROLIFICID) )
  anova(m1f1)
  summary(m1f1)
  g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  g2<- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  print(g2)
  
  
  ### full model with slopes
  m2f1 <- lmer(data=tdf, F5_Worry~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + session|PROLIFICID) )
  anova(m2f1)
  summary(m2f1)
  plot_model(m2f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  plot_model(m2f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  
  m3f1 <- lmer(data=tdf, F5_Worry ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + sess|PROLIFICID) )
  
  summary(m3f1)

  g1<-plot_model(m3f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  
  g2 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
               show.p=TRUE,
               show.values=TRUE)
  print(g2)
  
  g3 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
               show.p=TRUE,
               show.values=TRUE) + ylim(-0.05, 0.05)
  print(g3)
  
  
  ### MOdel comaprison 
  anova(m1f1, m2f1, m3f1)
  
  

```


### F6_Skepticism
```{r}
    m1f1 <- lmer(data=tdf, F6_Skepticism~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (session|PROLIFICID) )
  anova(m1f1)
  summary(m1f1)
  g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  g2<- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  print(g2)
  
  
  ### full model with slopes
  m2f1 <- lmer(data=tdf, F6_Skepticism~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + session|PROLIFICID) )
  anova(m2f1)
  summary(m2f1)
  plot_model(m2f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  plot_model(m2f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + 
  ylim(-.15, .15)
  
  m3f1 <- lmer(data=tdf, F6_Skepticism ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + sess|PROLIFICID) )
  
  summary(m3f1)

  g1<-plot_model(m3f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)
  print(g1)
  
  g2 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
               show.p=TRUE,
               show.values=TRUE)
  print(g2)
  
  g3 <- plot_model(m3f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
               show.p=TRUE,
               show.values=TRUE) + ylim(-0.05, 0.05)
  print(g3)

  
  ### MOdel comaprison 
  anova(m1f1, m2f1, m3f1)

```

### Model selection
```{r}
#  m2f1 <- lmer(data=tdf, F1_Close_Person_Worry~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + #state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + #state_severity*TF6_Cognitive_Anx + (state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + session|PROLIFICID) )
#  anova(m2f1)
#tdf$stsev2<- tdf$state_severity
#glm.obj <- glmmLasso(F1_Close_Person_Worry ~  TF2_Positive + TF1_Self_Consciousness , rnd = list(PROLIFICID = ~ TF1_Self_Consciousness),
#family = gaussian(), data = tdf, lambda=10, 
#switch.NR=TRUE, control=list(method = "REML", print.iter = TRUE,))
#summary(glm.obj)
```

