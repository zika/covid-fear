---
title: "Untitled"
author: "Ondrej Zika"
date: "7/12/2022"
output: html_document
---
## Sensitivty analysis  -with session as a trend variable

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(lme4)
library(lmerTest)
library(emmeans)
library(sjPlot)
library(sjmisc)
library(ggplot2)
library(Jmisc)
library(reshape2)
library(Hmisc)
library(RColorBrewer)
library(PupillometryR)
library(plyr)
library(ggeffects)
library(ggpubr)
library(dplyr)
library(robustlmm)
library(broom)
library(tidyverse)
library(tidyr)
library(car)
library(glmmTMB)
library(boot)
library(glmmLasso)
emm_options(pbkrtest.limit = 5012)
```

```{r}
factors <- c("F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism")
here::i_am(".root_dir_covid")


```

```{r}
df <- read.csv(here::here("data/full_dataset_only_complete.csv"))
tdf <- df[,c("F1_Close_Person_Worry","F2_Anxiety_Avoidance", "F3_Economic_Impact_Worry", "F4_Prob_Estimates", "F5_Worry", "F6_Skepticism", "state_severity", "PROLIFICID", "session", "TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing","TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx")]


## zscore predictors
for (v in c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing","TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx", "state_severity")) {
  tdf[v] <- scale(tdf[v])
  
}

# make session numeric
tdf$sess <- as.numeric(tdf$session+1)
```

```{r}
#m1f1 <- lmer(data=tdf, F1_Close_Person_Worry ~ state_severity + TF6_Cognitive_Anx + sess + state_severity*TF6_Cognitive_Anx*sess + (sess|PROLIFICID) )
m1f1 <- lmer(data=tdf, F1_Close_Person_Worry ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ (sess|PROLIFICID) )

summary(m1f1)
car::vif(m1f1)

g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
           show.p=TRUE,
           show.values=TRUE)
print(g1)

g2 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)+ ylim(-0.35, 0.35)
print(g2)

g3 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
             show.p=TRUE,
             show.values=TRUE) + ylim(-0.05, 0.05)
print(g3)

#emtrends(m1f1, pairwise ~ state_severity*TF6_Cognitive_Anx, var = "sess")
#plot_model(m1f1, type = "pred", terms = c("state_severity","TF3_Catastrophizing", "sess"))

```

```{r}
#m1f1 <- lmer(data=tdf, F1_Close_Person_Worry ~ state_severity + TF6_Cognitive_Anx + sess + state_severity*TF6_Cognitive_Anx*sess + (sess|PROLIFICID) )
m1f1 <- lmer(data=tdf, F2_Anxiety_Avoidance ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ ( sess|PROLIFICID) )

summary(m1f1)
car::vif(m1f1)

g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
           show.p=TRUE,
           show.values=TRUE)
print(g1)

g2 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE) + ylim(-0.35, 0.35)
print(g2)

g3 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
             show.p=TRUE,
             show.values=TRUE) + ylim(-0.05, 0.05)
print(g3)

#emtrends(m1f1, pairwise ~ state_severity*TF6_Cognitive_Anx, var = "sess")
#plot_model(m1f1, type = "pred", terms = c("state_severity","TF3_Catastrophizing", "sess"))

```

```{r}
#m1f1 <- lmer(data=tdf, F1_Close_Person_Worry ~ state_severity + TF6_Cognitive_Anx + sess + state_severity*TF6_Cognitive_Anx*sess + (sess|PROLIFICID) )
m1f1 <- lmer(data=tdf, F3_Economic_Impact_Worry ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ (sess|PROLIFICID) )

summary(m1f1)
car::vif(m1f1)

g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
           show.p=TRUE,
           show.values=TRUE)
print(g1)

g2 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)  + ylim(-0.35, 0.35)
print(g2)

g3 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
             show.p=TRUE,
             show.values=TRUE) + ylim(-0.05, 0.05)
print(g3)

#emtrends(m1f1, pairwise ~ state_severity*TF6_Cognitive_Anx, var = "sess")
#plot_model(m1f1, type = "pred", terms = c("state_severity","TF3_Catastrophizing", "sess"))

```


```{r}
#m1f1 <- lmer(data=tdf, F1_Close_Person_Worry ~ state_severity + TF6_Cognitive_Anx + sess + state_severity*TF6_Cognitive_Anx*sess + (sess|PROLIFICID) )
m1f1 <- lmer(data=tdf, F4_Prob_Estimates ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ (sess|PROLIFICID) )

summary(m1f1)
car::vif(m1f1)


g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
           show.p=TRUE,
           show.values=TRUE)
print(g1)

g2 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)  + ylim(-0.35, 0.35)
print(g2)

g3 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
             show.p=TRUE,
             show.values=TRUE) + ylim(-0.05, 0.05)
print(g3)

#emtrends(m1f1, pairwise ~ state_severity*TF6_Cognitive_Anx, var = "sess")
#plot_model(m1f1, type = "pred", terms = c("state_severity","TF3_Catastrophizing", "sess"))

```

```{r}
#m1f1 <- lmer(data=tdf, F1_Close_Person_Worry ~ state_severity + TF6_Cognitive_Anx + sess + state_severity*TF6_Cognitive_Anx*sess + (sess|PROLIFICID) )
m1f1 <- lmer(data=tdf, F5_Worry ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ ( sess|PROLIFICID) )

summary(m1f1)
car::vif(m1f1)

g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
           show.p=TRUE,
           show.values=TRUE)
print(g1)

g2 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)  + ylim(-0.35, 0.35)
print(g2)

g3 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
             show.p=TRUE,
             show.values=TRUE) + ylim(-0.05, 0.05)
print(g3)

#emtrends(m1f1, pairwise ~ state_severity*TF6_Cognitive_Anx, var = "sess")
#plot_model(m1f1, type = "pred", terms = c("state_severity","TF3_Catastrophizing", "sess"))

```



```{r}
m1f1 <- lmer(data=tdf, F6_Skepticism ~ state_severity + TF1_Self_Consciousness + TF2_Positive + TF3_Catastrophizing + TF4_Physiological_Anx + TF5_Depression + TF6_Cognitive_Anx + state_severity*TF1_Self_Consciousness + state_severity*TF2_Positive + state_severity*TF3_Catastrophizing + state_severity*TF4_Physiological_Anx + state_severity*TF5_Depression + state_severity*TF6_Cognitive_Anx + 
state_severity*sess + TF1_Self_Consciousness*sess + TF2_Positive*sess + TF3_Catastrophizing*sess + TF4_Physiological_Anx*sess + TF5_Depression*sess + TF6_Cognitive_Anx*sess + state_severity*TF1_Self_Consciousness*sess + state_severity*TF2_Positive*sess + state_severity*TF3_Catastrophizing*sess + state_severity*TF4_Physiological_Anx*sess + 
state_severity*TF5_Depression*sess + state_severity*TF6_Cognitive_Anx*sess 
+ ( sess|PROLIFICID) )

summary(m1f1)
car::vif(m1f1)

g1<-plot_model(m1f1, terms=c("TF1_Self_Consciousness", "TF2_Positive", "TF3_Catastrophizing", "TF4_Physiological_Anx", "TF5_Depression", "TF6_Cognitive_Anx"), 
           show.p=TRUE,
           show.values=TRUE)
print(g1)

g2 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness", "state_severity:TF2_Positive", "state_severity:TF3_Catastrophizing", "state_severity:TF4_Physiological_Anx", "state_severity:TF5_Depression", "state_severity:TF6_Cognitive_Anx"), 
             show.p=TRUE,
             show.values=TRUE)  + ylim(-0.35, 0.35)
print(g2)

g3 <- plot_model(m1f1, terms=c("state_severity:TF1_Self_Consciousness:sess", "state_severity:TF2_Positive:sess", "state_severity:TF3_Catastrophizing:sess", "state_severity:TF4_Physiological_Anx:sess", "state_severity:TF5_Depression:sess", "state_severity:TF6_Cognitive_Anx:sess"), 
             show.p=TRUE,
             show.values=TRUE) + ylim(-0.05, 0.05)
print(g3)

#emtrends(m1f1, pairwise ~ state_severity*TF6_Cognitive_Anx, var = "sess")
#plot_model(m1f1, type = "pred", terms = c("state_severity","TF3_Catastrophizing", "sess"))

```
