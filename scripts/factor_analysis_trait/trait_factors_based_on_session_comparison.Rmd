---
title: "Untitled"
author: "Ondrej Zika"
date: "2023-11-07"
output: html_document
---

```{r setup, include=FALSE}
library(here)
here::i_am(".root_dir_covid")
here::here()
#renv::init(here::here("covid-fear"))
#renv::snapshot()

renv::load(here::here("covid-fear"))
library(corrplot)
library(ggplot)
```

```{r fig, fig.width=6, fig.height=8}
df  <- read.csv(here::here(dataf, paste0("session", "1"), paste0("trait_factor_scores_f", toString(nFactors), "_sess_", "1"  ,".csv")
                               ))[,c("PROLIFICID")]
df.stacked <- data.frame()

for (s in c("1", "8", "15")) {
  s_df<- read.csv(here::here(dataf, paste0("session", s), paste0("trait_factor_scores_f", toString(nFactors), "_sess_", s_bo  ,".csv")
                               ))[,c("PROLIFICID")]
  for (s_bo in c("1", "8", "15", "avg")) {
    tdf <- read.csv(here::here(dataf, paste0("session", s), paste0("trait_factor_scores_f", toString(nFactors), "_sess_", s_bo  ,".csv")
                               ))[,c("ML1", "ML2", "ML3")]
    print(dim(tdf))
    colnames(tdf) <- paste0(colnames(tdf), "_s", s, "_bs_", s_bo)
    
    s_df <- cbind(s_df, tdf)
    
    ## append stacked 
    tdf2 <- read.csv(here::here(dataf, paste0("session", s), paste0("trait_factor_scores_f", toString(nFactors), "_sess_", s_bo  ,".csv")))
    tdf2$trained_on <- s_bo
    df.stacked <- rbind(df.stacked, tdf2)
    
  }
  
  for (fac in c("ML1", "ML2", "ML3")) {
    cdf = cor(as.matrix(s_df[,grepl(fac, colnames(s_df))]), method = "spearman") 
    cp <- corrplot(cdf, method='number')
  }
  
  #df <- cbind(df, s_df)
}
df.stacked$SESSIONID<-as.factor(df.stacked$SESSIONID)
df.stacked$trained_on<-as.factor(df.stacked$trained_on)
```
```{r}

g<-ggplot(aes(x=SESSIONID, y=ML1, fill=interaction(SESSIONID, trained_on)), data=df.stacked) +
  geom_boxplot()
g

g<-ggplot(aes(x=SESSIONID, y=ML2, fill=interaction(SESSIONID, trained_on)), data=df.stacked) +
  geom_boxplot()
g

g<-ggplot(aes(x=SESSIONID, y=ML3, fill=interaction(SESSIONID, trained_on)), data=df.stacked) +
  geom_boxplot()
g
```




