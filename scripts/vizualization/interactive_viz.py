import streamlit as st
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

## create requirements.txt
create=False
if create:
    text_file = open("requirements.txt", "w")
    text_file.write('\n'.join(f'{m.__name__} {m.__version__}' for m in globals().values() if getattr(m, '__version__', None)))
    text_file.close()


df = pd.read_csv("../../data/full_dataset_only_complete.csv")
ms_vars = ["stai_ta",  "sticsa_ta", "sticsa_cog_ta", "sticsa_som_ta", "bdi", "cat"]
wo_vars = ["q7_thinkof_nocases", "q7_thinkof_nodeaths", "q7_thinkof_strain_healthsys", "q7_thinkof_mishandled", 
       "q7_thinkof_famimpact", "q7_thinkof_jobsimpact", "q7_thinkof_safety", "q7_thinkof_wantitover", "q7_thinkof_vaccine"]
wo2_vars = ["q7_worry_infected", "q7_worry_die", "q7_worry_econ_impact","q7_worry_sthg_bad", "q7_worry_insuf_help", "q7_worry_closep_inf", 
       "q7_closep_die", "q7_worry_shortage", "q7_period_rel_danger", "q7_period_rel_safety", "q7_initial_surprise", "q7_initial_scared", 
       "q7_people_overreact", "q7_vir_not_as_dangerous", "q7_vir_made_lab"]
ms_vars = [v+"_ms" for v in ms_vars]

st.sidebar.markdown('## Side menu')
st.sidebar.markdown('`made in markdown!`')
v = st.sidebar.slider('How much do you like markdown?', 0, 20)
if v < 10:
    st.sidebar.markdown('## markdown hater!')
else:
    st.sidebar.markdown('## markdown lover!')
st.sidebar.markdown('---')
st.sidebar.markdown('## Serious stuff')



dt_type = st.sidebar.selectbox(
    'Select dataset type',
     ['onceaday', 'worry'])
    
trait = st.sidebar.selectbox(
    'Select a trait measure',
     ms_vars)
if dt_type=='onceaday':
    var2 = st.sidebar.selectbox(
        'Select a "Do you think at least once a day about" measure',
         wo_vars)
elif dt_type=='worry':
    var2 = st.sidebar.selectbox(
        'Select a "Worry" measure',
         wo2_vars)

st_sess = st.sidebar.slider('Starting session', 0, 19, 0)
end_sess = st.sidebar.slider('Ending session', 0, 19, 19)
if st_sess >= end_sess:
    st.sidebar.markdown('# Starting session must be lower than ending!')

st.sidebar.markdown('---') 
esize = st.sidebar.slider('Error bar size', 0, 100, 68)

st.title('Trait measures and COVID-related worry')
'Trait measure: ', trait
'COVID-worry measure: ', var2

fig=plt.figure()
sdf=df.loc[df["session"].isin(np.arange(st_sess, end_sess)), :]
if dt_type=='onceaday':
    sns.pointplot(x="session", y=var2, hue=trait,
                      data=sdf, dodge=.1, join=True, palette=sns.color_palette("rocket",2),
                      markers="d", scale=.75, ci=esize);
    plt.ylabel("Proportion [0/1 anser]")
    plt.ylim((0,1))
elif dt_type=='worry':
    sns.pointplot(x="session", y=var2, hue=trait,
                  data=sdf, dodge=.1, join=True, palette=sns.color_palette("viridis",2),
                  markers="d", scale=.75, ci=esize);
    plt.ylabel("")
    plt.ylim((1,7))
st.pyplot(fig)




#left_column, right_column = st.beta_columns(2)
#pressed = left_column.button('Press me?')
#if pressed:
#    right_column.write("Woohoo!")