import os
os.chdir(os.getcwd()+"/../../")
root_dir = os.getcwd()
import sys
sys.path.append(os.path.join(root_dir, "covid-fear", "scripts                   
import cov_model_management as mm
         
db_path = os.path.join(root_dir, "output", "models", "databse_name.json")                            
                             
## initialize a database 
# stored as models = {} in a JSON file
mm.models_init(path)
                             
## initialize a model from a template (use during first initiation only) 
model = mm.get_template()
# add model to database
# models["model_name"] = model 
              
## specify model parameters, here is an example
# mod["type"] = "lmm"
# mod["lmm"]["dep_var"] = f
# mod["lmm"]["fxeff"] = [] 
# mod["lmm"]["rneff"] = ["stai_ta*state_severity|session"]
# mod["est"]["nchains"] = 4
# mod["est"]["nsamples"] = 4000
# mod["name"] = "F"+str(f_idx)+"_state_anx_"+str(mod["est"]["nchains"])+"_"+str(mod["est"]["nsamples"])
# mod["lmm"]["eq"] = mm.generate_equation(mod["lmm"]["dep_var"], mod["lmm"]["fxeff"], mod["lmm"]["rneff"]) 
# mod["location"] = os.path.join(root_dir, "output", "models", "model_data", mod["name"]+".dic" )
                             
## estiamte model
# checks whether model data already exist and if they do LOADS them, unless override is set to 1 (default 0)
# results  - samples estimated by bambi/pymc, in xarray format  
# this also saves data to a location specified in mod["location"]
mod, results = mm.estimate_lmm(mod, data, override=0)
                             
## update model database 
# write changes and new model to model database
mm.save_model_info(models, db_path)
                             
## remove model from a database 
# names need to be a list (even if it's just one model) 
mm.remove_model_from_db(db_path, names, delete_data=0)